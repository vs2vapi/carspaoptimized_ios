//
//  ProfileCell.h
//  CarSpa
//
//  Created by R@j on 24/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgVwProfileIcons;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTapToChange;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblTapToChangeHeightConst;
@property (weak, nonatomic) IBOutlet UIView *containerVw;

@end
