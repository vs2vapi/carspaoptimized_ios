//
//  JoinPartnerCell.h
//  CarCleaner
//
//  Created by Rajan on 03/05/17.
//  Copyright © 2017 Nishit Patel. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ServiceCollectionVw : UICollectionView

@property (nonatomic, strong) NSIndexPath *indexPath;


@end

@interface ServiceCollectionCell : UICollectionViewCell
@property (nonatomic, strong) UIButton *btnCheck;
@property (nonatomic, strong) UILabel *lblTitle;
@end

static NSString *CollectionViewCellIdentifier = @"CollectionViewCellIdentifier";


@interface JoinPartnerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *txtFieldPartners;
@property (weak, nonatomic) IBOutlet UIButton *btnPartners;
@property (weak, nonatomic) IBOutlet UILabel *lblCounter;
@property (strong, nonatomic) IBOutlet UITextView *txtVwCategory;
@property (weak, nonatomic) IBOutlet UIView *vwContainer;
@property (weak, nonatomic) IBOutlet UIView *lineVw;
@property (weak, nonatomic) IBOutlet UIButton *btnBrowse1;
@property (weak, nonatomic) IBOutlet UIButton *btnBrowse2;
@property (weak, nonatomic) IBOutlet UIButton *btnBrowse3;

@property (weak, nonatomic) IBOutlet UIButton *btnPath1;
@property (weak, nonatomic) IBOutlet UIButton *btnPath2;
@property (weak, nonatomic) IBOutlet UIButton *btnPath3;

@property (nonatomic, strong) ServiceCollectionVw *collectionView;

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath;

@end
