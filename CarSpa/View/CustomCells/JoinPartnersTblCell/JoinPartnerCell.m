//
//  JoinPartnerCell.m
//  CarCleaner
//
//  Created by Rajan on 03/05/17.
//  Copyright © 2017 Nishit Patel. All rights reserved.
//

#import "JoinPartnerCell.h"

@implementation ServiceCollectionVw

@end

@implementation ServiceCollectionCell
-(instancetype)initWithFrame:(CGRect)frame{
    
 if(self = [super initWithFrame:frame]){
     
     
     _btnCheck = [UIButton buttonWithType:UIButtonTypeCustom];
   
     if([AppContext.appLanguage isEqualToString:@"ar"]){
             [_btnCheck setFrame:CGRectMake(CGRectGetMaxX(self.frame)-30,0,30, 30)];
     }else{
          [_btnCheck setFrame:CGRectMake(8, 0,30,30)];
     }
   
     [_btnCheck setBackgroundColor:[UIColor clearColor]];
     [_btnCheck setImage:[UIImage imageNamed:@"icn_uncheck_gray"] forState:UIControlStateNormal];
     [_btnCheck setUserInteractionEnabled:NO];
     _btnCheck.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
     
     if([AppContext.appLanguage isEqualToString:@"ar"]){
         CGFloat screenWidth = [[UIScreen mainScreen]bounds].size.width;
         if(screenWidth > 320){
             _lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetWidth(self.frame)/2 - 60,3,150, 20)];
         }else{
             _lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetWidth(self.frame)/2 - 70,3,150, 20)];
         }
        
     }else{
         _lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_btnCheck.frame) + 8,3,160, 20)];
     }
  
     [_lblTitle setBackgroundColor:[UIColor clearColor]];
     [_lblTitle setFont:[UIFont fontWithName:@"dax-regular" size:14]];
     _lblTitle.numberOfLines = 2;
     [_lblTitle setTextAlignment:NSTextAlignmentNatural];
     
     [self addSubview:_btnCheck];
     [self addSubview:_lblTitle];

     
 }
    return self;
}


@end

@implementation JoinPartnerCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    // Initialization code
    
    _txtFieldPartners.layer.cornerRadius = 6.0f;
    _txtFieldPartners.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _txtFieldPartners.layer.borderWidth = 1.0f;
    
    _btnPartners.layer.cornerRadius = 6.0f;
    _btnPartners.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _btnPartners.layer.borderWidth = 1.0f;
    
    _txtVwCategory.layer.cornerRadius = 6.0f;
    _txtVwCategory.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _txtVwCategory.layer.borderWidth = 1.0f;
    
    _vwContainer.layer.cornerRadius = 6.0f;
    _vwContainer.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _vwContainer.layer.borderWidth = 1.0f;
    
    
    _btnBrowse1.layer.cornerRadius = 4.0f;
     _btnBrowse2.layer.cornerRadius = 4.0f;
     _btnBrowse3.layer.cornerRadius = 4.0f;
    
    _btnPath1.layer.cornerRadius = 4.0f;
    _btnPath2.layer.cornerRadius = 4.0f;
    _btnPath3.layer.cornerRadius = 4.0f;
    
    _btnPath1.layer.masksToBounds = YES;
    _btnPath2.layer.masksToBounds = YES;
    _btnPath3.layer.masksToBounds = YES;

   
  
    if([AppContext.appLanguage isEqualToString:@"ar"]){
        _txtVwCategory.textAlignment = NSTextAlignmentRight;
    }
    
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(44, 44);
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    

     if([AppContext.appLanguage isEqualToString:@"ar"]){
         CGFloat screenWidth = [[UIScreen mainScreen]bounds].size.width;
         if(screenWidth > 320){
             self.collectionView = [[ServiceCollectionVw alloc] initWithFrame:CGRectMake(8,CGRectGetMaxY(_lineVw.frame) + 8,self.vwContainer.frame.size.width - 16,500) collectionViewLayout:layout];
         }else{
             self.collectionView = [[ServiceCollectionVw alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(_lineVw.frame) + 8, 250,500) collectionViewLayout:layout];
         }
     }else{
          self.collectionView = [[ServiceCollectionVw alloc] initWithFrame:CGRectMake(8,CGRectGetMaxY(_lineVw.frame) + 8,self.vwContainer.frame.size.width - 16,500) collectionViewLayout:layout];
     }
    

    
    [self.collectionView registerClass:[ServiceCollectionCell class] forCellWithReuseIdentifier:CollectionViewCellIdentifier];
    self.collectionView.backgroundColor = RGBA(250, 250, 250, 1);
    self.collectionView.showsHorizontalScrollIndicator = NO;

   
    [self.vwContainer addSubview:self.collectionView];
  
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void)layoutSubviews{
    
    [super layoutSubviews];
    
 
   // self.collectionView.frame = CGRectMake(8,CGRectGetMaxY(_lineVw.frame) + 8,self.vwContainer.frame.size.width - 16,500);
//    self.collectionView.frame = CGRectMake(8,CGRectGetMaxY(_lineVw.frame) + 8,self.vwContainer.frame.size.width - 16,180);
 
//    self.collectionView.backgroundColor = [UIColor blueColor];
//    self.collectionView.frame = CGRectMake(8,CGRectGetMaxY(_lineVw.frame) + 8,self.vwContainer.frame.size.width - 16,self.collectionView.contentSize.height+8);
//    self.vwContainer.backgroundColor = [UIColor redColor];
//    self.vwContainer.frame = CGRectMake(self.vwContainer.frame.origin.x,self.vwContainer.frame.origin.y,self.vwContainer.frame.size.width,CGRectGetMaxY(self.collectionView.frame)+8);
//    self.contentView.frame = CGRectMake(self.contentView.frame.origin.x,self.contentView.frame.origin.y,self.contentView.frame.size.height,CGRectGetMaxY(self.vwContainer.frame)+8);
}

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath
{
    self.collectionView.dataSource = dataSourceDelegate;
    self.collectionView.delegate = dataSourceDelegate;
    self.collectionView.indexPath = indexPath;
   
//    [self.collectionView setFrame:CGRectMake(8,CGRectGetMaxY(_lineVw.frame) + 8,self.vwContainer.frame.size.width - 16,self.collectionView.contentSize.height)];
//    self.vwContainer.frame = CGRectMake(self.vwContainer.frame.origin.x,self.vwContainer.frame.origin.y,self.vwContainer.frame.size.width,CGRectGetMaxY(self.collectionView.frame) +8);
//    self.frame = CGRectMake(0,0,self.frame.size.width,CGRectGetMaxY(self.vwContainer.frame)+8);
  
    //[self.collectionView setContentOffset:self.collectionView.contentOffset animated:NO];
    
 //   [self.collectionView reloadData];
}

@end
