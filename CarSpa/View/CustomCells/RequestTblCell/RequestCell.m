//
//  RequestCell.m
//  CarSpa
//
//  Created by R@j on 24/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "RequestCell.h"

@implementation RequestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _containerVw.layer.cornerRadius = 6.0f;
    _btnRateMe.layer.cornerRadius = 4.0f;
    [_containerVw.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [_containerVw.layer setShadowOpacity:0.8];
    [_containerVw.layer setShadowRadius:3.0];
    [_containerVw.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setRequestStatus:(int)status{
    
    switch (status) {
        case STATUS_BOOKED:{
            
            self.lblRequestStatus.text = kStatus_Booked;
            [self.lblRequestStatus setTextColor:[UIColor colorWithRed:(77.0/255.0) green:(148.0/255.0) blue:(255.0/255.0) alpha:1.0]];
        }
            break;
        case STATUS_IN_PROGRESS:{
            
            self.lblRequestStatus.text = kStatus_InProgress;
            [self.lblRequestStatus setTextColor:AppThemeColor];
        }
            break;
        case STATUS_COMPLETED:{
            
            self.lblRequestStatus.text = kStatus_Completed;
            [ self.lblRequestStatus setTextColor:[UIColor colorWithRed:(86.0/255.0) green:(86.0/255.0) blue:(86.0/255.0) alpha:1.0]];
        }
            break;
        case STATUS_CANCELLED:{
            
            self.lblRequestStatus.text = kStatus_Cancelled;
            [ self.lblRequestStatus setTextColor:[UIColor colorWithRed:(255.0/255.0) green:(86.0/255.0) blue:(86.0/255.0) alpha:1.0]];
        }
            break;
        case STATUS_ON_THE_WAY:{
            
            self.lblRequestStatus.text = kStatus_OntheWay;
            [ self.lblRequestStatus setTextColor:[UIColor colorWithRed:(77.0/255.0) green:(148.0/255.0) blue:(255.0/255.0) alpha:1.0]];
        }
            break;
        default:{
            self.lblRequestStatus.text = kStatus_Booked;
            [self.lblRequestStatus setTextColor:[UIColor colorWithRed:(77.0/255.0) green:(148.0/255.0) blue:(255.0/255.0) alpha:1.0]];
        }
            break;
    }
}

@end
