//
//  RequestCell.h
//  CarSpa
//
//  Created by R@j on 24/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *containerVw;
@property (weak, nonatomic) IBOutlet UILabel *lblRequestStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblCarType;
@property (weak, nonatomic) IBOutlet UILabel *lblCategory;
@property (weak, nonatomic) IBOutlet UILabel *lblService;
@property (weak, nonatomic) IBOutlet UILabel *lblAppointmentTime;
@property (weak, nonatomic) IBOutlet UILabel *lblPaymentStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblRequestNo;
@property (weak, nonatomic) IBOutlet UILabel *lblAmount;
@property (weak, nonatomic) IBOutlet UIButton *btnRateMe;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnRateHeightConst;
@property (weak, nonatomic) IBOutlet UILabel *lblTapHere;

-(void)setRequestStatus:(int)status;
@end
