//
//  RegisterCell.m
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "RegisterCell.h"

@implementation RegisterCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    _txtFieldInfo.layer.cornerRadius = 5.0f;
    _txtFieldInfo.layer.borderWidth = 1;
    _txtFieldInfo.layer.borderColor = RGBA(200, 200, 200, 1).CGColor;
    
    _btnPhoneNo.layer.cornerRadius = 5.0f;
    _btnPhoneNo.layer.borderWidth = 1;
    _btnPhoneNo.layer.borderColor = RGBA(200, 200, 200, 1).CGColor;
    
    _btnProfileImg.layer.cornerRadius = _btnProfileImg.frame.size.height/2;
    _btnProfileImg.clipsToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
