//
//  RegisterCell.h
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnProfileImg;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldInfo;

@property (weak, nonatomic) IBOutlet UIButton *btnPhoneNo;
@property (weak, nonatomic) IBOutlet UIButton *btnVwTerms;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;

@end
