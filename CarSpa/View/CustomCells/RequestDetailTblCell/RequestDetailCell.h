//
//  RequestDetailCell.h
//  CarSpa
//
//  Created by R@j on 24/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *containerVw;
@property (weak, nonatomic) IBOutlet UILabel *lblRequestStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblCategoryName;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceName;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleType;
@property (weak, nonatomic) IBOutlet UILabel *lblCarName;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDestinationAddress;
@property (weak, nonatomic) IBOutlet UIView *vwDestinationAddress;

@property (weak, nonatomic) IBOutlet UIButton *btnEditCar;
@property (weak, nonatomic) IBOutlet UIButton *btnEditDestinationAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnEditAddress;

@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEditDate;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *destinationVwHeightConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *serviceVwHeightConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *serviceNameHeightConst;
@property (weak, nonatomic) IBOutlet UIView *vwService;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwVehicleType;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwCategory;
@property (weak, nonatomic) IBOutlet UILabel *lblNetAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblTaxmount;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblTaxPer;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscountPer;

@end
