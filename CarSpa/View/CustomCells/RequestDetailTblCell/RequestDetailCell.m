//
//  RequestDetailCell.m
//  CarSpa
//
//  Created by R@j on 24/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "RequestDetailCell.h"

@implementation RequestDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _containerVw.layer.cornerRadius = 6.0f;
    [_containerVw.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [_containerVw.layer setShadowOpacity:0.8];
    [_containerVw.layer setShadowRadius:3.0];
    [_containerVw.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    //Initialization code
    
    _imgVwCategory.layer.cornerRadius = _imgVwCategory.frame.size.height/2;
      _imgVwVehicleType.layer.cornerRadius = _imgVwVehicleType.frame.size.height/2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
