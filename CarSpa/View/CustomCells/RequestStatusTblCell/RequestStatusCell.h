//
//  RequestStatusCell.h
//  CarSpa
//
//  Created by R@j on 24/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestStatusCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *containerVw;

@property (weak, nonatomic) IBOutlet UIImageView *imgVwStatus1;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwStatus2;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwStatus3;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwStatus4;

@property (weak, nonatomic) IBOutlet UIView *vwStatus1;
@property (weak, nonatomic) IBOutlet UIView *vwStatus2;
@property (weak, nonatomic) IBOutlet UIView *vwStatus3;

@property (weak, nonatomic) IBOutlet UILabel *lblBookedTime;
@property (weak, nonatomic) IBOutlet UILabel *lblOnTheWayTime;
@property (weak, nonatomic) IBOutlet UILabel *lblInProgressTime;
@property (weak, nonatomic) IBOutlet UILabel *lblFinishedTime;




@end
