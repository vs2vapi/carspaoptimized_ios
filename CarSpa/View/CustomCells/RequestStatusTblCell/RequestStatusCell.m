//
//  RequestStatusCell.m
//  CarSpa
//
//  Created by R@j on 24/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "RequestStatusCell.h"

@implementation RequestStatusCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _containerVw.layer.cornerRadius = 6.0f;
    [_containerVw.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [_containerVw.layer setShadowOpacity:0.8];
    [_containerVw.layer setShadowRadius:3.0];
    [_containerVw.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
