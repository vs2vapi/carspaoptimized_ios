//
//  ServiceTblCell.m
//  CarSpa
//
//  Created by R@j on 22/02/18.
//  Copyright © 2018 Rajan@VS2. All rights reserved.
//

#import "ServiceTblCell.h"

@implementation ServiceTblCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(0,0,0,0);
    layout.itemSize = CGSizeMake(self.collContainerVw.frame.size.width/3,self.collContainerVw.frame.size.height);
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 5;
    
    self.collectionView = [[ServiceCollectionVw alloc] initWithFrame:self.collContainerVw.frame collectionViewLayout:layout];
    self.collectionView.backgroundColor = [UIColor clearColor];//RGBA(230, 230, 230,1);
    self.collectionView.showsHorizontalScrollIndicator = NO;
    [self.collectionView registerNib:[UINib nibWithNibName:@"ServiceCollVwCell" bundle:nil] forCellWithReuseIdentifier:CollectionViewCellIdentifier];
    [self.collContainerVw addSubview:self.collectionView];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    self.collContainerVw.frame = CGRectMake(0, 0,self.contentView.frame.size.width,self.contentView.frame.size.height - self.greenLineVw.frame.size.height);
    self.collectionView.frame = self.collContainerVw.bounds;
}

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath{
    
    self.collectionView.delegate = dataSourceDelegate;
    self.collectionView.dataSource = dataSourceDelegate;
    self.collectionView.indexPath = indexPath;
    [self.collectionView setContentOffset:self.collectionView.contentOffset animated:NO];
    [self.collectionView reloadData];
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
