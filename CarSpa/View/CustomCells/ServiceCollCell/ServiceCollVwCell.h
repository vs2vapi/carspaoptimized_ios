//
//  ServiceCollVwCell.h
//  CarSpa
//
//  Created by R@j on 22/02/18.
//  Copyright © 2018 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceCollVwCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgVwService;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceTitle;

@property (weak, nonatomic) IBOutlet UIImageView *imgVwArrow;
@property (weak, nonatomic) IBOutlet UILabel *lblCost;
@property (weak, nonatomic) IBOutlet UIButton *btnInformation;
@property (weak, nonatomic) IBOutlet UIImageView *bgImgVw;
@end
