//
//  ServiceCollVwCell.m
//  CarSpa
//
//  Created by R@j on 22/02/18.
//  Copyright © 2018 Rajan@VS2. All rights reserved.
//

#import "ServiceCollVwCell.h"

@implementation ServiceCollVwCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _imgVwService.layer.cornerRadius = _imgVwService.frame.size.height/2;
    _imgVwService.clipsToBounds = YES;
    // Initialization code
}

@end
