//
//  ServiceTblCell.h
//  CarSpa
//
//  Created by R@j on 22/02/18.
//  Copyright © 2018 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceCollectionVw : UICollectionView

@property (nonatomic, strong) NSIndexPath *indexPath;

@end

static NSString *CollectionViewCellIdentifier = @"CellService";

@interface ServiceTblCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *greenLineVw;
@property (nonatomic, strong) ServiceCollectionVw *collectionView;
@property (weak, nonatomic) IBOutlet UIView *collContainerVw;

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath;
@end
