//
//  CarTypeCell.m
//  CarSpa
//
//  Created by Rajan on 15/02/17.
//  Copyright © 2017 Rajan. All rights reserved.
//

#import "CarTypeCell.h"

@implementation CarTypeCell
-(void)awakeFromNib{
    [super awakeFromNib];
    _imgView.layer.cornerRadius = _imgView.frame.size.height/2;
}
@end
