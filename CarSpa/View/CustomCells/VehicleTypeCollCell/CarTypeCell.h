//
//  CarTypeCell.h
//  CarSpa
//
//  Created by Rajan on 15/02/17.
//  Copyright © 2017 Rajan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarTypeCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwArrow;
@property (weak, nonatomic) IBOutlet UIImageView *bgImgVw;

@end
