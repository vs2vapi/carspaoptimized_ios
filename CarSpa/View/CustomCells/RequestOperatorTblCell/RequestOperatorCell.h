//
//  RequestOperatorCell.h
//  CarSpa
//
//  Created by R@j on 25/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestOperatorCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *containerVw;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblVanNo;
@property (weak, nonatomic) IBOutlet UILabel *lblVanType;

@end
