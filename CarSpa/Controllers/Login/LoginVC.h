//
//  LoginVC.h
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginVC : UIViewController

#pragma mark: -------------------: Properties
@property (weak, nonatomic) IBOutlet UIView *loginContainerVw;
@property (weak, nonatomic) IBOutlet UIView *forgotPswdContainerVw;
@property (weak, nonatomic) IBOutlet UITextField *txtLoginEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtLoginPswd;
@property (weak, nonatomic) IBOutlet UIButton *btnRememberMe;
@property (weak, nonatomic) IBOutlet UIView *frgtPswdTxtContainerVw;
@property (weak, nonatomic) IBOutlet UIView *introContainerVw;
@property (weak, nonatomic) IBOutlet UIView *introVw;
@property (weak, nonatomic) IBOutlet UIButton *btnLanguage;
@property (weak, nonatomic) IBOutlet UIButton *btnIntroSignUp;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *introVwHeightConst;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollVwIntro;
@property (weak, nonatomic) IBOutlet UIView *bottomVw;
@property (weak, nonatomic) IBOutlet UITextField *txtForgotEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnAlreadyMember;
@property (weak, nonatomic) IBOutlet UIButton *btnGuestLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property(strong,nonatomic)NSString *typeOfAuthentication;

#pragma mark: -------------------: Button Actions

- (IBAction)btnRememberMeAction:(id)sender;
- (IBAction)btnLoginAction:(id)sender;
- (IBAction)btnForgotPswdAction:(id)sender;
- (IBAction)btnSignUpAction:(id)sender;

- (IBAction)btnFbAction:(id)sender;
- (IBAction)btnTwitterAction:(id)sender;
- (IBAction)btnGoogleAction:(id)sender;
- (IBAction)btnInstaAction:(id)sender;
- (IBAction)btnLinkedInAction:(id)sender;

- (IBAction)btnForgotPswdOkAction:(id)sender;
- (IBAction)btnForgotPswdCancelAction:(id)sender;

- (IBAction)btnLanguageAction:(id)sender;
- (IBAction)btnIntroSignUpAction:(id)sender;
- (IBAction)btnAlreadyMemberAction:(id)sender;
- (IBAction)btnGuestLoginAction:(id)sender;

@end
