//
//  LoginVC.m
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "LoginVC.h"
#import "EAIntroPage.h"
#import "EAIntroView.h"
#import "RegisterVC.h"
#import "TabBarVC.h"
#import "CarSpa-Swift.h"
#import "WSUser.h"
#import "VerificationVC.h"
#import <SafariServices/SafariServices.h>
#import <TwitterKit/TWTRLogInButton.h>


@interface LoginVC ()<EAIntroDelegate,RTCountryPickerDelegate,GIDSignInDelegate,GIDSignInUIDelegate,UIWebViewDelegate>{
    CDAlertView *alertViewPhone;
    UIButton *btnPhoneNoAction;
    NSString *userCountryCode;
    WSUser *socialMediaUserObj;
    
    IBOutlet UIWebView *loginWebView;
    IBOutlet UIActivityIndicatorView* loginIndicator;
    IBOutlet UILabel *loadingLabel;
    
}

@end

@implementation LoginVC

@synthesize typeOfAuthentication;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    hideNavBar(YES);
    
    NSString *strEmail = [Functions getStringValueFromDefaults:USER_EMAIL];
    NSString *strPswd = [Functions getStringValueFromDefaults:USER_PSWD];
    if(strEmail==nil || [strEmail isEqualToString:@""]){
        _txtLoginEmail.text = @"";
        _txtLoginPswd.text = @"";
    }else{
        _txtLoginEmail.text = strEmail;
        _txtLoginPswd.text = strPswd;
        [_btnRememberMe setSelected:YES];
    }
#if TARGET_IPHONE_SIMULATOR
    _txtLoginEmail.text = @"rajan@vs2.in"; //@"+9659033490424"
    _txtLoginPswd.text = @"welcome";
#endif
}


#pragma mark: -------------------: Setup UI
-(void)setUI{

    [self.btnGuestLogin setTitle:kGuestLogin forState:UIControlStateNormal];
    
    if(![self isEnglishLanguage]){
         [_btnRememberMe setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    }
    
    NSAttributedString *attrMember = [[NSAttributedString alloc]initWithString:kAlreadyMember attributes:@{NSFontAttributeName : [UIFont fontWithName:@"dax-bold" size:18],NSForegroundColorAttributeName : [UIColor whiteColor]}];

    NSAttributedString *attrLogin = [[NSAttributedString alloc]initWithString:kLoginWelcome attributes:@{NSFontAttributeName : [UIFont fontWithName:@"dax-bold" size:18],NSForegroundColorAttributeName : [UIColor darkGrayColor]}];
    
    
    NSMutableAttributedString *attrConcat = [[NSMutableAttributedString alloc]init];
    [attrConcat appendAttributedString:attrMember];
    [attrConcat appendAttributedString:attrLogin];
    
    [_btnAlreadyMember setAttributedTitle:attrConcat forState:UIControlStateNormal];
    
    UIView *containerVwEmail = [[UIView alloc]initWithFrame:CGRectMake(0, 0,10, _txtLoginEmail.frame.size.height)];
    [containerVwEmail setBackgroundColor:[UIColor clearColor]];
    
    UIView *paddingVwEmail = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 5, _txtLoginEmail.frame.size.height)];
    [paddingVwEmail setBackgroundColor:RGBA(200, 200, 200, 1)];
    
    [containerVwEmail addSubview:paddingVwEmail];
    
    [_txtLoginEmail setLeftViewMode:UITextFieldViewModeAlways];
    [_txtLoginEmail setLeftView:containerVwEmail];
    
    UIView *containerVwPassword = [[UIView alloc]initWithFrame:CGRectMake(0, 0,10, _txtLoginPswd.frame.size.height)];
    [containerVwPassword setBackgroundColor:[UIColor clearColor]];
    
    UIView *paddingVwPassword= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 5, _txtLoginPswd.frame.size.height)];
    [paddingVwPassword setBackgroundColor:RGBA(200, 200, 200, 1)];
    [containerVwPassword addSubview:paddingVwPassword];
    
    [_txtLoginPswd setLeftViewMode:UITextFieldViewModeAlways];
    [_txtLoginPswd setLeftView:containerVwPassword];
    
    _frgtPswdTxtContainerVw.layer.cornerRadius = 6.0f;
    _forgotPswdContainerVw.transform = CGAffineTransformMakeScale(0, 0);
    
    //Check for force update.
    [self setUpIntroVC];
//    [self checkForceUpdate];
    [self proceedWithLogin];
    
    //Adding observer to find country code.
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(locationFound:) name:LOCATION_FOUND object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(loginSuccess:) name:LOGIN_SUCCESS object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(languageChanged:) name:LANGUAGE_CHANGED object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(checkForceUpdate) name:CHECK_FORCE_UPDATE object:nil];

}


-(void)checkForceUpdate{

    if([ReachabilityManager isReachable]){
        [[WebServiceInvocation alloc]initWithWS:kCheckForceUpdateWS withParams:nil withServiceType:TYPE_GET withSelector:@selector(forceUpdateWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)forceUpdateWSAction:(id)sender{
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        // [self showAlert:[sender responseErr]];
    }else{
        
        BOOL forceUpdate =[[[sender responseDictionary]valueForKey:@"forceUpdate"]boolValue];
        if(forceUpdate){
            NSString  *appVersion = [self fetchAppVersion];
            NSString *updatedVersion = [[sender responseDictionary]valueForKey:@"currentVersion"];
            NSString *msgKey = kServiceResponseMsgKey;
            if( [AppContext.appLanguage isEqualToString:@"ar"] || [AppContext.appLanguage isEqualToString:@"ar-US"] ){
                msgKey = kServiceResponseMsgARKey;
            }
        
            NSString *alertMsg = [[sender responseDictionary]valueForKey:msgKey];
            if(![appVersion isEqualToString:updatedVersion]){
                [UIAlertController showAlertInVC:self withTitle:AppName withMsg:alertMsg withOtherAlertActionTitle:@[kAlertUpdateNow] withAlertActionCancel:@"" withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
                    if(buttonIndex == 0){
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kApp_AppStoreLink]];
                    }
                }];
            }else{
                [self proceedWithLogin];
            }
        }else{
            [self proceedWithLogin];
        }
    }
}

-(void)proceedWithLogin{
    BOOL isUserLoggedIn = [Functions getBoolValueFromDefaults:IS_USER_LOGGED_IN];
    if(isUserLoggedIn){
        [self loadTabBarVC];
    }
}

-(void)locationFound:(id)notification{
    [self fetchCountryCode:[notification object]];
}

-(void)fetchCountryCode:(CLLocation *)loc{
    
    CLGeocoder *reverseGeocoder = [[CLGeocoder alloc] init];
    __block NSString  *countryCode=@"";
    [reverseGeocoder reverseGeocodeLocation:loc completionHandler:^(NSArray *placemarks, NSError *error){
         if (error){
             NSLog(@"Geocode failed with error: %@", error);
             return;
         }
         
         NSLog(@"Received placemarks: %@", placemarks);
         
         CLPlacemark *myPlacemark = [placemarks objectAtIndex:0];
         countryCode = myPlacemark.ISOcountryCode;
         NSString *countryName = myPlacemark.country;
         NSLog(@"My country code: %@ and countryName: %@", countryCode, countryName);
         [Functions setStringValueToUserDefaults:countryCode withKey:USER_COUNTRY_CODE];
         [[NSNotificationCenter defaultCenter]removeObserver:LOCATION_FOUND];
       
      //  BOOL isUserLoggedIn = [Functions getBoolValueFromDefaults:IS_USER_LOGGED_IN];
      //  if(!isUserLoggedIn){
             [self fetchCurrencyExchangeRate];
      //  }
     }];
    
    NSArray *arrCountryPhoneCodes = [[NSArray alloc] initWithObjects:@"973", @"965",@"974",@"966", @"971",@"968",@"961", nil];
    NSArray *arrRegion = [[NSArray alloc] initWithObjects:@"BH", @"KW",@"QA",@"SA", @"AE",@"OM",@"LB", nil];
   
    __block NSString *strPhoneCode = @"965";
    [arrRegion enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if([countryCode isEqualToString:obj]){
            strPhoneCode = [arrCountryPhoneCodes objectAtIndex:idx];
        }
    }];
    [Functions setStringValueToUserDefaults:strPhoneCode withKey:USER_PHONE_CODE];
}

-(void)fetchCurrencyExchangeRate{
    
    
    [self setDefaultCurrency];
    
    if([ReachabilityManager isReachable]){
        
        NSString *strCountryCode =  [Functions getStringValueFromDefaults:USER_COUNTRY_CODE];
        NSString *strUrl = [NSString stringWithFormat:@"%@?countryCode=%@&%@=%@",kFetchCurrencyWS,strCountryCode,kAppVersionServiceKey,[self fetchAppVersion]];
        [[WebServiceInvocation alloc]initWithWS:strUrl withParams:nil withServiceType:TYPE_GET withSelector:@selector(currencyWSAction:) withTarget:self showLoader:NO loaderMsg:kWSLoaderDialoagMsg];
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)currencyWSAction:(id)sender{
    
    if([sender responseCode] > 200){
    }else{

        NSInteger currencyId = [[[[sender responseDictionary]objectForKey:kServiceResponseKey]valueForKey:@"currencyId"]integerValue];
        [Functions setIntegerValueToUserDefaults:currencyId withKey:CURRENCY_ID];
        
        NSString *key = ([AppContext.appLanguage isEqualToString:@"ar"]) ? @"currencyAR" : @"currency";
        AppContext.currency = [NSString stringWithFormat:@"%@",[[[sender responseDictionary]objectForKey:kServiceResponseKey]valueForKey:key]];
        [Functions setStringValueToUserDefaults:AppContext.currency withKey:CURRENCY];
        
        AppContext.exchangeRate =  [[[[sender responseDictionary]objectForKey:kServiceResponseKey]valueForKey:@"excahngeRate"]doubleValue];
        [Functions setDoubleValueToUserDefaults:AppContext.exchangeRate withKey:EXCHANGE_RATE];
        
    }
}

-(void)setDefaultCurrency{
    
    AppContext.exchangeRate = 1;
    AppContext.currency = kSAR;
    [Functions setIntegerValueToUserDefaults:1 withKey:CURRENCY_ID];
    [Functions setStringValueToUserDefaults:AppContext.currency withKey:CURRENCY];
    [Functions setDoubleValueToUserDefaults:1 withKey:EXCHANGE_RATE];

}

-(void)loginSuccess:(id)notification{
    [_introContainerVw setHidden:YES];
    [self loadTabBarVC];
    [[NSNotificationCenter defaultCenter]removeObserver:LOGIN_SUCCESS];
}

-(void)openForgotPswdVw:(BOOL)openVw{
    
    if(openVw){
        [_forgotPswdContainerVw setHidden:NO];
           _forgotPswdContainerVw.alpha = 1.0f;
        _forgotPswdContainerVw.transform = CGAffineTransformMakeScale(0.5f, 0.5f);
        [UIView animateWithDuration:.3f delay:.0f usingSpringWithDamping:.6f initialSpringVelocity:.8f options:UIViewAnimationOptionCurveEaseOut animations:^{
             _forgotPswdContainerVw.transform = CGAffineTransformMakeScale(1,1);
        } completion:^(BOOL finished){
            
        }];
    }else{
        
        [UIView animateWithDuration:.3f delay:.0f usingSpringWithDamping:.6f initialSpringVelocity:.5f options:UIViewAnimationOptionCurveEaseIn animations:^{
            _forgotPswdContainerVw.transform = CGAffineTransformMakeScale(0.5f, 0.5f);
            _forgotPswdContainerVw.alpha = 0;
        } completion:^(BOOL finished){
           _forgotPswdContainerVw.transform = CGAffineTransformMakeScale(0, 0);
           [_forgotPswdContainerVw setHidden:YES];
        }];
    }
    
}

-(void)languageChanged:(NSNotification *)notification{
    [self loadTabBarVC];
}

-(void)setUpIntroVC{
    
    NSString *appLanguage = [self isEnglishLanguage] ? kEnglish : kArabic;
    [_btnLanguage setTitle:[NSString stringWithFormat:@"%@: %@",kLanguage,appLanguage] forState:UIControlStateNormal];
     [_introContainerVw setHidden:NO];
    _btnIntroSignUp.layer.borderColor = [UIColor whiteColor].CGColor;
    _btnIntroSignUp.layer.borderWidth = 1.0f;
    _btnIntroSignUp.layer.cornerRadius = 4.0f;
    
    EAIntroPage *page1 = [EAIntroPage page];
    page1.title = kIntro_text_1;
    page1.desc =  kIntro_desc_1;
    page1.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icn_intro1"]];
    
    
    EAIntroPage *page2 = [EAIntroPage page];
    page2.title = kIntro_text_2;
    page2.desc =  kIntro_desc_2;
    page2.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icn_intro2"]];
    
    
    EAIntroPage *page3 = [EAIntroPage page];
    page3.title = kIntro_text_3;
    page3.desc =  kIntro_desc_3;
    page3.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icn_intro3"]];
    
    
    EAIntroPage *page4 = [EAIntroPage page];
    page4.title = kIntro_text_4;
    page4.desc =  kIntro_desc_4;
    page4.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icn_intro4"]];
    
    EAIntroPage *page5 = [EAIntroPage page];
    page5.title = kIntro_text_5;
    page5.desc =  kIntro_desc_5;
    page5.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icn_intro5"]];
    
    EAIntroPage *page6 = [EAIntroPage page];
    page6.title = kIntro_text_6;
    page6.desc = kIntro_desc_6;
    page6.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icn_intro6"]];
    
    EAIntroView *intro = [[EAIntroView alloc] initWithFrame:_introVw.bounds];
    [intro setDelegate:self];
    
    intro.skipButton.alpha = 0.f;
    intro.skipButton.enabled = NO;
    intro.swipeToExit = NO;
    intro.pageControlY = 25;
    
    [intro setPages:@[page1,page2,page3,page4,page5,page6]];
    [intro showInView:_introVw animateDuration:0.3];
    [self performSelector:@selector(animateScrollVw) withObject:nil afterDelay:1.0f];

}

-(void)animateScrollVw{
    
    int scrollToBottomOffset = _scrollVwIntro.contentSize.height + _scrollVwIntro.contentInset.bottom - _scrollVwIntro.bounds.size.height;
    [_scrollVwIntro setContentOffset:CGPointMake(0,scrollToBottomOffset) animated:YES];
}

-(void)configAlertVwForLanguage{

    @try {
        
        UIImage *img = [[UIImage imageNamed:@"logo-icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        CDAlertView *alertView = [[CDAlertView alloc]init];
        alertView.messageLabel.text = kSelectLanguage;
        alertView.backgroundColor = RGBA(0, 0, 0, 0.7f);
        alertView.circleFillColor = [UIColor colorWithPatternImage:img];
        alertView.tintColor = [UIColor whiteColor];
        alertView.isActionButtonsVertical = YES;
        alertView.alertBackgroundColor = [UIColor whiteColor];
        alertView.messageTextColor = [UIColor grayColor];
        
        
        CDAlertViewAction *action_eng = [[CDAlertViewAction alloc]initWithTitle:kEnglish font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            [self changeLanguage:@"en"];
        }];
        
        CDAlertViewAction *action_arabic = [[CDAlertViewAction alloc]initWithTitle:kArabic font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            [self changeLanguage:@"ar"];
        }];
        
        
        CDAlertViewAction *action_cancel = [[CDAlertViewAction alloc]initWithTitle:kCancel font:Dax_Regular(15) textColor:[UIColor redColor] backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            
        }];
        
        [alertView addWithAction:action_eng];
        [alertView addWithAction:action_arabic];
        [alertView addWithAction:action_cancel];
        
        [alertView show:^(CDAlertView *action) {
            
        }];
        
    } @catch (NSException *exception) {
        NSLog(@"Alertview Exception : %@ , In Controller : %@",[exception description],self);
    } @finally {
        
    }
    
  
}

-(void)changeLanguage:(NSString *)language{
    
    [UIAlertController showAlertInVC:self withTitle:AppName withMsg:kChangeLanguage withOtherAlertActionTitle:@[kAlertYes] withAlertActionCancel:kAlertNo withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
        if(buttonIndex == 0){
            [Functions setStringValueToUserDefaults:language withKey:DEFAULT_APP_LANGUAGE];
            AppContext.appLanguage = language;
            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:language, nil] forKey:@"AppleLanguages"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [self updateLanguage:language];
        }
    }];
}

-(void)updateLanguage:(NSString *)language{
    
    
    if([ReachabilityManager isReachable]){
        NSString *deviceToken = [Functions getStringValueFromDefaults:DEVICE_TOKEN];
        if(deviceToken == nil){
            deviceToken = @"FailToRetrievetoken";
        }
        
        NSDictionary *dictParams = @{@"deviceToken" : deviceToken,
                                        @"languageCode" : language
                                        };
        [[WebServiceInvocation alloc]initWithWS:kUpdateLanguageWS withParams:dictParams withServiceType:TYPE_POST withSelector:@selector(updateLanguageWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)updateLanguageWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        exit(EXIT_SUCCESS);
    }
}

#pragma mark: --------------------: EAIntroView delegate
- (void)introDidFinish:(EAIntroView *)introView wasSkipped:(BOOL)wasSkipped {

}

-(void)showRegisterVC{
    RegisterVC *rgvc = loadViewController(kSBMain, kRegisterVC);
    [self.navigationController pushViewController:rgvc animated:YES];
}

-(void)loadTabBarVC{

    [self btnAlreadyMemberAction:nil];
     TabBarVC *tbvc = loadViewController(kSBMain, kTabBarVC);
    AppContext.appTabVC = tbvc;
    [self presentViewController:tbvc animated:YES completion:nil];
}

#pragma mark: -------------------: Button Actions
- (IBAction)btnRememberMeAction:(id)sender {
    
    if([_btnRememberMe isSelected]){
        _btnRememberMe.selected = NO;
    }else{
        _btnRememberMe.selected = YES;
    }
    _btnRememberMe.imageView.transform = CGAffineTransformMakeScale(0.5f,0.5f);
    [UIView animateWithDuration:.3f delay:.0f usingSpringWithDamping:.6f initialSpringVelocity:.8f options:UIViewAnimationOptionCurveEaseOut animations:^{
        _btnRememberMe.imageView.transform = CGAffineTransformMakeScale(1,1);
    } completion:^(BOOL finished){
        
    }];
}

- (IBAction)btnLoginAction:(id)sender {
    
    if([_txtLoginEmail.text isEqualToString:@""]){
        [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kEmail]];
        return;
    }else if ([_txtLoginPswd.text isEqualToString:@""]){
        [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kPassword]];
        return;
    }else{
        [self storeLoginCredentials];
        [self loginUser];
    }
}

- (IBAction)btnForgotPswdAction:(id)sender {
    [self openForgotPswdVw:YES];
}

- (IBAction)btnSignUpAction:(id)sender {
      [self showRegisterVC];
}

- (IBAction)btnFbAction:(id)sender {
    if([ReachabilityManager isReachable]){
        [self showPhoneNoAlertForType:FACEBOOK_LOGIN];
    }else{
        ShowNoNetworkAlert(self);
    }
}

- (IBAction)btnTwitterAction:(id)sender {
    
    if([ReachabilityManager isReachable]){
          [self showPhoneNoAlertForType:TWITTER_LOGIN];
    }else{
        ShowNoNetworkAlert(self);
    }

}

- (IBAction)btnGoogleAction:(id)sender {
    if([ReachabilityManager isReachable]){
          [self showPhoneNoAlertForType:GOOGLE_PLUS_LOGIN];
    }else{
        ShowNoNetworkAlert(self);
    }
   
}

- (IBAction)btnInstaAction:(id)sender {
    if([ReachabilityManager isReachable]){
         [self showPhoneNoAlertForType:INSTAGRAM_LOGIN];
    }else{
        ShowNoNetworkAlert(self);
    }
}

- (IBAction)btnLinkedInAction:(id)sender {
    if([ReachabilityManager isReachable]){
         [self showPhoneNoAlertForType:LINKEDIN_LOGIN];
    }else{
        ShowNoNetworkAlert(self);
    }
}

- (IBAction)btnForgotPswdOkAction:(id)sender {
    
    [self.view endEditing:YES];
    if([_txtForgotEmail.text isEqualToString:@""]){
        [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kEmail]];
        return;
    }else{
        [self forgotPswd];
        [self openForgotPswdVw:NO];
    }
}

- (IBAction)btnForgotPswdCancelAction:(id)sender {
   [self openForgotPswdVw:NO];
}

- (IBAction)btnLanguageAction:(id)sender {
    [self configAlertVwForLanguage];
}

- (IBAction)btnIntroSignUpAction:(id)sender {
    [self showRegisterVC];
}

- (IBAction)btnAlreadyMemberAction:(id)sender {
    
    [UIView animateWithDuration:0.5f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [_introContainerVw setAlpha:0.0f];
    } completion:^(BOOL finished) {
        [_introContainerVw setHidden:YES];
    }];
    
}

- (IBAction)btnGuestLoginAction:(id)sender {
    [Functions setBoolValueToUserDefaults:NO withKey:IS_USER_LOGGED_IN];
    [self loadTabBarVC];
}

-(NSString *)validatePhoneField:(NSString *)txt{
    
    NSString *strPhoneText = txt;
    if([strPhoneText containsString:@"+"]){
        strPhoneText = [strPhoneText stringByReplacingOccurrencesOfString:@"+" withString:@""];
    }
    
    NSRange range = NSMakeRange(0,1);
    NSString *discardZero = [strPhoneText substringWithRange:range];
    
    if([discardZero isEqualToString:@"0"]){
        strPhoneText = [strPhoneText stringByReplacingCharactersInRange:range withString:@""];
    }
    
    range = NSMakeRange(0,1);
    discardZero = [strPhoneText substringWithRange:range];
    if([discardZero isEqualToString:@"0"]){
        strPhoneText = [strPhoneText stringByReplacingCharactersInRange:range withString:@""];
    }
    
    return strPhoneText;
}

-(void)storeLoginCredentials{
    
    if([_btnRememberMe isSelected]){
        [Functions setStringValueToUserDefaults:_txtLoginEmail.text withKey:USER_EMAIL];
        [Functions setStringValueToUserDefaults:_txtLoginPswd.text withKey:USER_PSWD];
    }else{
        [Functions setStringValueToUserDefaults:@"" withKey:USER_EMAIL];
        [Functions setStringValueToUserDefaults:@"" withKey:USER_PSWD];
    }
    
}

#pragma mark: --------------------: WebService Functions

-(void)loginUser{
    
    if([ReachabilityManager isReachable]){
        
         [self.view endEditing:YES];
        NSString *strEmail =   [self validatePhoneField:_txtLoginEmail.text];
        NSString *strPassword =_txtLoginPswd.text;
        NSString *deviceToken = [Functions getStringValueFromDefaults:DEVICE_TOKEN];
        if(deviceToken == nil){
            deviceToken = @"FailToRetrievetoken";
        }
        
        double lattitude = [Functions getDoubleValueFromDefaults:USER_LATITUDE];
        double longitude = [Functions getDoubleValueFromDefaults:USER_LONGITUDE];
        
         NSDictionary *dictParams= @{
                                                            @"email":strEmail,
                                                            @"password":strPassword,
                                                            @"deviceType":kDeviceType,
                                                            @"deviceToken":deviceToken,
                                                            @"latitude":[NSNumber numberWithDouble:lattitude],
                                                            @"longitude":[NSNumber numberWithDouble:longitude],
                                                            @"language":AppContext.appLanguage
                                                        };
        

        [[WebServiceInvocation alloc]initWithWS:kLoginWS withParams:dictParams withServiceType:TYPE_POST withSelector:@selector(loginWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)loginWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        NSInteger loginStatus = [[[sender responseDictionary]valueForKey:@"Status"]integerValue];
        if(loginStatus == 3){
            NSString *msg = [NSString stringWithFormat:@"%@", [[sender responseDictionary]valueForKey:@"Message"]];
            [UIAlertController showAlertInVC:self withTitle:AppName withMsg:msg withOtherAlertActionTitle:@[kAlertOk] withAlertActionCancel:@"" withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
                if(buttonIndex == 0){
                    NSString *mobileNumber = [NSString stringWithFormat:@"%@",[[sender responseDictionary]valueForKey:@"mobileNumber"]];
                    VerificationVC *vfvc = loadViewController(kSBMain, kVerificationVC);
                    vfvc.mobileNo = mobileNumber;
                    [self.navigationController pushViewController:vfvc animated:YES];
                }
            }];
        }else{
            [Functions setBoolValueToUserDefaults:YES withKey:IS_USER_LOGGED_IN];
            [self loadTabBarVC];
        }
    }
}

-(void)forgotPswd{
    if([ReachabilityManager isReachable]){
        NSDictionary *dictParams= @{
                                    @"email":_txtForgotEmail.text,
                                    @"language":AppContext.appLanguage
                                    };
        
        [[WebServiceInvocation alloc]initWithWS:kForgotPswdWS withParams:dictParams withServiceType:TYPE_POST withSelector:@selector(forgotPswdWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)forgotPswdWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        [self showAlert:[[sender responseDictionary] objectForKey:@"Message"]];
    }
}

#pragma mark: -------------------: AlertView Functions
-(void)showPhoneNoAlertForType:(int)loginType{
    
    
    @try {
        
        userCountryCode = [self fetchUserCountryCode];
        
        UIImage *img = [[UIImage imageNamed:@"logo-icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        CDAlertView *alertView = [[CDAlertView alloc]init];
        alertView.titleLabel.text = kSocialLoginAlertMsg;
        alertView.titleFont = Dax_Medium(15);
        alertView.backgroundColor = RGBA(0, 0, 0, 0.7f);
        alertView.circleFillColor = [UIColor colorWithPatternImage:img];
        alertView.tintColor = [UIColor whiteColor];
        alertView.isActionButtonsVertical = NO;
        alertView.isTextFieldHidden = YES;
        alertView.alertBackgroundColor = [UIColor whiteColor];
        alertView.tintColor = AppThemeColor;
        
        
        CDAlertViewAction *action_yes = [[CDAlertViewAction alloc]initWithTitle:kAlertYes font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            if(loginType == FACEBOOK_LOGIN){
                [alertView removeFromSuperview];
                [self loginWithFacebook];
                return;
            }
            else if(loginType == TWITTER_LOGIN){
                [self loginWithTwitter];
                return;
            }
            else  if(loginType == GOOGLE_PLUS_LOGIN){
                [self loginWithGmail];
                return;
            }
            else  if(loginType == LINKEDIN_LOGIN){
                [alertView removeFromSuperview];
                [self loginWithLinkedIn];
                return;
            }else{
                  [self loginwithInsta];
            }
        }];
        
        
        CDAlertViewAction *action_cancel = [[CDAlertViewAction alloc]initWithTitle:kAlertNo font:Dax_Regular(15) textColor:[UIColor redColor] backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            
        }];
        
        [alertView addWithAction:action_cancel];
        [alertView addWithAction:action_yes];
        
        
        [alertView show:^(CDAlertView *action) {
            
        }];
        
    } @catch (NSException *exception) {
        NSLog(@"Alertview Exception : %@ , In Controller : %@",[exception description],self);
    } @finally {
        
    }
    
}



#pragma mark: -------------------: CountryPicker Delegates
-(void)pickerSelectedCountry:(NSString *)selectedCode{
    userCountryCode = selectedCode;
    [btnPhoneNoAction setTitle:[NSString stringWithFormat:@"+%@",userCountryCode] forState:UIControlStateNormal];
}

#pragma mark: -------------------: Social Media Login Functions

-(void)loginWithFacebook{

    [Functions setIntegerValueToUserDefaults:FACEBOOK_LOGIN withKey:SOCIAL_LOGIN_TYPE];
    
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login
         logInWithReadPermissions: @[@"public_profile",@"email"]
         fromViewController:self
         handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
             if (error) {
                 NSLog(@"Process error");
             } else if (result.isCancelled) {
                 NSLog(@"Cancelled");
             } else {
                 
                 if(result.token){
                     [self fetchFacebookProfileInfos];
                 }
    
                 NSLog(@"Logged in");
             }
         }];
    
}

-(void)fetchFacebookProfileInfos {
    
 
    [RTProgressHUD showProgressWithMsg:kWSLoaderDialoagMsg];
    
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields" : @"id,name,picture.width(120).height(120),email"}]startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            
            [RTProgressHUD dismiss];
            
            if (!error) {
                
                NSString *fullName = [result valueForKey:@"name"];
                NSString *profileUrl = [[[result valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"];
                NSString *userId = [result valueForKey:@"id"];
                NSString *email = [NSString stringWithFormat:@"%@",[result valueForKey:@"email"]];
                
                double lattitude = [Functions getDoubleValueFromDefaults:USER_LATITUDE];
                double longitude = [Functions getDoubleValueFromDefaults:USER_LONGITUDE];
                
                socialMediaUserObj = [WSUser new];
                socialMediaUserObj.socialId = userId;
                socialMediaUserObj.socialType = FACEBOOK_LOGIN;
                socialMediaUserObj.fullName = fullName;
                socialMediaUserObj.email = email;
                socialMediaUserObj.latitude = lattitude;
                socialMediaUserObj.longitude =longitude;
                socialMediaUserObj.thumbUrl = profileUrl;
                socialMediaUserObj.fileUrl = profileUrl;
                dispatch_async(dispatch_get_main_queue(), ^{
                      [self configPhoneAlertVw];
                });
              

                
            }
        }];
    }
  
}


-(void)loginWithLinkedIn{
    
      [Functions setIntegerValueToUserDefaults:LINKEDIN_LOGIN withKey:SOCIAL_LOGIN_TYPE];

        [RTProgressHUD showProgressWithMsg:kWSLoaderDialoagMsg];
        NSArray *permissions = [NSArray arrayWithObjects:LISDK_BASIC_PROFILE_PERMISSION, LISDK_EMAILADDRESS_PERMISSION,nil];
        [LISDKSessionManager createSessionWithAuth:permissions state:nil showGoToAppStoreDialog:YES successBlock:^(NSString *returnState)
         {
           
             LISDKSession *session = [[LISDKSessionManager sharedInstance] session];
             if(session!=nil){
                 
                 NSString *urlString = [NSString stringWithFormat:@"%@/people/~:(id,first-name,last-name,maiden-name,email-address,public-profile-url,picture-url)", LINKEDIN_API_URL];
                 
                 [[LISDKAPIHelper sharedInstance] getRequest:urlString
                                                     success:^(LISDKAPIResponse *response){
                                                         
                                                         [RTProgressHUD dismiss];
                                                         NSData* data = [response.data dataUsingEncoding:NSUTF8StringEncoding];
                                                         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                                         
                                                         double lattitude = [Functions getDoubleValueFromDefaults:USER_LATITUDE];
                                                         double longitude = [Functions getDoubleValueFromDefaults:USER_LONGITUDE];
                                                         
                                                         socialMediaUserObj  = [WSUser new];
                                                         socialMediaUserObj.socialId = [NSString stringWithFormat:@"%@",[dictResponse valueForKey: @"id"]];
                                                         socialMediaUserObj.socialType = LINKEDIN_LOGIN;
                                                         socialMediaUserObj.fullName = [NSString stringWithFormat:@"%@ %@",[dictResponse valueForKey: @"firstName"], [dictResponse valueForKey: @"lastName"]];
                                                         socialMediaUserObj.email = [NSString stringWithFormat:@"%@",[dictResponse valueForKey: @"emailAddress"]];
                                                         socialMediaUserObj.latitude = lattitude;
                                                         socialMediaUserObj.longitude = longitude;
                                                         socialMediaUserObj.thumbUrl = [dictResponse valueForKey: @"pictureUrl"];
                                                         socialMediaUserObj.fileUrl =  [dictResponse valueForKey: @"publicProfileUrl"];
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             [self configPhoneAlertVw];
                                                         });
                                                         
                                                         
                                                     } error:^(LISDKAPIError *apiError){
                                                         NSLog(@"Error : %@", apiError);
                                                         [RTProgressHUD dismiss];
                                                         [self showAlert:apiError.description];
                                                     }];
             }else{
                 [self showAlert:kTechnicalIssueMsg];
             }
         } errorBlock:^(NSError *error){
             [self showAlert:error.description];
             [RTProgressHUD dismiss];
         }];
    
    
    
}

-(void)loginWithTwitter{
    
    [Functions setIntegerValueToUserDefaults:TWITTER_LOGIN withKey:SOCIAL_LOGIN_TYPE];
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
             socialMediaUserObj = [WSUser new];
            socialMediaUserObj.fullName = [session userName];
            socialMediaUserObj.socialId = [session userID];
            [self requestTwitterUserEmail];
        } else {
            [self showAlert:[error localizedDescription]];
        }
    }];
    
}

-(void)requestTwitterUserEmail{
    
    TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser];
    [RTProgressHUD showProgressWithMsg:kWSLoaderDialoagMsg];
    [client requestEmailForCurrentUser:^(NSString *email, NSError *error) {
        [RTProgressHUD dismiss];
        if (email) {
            socialMediaUserObj.email = email;
            [self setTwitterUserDetails];
        } else {
            [self showAlert:[error localizedDescription]];
        }
    }];
    
}

-(void)setTwitterUserDetails{
    
    double lattitude = [Functions getDoubleValueFromDefaults:USER_LATITUDE];
    double longitude = [Functions getDoubleValueFromDefaults:USER_LONGITUDE];
    socialMediaUserObj.socialType = TWITTER_LOGIN;
    socialMediaUserObj.latitude = lattitude;
    socialMediaUserObj.longitude =longitude;
    [self configPhoneAlertVw];
}

-(void)loginWithGmail{
    
    [Functions setIntegerValueToUserDefaults:GOOGLE_PLUS_LOGIN withKey:SOCIAL_LOGIN_TYPE];
    GIDSignIn *signin = [GIDSignIn sharedInstance];
    signin.shouldFetchBasicProfile = true;
    signin.delegate = self;
    signin.uiDelegate = self;
    [signin signIn];
    
}

#pragma mark:-----------------: Google SingIn Delegates
// Implement these methods only if the GIDSignInUIDelegate is not a subclass of
// UIViewController.

// Stop the UIActivityIndicatorView animation that was started when the user
// pressed the Sign In button
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
   
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {

    if(error){
        [self showAlert:[error localizedDescription]];
        return;
    }
    
    double lattitude = [Functions getDoubleValueFromDefaults:USER_LATITUDE];
    double longitude = [Functions getDoubleValueFromDefaults:USER_LONGITUDE];
    
    GIDProfileData *profile = user.profile;
    NSString *userId = user.userID;
    NSString *fullName =[NSString stringWithFormat:@"%@ %@",profile.givenName,profile.familyName];
    
    socialMediaUserObj  = [WSUser new];
    socialMediaUserObj.socialId = userId;
    socialMediaUserObj.socialType = GOOGLE_PLUS_LOGIN;
    socialMediaUserObj.fullName = fullName;
    socialMediaUserObj.email = [NSString stringWithFormat:@"%@",profile.email];
    socialMediaUserObj.latitude = lattitude;
    socialMediaUserObj.longitude = longitude;
    if(user.profile.hasImage){
        NSURL *url =   [user.profile imageURLWithDimension:1];
         socialMediaUserObj.fileUrl = url.absoluteString;
    }
      [self configPhoneAlertVw];

}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
}

#pragma mark: -------------------: Phone AlertView Functions
-(void)configPhoneAlertVw{
    
    
    UIImage *img = [[UIImage imageNamed:@"logo-icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    alertViewPhone = [[CDAlertView alloc]initWithFrame:self.view.bounds];
    alertViewPhone.messageLabel.text = kTapOnCountryCode;
    alertViewPhone.titleLabel.text = [NSString stringWithFormat:@"%@ %@",kEnter,kMobileNo];
    alertViewPhone.titleFont = Dax_Medium(15);
    alertViewPhone.backgroundColor = RGBA(0, 0, 0, 0.7f);
    alertViewPhone.circleFillColor = [UIColor colorWithPatternImage:img];
    alertViewPhone.tintColor = [UIColor whiteColor];
    alertViewPhone.isActionButtonsVertical = NO;
    alertViewPhone.isTextFieldHidden = NO;
    alertViewPhone.alertBackgroundColor = [UIColor whiteColor];
    alertViewPhone.messageTextColor = AppThemeBlueColor;
    alertViewPhone.textFieldFont = Dax_Regular(15);
    alertViewPhone.textFieldText = @"";
    alertViewPhone.tintColor = AppThemeColor;
    alertViewPhone.messageFont = Dax_Regular(13);
    alertViewPhone.textField.keyboardType = UIKeyboardTypePhonePad;
    [self setPaddingVwforTextField:alertViewPhone.textField];
    
    
    CDAlertViewAction *action_txt = [[CDAlertViewAction alloc]initWithTitle:kSave font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
        if([alertViewPhone.textFieldText isEqualToString:@""]){
            [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kMobileNo]];
        }else{
            socialMediaUserObj.mobileNumber = alertViewPhone.textField.text.doubleValue;
            socialMediaUserObj.countryCode = userCountryCode.integerValue;
            [self socialLogin:socialMediaUserObj];
        }
        
    }];
    
    
    CDAlertViewAction *action_cancel = [[CDAlertViewAction alloc]initWithTitle:kCancel font:Dax_Regular(15) textColor:[UIColor redColor] backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
        btnPhoneNoAction = nil;
    }];
    
   
    [alertViewPhone addWithAction:action_cancel];
    [alertViewPhone addWithAction:action_txt];
    
    
    [alertViewPhone show:^(CDAlertView *action) {
        
    }];
}

-(void)setPaddingVwforTextField:(UITextField *)txtField{
    
    
    btnPhoneNoAction = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnPhoneNoAction setFrame:CGRectMake(5, 0,35, 30)];
    [btnPhoneNoAction setBackgroundColor:[UIColor clearColor]];
    [btnPhoneNoAction setTitle:[NSString stringWithFormat:@"+%@",userCountryCode] forState:UIControlStateNormal];
    [btnPhoneNoAction setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnPhoneNoAction.titleLabel setFont:Dax_Bold(15)];
    [btnPhoneNoAction addTarget:self action:@selector(showCountryPicker:) forControlEvents:UIControlEventTouchUpInside];
    txtField.leftViewMode = UITextFieldViewModeAlways;
    txtField.leftView = btnPhoneNoAction;
    
}
-(void)showCountryPicker:(id)sender{
    [alertViewPhone endEditing:YES];
    RTCountryPicker *countryPicker = [[RTCountryPicker alloc]initWithFrame:self.view.bounds];
    countryPicker.delegate = self;
    [alertViewPhone addSubview:countryPicker];
}

-(void)socialLogin:(WSUser *)objUser{
    
    if([ReachabilityManager isReachable]){
        
        [self.view endEditing:YES];
        NSInteger socialType = [Functions getIntegerValueFromDefaults:SOCIAL_LOGIN_TYPE];
        NSString *deviceToken = [Functions getStringValueFromDefaults:DEVICE_TOKEN];
        if(deviceToken == nil){
            deviceToken = @"FailToRetrievetoken";
        }

        if([objUser.email isEqualToString:@""] || objUser.email == nil){
            [self showAlert:[NSString stringWithFormat:@"%@",kEmailNotFoundErr]];
            return;
        }
        
        NSDictionary *dictParams= @{
                                    @"fullName":objUser.fullName,
                                    @"email":objUser.email,
                                    @"socialId":objUser.socialId,
                                    @"socialType":[NSNumber numberWithInteger:socialType],
                                    @"deviceType":kDeviceType,
                                    @"deviceToken":deviceToken,
                                    @"latitude":[NSNumber numberWithDouble:objUser.latitude],
                                    @"longitude":[NSNumber numberWithDouble:objUser.longitude],
                                    @"countryCode":userCountryCode,
                                    @"mobileNumber":[NSNumber numberWithDouble:objUser.mobileNumber],
                                    @"language":AppContext.appLanguage
                                    };
        
        [[WebServiceInvocation alloc]initWithWS:kSocialLoginWS withParams:dictParams withServiceType:TYPE_POST withSelector:@selector(socialLoginWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)socialLoginWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
       int status = [[[sender responseDictionary] valueForKey:@"Status"]intValue];
        if(status == 1){
            NSString *msg = [[sender responseDictionary]valueForKey:kServiceResponseMsgKey];
            [UIAlertController showAlertInVC:self withTitle:AppName withMsg:msg withOtherAlertActionTitle:@[kAlertOk] withAlertActionCancel:@"" withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
                if(buttonIndex == 0){
                    VerificationVC *mobilevc = loadViewController(kSBMain,kVerificationVC);
                    mobilevc.mobileNo = [NSString stringWithFormat:@"%.0f",socialMediaUserObj.mobileNumber];
                    [self.navigationController pushViewController:mobilevc animated:YES];
                }
            }];
        
        }else if (status == 2){
            [Functions setBoolValueToUserDefaults:YES withKey:IS_USER_LOGGED_IN];
            [self loadTabBarVC];
        }
    }
}

#pragma mark: -------------------: Instagram
-(void)loginwithInsta{
    
    loginWebView = [[UIWebView alloc]initWithFrame:self.view.frame];
    [Functions setIntegerValueToUserDefaults:LINKEDIN_LOGIN withKey:SOCIAL_LOGIN_TYPE];
    
    NSString* authURL = nil;
    
    if ([typeOfAuthentication isEqualToString:@"UNSIGNED"]){
        authURL = [NSString stringWithFormat: @"%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True",
                   INSTAGRAM_AUTH_URL,
                   INSTAGRAM_CLIENT_ID,
                   INSTAGRAM_REDIRECT_URL,
                   INSTAGRAM_SCOPE];
        
    }
    else{
        authURL = [NSString stringWithFormat: @"%@?client_id=%@&redirect_uri=%@&response_type=code&scope=%@&DEBUG=True",
                   INSTAGRAM_AUTH_URL,
                   INSTAGRAM_CLIENT_ID,
                   INSTAGRAM_REDIRECT_URL,
                   INSTAGRAM_SCOPE];
    }
    
    [self.view addSubview:loginWebView];
    [loginWebView loadRequest: [NSURLRequest requestWithURL: [NSURL URLWithString: authURL]]];
    [loginWebView setDelegate:self];
    
}

#pragma mark: -------------------: Webview Delegates

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType{
    return [self checkRequestForCallbackURL: request];
}

- (void) webViewDidStartLoad:(UIWebView *)webView{
    [loginIndicator startAnimating];
    loadingLabel.hidden = NO;
    [loginWebView.layer removeAllAnimations];
    loginWebView.userInteractionEnabled = NO;
    [UIView animateWithDuration: 0.1 animations:^{
        //  loginWebView.alpha = 0.2;
    }];
}

- (void) webViewDidFinishLoad:(UIWebView *)webView{
    [loginIndicator stopAnimating];
    loadingLabel.hidden = YES;
    [loginWebView.layer removeAllAnimations];
    loginWebView.userInteractionEnabled = YES;
    [UIView animateWithDuration: 0.1 animations:^{
        //loginWebView.alpha = 1.0;
    }];
}

- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [self webViewDidFinishLoad: webView];
}

#pragma mark: -------------------: Instagram Authentication


- (BOOL) checkRequestForCallbackURL: (NSURLRequest*) request{
    NSString* urlString = [[request URL] absoluteString];
    
    if ([typeOfAuthentication isEqualToString:@"UNSIGNED"]){
        // check, if auth was succesfull (check for redirect URL)
        if([urlString hasPrefix: INSTAGRAM_REDIRECT_URL]){
            // extract and handle access token
            NSRange range = [urlString rangeOfString: @"#access_token="];
            [self handleAuth: [urlString substringFromIndex: range.location+range.length]];
            return NO;
        }
    }
    else{
        if([urlString hasPrefix: INSTAGRAM_REDIRECT_URL]){
            // extract and handle access token
            NSRange range = [urlString rangeOfString: @"code="];
            [self makePostRequest:[urlString substringFromIndex: range.location+range.length]];
            return NO;
        }
    }
    return YES;
}

-(void)makePostRequest:(NSString *)code{
    
    NSString *post = [NSString stringWithFormat:@"client_id=%@&client_secret=%@&grant_type=authorization_code&redirect_uri=%@&code=%@",INSTAGRAM_CLIENT_ID,INSTAGRAM_CLIENT_SERCRET,INSTAGRAM_REDIRECT_URL,code];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *requestData = [NSMutableURLRequest requestWithURL:
                                        [NSURL URLWithString:INSTAGRAM_ACCESS_TOKEN_URL]];
    [requestData setHTTPMethod:@"POST"];
    [requestData setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [requestData setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [requestData setHTTPBody:postData];
    

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
        
        NSURLSessionDataTask *task = [session dataTaskWithRequest:requestData completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            [RTProgressHUD dismiss];
            if(error){
                [self showAlert:[error description]];
            }else{
                dispatch_sync(dispatch_get_main_queue(), ^{
                    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                     [self handleAuth:[dict valueForKey:@"access_token"]];
                });
            }
        }];
        [task resume];
    });
    
}

- (void) handleAuth: (NSString*) authToken{
    NSLog(@"successfully logged in with Tocken == %@",authToken);
    
}


@end
