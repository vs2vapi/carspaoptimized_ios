//
//  RTCountryPicker.h
//  CarSpa
//
//  Created by R@j on 26/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RTCountryPickerDelegate <NSObject>

-(void)pickerSelectedCountry:(NSString *)selectedCode;

@end

@interface RTCountryPicker : UIView <UIPickerViewDelegate,UIPickerViewDataSource>

@property (strong, nonatomic) IBOutlet UIView *bgVw;
@property (weak, nonatomic) IBOutlet UIView *vwCountryPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *countryPickerVw;
@property(strong,nonatomic)id<RTCountryPickerDelegate>delegate;

- (IBAction)btnCancelAction:(id)sender;
- (IBAction)btnOkAction:(id)sender;
- (IBAction)bgBtnAction:(id)sender;

@end
