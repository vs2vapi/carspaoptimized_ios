//
//  RTCountryPicker.m
//  CarSpa
//
//  Created by R@j on 26/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "RTCountryPicker.h"

@interface RTCountryPicker (){
    NSArray *arrCountry,*arrPhoneCodes,*arrFlags;
    NSInteger selectedRow;
}

@end

@implementation RTCountryPicker

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if(self){
        [[NSBundle mainBundle]loadNibNamed:@"RTCountryPicker" owner:self options:nil];
        [self addSubview:self.bgVw];
        [self showVw:YES];
        [self setPickerValues];
    }
    
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    self.bgVw.frame = self.frame;
}

#pragma mark :--------------------: Setup UI
-(void)setPickerValues{
    
    selectedRow = 0;
    arrCountry = [[NSArray alloc] initWithObjects:kCountry_Bahrain,kCountry_Kuwait,kCountry_Qatar,kCountry_Saudi,kCountry_UnitedArab,kCountry_Oman,kCountry_Lebanon,nil];
    
    arrPhoneCodes = [[NSArray alloc] initWithObjects:@"973", @"965",@"974",@"966", @"971",@"968",@"961", nil];
    
    arrFlags = [[NSArray alloc] initWithObjects:[UIImage imageNamed:@"ic_BH"],[UIImage imageNamed:@"ic_KW"],[UIImage imageNamed:@"ic_QA"],[UIImage imageNamed:@"ic_SA"],[UIImage imageNamed:@"ic_AE"],[UIImage imageNamed:@"ic_OM"],[UIImage imageNamed:@"ic_LB"], nil];
    
    
}

-(void)showVw:(BOOL)show{
    
    if(show){
        
        [_vwCountryPicker setFrame:CGRectMake(0,self.frame.size.height, _vwCountryPicker.frame.size.width, _vwCountryPicker.frame.size.height)];
        [UIView animateWithDuration:0.5f delay:0.0f usingSpringWithDamping:0.7f initialSpringVelocity:.5f options:UIViewAnimationOptionCurveEaseOut animations:^{
            [_vwCountryPicker setFrame:CGRectMake(0,self.frame.size.height - _vwCountryPicker.frame.size.height, _vwCountryPicker.frame.size.width, _vwCountryPicker.frame.size.height)];
        } completion:^(BOOL finished) {
            
        }];
    }else{
        
        [UIView animateWithDuration:0.3f animations:^{
            [_vwCountryPicker setFrame:CGRectMake(0,self.frame.size.height, _vwCountryPicker.frame.size.width, _vwCountryPicker.frame.size.height)];
        } completion:^(BOOL finished) {
            [self.bgVw removeFromSuperview];
            [self removeFromSuperview];
        }];
    }
    
}

#pragma mark :-------------------: PickerView Delegates & Datasource
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
{
    return [arrCountry count];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    if (!view){
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 30)];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(35, 3, 245, 24)];
        label.backgroundColor = [UIColor clearColor];
        label.tag = 1;
        label.font = Dax_Regular(15);
        [view addSubview:label];
        
        UIImageView *flagView = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, 24, 24)];
        flagView.contentMode = UIViewContentModeScaleAspectFit;
        flagView.tag = 2;
        [view addSubview:flagView];
    }
    
    ((UILabel *)[view viewWithTag:1]).text = [NSString stringWithFormat:@"+%@ %@",[arrPhoneCodes objectAtIndex:row],[arrCountry objectAtIndex:row]];
    ((UIImageView *)[view viewWithTag:2]).image = [arrFlags objectAtIndex:row];
    return view;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    selectedRow = row;
    
}


#pragma mark :--------------------: Button Actions
- (IBAction)bgBtnAction:(id)sender {
     [self showVw:NO];
}
- (IBAction)btnCancelAction:(id)sender {
     [self showVw:NO];
}

- (IBAction)btnOkAction:(id)sender {
  
    NSString *phoneCode = [arrPhoneCodes objectAtIndex:selectedRow];
    [self.delegate pickerSelectedCountry:phoneCode];
    [self showVw:NO];
}
@end
