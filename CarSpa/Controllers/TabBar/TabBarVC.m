//
//  TabBarVC.m
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "TabBarVC.h"

@interface TabBarVC ()

@end

@implementation TabBarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       AppThemeBlueColor, NSForegroundColorAttributeName,
                                                       [UIFont fontWithName:@"dax-regular" size:12],
                                                       NSFontAttributeName,
                                                       nil] forState:UIControlStateNormal];
    
    [[UITabBarItem appearance]  setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       AppThemeColor, NSForegroundColorAttributeName,
                                                       [UIFont fontWithName:@"dax-regular" size:12], NSFontAttributeName,
                                                       nil] forState:UIControlStateSelected];
    
    if ([self.tabBar respondsToSelector:@selector(setUnselectedItemTintColor:)]) {
        [self.tabBar setUnselectedItemTintColor:AppThemeBlueColor];
    }

    self.tabBar.tintColor = AppThemeColor;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    
//    CATransition* transition = [CATransition animation];
//    transition.duration = 0.25;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionFade; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
//    transition.subtype =kCATransitionFromTop;// kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
//    [self.view.window.layer addAnimation:transition forKey:nil];

}

@end
