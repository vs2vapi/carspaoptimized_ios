//
//  HistoryVC.m
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "HistoryVC.h"
#import "RequestCell.h"
#import "RatingVC.h"
#import "RTTableLoader.h"
#import "WSJobRequests.h"
#import "WSBill.h"
#import "WSVan.h"
#import "WSCartype.h"
#import "WSServices.h"
#import "WSAppointmentStatus.h"
#import "WSCategory.h"

@interface HistoryVC ()<RTDatePickerDelegate>{
     NSDate *startDate,*endDate;
    BOOL isStartDate,loadMore,isSearch;
    NSInteger pageIndex;
    RTTableLoader *objTableLoader;
    NSString *strStartDate,*strEndDate;
}
@end

@implementation HistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(![self isUserLoggedIn]){
        [self showAlertForLogin];
        [self.vwDateSearch setHidden:YES];
    }else{
          [self setupUI];
    }
  
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark:--------------------: Basic Setups

-(void)setupUI{
    
    pageIndex = 0;
    _arrHistory = [NSMutableArray new];
    
    _tblVwHistory.separatorColor = [UIColor clearColor];
    objTableLoader = [[RTTableLoader alloc]initWithFrame:_tblVwHistory.frame];
    [objTableLoader setLoaderMsg:@""];
    _tblVwHistory.tableFooterView = objTableLoader;
    [objTableLoader stopIndicator];
    
     _btnStartDate.layer.cornerRadius = 4.0f;
    _btnEndDate.layer.cornerRadius = 4.0f;
    
    startDate = [self getFirstDateOfCurrentMonth];
    endDate = [NSDate date];
    [self btnSearchAction:nil];
    [self setButtonDate];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadDetails) name:RATING_ADDED object:nil];
}

-(void)reloadDetails{
  [self btnSearchAction:nil];
}

-(void)setButtonDate{

    [_btnStartDate setTitle:strStartDate forState:UIControlStateNormal];
    [_btnEndDate setTitle:strEndDate forState:UIControlStateNormal];
}

- (NSDate *)getFirstDateOfCurrentMonth{
    NSCalendar *calender = [NSCalendar currentCalendar];
    NSDateComponents *currentDateComponent = [calender components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:[NSDate date]];
    NSDate *firstDate = [calender dateFromComponents:currentDateComponent];
    return firstDate;
}

-(NSDateFormatter *)dateFormatter{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    return dateFormat;
}

-(void)fetchListOfRequest:(NSString *)fromDate withTodate:(NSString *)toDate withLoader:(BOOL)showloader withPageIndex:(NSInteger )index{
    

    if([ReachabilityManager isReachable]){
        
        if(!showloader){
            [objTableLoader startIndicator];
        }
        
        NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
        NSString *url = [NSString stringWithFormat:@"%@?userId=%@&fromDate=%@&toDate=%@&pageIndex=%lu&language=%@&%@=%@",kFetchHistoryWS,userId,fromDate,toDate,index,AppContext.appLanguage,kAppVersionServiceKey,[self fetchAppVersion]];

        [[WebServiceInvocation alloc]initWithWS:url withParams:nil withServiceType:TYPE_GET withSelector:@selector(listRequestWSAction:) withTarget:self showLoader:showloader loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }

}

-(void)listRequestWSAction:(id)sender{
    
    [objTableLoader stopIndicator];
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        if(isSearch){
              [self showAlert:[sender responseErr]];
        }
    }else{
        
        [[sender responseArray]enumerateObjectsUsingBlock:^(WSJobRequests *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [_arrHistory addObject:obj];
        }];
        
        if(_arrHistory.count > 0){
            pageIndex = [[[sender responseDictionary]valueForKey:@"CurrentPage"]integerValue];
            loadMore = (pageIndex > 0) ? YES : NO;
        }else{
            loadMore = NO;
        }
        
        [_tblVwHistory reloadData];
        
    }
}

#pragma mark:--------------------: Tableview Datasource & Delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrHistory.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    WSJobRequests *objRequest = [_arrHistory objectAtIndex:indexPath.row];
    if(objRequest.status == 3){
        if(objRequest.rate ==nil || objRequest.rate.count == 0){
            return 270;
        }
    }
    return 240;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static  NSString *cellIdentifier = @"Cell";
    RequestCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"RequestCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    WSJobRequests *objRequest = [_arrHistory objectAtIndex:indexPath.row];
    cell.lblRequestNo.text = [NSString stringWithFormat:@"%@ %@",kRequest,objRequest.suffix];
    cell.lblTime.text = objRequest.appointmentDatetime;
    [cell.lblTapHere setHidden:YES];
    
    WSBill *objBill = objRequest.bill;
    NSString  *strCurrency =  [NSString stringWithFormat:@"%@",objBill.currency];
    cell.lblAmount.text = [NSString stringWithFormat:@"%.2f %@",objBill.totalAmount,strCurrency];
    
    WSCartype *objCarType = objRequest.carType;
    if(objCarType==nil){
        cell.lblCarType.text = kNotProvided;
    }else{
        cell.lblCarType.text = objCarType.name;
    }
    
    __block NSString *strServices = @"";
    [objRequest.services enumerateObjectsUsingBlock:^(WSServices *objService, NSUInteger idx, BOOL * _Nonnull stop) {
        if(idx == 0){
            strServices = [strServices stringByAppendingString:[NSString stringWithFormat:@"%@",objService.name]];
        }else{
            strServices = [strServices stringByAppendingString:[NSString stringWithFormat:@", %@",objService.name]];
        }
        
    }];
    cell.lblService.text = strServices;
    
    WSAppointmentStatus *objAppointment = [objRequest.appointmentStatus objectAtIndex:0];
    cell.lblTime.text = objAppointment.datetime;
    cell.lblAppointmentTime.text = objRequest.appointmentDatetime;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [cell setRequestStatus:objAppointment.status];
    });
    
    
    WSCategory *objCategory  = objRequest.category;
    cell.lblCategory.text = objCategory.name;
    
    if(objRequest.status == 3){
        if(objRequest.rate ==nil || objRequest.rate.count == 0){
            [cell.btnRateMe setHidden:NO];
            cell.btnRateHeightConst.constant = 30;
        }else{
            [cell.btnRateMe setHidden:YES];
            cell.btnRateHeightConst.constant = 0;

        }
    }else{
        [cell.btnRateMe setHidden:YES];
        cell.btnRateHeightConst.constant = 0;
    }
   
    isSearch = NO;
    
    return cell;
}



- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if((indexPath.row ==( _arrHistory.count - 1) && (loadMore))){
        [self fetchListOfRequest:strStartDate withTodate:strEndDate withLoader:NO withPageIndex:pageIndex];
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    WSJobRequests *objRequest = [_arrHistory objectAtIndex:indexPath.row];
    if(objRequest.status == 3){
        if(objRequest.rate ==nil || objRequest.rate.count == 0){
            [self moveToRatingVC:[NSString stringWithFormat:@"%lu",objRequest.internalBaseClassIdentifier]];
        }
    }
    
   
}

#pragma mark:--------------------: Button Actions
- (IBAction)btnStartDateAction:(id)sender {
    isStartDate = YES;
    [self showPickerVw];
}

- (IBAction)btnEndDateAction:(id)sender {
    isStartDate = NO;
    [self showPickerVw];
}

- (IBAction)btnSearchAction:(id)sender {
 
    if(sender == nil){
        isSearch =NO;
    }else{
        isSearch = YES;
        [_arrHistory removeAllObjects];
        [_tblVwHistory reloadData];
    }
   
    strStartDate  = [NSString stringWithFormat:@"%@",[[self dateFormatter]stringFromDate:startDate]];
    strEndDate  = [NSString stringWithFormat:@"%@",[[self dateFormatter]stringFromDate:endDate]];
    pageIndex = 0;
    [_arrHistory removeAllObjects];
    [self fetchListOfRequest:strStartDate withTodate:strEndDate withLoader:YES withPageIndex:pageIndex];
}

-(void)moveToRatingVC:(NSString *)appointmentId{
    RatingVC *rvc = loadViewController(kSBRequest, kRatingVC);
    rvc.appointmentId = appointmentId;
    [self.navigationController pushViewController:rvc animated:YES];
}

#pragma mark:--------------------: Pickerview Functions
-(void)showPickerVw{
    
    RTDatePickerVC *rtdpvc = [[RTDatePickerVC alloc]initWithFrame:self.view.bounds];
    rtdpvc.delegate = self;
//    [rtdpvc setMaxDate:[NSDate date]];
    [rtdpvc setDtPickerMode:DateMode];
    [rtdpvc setSelectedDate:(isStartDate)?startDate : endDate];
   [self.tabBarController.view addSubview:rtdpvc];
}

-(void)pickerSelectedDate:(NSDate *)selectedDate{
    
    NSLog(@"selected date: %@",selectedDate);
    if(isStartDate){
        startDate = selectedDate;
        strStartDate  = [NSString stringWithFormat:@"%@",[[self dateFormatter]stringFromDate:startDate]];
    }else{
        endDate = selectedDate;
        strEndDate  = [NSString stringWithFormat:@"%@",[[self dateFormatter]stringFromDate:endDate]];
    }
    [self setButtonDate];
}
@end
