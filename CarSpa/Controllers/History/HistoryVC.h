//
//  HistoryVC.h
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryVC : BaseVC <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblVwHistory;
@property (weak, nonatomic) IBOutlet UIButton *btnStartDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEndDate;
@property (weak, nonatomic) IBOutlet UIView *vwDateSearch;

@property (strong, nonatomic) NSMutableArray *arrHistory;

- (IBAction)btnStartDateAction:(id)sender;
- (IBAction)btnEndDateAction:(id)sender;
- (IBAction)btnSearchAction:(id)sender;

@end
