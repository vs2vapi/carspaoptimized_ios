//
//  ChangePasswordVC.m
//  CarSpa
//
//  Created by R@j on 25/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "ChangePasswordVC.h"

@interface ChangePasswordVC ()

@end

@implementation ChangePasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _btnChangePswd.layer.cornerRadius = 6.0f;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}

-(BOOL)isAllFieldValid{
    if([_txtCurrentPswd.text isEqualToString:@""]){
        [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kOldPswd]];
        return NO;
    }
  else  if([_txtNewPswd.text isEqualToString:@""]){
        [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kNewPswd]];
        return NO;
    }
   else if([_txtConfirmPswd.text isEqualToString:@""]){
       
       [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kConfirmPswd]];
        return NO;
    }
    if(![_txtNewPswd.text isEqualToString:_txtConfirmPswd.text]){
        [self showAlert:[NSString stringWithFormat:@"%@",kPasswordNotMatched]];
        return NO;
    }
    return YES;
}
#pragma mark: -------------------: Button Actions
- (IBAction)btnChangePswdAction:(id)sender {
    
    [self.view endEditing:YES];
    if([self isAllFieldValid]){
        if([ReachabilityManager isReachable]){
            
            NSString *oldPswd = _txtCurrentPswd.text;
            NSString *newPswd = _txtNewPswd.text;
            NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
            
            NSDictionary *dictParams = @{@"userId" : userId,
                                                            @"newPassword":newPswd,
                                                            @"oldPassword":oldPswd,
                                                            @"language":AppContext.appLanguage
                                                            };

            [[WebServiceInvocation alloc]initWithWS:kChangePasswordWS withParams:dictParams withServiceType:TYPE_POST withSelector:@selector(passwordWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];

        }else{
            ShowNoNetworkAlert(self);
        }
    }
}

-(void)passwordWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
    
        NSString *msg = [[sender responseDictionary]valueForKey:kServiceResponseMsgKey];
        [UIAlertController showAlertInVC:self withTitle:AppName withMsg:msg withOtherAlertActionTitle:@[kAlertOk] withAlertActionCancel:@"" withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
            if(buttonIndex == 0){
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }
}
@end
