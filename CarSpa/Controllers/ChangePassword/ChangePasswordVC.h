//
//  ChangePasswordVC.h
//  CarSpa
//
//  Created by R@j on 25/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordVC : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *txtCurrentPswd;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPswd;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPswd;
@property (weak, nonatomic) IBOutlet UIButton *btnChangePswd;

- (IBAction)btnChangePswdAction:(id)sender;

@end
