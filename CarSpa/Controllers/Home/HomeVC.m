//
//  HomeVC.m
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "HomeVC.h"
#import "WSNearByVans.h"

@interface HomeVC (){
    BOOL isFirstTime;
    NSMutableArray *arrNearByVans;
}

@end

@implementation HomeVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    isFirstTime = YES;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateLocation) name:UPDATE_LOCATION object:nil];
    [self setUserLocation];
    [self fetchNearByVansWithLoader:YES];
    [self startTimer];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark: -------------------: Map Functions
-(void)updateLocation{
    [self setUserLocation];
    [self fetchNearByVansWithLoader:NO];
}

-(void)fetchNearByVansWithLoader:(BOOL)showloader{
    
    if([ReachabilityManager isReachable]){
        
        double latitude = [Functions getDoubleValueFromDefaults:USER_LATITUDE];
        double longitude = [Functions getDoubleValueFromDefaults:USER_LONGITUDE];
        
        NSDictionary *dictParam = @{@"latitude":[NSNumber numberWithDouble:latitude],
                                                        @"longitude":[NSNumber numberWithDouble:longitude]};
        
        [[WebServiceInvocation alloc]initWithWS:kFetchNearByVansWS withParams:dictParam withServiceType:TYPE_POST withSelector:@selector(nearByVanAction:) withTarget:self showLoader:showloader loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)nearByVanAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
       // [self showAlert:[sender responseErr]];
    }else{
        arrNearByVans = [NSMutableArray arrayWithArray:[sender responseArray]];
        [self showAnnotation];
    }
    
}

- (void)showAnnotation{
    
     [self setUserLocation];
    if (arrNearByVans.count > 0) {
        
        [arrNearByVans enumerateObjectsUsingBlock:^(WSNearByVans *objVans, NSUInteger idx, BOOL * _Nonnull stop) {
            
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake(objVans.latitude, objVans.longitude);
            [marker setAppearAnimation:kGMSMarkerAnimationPop];
            marker.title =  objVans.name;
            marker.icon = [UIImage imageNamed:@"icn_green_van"];
            marker.map = _googleMapVw;

        }];
    }
    
}

-(void)setUserLocation{
    double latitude = [Functions getDoubleValueFromDefaults:USER_LATITUDE];
    double longitude = [Functions getDoubleValueFromDefaults:USER_LONGITUDE];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
    [self addMarkerOnMap:loc];
}

-(void)addMarkerOnMap:(CLLocation *)location{
    
    [_googleMapVw clear];
    if(isFirstTime){
        isFirstTime = NO;
        GMSCameraPosition *myLocation = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                                                    longitude:location.coordinate.longitude
                                                                         zoom:12
                                         ];
        
        
        [_googleMapVw setCamera:myLocation];
    }

    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
    [marker setAppearAnimation:kGMSMarkerAnimationPop];
    marker.title = kDestination;
    marker.snippet = kDestination;
    marker.icon = [UIImage imageNamed:@"icn_green_flag"];
    marker.map = _googleMapVw;
}

#pragma mark: -------------------: Timer Functions
-(void)startTimer{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        AppContext.fetchVanTimer = [NSTimer scheduledTimerWithTimeInterval:120 target:self selector:@selector(fetchVans) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer: AppContext.fetchVanTimer forMode:NSDefaultRunLoopMode];
        [[NSRunLoop currentRunLoop] run];
        
    });
    
}

-(void)fetchVans{
    [self fetchNearByVansWithLoader:NO];
}

#pragma mark: -------------------: Button Actions
- (IBAction)btnMyLocationAction:(id)sender {
    isFirstTime = YES;
    [self setUserLocation];
    [self fetchNearByVansWithLoader:NO];
}

- (IBAction)btnMapNormalVwAction:(id)sender {
    [_btnMapTypeNormal setSelected:YES];
    [_btnMapTypeHybrid setSelected:NO];
    _googleMapVw.mapType = kGMSTypeNormal;
}

- (IBAction)btnMapHybridVwAction:(id)sender {
    [_btnMapTypeNormal setSelected:NO];
    [_btnMapTypeHybrid setSelected:YES];
    _googleMapVw.mapType = kGMSTypeHybrid;
}

@end
