//
//  HomeVC.h
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeVC : BaseVC

@property (weak, nonatomic) IBOutlet UIView *mapContainverVw;
@property (weak, nonatomic) IBOutlet GMSMapView *googleMapVw;
@property (weak, nonatomic) IBOutlet UIButton *btnMyLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnMapTypeNormal;
@property (weak, nonatomic) IBOutlet UIButton *btnMapTypeHybrid;


- (IBAction)btnMyLocationAction:(id)sender;
- (IBAction)btnMapNormalVwAction:(id)sender;
- (IBAction)btnMapHybridVwAction:(id)sender;

@end
