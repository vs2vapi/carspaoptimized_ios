//
//  MapRouteVC.m
//  CarSpa
//
//  Created by R@j on 25/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "MapRouteVC.h"

@interface MapRouteVC (){
     GMSMarker *vanMarker;
    GMSPolyline *polylinePath;
}

@end

@implementation MapRouteVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addMarkerOnMap];
    [self fetchDistanceFromTwoLocation];
    [self drawRoute];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addMarkerOnMap{
    
    
    vanMarker = [[GMSMarker alloc] init];
    vanMarker.position = CLLocationCoordinate2DMake(_vanLocation.coordinate.latitude, _vanLocation.coordinate.longitude);
    [vanMarker setAppearAnimation:kGMSMarkerAnimationPop];
    vanMarker.icon = [UIImage imageNamed:@"icn_green_van"];
    vanMarker.title = kServiceProviderLocation;
    vanMarker.snippet = kServiceProviderLocation;
    vanMarker.map = _googleMapVw;
    [_googleMapVw setSelectedMarker:vanMarker];
    
    
    GMSMarker *userMarker = [[GMSMarker alloc] init];
    userMarker.position = CLLocationCoordinate2DMake(_userLocation.coordinate.latitude, _userLocation.coordinate.longitude);
    [userMarker setAppearAnimation:kGMSMarkerAnimationPop];
    //    marker.icon = [UIImage imageNamed:@"ic_location_green"];
    userMarker.title = kDestination;
    userMarker.snippet = kDestination;
    userMarker.icon = [UIImage imageNamed:@"icn_green_flag"];
    
    userMarker.map = _googleMapVw;
    
    GMSCameraPosition *userCamPosition = [GMSCameraPosition cameraWithLatitude:_vanLocation.coordinate.latitude
                                                                     longitude:_vanLocation.coordinate.longitude
                                                                          zoom:17
                                          ];
    
    
    [_googleMapVw setCamera:userCamPosition];
    
    
}

-(void)fetchDistanceFromTwoLocation{
    
    if([ReachabilityManager isReachable]){
        
        
    NSString * strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f&mode=driving&language=pl-PL",_vanLocation.coordinate.latitude,_vanLocation.coordinate.longitude,_userLocation.coordinate.latitude,_userLocation.coordinate.longitude];
        
        [[WebServiceInvocation alloc]initWithWS:strUrl withParams:nil withServiceType:TYPE_GET withSelector:@selector(distanceWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
    }else{
        ShowNoNetworkAlert(self);
    }
    
}

-(void)distanceWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        @try {
            NSString *status = [NSString stringWithFormat:@"%@",[[[[[[sender responseDictionary] valueForKey:@"rows"] objectAtIndex:0] valueForKey:@"elements"] objectAtIndex:0] valueForKey:@"status"] ];
            if([status isEqualToString:@"ZERO_RESULTS"]){
                
            }else{
                
                NSString *strDuration = [NSString stringWithFormat:@"%@",[[[[[[[sender responseDictionary] valueForKey:@"rows"] objectAtIndex:0] valueForKey:@"elements"] objectAtIndex:0] valueForKey:@"duration"] valueForKey:@"text"]];
                
                NSString *distance = [NSString stringWithFormat:@"%@",[[[[[[[sender responseDictionary] valueForKey:@"rows"] objectAtIndex:0] valueForKey:@"elements"] objectAtIndex:0] valueForKey:@"distance"] valueForKey:@"text"]];
                
                vanMarker.snippet = [NSString stringWithFormat:@"%@ (%@)",distance,strDuration];
                
            }

        } @catch (NSException *exception) {
            
        } @finally {
            
        }
      
    }
}

- (void)drawRoute{
    
    CLLocation *userlocation = [[CLLocation alloc]initWithLatitude:_userLocation.coordinate.latitude longitude:_userLocation.coordinate.longitude];
    CLLocation *serviceLocation = [[CLLocation alloc]initWithLatitude:_vanLocation.coordinate.latitude longitude:_vanLocation.coordinate.longitude];
    
    NSString *originString = [NSString stringWithFormat:@"%f,%f", serviceLocation.coordinate.latitude, serviceLocation.coordinate.longitude];
    NSString *destinationString = [NSString stringWithFormat:@"%f,%f", userlocation.coordinate.latitude, userlocation.coordinate.longitude];
    NSString *directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
    NSString *directionsUrlString = [NSString stringWithFormat:@"%@&origin=%@&destination=%@&mode=driving", directionsAPI, originString, destinationString];
    
    if([ReachabilityManager isReachable]){
        
        [[WebServiceInvocation alloc]initWithWS:directionsUrlString withParams:nil withServiceType:TYPE_GET withSelector:@selector(directionWSAction:) withTarget:self showLoader:NO loaderMsg:kWSLoaderDialoagMsg];
        
        
    }else{
        ShowNoNetworkAlert(self);
    }

    
}

-(void)directionWSAction:(id)sender{
    
   
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        
        NSArray *routesArray = [[sender responseDictionary] objectForKey:@"routes"];
        polylinePath = nil;
        if ([routesArray count] > 0){
            
            NSDictionary *routeDict = [routesArray objectAtIndex:0];
            NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
            NSString *points = [routeOverviewPolyline objectForKey:@"points"];
            dispatch_async(dispatch_get_main_queue(), ^{
                GMSPath *path = [GMSPath pathFromEncodedPath:points];
                polylinePath = [GMSPolyline polylineWithPath:path];
                polylinePath.strokeWidth = 6.0f;
                polylinePath.strokeColor = AppThemeBlueColor;
                polylinePath.map = _googleMapVw;
            });
         

        }

    }
    
}

@end
