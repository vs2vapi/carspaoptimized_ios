//
//  MapRouteVC.h
//  CarSpa
//
//  Created by R@j on 25/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapRouteVC : UIViewController
@property (weak, nonatomic) IBOutlet GMSMapView *googleMapVw;
@property (strong, nonatomic) CLLocation *vanLocation;
@property (strong, nonatomic) CLLocation *userLocation;

@end
