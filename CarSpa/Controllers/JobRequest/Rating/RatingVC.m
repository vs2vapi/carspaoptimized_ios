//
//  RatingVC.m
//  CarSpa
//
//  Created by R@j on 25/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "RatingVC.h"
#import "RatingCell.h"
#import "WSRating.h"

#define kMaxTextLength  1000
@interface RatingVC (){
      NSInteger textLength;
    NSMutableArray *arrRateMeValue;
     NSMutableDictionary *dictRating;
    BOOL isRatingAdded;
}

@end

@implementation RatingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self fetchRating];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}

#pragma mark:--------------------: Basic Setups
-(void)setupUI{
    _tblVwRating.tableFooterView = [UIView new];
    _tblVwRating.separatorColor = [UIColor clearColor];
    _txtVwRating.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _txtVwRating.layer.borderWidth = 1.0f;
    _txtVwRating.layer.cornerRadius = 6.0f;
    _btnSubmit.layer.cornerRadius = 6.0f;
}

-(void)fetchRating{
    
    
    if([ReachabilityManager isReachable]){
        NSString *url = [NSString stringWithFormat:@"%@?%@=%@",kRatingWS,kAppVersionServiceKey,[self fetchAppVersion]];
        [[WebServiceInvocation alloc]initWithWS:url withParams:nil withServiceType:TYPE_GET withSelector:@selector(ratingWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
    }else{
        ShowNoNetworkAlert(self);
    }

}

-(void)ratingWSAction:(id)sender{
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        
      
        _arrRating = [NSMutableArray arrayWithArray:[sender responseArray]];
        arrRateMeValue = [NSMutableArray new];
         dictRating = [NSMutableDictionary new];
        [_arrRating enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [arrRateMeValue addObject:[NSNumber numberWithFloat:0]];
        }];
        [_tblVwRating reloadData];
    }
}

#pragma mark:--------------------: Tableview Datasource & Delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrRating.count;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static  NSString *cellIdentifier = @"Cell";
    RatingCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        cell = [[RatingCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    WSRating *objRating = [_arrRating objectAtIndex:indexPath.row];
    
    NSString *nameKey = objRating.question;
    if(![self isEnglishLanguage]){
       nameKey = objRating.questionAR;
        cell.ratingVw.transform = CGAffineTransformMakeScale(-1, 1);
    }

    cell.lblTitle.text =  [NSString stringWithFormat:@"%lu. %@",indexPath.row+1,nameKey];
    [cell.ratingVw addTarget:self action:@selector(btnRatingClick:event:) forControlEvents:UIControlEventTouchUpInside];
    cell.ratingVw.value = [[arrRateMeValue objectAtIndex:indexPath.row]floatValue];
    

    return cell;
}


-(void)btnRatingClick:(id)sender event:(id)event{
    
    isRatingAdded = YES;
    UITouch *touch = [event allTouches].anyObject;
    CGPoint location = [touch locationInView:_tblVwRating];
    NSIndexPath *indexPath = [_tblVwRating indexPathForRowAtPoint:location];
    
    WSRating *objRating = [_arrRating objectAtIndex:indexPath.row];
    RatingCell *cell = [_tblVwRating cellForRowAtIndexPath:indexPath];
    
    NSString *ratingId = [NSString stringWithFormat:@"%.0f",objRating.internalBaseClassIdentifier];
    [dictRating setObject:[NSString stringWithFormat:@"%.0f",cell.ratingVw.value] forKey:ratingId];
    [arrRateMeValue replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithFloat:cell.ratingVw.value]];
    [_tblVwRating reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark :-------------------: Textview Delegates
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if(range.location == kMaxTextLength){
        return NO;
    }else{
        textLength =   range.location + 1;
        if([text isEqualToString:@""] && textLength == 1){
            textLength = 0;
        }
        _lblCounter.text = [NSString stringWithFormat:@"%lu / 1000",textLength];
    }
    return YES;
}

#pragma mark:--------------------: Button Actions
- (IBAction)btnSubmitAction:(id)sender {
    
    [self.view endEditing:YES];
    if(isRatingAdded){

        if([ReachabilityManager isReachable]){
            
            NSString *strUserId = [Functions getStringValueFromDefaults:USER_ID];
            NSDictionary *dictParms = @{@"userId":strUserId,
                                        @"appointmentId":self.appointmentId,
                                        @"rateDetail":[self starRating],
                                        @"comment":_txtVwRating.text};
            NSLog(@"Dict : %@",dictParms);
            [[WebServiceInvocation alloc]initWithWS:kAddRatingWS withParams:dictParms withServiceType:TYPE_POST withSelector:@selector(addRatingWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        }else{
            ShowNoNetworkAlert(self);
        }
        
    }else{
        [self showAlert:kPlsAddRating];
    }
}

-(void)addRatingWSAction:(id)sender{
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
    
        [[NSNotificationCenter defaultCenter]postNotificationName:RATING_ADDED object:nil];
        [UIAlertController showAlertInVC:self withTitle:AppName withMsg:kRatingSuccess withOtherAlertActionTitle:@[kAlertOk] withAlertActionCancel:@"" withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
            if(buttonIndex == 0){
                if(_fromDetailScreen){
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }else{
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
        }];
    }
}

- (NSString *) starRating{
    
    __block NSString *strStartRating = @"";
    [dictRating enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        strStartRating = [strStartRating stringByAppendingString:[NSString stringWithFormat:@"%@:%@,",key,obj]];
        
    }];
    NSRange range = NSMakeRange(strStartRating.length-1,1);
    strStartRating = [strStartRating stringByReplacingCharactersInRange:range withString:@""];
    return strStartRating;
}
@end
