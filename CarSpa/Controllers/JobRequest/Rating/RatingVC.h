//
//  RatingVC.h
//  CarSpa
//
//  Created by R@j on 25/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RatingVC : UIViewController <UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblVwRating;
@property (weak, nonatomic) IBOutlet UITextView *txtVwRating;
@property (weak, nonatomic) IBOutlet UILabel *lblCounter;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic) NSMutableArray *arrRating;
@property (strong, nonatomic) NSString * appointmentId;
@property(nonatomic,assign) BOOL fromDetailScreen;

- (IBAction)btnSubmitAction:(id)sender;

@end
