//
//  JobDetailsVC.h
//  CarSpa
//
//  Created by R@j on 24/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSJobRequests.h"

@interface EditRequestObj : NSObject
@property(strong,nonatomic)WSCar *car;
@property(strong,nonatomic)WSAddress *address;
@end

@interface JobDetailsVC : UIViewController <UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *btnVanLocation;
@property (weak, nonatomic) IBOutlet UITableView *tblVwDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnRateMe;
@property (strong, nonatomic) WSJobRequests *objRequestDetail;

- (IBAction)btnVanLocationAction:(id)sender;
- (IBAction)btnRateMeAction:(id)sender;

@end
