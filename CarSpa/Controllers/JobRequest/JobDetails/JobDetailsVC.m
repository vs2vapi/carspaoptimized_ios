//
//  JobDetailsVC.m
//  CarSpa
//
//  Created by R@j on 24/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "JobDetailsVC.h"
#import "CarSpa-Swift.h"
#import "RequestDetailCell.h"
#import "RequestStatusCell.h"
#import "RequestOperatorCell.h"
#import "MapRouteVC.h"
#import "RatingVC.h"
#import "VehicleListVC.h"
#import "AppointmentsListVC.h"
#import "RTDatePickerVC.h"

#import "WSBill.h"
#import "WSVan.h"
#import "WSCartype.h"
#import "WSServices.h"
#import "WSAppointmentStatus.h"
#import "WSCategory.h"
#import "WSCar.h"
#import "WSAddress.h"
#import "WSOperator.h"
#import "WSHolidayList.h"
#import "MapVC.h"
#import "WSOwner.h"

#define kDetailSectionHeight 692
#define kOperatorSectionHeight 220
#define kStatusSectionHeight 250


@implementation EditRequestObj

-(instancetype)init{
    if(self = [super init]){
        _car = [WSCar new];
        _address = [WSAddress new];
    }
    return self;
}

@end

@interface JobDetailsVC ()<AppointmentsListVCDelegate,RTDatePickerDelegate>{
    BOOL isEditRequestMode;
    EditRequestObj *objEditRequest;
    __block NSInteger serviceDuration;
       __block NSString *strServiceId;
}

@end

@implementation JobDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setEditOptions];
    objEditRequest = [[EditRequestObj alloc]init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark:--------------------: Basic Setups
-(void)setupUI{
    
    [self.tabBarController.tabBar setHidden:YES];
    _tblVwDetails.separatorColor = [UIColor clearColor];
    _tblVwDetails.tableFooterView  = [UIView new];
    _btnVanLocation.layer.cornerRadius = 6.0f;
    _btnRateMe.layer.cornerRadius = 6.0f;
    isEditRequestMode = NO;
    self.navigationItem.title = [NSString stringWithFormat:@"%@ %@",kRequest,_objRequestDetail.suffix];
    
    //Adding Edit Appointment Observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationReceived:) name:NOTIFICATION_RECEIVED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(carChanged:) name:EDIT_REQUEST_CAR_CHANGED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addressChanged:) name:EDIT_REQUEST_ADDRESS_CHANGED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(towAddressChanged:) name:EDIT_REQUEST_TOW_ADDRESS_CHANGED object:nil];
    
}

-(void)setEditOptions{
    
    if( _objRequestDetail.status == STATUS_BOOKED){
        
        UIView *customVw = [[UIView alloc]initWithFrame:CGRectMake(0, 0,80, 40)];
        customVw.backgroundColor = [UIColor clearColor];
        
        UIButton *btnEdit = [UIButton buttonWithType:UIButtonTypeCustom];
        btnEdit.frame = CGRectMake(0, 0,40, 40);
        [btnEdit setImage:[UIImage imageNamed:@"icn_job_edit"] forState:UIControlStateNormal];
        btnEdit.imageEdgeInsets = UIEdgeInsetsMake(-20, 0, 0, 0);
        [btnEdit addTarget:self action:@selector(jobEditAction:) forControlEvents:UIControlEventTouchUpInside];
        [customVw addSubview:btnEdit];
        
        UILabel *lblEdit = [[UILabel alloc]initWithFrame:CGRectMake(0,21,40, 21)];
        lblEdit.text = kEdit;
        lblEdit.textColor = [UIColor whiteColor];
        lblEdit.textAlignment = NSTextAlignmentCenter;
        lblEdit.font = Dax_Regular(13);
        [customVw addSubview:lblEdit];
        
        UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCancel.frame = CGRectMake(40,0,40, 40);
        [btnCancel setImage:[UIImage imageNamed:@"icn_job_cancel"] forState:UIControlStateNormal];
        btnCancel.imageEdgeInsets = UIEdgeInsetsMake(-20, 0, 0, 0);
        [btnCancel addTarget:self action:@selector(jobCancelAction:) forControlEvents:UIControlEventTouchUpInside];
        [customVw addSubview:btnCancel];
        
        UILabel *lblCancel = [[UILabel alloc]initWithFrame:CGRectMake(40,20,40, 21)];
        lblCancel.text = kCancel;
        lblCancel.textColor = [UIColor whiteColor];
        lblCancel.textAlignment = NSTextAlignmentCenter;
        lblCancel.font = Dax_Regular(13);
        [customVw addSubview:lblCancel];
        
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc]initWithCustomView:customVw];
        self.navigationItem.rightBarButtonItem = rightBarButton;
        
       /* UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"icn_job_edit"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                                        style:UIBarButtonItemStylePlain
                                                                       target:self
                                                                       action:@selector(btnEditAction:)];
        
        UIBarButtonItem *rightButton1 = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"icn_job_cancel"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                                        style:UIBarButtonItemStylePlain
                                                                       target:self
                                                                       action:@selector(btnEditAction:)];
        
        
        self.navigationItem.rightBarButtonItems = @[rightButton1,rightButton];
        */
    }
}

-(void)jobEditAction:(id)sender{
    isEditRequestMode = YES;
    //Assigning edit request object with current value
    WSCar *objCar = _objRequestDetail.car;
    objEditRequest.car = objCar;
    WSAddress *objAddress= _objRequestDetail.address;
    objEditRequest.address = objAddress;
    [self.tblVwDetails reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
    [self setRequestDictParmas];
}

-(void)jobCancelAction:(id)sender{
     [self configAlertVwForCancelRequest];
}

-(void)btnEditAction:(id)sender{
    
    
    UIImage *img = [[UIImage imageNamed:@"logo-icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    CDAlertView *alert = [[CDAlertView alloc]init];
    alert.messageLabel.text = kSelectAction;
    alert.backgroundColor = RGBA(0, 0, 0, 0.7f);
    alert.circleFillColor = [UIColor colorWithPatternImage:img];
    alert.tintColor = [UIColor whiteColor];
    alert.isActionButtonsVertical = YES;
    alert.alertBackgroundColor = [UIColor whiteColor];
    alert.messageTextColor = [UIColor grayColor];
    
    
    CDAlertViewAction *action_edit = [[CDAlertViewAction alloc]initWithTitle:kEditRequest font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
        isEditRequestMode = YES;
        //Assigning edit request object with current value
        WSCar *objCar = _objRequestDetail.car;
        objEditRequest.car = objCar;
        WSAddress *objAddress= _objRequestDetail.address;
        objEditRequest.address = objAddress;
        [self.tblVwDetails reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
        [self setRequestDictParmas];
    }];
    
    CDAlertViewAction *action_delete = [[CDAlertViewAction alloc]initWithTitle:kCancelRequest font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
        [self configAlertVwForCancelRequest];
    }];
    
    
    CDAlertViewAction *action_cancel = [[CDAlertViewAction alloc]initWithTitle:kCancel font:Dax_Regular(15) textColor:[UIColor redColor] backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
      
    }];
    
    [alert addWithAction:action_edit];
    [alert addWithAction:action_delete];
    [alert addWithAction:action_cancel];
    
    [alert show:^(CDAlertView *action) {
        
    }];
}

- (void)notificationReceived:(NSNotification *)notification {
    
    NSDictionary *dict = [notification object];
    NSString *appointmentId = [dict valueForKey:@"appointmentId"];
    NSInteger statusType =  [[dict valueForKey:@"notificationType"]integerValue];
    
    if(appointmentId.integerValue == _objRequestDetail.internalBaseClassIdentifier){
        if(statusType == STATUS_COMPLETED){
            [self showRateMeBtn:YES];
        }else{
            [self showRateMeBtn:NO];
        }
        [_objRequestDetail.appointmentStatus enumerateObjectsUsingBlock:^(WSAppointmentStatus *objAppointment, NSUInteger idx, BOOL * _Nonnull stop) {
            if(objAppointment.status == statusType){
                objAppointment.status = statusType;
                objAppointment.datetime = kJustNow;
            }
        }];
        [self.tblVwDetails reloadData];
    }
   
}

-(void)carChanged:(NSNotification *)notification{
    WSCar *notifiyCarObj = [notification object];
    objEditRequest.car = notifiyCarObj;
    [self updateCarDetails];
}

-(void)addressChanged:(NSNotification *)notification{
    
    WSUserAddresses *notifyObj = [notification object];
    WSAddress *objAddress = [WSAddress new];
    objAddress.addressIdentifier = notifyObj.internalBaseClassIdentifier;
    objAddress.addresstitle = notifyObj.address;
    objAddress.address = notifyObj.address;
    objAddress.latitude = notifyObj.latitude;
    objAddress.longitude = notifyObj.longitude;
    objAddress.city= notifyObj.city;
    objAddress.country = notifyObj.country;
    objEditRequest.address = objAddress;
    [_tblVwDetails reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
}

-(void)towAddressChanged:(NSNotification *)notification{

    WSBill *objBill = _objRequestDetail.bill;
    objBill.totalAmount = AppContext.requestHelper.totalAmount;
    _objRequestDetail.bill = objBill;
    _objRequestDetail.destinationAddress = AppContext.requestHelper.towAddress;
     _objRequestDetail.destinationLatitude = AppContext.requestHelper.towAddressLatitude;
     _objRequestDetail.destinationLongitude = AppContext.requestHelper.towAddressLongitude;
    [_tblVwDetails reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
}

-(void)updateCarDetails{
    
    if([ReachabilityManager isReachable]){
        
        NSDictionary *params = @{@"appointmentId":[NSNumber numberWithInteger:_objRequestDetail.internalBaseClassIdentifier],
                                                    @"carId":[NSNumber numberWithInteger:objEditRequest.car.carIdentifier],
                                                    @"language":AppContext.appLanguage
                                 };
        [[WebServiceInvocation alloc]initWithWS:kUpdateRequestWS withParams:params withServiceType:TYPE_POST withSelector:@selector(updateCarDetailsWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }

}

-(void)updateCarDetailsWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
         _objRequestDetail.car = objEditRequest.car;
        [_tblVwDetails reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
        [self showAlert:[[sender responseDictionary]valueForKey:kServiceResponseMsgKey]];
    }
}



#pragma mark:--------------------: Tableview Datasource & Delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 1){
         WSVan *objVan = _objRequestDetail.van;
        return objVan.operator.count;
    }
    return 1;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        NSInteger noOfServices =_objRequestDetail.services.count;
        int heightOfCell = [[self.tblVwDetails dataSource]tableView:tableView cellForRowAtIndexPath:indexPath].contentView.frame.size.height;
        return ([self isTowService] ? kDetailSectionHeight : (noOfServices > 1)?heightOfCell : heightOfCell - 70);
    }
    else if(indexPath.section == 1){
            return kOperatorSectionHeight;
    }
    
    else{
          return kStatusSectionHeight;
    }
  
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    switch (indexPath.section) {
        case 0:{
            return [self tableView:tableView cellForRequestDetailIndexPath:indexPath];
        }
        break;
        case 1:{
              return [self tableView:tableView cellForRequestOperatorAtIndexPath:indexPath];
        }
        break;
        case 2:{
              return [self tableView:tableView cellForRequestStatusAtIndexPath:indexPath];
        }
            break;
            
        default:{
             return [self tableView:tableView cellForRequestStatusAtIndexPath:indexPath];
        }
            
        break;
    }

}

-(RequestDetailCell *)tableView:(UITableView *)tableView cellForRequestDetailIndexPath:(NSIndexPath *)indexPath{
    
    
    static  NSString *cellIdentifier = @"CellDetails";
    RequestDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"RequestDetailCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }

    [self setRequestStatus:_objRequestDetail.status withCell:cell];
    
    
    if(isEditRequestMode){
        [cell.btnEditCar setHidden:NO];
        [cell.btnEditDate setHidden:NO];
        [cell.btnEditAddress setHidden:NO];
        [cell.btnEditDestinationAddress setHidden:NO];
        
        [cell.btnEditCar addTarget:self action:@selector(btnEditCarAction:event:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnEditDate addTarget:self action:@selector(btnEditDateAction:event:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnEditAddress addTarget:self action:@selector(btnEditAddressAction:event:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnEditDestinationAddress addTarget:self action:@selector(btnEditDestinationAddressAction:event:) forControlEvents:UIControlEventTouchUpInside];
        
        WSCar *objCar = objEditRequest.car;
        cell.lblCarName.text =[NSString stringWithFormat:@"%@ - %@",objCar.brand,objCar.model];
        
        WSAddress *objAddress =   objEditRequest.address;
        cell.lblAddress.text = objAddress.address;
        
    }else{
        
        [cell.btnEditCar setHidden:YES];
        [cell.btnEditDate setHidden:YES];
        [cell.btnEditAddress setHidden:YES];
        [cell.btnEditDestinationAddress setHidden:YES];
        
        WSCar *objCar = _objRequestDetail.car;
        cell.lblCarName.text =[NSString stringWithFormat:@"%@ - %@",objCar.brand,objCar.model];
        
        WSAddress *objAddress =   _objRequestDetail.address;
        cell.lblAddress.text = objAddress.address;
    }
    
    __block NSString *strServices = @"";
    [_objRequestDetail.services enumerateObjectsUsingBlock:^(WSServices *objService, NSUInteger idx, BOOL * _Nonnull stop) {
        if(idx == 0){
            strServices = [strServices stringByAppendingString:[NSString stringWithFormat:@"- %@",objService.name]];
        }else{
            strServices = [strServices stringByAppendingString:[NSString stringWithFormat:@"\n- %@",objService.name]];
        }
    }];
    
    if(cell.serviceVwHeightConst.constant == 51){
        UILabel *tmpLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.lblServiceName.frame.origin.x,cell.lblServiceName.frame.origin.y,cell.lblServiceName.frame.size.width,21)];
        tmpLabel.numberOfLines = 0;
        tmpLabel.text = strServices;
        [tmpLabel sizeToFit];
        
        cell.lblServiceName.text = strServices;
        cell.serviceNameHeightConst.constant = tmpLabel.frame.size.height;
        int constHeight = CGRectGetMaxY(tmpLabel.frame);
        cell.serviceVwHeightConst.constant = constHeight;
        
    }
    
    WSCategory *objCategory  = _objRequestDetail.category;
    cell.lblCategoryName.text = objCategory.name;
    [cell.imgVwCategory sd_setImageWithURL:[NSURL URLWithString:objCategory.iconThump]
                          placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    if(objCategory.categoryIdentifier == 1){
        //Tow Service Show Destination Address View
        cell.destinationVwHeightConst.constant = 70;
        [cell.vwDestinationAddress setHidden:NO];
        cell.lblDestinationAddress.text = _objRequestDetail.destinationAddress;
    }else{
        //Normal Service Hide Destination Address View
        cell.destinationVwHeightConst.constant = 0;
        [cell.vwDestinationAddress setHidden:YES];
    }
    
    
    WSCartype *objCarType = _objRequestDetail.carType;
    if(objCarType==nil){
        cell.lblVehicleType.text = kNotProvided;
    }else{
        cell.lblVehicleType.text = objCarType.name;
    }
    
    [cell.imgVwVehicleType sd_setImageWithURL:[NSURL URLWithString:objCarType.iconThump]
                             placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    cell.lblDate.text = [self displayAppointmentDate:_objRequestDetail.appointmentDatetime];
    
   
   // For new Tax & Discount Feature
    
    WSVan *objVan = _objRequestDetail.van;
    WSOwner *objOwner = objVan.owner;
    WSBill *objBill = _objRequestDetail.bill;
    
    cell.lblNetAmount.text = [NSString stringWithFormat:@"%.2f %@",objBill.totalAmount,objBill.currency];
    cell.lblTaxmount.text = [NSString stringWithFormat:@"%.2f %@",objBill.tax,objBill.currency];
    cell.lblDiscAmount.text = [NSString stringWithFormat:@"%.2f %@",objBill.discount,objBill.currency];
    
    if(objOwner.isAppliedForTax == 1){
        cell.lblTaxPer.text = [NSString stringWithFormat:@"%@ (%.1f%%):",kTaxAmount,objOwner.taxPercentage];
    }
    cell.lblTotalAmount.text = [NSString stringWithFormat:@"%.2f %@",objBill.netAmount,objBill.currency];

    if(objBill.discountPercentage > 0){
         cell.lblDiscountPer.text = [NSString stringWithFormat:@"%@ (%.1f%%):",kDiscount,objBill.discountPercentage];
    }
    
    //*********  Old *********  //
   /*  WSBill *objBill = _objRequestDetail.bill;
    cell.lblNetAmount.text = [NSString stringWithFormat:@"%.2f %@",objBill.totalAmount,objBill.currency];
    cell.lblTaxmount.text = [NSString stringWithFormat:@"%.2f %@",objBill.tax,objBill.currency];
    cell.lblDiscAmount.text = [NSString stringWithFormat:@"%.2f %@",objBill.discount,objBill.currency];
     cell.lblTotalAmount.text = [NSString stringWithFormat:@"%.2f %@",objBill.totalAmount,objBill.currency];
    */
    //******************  //
    return cell;
    
}

-(NSString *)displayAppointmentDate:(NSString *)date{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    NSLocale *arabicLocale = [[NSLocale alloc] initWithLocaleIdentifier:AppContext.appLanguage];
    [formatter setLocale:arabicLocale];
    [formatter setDateFormat:YYYY_MM_dd_HH];
    NSDate *appointmentDate = [formatter dateFromString:date];
    
    [formatter setDateFormat:EE_dd_MMM_HH];
    NSString *dateInString = [formatter stringFromDate:appointmentDate];
    if(dateInString == nil || [dateInString isEqualToString:@""]){
        dateInString = @"";
    }
    return dateInString;
}


-(RequestOperatorCell *)tableView:(UITableView *)tableView cellForRequestOperatorAtIndexPath:(NSIndexPath *)indexPath{
    
    static  NSString *cellIdentifier = @"CellOperator";
    RequestOperatorCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"RequestOperatorCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    WSVan *objVan = _objRequestDetail.van;
    WSCartype *objCarType = objVan.cartype;
    
    WSOperator *objOperator = [objVan.operator objectAtIndex:indexPath.row];
    cell.lblName.text = [NSString stringWithFormat:@"%@",objOperator.fullName];
    cell.lblPhone.text = [NSString stringWithFormat:@"%.0f%.0f",objOperator.countryCode,objOperator.mobileNumber];
    cell.lblEmail.text = [NSString stringWithFormat:@"%@",objOperator.email];
    cell.lblVanNo.text =[NSString stringWithFormat:@"%@", objVan.plateno];
    cell.lblVanType.text = [NSString stringWithFormat:@"%@",objCarType.name];
    [cell.btnPhone addTarget:self action:@selector(callOperator:event:) forControlEvents:UIControlEventTouchUpInside];
    
    if(![self isEnglishLanguage]){
        [cell.btnPhone setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    }
    
    return cell;
    
}

-(RequestStatusCell *)tableView:(UITableView *)tableView cellForRequestStatusAtIndexPath:(NSIndexPath *)indexPath{
    
    static  NSString *cellIdentifier = @"CellStatus";
    RequestStatusCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"RequestStatusCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    [_objRequestDetail.appointmentStatus enumerateObjectsUsingBlock:^(WSAppointmentStatus *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self setStatusOfRequest:cell withAppointmentObj:obj];
    }];
    return cell;
}

-(BOOL)isTowService{
    WSCategory *objCategory  = _objRequestDetail.category;
    if(objCategory.categoryIdentifier == 1){
         return YES;
    }
    return NO;
}

-(void)setRequestStatus:(int)status withCell:(RequestDetailCell *)cell{
    
    switch (status) {
        case STATUS_BOOKED:{
            
            cell.lblRequestStatus.text = kStatus_Booked;
            [cell.lblRequestStatus setTextColor:[UIColor colorWithRed:(77.0/255.0) green:(148.0/255.0) blue:(255.0/255.0) alpha:1.0]];
        }
            break;
        case STATUS_IN_PROGRESS:{
            
            cell.lblRequestStatus.text = kStatus_InProgress;
            [cell.lblRequestStatus setTextColor:AppThemeColor];
        }
            break;
        case STATUS_COMPLETED:{
            
            cell.lblRequestStatus.text = kStatus_Completed;
            [ cell.lblRequestStatus setTextColor:[UIColor colorWithRed:(86.0/255.0) green:(86.0/255.0) blue:(86.0/255.0) alpha:1.0]];
         
        }
            break;
        case STATUS_CANCELLED:{
            
            cell.lblRequestStatus.text = kStatus_Cancelled;
            [ cell.lblRequestStatus setTextColor:[UIColor colorWithRed:(255.0/255.0) green:(86.0/255.0) blue:(86.0/255.0) alpha:1.0]];
        }
            break;
        case STATUS_ON_THE_WAY:{
            
            cell.lblRequestStatus.text = kStatus_OntheWay;
            [ cell.lblRequestStatus setTextColor:[UIColor colorWithRed:(77.0/255.0) green:(148.0/255.0) blue:(255.0/255.0) alpha:1.0]];
        }
            break;
        default:{
            cell.lblRequestStatus.text = kStatus_Booked;
            [cell.lblRequestStatus setTextColor:[UIColor colorWithRed:(77.0/255.0) green:(148.0/255.0) blue:(255.0/255.0) alpha:1.0]];
        }
            break;
    }
}

-(void)setStatusOfRequest:(RequestStatusCell *)cell withAppointmentObj:(WSAppointmentStatus *)obj{

    NSString *status = [NSString stringWithFormat:@"%.0f",obj.status];
    switch (status.intValue) {
        case STATUS_BOOKED:{
            // Request Status is BOOKED
            [cell.lblBookedTime setHidden:NO];
            cell.lblBookedTime.text = [NSString stringWithFormat:@"%@",obj.datetime];
            cell.imgVwStatus1.image = [UIImage imageNamed:@"ic_select_dot"];
        }
        break;
        case STATUS_IN_PROGRESS:{
            
            // Request Status is IN PROGRESS
            [cell.lblInProgressTime setHidden:NO];
            cell.lblInProgressTime.text =[NSString stringWithFormat:@"%@",obj.datetime];
            cell.imgVwStatus3.image = [UIImage imageNamed:@"ic_select_dot"];
            [cell.vwStatus2 setBackgroundColor:AppThemeColor];
        }
        break;
            
        case STATUS_COMPLETED:{
            // Request Status is FINISH
            [cell.lblFinishedTime setHidden:NO];
            cell.lblFinishedTime.text = [NSString stringWithFormat:@"%@",obj.datetime];
            cell.imgVwStatus4.image = [UIImage imageNamed:@"ic_select_dot"];
            [cell.vwStatus3 setBackgroundColor:AppThemeColor];
        }
        break;
            
        case STATUS_ON_THE_WAY:{
            // Request Status is ON THE WAY
            [cell.lblBookedTime setHidden:NO];
            cell.imgVwStatus1.image = [UIImage imageNamed:@"ic_select_dot"];
            
            [cell.lblOnTheWayTime setHidden:NO];
            cell.lblOnTheWayTime.text = [NSString stringWithFormat:@"%@",obj.datetime];
            cell.imgVwStatus2.image = [UIImage imageNamed:@"ic_select_dot"];
             [cell.vwStatus1 setBackgroundColor:AppThemeColor];
        
        }
        break;
           

        default:
         
            break;
    }
    
}

-(void)showRateMeBtn:(BOOL)isShow{
    if(isShow){
        [self.btnRateMe setHidden:NO];
        [self.btnVanLocation setHidden:YES];
    }else{
        [self.btnRateMe setHidden:YES];
        [self.btnVanLocation setHidden:NO];
    }
}
#pragma mark:--------------------: Button Actions

- (IBAction)btnVanLocationAction:(id)sender {
    
    WSVan *objVan = _objRequestDetail.van;
    WSAddress *objAddress = _objRequestDetail.address;
    if(objVan.latitude == 0 || objVan.longitude == 0){
        return;
    }
   else if(objAddress!=nil && (objAddress.longitude != 0 && objAddress.latitude != 0)){
        MapRouteVC *mrvc = loadViewController(kSBOthers, kMapRouteVC);
        mrvc.vanLocation = [[CLLocation alloc]initWithLatitude:objVan.latitude longitude:objVan.longitude];
        mrvc.userLocation = [[CLLocation alloc]initWithLatitude:objAddress.latitude longitude:objAddress.longitude];
        [self.navigationController pushViewController:mrvc animated:YES];
    }
  
}

- (IBAction)btnRateMeAction:(id)sender {
    [self moveToRatingVC:[NSString stringWithFormat:@"%lu",_objRequestDetail.internalBaseClassIdentifier]];
}


-(void)btnEditCarAction:(id)sender event:(id)event{
    
    VehicleListVC *vlvc = loadViewController(kSBRequest, kVehicleListVC);
    vlvc.fromRequestDetails = YES;
    vlvc.objRequest = objEditRequest;
    [self.navigationController pushViewController:vlvc animated:YES];
}

-(void)btnEditDateAction:(id)sender event:(id)event{
    
    serviceDuration = 0;
    strServiceId = @"";
    [_objRequestDetail.services enumerateObjectsUsingBlock:^(WSServices *objService, NSUInteger idx, BOOL * _Nonnull stop) {
        if(idx == 0){
            strServiceId = [strServiceId stringByAppendingString:[NSString stringWithFormat:@"%lu",objService.servicesIdentifier]];
        }else{
            strServiceId = [strServiceId stringByAppendingString:[NSString stringWithFormat:@",%lu",objService.servicesIdentifier]];
        }
        serviceDuration = serviceDuration + objService.duration;
    }];
   // [self fetchHolidaysList:strServiceId];
    [self showDatePicker];
}

-(void)btnEditAddressAction:(id)sender event:(id)event{
    
  [UIAlertController showAlertInVC:self withTitle:AppName withMsg:kUpdateAddressMsg withOtherAlertActionTitle:@[kAlertYes] withAlertActionCancel:kAlertNo withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
      if(buttonIndex == 0){
          VehicleListVC *vlvc = loadViewController(kSBRequest, kVehicleListVC);
          vlvc.fromRequestDetails = YES;
          vlvc.showAddress = YES;
          [self.navigationController pushViewController:vlvc animated:YES];
      }
  }];
}

-(void)btnEditDestinationAddressAction:(id)sender event:(id)event{
    [UIAlertController showAlertInVC:self withTitle:AppName withMsg:kUpdateAddressMsg withOtherAlertActionTitle:@[kAlertYes] withAlertActionCancel:kAlertNo withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
        if(buttonIndex == 0){
            [self moveToMapVC];
        }
    }];
}

-(void)callOperator:(id)sender event:(id)event{
    
    UITouch *touch = [event allTouches].anyObject;
    CGPoint location = [touch locationInView:_tblVwDetails];
    NSIndexPath *indexPath = [_tblVwDetails indexPathForRowAtPoint:location];
    
    WSVan *objVan = _objRequestDetail.van;
    WSOperator *objOperator = [objVan.operator objectAtIndex:indexPath.row];
    
    NSString *countryCode = [NSString stringWithFormat:@"%.0f",objOperator.countryCode];
    NSString *mobileNumber = [NSString stringWithFormat:@"%@%.0f",countryCode,objOperator.mobileNumber];
    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:mobileNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

-(void)moveToRatingVC:(NSString *)appointmentId{
    RatingVC *rvc = loadViewController(kSBRequest, kRatingVC);
    rvc.appointmentId = appointmentId;
    rvc.fromDetailScreen = YES;
    [self.navigationController pushViewController:rvc animated:YES];
}

#pragma mark :--------------------: Holiday List Functions
-(void)fetchHolidaysList:(NSString *)serviceId{
    
    if([ReachabilityManager isReachable]){
        NSDictionary *dictParms = @{@"serviceId":serviceId};
        [[WebServiceInvocation alloc]initWithWS:kFetchHolidayWS withParams:dictParms withServiceType:TYPE_POST withSelector:@selector(holidayWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)holidayWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){

    }else{
        
        NSMutableArray *arrHolidays = [NSMutableArray new];
        NSMutableArray *arrId = [NSMutableArray new];
        NSMutableArray *tmpArr = [NSMutableArray arrayWithArray:[sender responseArray]];
        [tmpArr enumerateObjectsUsingBlock:^(WSHolidayList *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSInteger holidayId = obj.internalBaseClassIdentifier;
            if(![arrId containsObject:[NSNumber numberWithInteger:holidayId]]){
                [arrId addObject:[NSNumber numberWithInteger:holidayId]];
                [arrHolidays addObject:obj];
            }
        }];
        
        NSDateFormatter *formatter = [NSDateFormatter new];
        [formatter setDateFormat:YYYY_MM_dd_HH];
        NSDate *date = [formatter dateFromString:_objRequestDetail.appointmentDatetime];
        [formatter setDateFormat:YYYY_MM_dd];
        NSString *strDate = [formatter stringFromDate:date];
        
        WSVan *objVan = _objRequestDetail.van;
        WSOwner *objOwner = objVan.owner;
        
        AppointmentsListVC *aplvc = loadViewController(kSBRequest, kAppointmentsListVC);
         aplvc.delegate = self;
        aplvc.isEditRequest = YES;
        aplvc.selDate = strDate;
        aplvc.duration = serviceDuration;
        aplvc.serviceIds = strServiceId;
        aplvc.addressId = objEditRequest.address.addressIdentifier;
        aplvc.ownerId = objOwner.ownerIdentifier;
        if(arrHolidays.count > 0){
            aplvc.arrHolidayList = arrHolidays;
        }
         [self.navigationController pushViewController:aplvc animated:YES];
    }
    
}
#pragma mark :--------------------: DatePicker Functions
-(void)showDatePicker{
    
    NSMutableArray *arrServices = [NSMutableArray new];
    [_objRequestDetail.services enumerateObjectsUsingBlock:^(WSServices *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [arrServices addObject:[NSString stringWithFormat:@"%lu",obj.servicesIdentifier]];
    }];
    
    RTDatePickerVC *rtdpvc = [[RTDatePickerVC alloc]initWithFrame:self.view.bounds];
    rtdpvc.delegate = self;
    rtdpvc.arrSeviceId = arrServices;
    rtdpvc.showHolidayList = YES;
    [rtdpvc setMinDate:[NSDate date]];
    [self.navigationController.view addSubview:rtdpvc];
}

-(void)pickerSelectedDate:(NSDate *)selectedDate{
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:YYYY_MM_dd];
    NSString *strDate = [formatter stringFromDate:selectedDate];
    
    
    WSVan *objVan = _objRequestDetail.van;
    WSOwner *objOwner = objVan.owner;
    
    AppointmentsListVC *aplvc = loadViewController(kSBRequest, kAppointmentsListVC);
    aplvc.delegate = self;
    aplvc.isEditRequest = YES;
    aplvc.selDate = strDate;
    aplvc.duration = serviceDuration;
    aplvc.serviceIds = strServiceId;
    aplvc.addressId = objEditRequest.address.addressIdentifier;
    aplvc.ownerId = objOwner.ownerIdentifier;
    [self.navigationController pushViewController:aplvc animated:YES];
}

#pragma mark :--------------------: Appointment List Delegates
-(void)updateRequest:(WSAppointmentList *)objList{
    
    NSDictionary *params;
    WSCategory *objCategory = _objRequestDetail.category;
    WSVan *ojbVan = _objRequestDetail.van;
    WSBill *objBill = _objRequestDetail.bill;
    if(objCategory.categoryIdentifier == 1){
        
        //Tow Service
        params =@{
                  @"appointmentId":[NSNumber numberWithInteger:_objRequestDetail.internalBaseClassIdentifier],
                  @"vanId":[NSNumber numberWithInteger:ojbVan.vanIdentifier],
                  @"travelingTime":[NSNumber numberWithInteger:objList.travelingTime],
                  @"appoinmentDateTime":objList.dateTime,
                  @"addressId":[NSNumber numberWithInteger:objEditRequest.address.addressIdentifier],
                  @"duration":[NSNumber numberWithInt:0],
                  @"destinationAddress":AppContext.requestHelper.towAddress,
                  @"destinationLatitude":[NSNumber numberWithDouble:AppContext.requestHelper.towAddressLatitude],
                  @"destinationLongitude":[NSNumber numberWithDouble:AppContext.requestHelper.towAddressLongitude],
                  @"amount":[NSNumber numberWithDouble:AppContext.requestHelper.totalAmount],
                  @"localAmount":[NSNumber numberWithDouble:AppContext.requestHelper.totalLocalAmount],
                  @"language":AppContext.appLanguage
                  };
         NSLog(@"%@",params);
        
    }else{

        params =@{
                  @"appointmentId":[NSNumber numberWithInteger:_objRequestDetail.internalBaseClassIdentifier],
                  @"vanId":[NSNumber numberWithInteger:ojbVan.vanIdentifier],
                  @"travelingTime":[NSNumber numberWithInteger:objList.travelingTime],
                  @"appoinmentDateTime":objList.dateTime,
                  @"addressId":[NSNumber numberWithInteger:objEditRequest.address.addressIdentifier],
                  @"duration":[NSNumber numberWithDouble:serviceDuration],
                  @"destinationAddress":@"",
                  @"destinationLatitude":@"",
                  @"destinationLongitude":@"",
                  @"amount":[NSNumber numberWithDouble:objBill.totalAmount],
                  @"localAmount":[NSNumber numberWithDouble:objBill.localAmount],
                  @"language":AppContext.appLanguage
                  };
        
        NSLog(@"%@",params);
        
    }
    
    if([ReachabilityManager isReachable]){
        [[WebServiceInvocation alloc]initWithWS:kUpdateRequestWS withParams:params withServiceType:TYPE_POST withSelector:@selector(updateRequestWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)updateRequestWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        _objRequestDetail = [[sender responseArray]objectAtIndex:0];
        [self.tblVwDetails reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
        [[NSNotificationCenter defaultCenter]postNotificationName:EDIT_REQUEST_APPOINTMENT_CHANGED object:_objRequestDetail];
    }
}

#pragma mark :--------------------: Set Dictionary
-(void)setRequestDictParmas{
    
    WSAddress *objAddress = objEditRequest.address;
    WSBill *objBill = _objRequestDetail.bill;
    
    __block double variableAmount = 0;
    [_objRequestDetail.services enumerateObjectsUsingBlock:^(WSServices *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        variableAmount = variableAmount + obj.variableAmount;
    }];
    
    AppContext.requestHelper =  [AddRequestHelper defaultManager];
    AppContext.requestHelper.addressDetails = objAddress.address;
    AppContext.requestHelper.addressId = objAddress.addressIdentifier;
    AppContext.requestHelper.addressLatitude = objAddress.latitude;
    AppContext.requestHelper.addressLongitude =  objAddress.longitude;
    AppContext.requestHelper.towAddressLatitude = _objRequestDetail.destinationLatitude;
    AppContext.requestHelper.towAddressLongitude =   _objRequestDetail.destinationLongitude;
    AppContext.requestHelper.towAddress = _objRequestDetail.destinationAddress;
    AppContext.requestHelper.isTowService = YES;
    AppContext.requestHelper.variableAmount = variableAmount;
    AppContext.requestHelper.totalLocalAmount = objBill.localAmount;
    AppContext.requestHelper.totalAmount = objBill.totalAmount;
  
}

#pragma mark :--------------------: Redirect To MapVC
-(void)moveToMapVC{

    MapVC *mpvc = loadViewController(kSBRequest, kMapVC);
    mpvc.isEditRequest = YES;
    [self.navigationController pushViewController:mpvc animated:YES];
    
}

#pragma mark :--------------------: Cancel Request Functions
-(void)configAlertVwForCancelRequest{
    
    @try {
        
        UIImage *img = [[UIImage imageNamed:@"logo-icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        CDAlertView *alertView = [[CDAlertView alloc]init];
        alertView.messageLabel.text = @"";
        alertView.titleLabel.text = [NSString stringWithFormat:@"%@ %@",kEnter,kReasonForCancelRequest];
        alertView.titleFont = Dax_Medium(15);
        alertView.backgroundColor = RGBA(0, 0, 0, 0.7f);
        alertView.circleFillColor = [UIColor colorWithPatternImage:img];
        alertView.tintColor = [UIColor whiteColor];
        alertView.isActionButtonsVertical = NO;
        alertView.isTextFieldHidden = NO;
        alertView.alertBackgroundColor = [UIColor whiteColor];
        alertView.messageTextColor = [UIColor grayColor];
        alertView.textFieldFont = Dax_Regular(15);
        alertView.tintColor = AppThemeColor;
        
        
        CDAlertViewAction *action_txt = [[CDAlertViewAction alloc]initWithTitle:kCancelOrder font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            if([alertView.textFieldText isEqualToString:@""]){
                [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kReasonForCancelRequest]];
            }else{
                [self cancelRequest:alertView.textField.text];
            }
        }];
        
        
        CDAlertViewAction *action_cancel = [[CDAlertViewAction alloc]initWithTitle:kBack font:Dax_Regular(15) textColor:[UIColor redColor] backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            
        }];
        
        [alertView addWithAction:action_cancel];
        [alertView addWithAction:action_txt];
        
        
        [alertView show:^(CDAlertView *action) {
            
        }];

        
    } @catch (NSException *exception) {
        NSLog(@"Alertview Exception : %@ , In Controller : %@",[exception description],self);
    } @finally {
        
    }
    
}

-(void)cancelRequest:(NSString *)reason{
    
    if([ReachabilityManager isReachable]){
        
        NSString *strUserId = [Functions getStringValueFromDefaults:USER_ID];
        NSInteger requestId = _objRequestDetail.internalBaseClassIdentifier;
        
        NSString *strUrl = [NSString stringWithFormat:@"%@?userId=%@&appointmentId=%lu&reason=%@&language=%@&%@=%@",kCancelRequestWS,strUserId,requestId,reason,AppContext.appLanguage,kAppVersionServiceKey,[self fetchAppVersion]];
        
        [[WebServiceInvocation alloc]initWithWS:strUrl withParams:nil withServiceType:TYPE_GET withSelector:@selector(cancelWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
      
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)cancelWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        NSString *appointmentId = [NSString stringWithFormat:@"%lu",_objRequestDetail.internalBaseClassIdentifier];
        [[NSNotificationCenter defaultCenter]postNotificationName:APPOINTMENT_CANCELLED object:appointmentId];
        [UIAlertController showAlertInVC:self withTitle:AppName withMsg:[[sender responseDictionary]valueForKey:kServiceResponseMsgKey] withOtherAlertActionTitle:@[kAlertOk] withAlertActionCancel:@"" withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
            if(buttonIndex == 0){
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];

    }
}

@end
