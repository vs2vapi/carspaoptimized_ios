//
//  ServiceVC.h
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ServiceVC : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,PaymentPopUpDelegate,UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIImageView *imgVwService;
@property (weak, nonatomic) IBOutlet UIView *containerVw;
@property (weak, nonatomic) IBOutlet UIView *bottomVw;
@property (weak, nonatomic) IBOutlet UIButton *btnRequest;


@property (weak, nonatomic) IBOutlet UIButton *btnServeNow;
@property (weak, nonatomic) IBOutlet UILabel *lblSwipeForMore;
@property (weak, nonatomic) IBOutlet UITableView *tblVwService;

@property(strong,nonatomic)NSMutableArray *arrServices;
@property(strong,nonatomic)NSMutableArray *arrSelectedServices;
@property(strong,nonatomic)NSMutableArray *arrSelectedSupplier;
@property(strong,nonatomic)NSMutableDictionary *contentOffsetDictionary;

- (IBAction)btnServiceAction:(id)sender;
- (IBAction)btnRequestAction:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *btnSelectedDate;
- (IBAction)btnSelDtAction:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *segmentVwHeightConst;
@property (weak, nonatomic) IBOutlet UISegmentedControl *serviceSegment;
- (IBAction)segmnentAction:(id)sender;

@end
