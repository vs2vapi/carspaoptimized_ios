//
//  ServiceVC.m
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "ServiceVC.h"
#import "ServiceCollVwCell.h"
#import "MapVC.h"
#import "WSServices.h"
#import "AppointmentsListVC.h"
#import "ActiveOrdersVC.h"
#import "WSOwner.h"
#import "ServiceTblCell.h"
#import "WSSupplierServices.h"

#define kRowHeight 150
#define kHeaderHeight 35
#define kItemCellHeight 147

@interface ServiceVC () <RTDatePickerDelegate>{
    NSInteger tempDuration;
    double tempVariableAmount;
    double tempAmount;
    BOOL pickerForService;
    BOOL isViewByDateSelected;
    NSString *strServiceDate;
}

@end

@implementation ServiceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self fetchServices:YES];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark :---------: Setup UI

-(void)setShadowForVw{
    
    _containerVw.layer.cornerRadius = 4.0f;
    _containerVw.layer.borderWidth = 1.0f;
    _containerVw.layer.borderColor = RGBA(200, 200, 200, 1).CGColor;
    
    [_containerVw.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [_containerVw.layer setShadowOpacity:0.8];
    [_containerVw.layer setShadowRadius:3.0];
    [_containerVw.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

-(void)setupUI{
    
    
    pickerForService = NO;
    [_tblVwService setTableFooterView:[UIView new]];
    [_tblVwService setSeparatorColor:[UIColor clearColor]];
    
    _btnRequest.titleLabel.numberOfLines = 2;
    _btnRequest.titleLabel.adjustsFontSizeToFitWidth = NO;
    _btnRequest.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.navigationItem.title = [NSString stringWithFormat:@"%@",AppContext.requestHelper.categoryName];
    [_imgVwService sd_setImageWithURL:[NSURL URLWithString:AppContext.requestHelper.serviceMainImgURL]
                     placeholderImage:nil];

    _contentOffsetDictionary = [NSMutableDictionary new];
    
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:Dax_Regular(15)
                                                           forKey:NSFontAttributeName];
    [self.serviceSegment setTitleTextAttributes:attributes
                                    forState:UIControlStateNormal];
    
    [self.serviceSegment setTitle:kSupplier forSegmentAtIndex:0];
    [self.serviceSegment setTitle:kDate forSegmentAtIndex:1];
    
    strServiceDate = @"";
    self.segmentVwHeightConst.constant = 40;
    self.btnSelectedDate.layer.cornerRadius = 3.0f;

}


-(void)fetchServices:(BOOL)showLoader{
    
    if([ReachabilityManager isReachable]){

        NSInteger categoryId = AppContext.requestHelper.categoryId;
        NSInteger carTypeId = AppContext.requestHelper.carTypeId;
        NSInteger addressId = AppContext.requestHelper.addressId;
        double lat = AppContext.requestHelper.addressLatitude;
        double longitude = AppContext.requestHelper.addressLongitude;

        NSString *strUrl = [NSString stringWithFormat:@"%@?categoryId=%lu&carTypeId=%lu&addressId=%lu&language=%@&latitude=%f&longitude=%f&version=1&%@=%@",kFetchVehicleServicesWS,categoryId,carTypeId,addressId,AppContext.appLanguage,lat,longitude,kAppVersionServiceKey,[self fetchAppVersion]];
//        NSLog(@"Service URL : %@",strUrl);
        [[WebServiceInvocation alloc]initWithWS:strUrl withParams:nil withServiceType:TYPE_GET withSelector:@selector(serviceWSAction:) withTarget:self showLoader:showLoader loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }
    
}

-(void)serviceWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        
        [UIAlertController showAlertInVC:self withTitle:AppContext.requestHelper.categoryName withMsg:[sender responseErr] withOtherAlertActionTitle:@[kAlertOk] withAlertActionCancel:@"" withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }else{

        _arrServices = [NSMutableArray arrayWithArray:[sender responseArray]];
        _arrSelectedServices = [NSMutableArray new];
        _arrSelectedSupplier = [NSMutableArray new];
        [_tblVwService reloadData];
        
        __block BOOL swipeOption = NO;
        [_arrServices enumerateObjectsUsingBlock:^(WSSupplierServices *objsupplier, NSUInteger idx, BOOL * _Nonnull stop) {
            if(objsupplier.services.count > 3){
                swipeOption = YES;
                *stop = YES;
            }
        }];
        
        if(swipeOption){
            [_lblSwipeForMore setHidden:NO];
            _lblSwipeForMore.text = kSwipeForMore;
            _lblSwipeForMore.adjustsFontSizeToFitWidth = YES;
            if(![self isEnglishLanguage]){
                _lblSwipeForMore.textAlignment = NSTextAlignmentLeft;
            }
        }else{
            [_lblSwipeForMore setHidden:YES];
        }
    }
}


-(BOOL)checkIfServiceSelected{
    if (_arrSelectedServices.count > 0) {
        return YES;
    }else{
        return NO;
    }
}

#pragma mark :----------: Tableview Delegates & Data Sources
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _arrServices.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kRowHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return kHeaderHeight;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerVw = [[UIView alloc]initWithFrame:CGRectMake(0, 0,tableView.frame.size.width, kHeaderHeight)];
    headerVw.backgroundColor =RGBA(255, 255, 255, 1); //[UIColor clearColor];
    
    WSSupplierServices *objSupplier = [_arrServices objectAtIndex:section];
    UILabel *lblHeaderTitle = [[UILabel alloc]initWithFrame:CGRectMake(16,5,headerVw.frame.size.width - 40,kHeaderHeight)]; //headerVw.frame.size.width - 90
    lblHeaderTitle.font = Dax_Bold(16);
    lblHeaderTitle.backgroundColor = [UIColor clearColor];

    if([AppContext.appLanguage isEqualToString:@"ar"] || [AppContext.appLanguage isEqualToString:@"ar-US"]){
         lblHeaderTitle.text = [NSString stringWithFormat:@"%@",objSupplier.nameAR];
    }else{
         lblHeaderTitle.text = [NSString stringWithFormat:@"%@",objSupplier.name];
    }
    
  //  lblHeaderTitle.text = [NSString stringWithFormat:@"%@",objSupplier.name];
    lblHeaderTitle.textAlignment = NSTextAlignmentJustified;
    lblHeaderTitle.adjustsFontSizeToFitWidth = YES;
    lblHeaderTitle.textColor = [UIColor blackColor];
    [headerVw addSubview:lblHeaderTitle];
    
    headerVw.layer.cornerRadius = 3.0f;
    
    return headerVw;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"CellIdentifier";
    ServiceTblCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell){
        cell = [[ServiceTblCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(ServiceTblCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
   
    [cell setCollectionViewDataSourceDelegate:self indexPath:indexPath];
    NSInteger index = cell.collectionView.indexPath.section;
    CGFloat horizontalOffset = [self.contentOffsetDictionary[[@(index) stringValue]] floatValue];
    [cell.collectionView setContentOffset:CGPointMake(horizontalOffset, 0)];
  
}

#pragma mark : -------------------: ScrollViewDelegate Methods
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (![scrollView isKindOfClass:[UICollectionView class]]) return;
    
    CGFloat horizontalOffset = scrollView.contentOffset.x;
    ServiceCollectionVw *collectionView = (ServiceCollectionVw *)scrollView;
    NSInteger index = collectionView.indexPath.section;
    self.contentOffsetDictionary[[@(index) stringValue]] = @(horizontalOffset);
    
}

#pragma mark :----------: CollectionView Delegates & Data Sources

-(NSInteger)numberOfSectionsInCollectionView:(ServiceCollectionVw *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(ServiceCollectionVw *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    WSSupplierServices *objSupplier = [_arrServices objectAtIndex:collectionView.indexPath.section];
    return objSupplier.services.count;
}

-(UIEdgeInsets)collectionView:(ServiceCollectionVw *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}


-(CGSize)collectionView:(ServiceCollectionVw *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(collectionView.frame.size.width /3 ,kItemCellHeight);
}


-(UICollectionViewCell *)collectionView:(ServiceCollectionVw *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"CellService";
    ServiceCollVwCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    WSSupplierServices *objSupplier = [_arrServices objectAtIndex:collectionView.indexPath.section];
    WSServices *objService = [objSupplier.services objectAtIndex:indexPath.row];
    

    
    if([AppContext.appLanguage isEqualToString:@"ar"] || [AppContext.appLanguage isEqualToString:@"ar-US"]){
        cell.lblServiceTitle.text = [NSString stringWithFormat:@"%@",objService.nameAR];
    }else{
        cell.lblServiceTitle.text = [NSString stringWithFormat:@"%@",objService.name];
    }
   
    double cost = 0;
    if(objSupplier.isAppliedForTax == 1){
        cost =[self calcVat:objSupplier.taxPercentage withAmount:objService.amount];
    }else{
        cost = objService.amount;
    }

    NSString *strExRate = [NSString stringWithFormat:@"%.2f",AppContext.exchangeRate];
    cost  = cost * [strExRate floatValue];
    cost = round(cost);
    
    NSString *strCost = kCost;
    NSMutableAttributedString *attCost = [[NSMutableAttributedString alloc] initWithString:strCost];
    NSRange range = NSMakeRange(0,strCost.length);
    [attCost addAttribute:NSForegroundColorAttributeName value:AppThemeColor range:range];
    
    NSString *curr = [NSString stringWithFormat:@" %.0f",cost];
    NSString *amountWithCommas = [NSNumberFormatter localizedStringFromNumber:@(curr.intValue)
                                                                  numberStyle:NSNumberFormatterDecimalStyle];
    
    amountWithCommas = [amountWithCommas stringByAppendingString:[NSString stringWithFormat:@" %@",AppContext.currency]];
    NSMutableAttributedString *attAmount = [[NSMutableAttributedString alloc] initWithString:amountWithCommas];
    [attCost appendAttributedString:attAmount];
    cell.lblCost.attributedText = attCost;
    
    [cell.imgVwService sd_setImageWithURL:[NSURL URLWithString:objService.iconThump]
                         placeholderImage:nil];
    
    cell.btnInformation.tag = indexPath.row;
    [cell.btnInformation setTitle:kServiceDetails forState:UIControlStateNormal];
    [cell.btnInformation addTarget:self action:@selector(btnInformationClicked:event:) forControlEvents:UIControlEventTouchUpInside];
    
    if(objService.isServiceSelected){
        [cell.imgVwArrow setHidden:NO];
        [cell.bgImgVw setImage:[UIImage imageNamed:@"icn_bg_green"]];
    }else{
        [cell.imgVwArrow setHidden:YES];
        [cell.bgImgVw setImage:[UIImage imageNamed:@"icn_bg_blue"]];
    }
    
    
    return cell;
}

-(double )calcVat:(double)percentage withAmount:(double)amount{
    double displayAmount = amount;
    double taxAmount = (displayAmount *percentage)/100;
    displayAmount = displayAmount + taxAmount;
    return displayAmount;
}

-(void)collectionView:(ServiceCollectionVw *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    WSSupplierServices *objSupplier = [_arrServices objectAtIndex:collectionView.indexPath.section];
    WSServices *objService = [objSupplier.services objectAtIndex:indexPath.row];

    AppContext.requestHelper.ownerId = objSupplier.ownerId;
    
    if(objSupplier.isAppliedForTax == 1){
        AppContext.requestHelper.isAppliedForTax = YES;
        AppContext.requestHelper.taxPercentage = objSupplier.taxPercentage;
    }
    
    if(AppContext.requestHelper.isTowService){
        //Tow Service. In tow service we have to manually calculate duration.
        AppContext.requestHelper.serviceAmount = objService.amount;
        tempDuration = objService.duration;
        tempAmount = objService.amount;
        tempVariableAmount = objService.variableAmount;

        
        [_arrServices enumerateObjectsUsingBlock:^(WSSupplierServices *objSupp, NSUInteger idx, BOOL * _Nonnull stop) {
            [objSupp.services enumerateObjectsUsingBlock:^(WSServices *objServ, NSUInteger idx, BOOL * _Nonnull stop) {
                objServ.isServiceSelected = NO;
            }];
        }];
        
        [_arrSelectedServices removeAllObjects];
        [_arrSelectedServices addObject:[NSNumber numberWithInteger:objService.servicesIdentifier]];
        
        objService.isServiceSelected = YES;
        [[objSupplier.services mutableCopy]replaceObjectAtIndex:indexPath.row withObject:objService];
        [_arrServices replaceObjectAtIndex:collectionView.indexPath.section withObject:objSupplier];
        [_tblVwService reloadData];
   
    }
    else{

        //Logic for normal services.
        //If owner Id is same as selected service owner Id, then allow user to select it, else show popup message.
        if([_arrSelectedSupplier containsObject:[NSNumber numberWithInteger:objService.ownerId]]){
             NSLog(@"same Id, insert it");
            [self insertSelectedServices:objSupplier withServices:objService withSectionIdx:collectionView.indexPath withRowIdx:indexPath];
            dispatch_async(dispatch_get_main_queue(), ^{
                // [_tblVwService reloadSections:[NSIndexSet indexSetWithIndex:collectionView.indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
                [_tblVwService reloadData];
            });
//            [_tblVwService reloadSections:[NSIndexSet indexSetWithIndex:collectionView.indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
         }else if(_arrSelectedSupplier.count == 0){
                NSLog(@"no service is added yet, allow user to add service");
                [self insertSelectedServices:objSupplier withServices:objService withSectionIdx:collectionView.indexPath withRowIdx:indexPath];
             dispatch_async(dispatch_get_main_queue(), ^{
                // [_tblVwService reloadSections:[NSIndexSet indexSetWithIndex:collectionView.indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
                 [_tblVwService reloadData];
             });
             
             
         }else{
             [self showAlert:kCannotSelService];
         }
        
    }
    
    NSLog(@"Duration : %.0f , Amount in local : %f",objService.duration,objService.amount);

}

-(void)insertSelectedServices:(WSSupplierServices *)supplierObj withServices:(WSServices *)objService withSectionIdx:(NSIndexPath *)sectionIdx withRowIdx:(NSIndexPath *)rowIdx{
    
    [_arrSelectedSupplier removeAllObjects];
    [_arrSelectedSupplier addObject:[NSNumber numberWithInteger:objService.ownerId]];
    
    //Other Services. Concat service id for multiple selections. In other service we will get duration in response.
    if ([_arrSelectedServices containsObject:[NSNumber numberWithInteger:objService.servicesIdentifier]]){
        
        tempDuration = tempDuration - objService.duration;
        tempAmount = tempAmount - objService.amount;
        
        [_arrSelectedServices removeObject:[NSNumber numberWithInteger:objService.servicesIdentifier]];
        objService.isServiceSelected = NO;
        if(_arrSelectedServices.count == 0){
            [_arrSelectedSupplier removeAllObjects];
        }
    }else{
        tempDuration = tempDuration + objService.duration;
        tempAmount = tempAmount + objService.amount;
        [_arrSelectedServices addObject:[NSNumber numberWithInteger:objService.servicesIdentifier]];
         objService.isServiceSelected = YES;
        
    }
    
    [[supplierObj.services mutableCopy]replaceObjectAtIndex:rowIdx.row withObject:objService];
    [_arrServices replaceObjectAtIndex:sectionIdx.section withObject:supplierObj];
    
}


/*
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _arrServices.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(AppContext.requestHelper.isAccesorryService){
        return CGSizeMake(collectionView.frame.size.width /3, 180);
    }else{
        return CGSizeMake(collectionView.frame.size.width /3, 180);
    }
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
 */


#pragma mark :----------: Button Actions
- (IBAction)btnServiceAction:(id)sender {
    
    if([self checkIfServiceSelected]){
        
        AppContext.requestHelper.serviceDuration =  tempDuration;
        AppContext.requestHelper.totalLocalAmount = tempAmount;
        AppContext.requestHelper.variableAmount = tempVariableAmount;       
        AppContext.requestHelper.arrServiceId = _arrSelectedServices;
    
        if(AppContext.requestHelper.isTowService){
            NSLog(@"Tow Service.. Redirecting to Map");
            [self gotoMapVC:YES];
        }else{
            NSLog(@"Normal Service.. Show Popup");
            [self showPopUpVC];
        }
    }else{
        [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseSelect,kAtleastOneService]];
    }

}

- (IBAction)btnRequestAction:(id)sender {
    
    if([self checkIfServiceSelected]){
        
        AppContext.requestHelper.serviceDuration =  tempDuration;
        AppContext.requestHelper.totalLocalAmount = tempAmount;
        AppContext.requestHelper.variableAmount = tempVariableAmount;
        AppContext.requestHelper.arrServiceId = _arrSelectedServices;
        
        if(AppContext.requestHelper.isTowService){
            NSLog(@"Tow Service.. Redirecting to Map");
            [self gotoMapVC:NO];
        }else{
            NSLog(@"Normal Service.. Show picker");
            pickerForService = NO;
            [self showDatePicker];
        }
    }else{
        [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseSelect,kAtleastOneService]];
    }
}

- (IBAction)btnSupplierAction:(id)sender {
      [self setUnsetColorForSupplierBtn:YES];
}

- (IBAction)btnDateAction:(id)sender {
    [self setUnsetColorForSupplierBtn:NO];
}

- (IBAction)btnSelDtAction:(id)sender {
    [self setUnsetColorForSupplierBtn:NO];
}

-(void)btnInformationClicked:(id)sender event:(id)event{

    
    UITouch *touch = [event allTouches].anyObject;
    CGPoint location = [touch locationInView:_tblVwService];
    NSIndexPath *sectionIndexPath = [_tblVwService indexPathForRowAtPoint:location];
    
    WSSupplierServices *objSupplier = [_arrServices objectAtIndex:sectionIndexPath.section];
    ServiceTblCell *cell = [_tblVwService cellForRowAtIndexPath:sectionIndexPath];
    
    touch = [event allTouches].anyObject;
    location = [touch locationInView:cell.collectionView];
    NSIndexPath *rowIndexPath = [cell.collectionView indexPathForItemAtPoint:location];
    WSServices *objItems = [objSupplier.services objectAtIndex:rowIndexPath.row];
    
    [UIAlertController showAlertInVC:self withTitle:objItems.name withMsg:objItems.servicesDescription withOtherAlertActionTitle:nil withAlertActionCancel:kAlertOk withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
        
    }];
    

}



-(void)showDatePicker{
 
    @try {
        RTDatePickerVC *rtdpvc = [[RTDatePickerVC alloc]initWithFrame:self.view.bounds];
        rtdpvc.delegate = self;
        rtdpvc.arrSeviceId =_arrSelectedServices;
        rtdpvc.showHolidayList = YES;
        [rtdpvc setMinDate:[NSDate date]];
        [self.navigationController.view addSubview:rtdpvc];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }

    
}

-(void)gotoMapVC:(BOOL)serveNow{
  
    MapVC *mpvc = loadViewController(kSBRequest, kMapVC);
    mpvc.isServeNow = serveNow;
    [self.navigationController pushViewController:mpvc animated:YES];
}

-(void)showPopUpVC{
  
    PaymentPopUpVC *pvc = [[PaymentPopUpVC alloc] initWithNibName:@"PaymentPopUpVC" bundle:nil];
    pvc.isServeNow = YES;
    pvc.delegate = self;
    pvc.isServiceByDateSelected = isViewByDateSelected;
    pvc.strServiceByDate = strServiceDate;
    [self presentPopupViewController:pvc animationType:MJPopupViewAnimationFade];
}

#pragma mark :----------: Picker Delegates
-(void)pickerSelectedDate:(NSDate *)selectedDate{

    NSDateFormatter *formatter = [NSDateFormatter new];
    NSLocale *enLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en"];
    [formatter setLocale:enLocale];
    if(pickerForService){
        [formatter setDateFormat:yyyy_MM_dd];
        NSString *strDate = [formatter stringFromDate:selectedDate];
        NSString *timeString = @"00:00:00";
        if([self compareDateOnly:selectedDate] == NSOrderedSame){
            [formatter setDateFormat:@"HH:mm:ss"];
            timeString = [formatter stringFromDate:[NSDate date]];
        }
        strServiceDate = strDate;
        [self fetchServiceByDate:strDate withTime:timeString];
        [self setSelectedDate:YES withDt:selectedDate];
        
    }else{
       
         [formatter setDateFormat:YYYY_MM_dd];
        NSString *strDate = [formatter stringFromDate:selectedDate];
        AppContext.requestHelper.appointmentDate = strDate;//[NSString stringWithFormat:@"%@",selectedDate];
        AppointmentsListVC *aplvc = loadViewController(kSBRequest, kAppointmentsListVC);
        [self.navigationController pushViewController:aplvc animated:YES];
    }
  
}

#pragma mark :----------: Popup Delegates
- (void)cancelButtonClicked:(PaymentPopUpVC *)paymentVC{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    if(paymentVC.cancelRequest){
        [self cancelRequest];
        return;
    }
    if(paymentVC.addAppointment){
        [self addAnotherAppointment];
        return;
    }
    if(paymentVC.addService){
        [self addAnotherServices];
        return;
    }
}

-(void)cancelRequest{
    //Back Changes
    [AppContext.requestHelper deallocInstance];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addAnotherAppointment{
    pickerForService = NO;
    [self showDatePicker];
}

-(void)noTimeSlotFound{
    pickerForService = NO;
    [self showDatePicker];
}

-(void)addAnotherServices{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)checkedOutSuccessfully:(NSMutableDictionary *)dictResponse{
    
    NSString *requestId = [NSString stringWithFormat:@"Request %@",[[[dictResponse objectForKey:kServiceResponseKey]objectAtIndex:0]valueForKey:@"suffix"]];
    [UIAlertController showAlertInVC:self withTitle:requestId withMsg:kAppoinmentBooked withOtherAlertActionTitle:@[kAlertOk] withAlertActionCancel:@"" withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
        if(buttonIndex == 0){
            [self.navigationController popToRootViewControllerAnimated:NO];
            AppContext.appTabVC.selectedIndex =1;
         
        }
    }];

}


-(void)fetchServiceByDate:(NSString *)date withTime:(NSString *)currentTime{
    
    if([ReachabilityManager isReachable]){
        
        NSInteger categoryId = AppContext.requestHelper.categoryId;
        NSInteger carTypeId = AppContext.requestHelper.carTypeId;
        NSInteger addressId = AppContext.requestHelper.addressId;
        double lat = AppContext.requestHelper.addressLatitude;
        double longitude = AppContext.requestHelper.addressLongitude;
        NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@?categoryId=%lu&carTypeId=%lu&addressId=%lu&language=%@&latitude=%f&longitude=%f&version=1&%@=%@&date=%@&currentTime=%@&userId=%@",kFetchServicesByDateWS,categoryId,carTypeId,addressId,AppContext.appLanguage,lat,longitude,kAppVersionServiceKey,[self fetchAppVersion],date,currentTime,userId];
        NSLog(@"Service Url: %@",strUrl);
      [[WebServiceInvocation alloc]initWithWS:strUrl withParams:nil withServiceType:TYPE_GET withSelector:@selector(serviceWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }
}


#pragma mark :----------: Segment Action
- (IBAction)segmnentAction:(id)sender {
    if(_serviceSegment.selectedSegmentIndex == 0){
        //supplier
        [self setUnsetColorForSupplierBtn:YES];
    }else{
        //date
        [self setUnsetColorForSupplierBtn:NO];
    }
}

-(void)setUnsetColorForSupplierBtn:(BOOL)isSupplierBtn{
    
    if(isSupplierBtn){
        pickerForService = NO;
        isViewByDateSelected = NO;
        strServiceDate = @"";
        [self fetchServices:YES];
        [self setSelectedDate:NO withDt:nil];
    }else{

        isViewByDateSelected = YES;
        pickerForService = YES;
        _lblSwipeForMore.hidden = YES;
        [self.arrServices removeAllObjects];
        [self.tblVwService reloadData];
        [self showDatePicker];
        
    }
}

-(void)setSelectedDate:(BOOL)isSet withDt:(NSDate *)dt{
    
    if(isSet){
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:DD_MM_YYYY];
        NSString *displayDate = [formatter stringFromDate:dt];
        [_btnSelectedDate setHidden:NO];
        _segmentVwHeightConst.constant = 80;
        [_btnSelectedDate setTitle:displayDate forState:UIControlStateNormal];
    }else{
        [_btnSelectedDate setHidden:YES];
        _segmentVwHeightConst.constant = 40;
        [_btnSelectedDate setTitle:@"" forState:UIControlStateNormal];
    }
 

}


@end
