//
//  AddressFromMapVC.h
//  CarSpa
//
//  Created by R@j on 08/08/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MVPlaceSearchTextField.h"
#import "WSUserAddresses.h"

@interface AddressFromMapVC : UIViewController


@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *txtPlaceSearch;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;

@property (weak, nonatomic) IBOutlet GMSMapView *gMapView;
@property (weak, nonatomic) IBOutlet UIImageView *imgPin;
@property (weak, nonatomic) IBOutlet UIView *vwTextField;
@property (weak, nonatomic) IBOutlet UIButton *btnNormalType;
@property (weak, nonatomic) IBOutlet UIButton *btnSatType;

@property (nonatomic,assign) BOOL isEditAddress;
@property (strong,nonatomic) WSUserAddresses *editAddressObj;

- (IBAction)btnSearchClick:(id)sender;
- (IBAction)btnMyLocationAction:(id)sender;
- (IBAction)btnSatVwAction:(id)sender;
- (IBAction)btnNormalAction:(id)sender;
@end
