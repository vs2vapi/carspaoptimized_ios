//
//  AddressFromMapVC.m
//  CarSpa
//
//  Created by R@j on 08/08/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "AddressFromMapVC.h"


#define METERS_PER_MILE 1609.344
@interface AddressFromMapVC ()<UITextFieldDelegate, PlaceSearchTextFieldDelegate,GMSMapViewDelegate>{
     NSMutableArray *addressList;
    CLLocationCoordinate2D searchPlace;
    NSString *addressDetails;
    double addrLatitude,addrLongitude;
    BOOL tappedOnMap;
}

@end

@implementation AddressFromMapVC

@synthesize isEditAddress;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark:--------------------: Basic Setups
-(void)setupUI{
  
    addressDetails = @"";
    _gMapView.delegate = self;
    tappedOnMap = NO;
    
    _txtPlaceSearch.placeSearchDelegate = self;
    _txtPlaceSearch.strApiKey = GOOGLE_API_KEY;
    _txtPlaceSearch.superViewOfList = self.view;
    _txtPlaceSearch.autoCompleteShouldHideOnSelection = true;
    _txtPlaceSearch.maximumNumberOfAutoCompleteRows = 5;
    
    _txtPlaceSearch.autoCompleteTableCornerRadius = 0.6f;
    _txtPlaceSearch.autoCompleteRowHeight = 35;
    _txtPlaceSearch.autoCompleteTableCellTextColor = [UIColor colorWithRed:163.0/255.0 green:165.0/255.0 blue:169.0/255.0 alpha:1.0];
    _txtPlaceSearch.autoCompleteFontSize = 13;
    _txtPlaceSearch.autoCompleteShouldHideOnSelection = true;
    _txtPlaceSearch.autoCompleteShouldHideClosingKeyboard = true;
    _txtPlaceSearch.autoCompleteShouldSelectOnExactMatchAutomatically = true;
    _txtPlaceSearch.autoCompleteTableFrame = CGRectMake(_vwTextField.frame.origin.x,CGRectGetMaxY(_vwTextField.frame)-6, _vwTextField.frame.size.width, 200.0);

    
    if(isEditAddress){
        [self showAddressLocation];
    }else{
        [self showCurrenctLocationOnMap];
    }
}

-(void)showCurrenctLocationOnMap{
    
    double userLat = [Functions getDoubleValueFromDefaults:USER_LATITUDE];
    double userLongitude = [Functions getDoubleValueFromDefaults:USER_LONGITUDE];
    CLLocation *location = [[CLLocation alloc]  initWithLatitude:userLat longitude:userLongitude];
    
    [self addMarkerOnMap:location];
}

-(void)showAddressLocation{
    
    CLLocation *location = [[CLLocation alloc]  initWithLatitude: _editAddressObj.latitude longitude: _editAddressObj.longitude];
    [self addMarkerOnMap:location];
}

-(void)addMarkerOnMap:(CLLocation *)location{
    
    [_gMapView clear];
    
    GMSCameraPosition *userCamPosition = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                                                     longitude:location.coordinate.longitude
                                                                          zoom:18];
    [_gMapView setCamera:userCamPosition];
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
    [marker setAppearAnimation:kGMSMarkerAnimationPop];
    marker.icon = [UIImage imageNamed:@"icn_red_pin"];
    [marker setTappable:YES];
    [marker setDraggable: YES];
    marker.map = _gMapView;
    
}


#pragma mark:--------------------:  Place search Textfield Delegates
- (void)placeSearchWillHideResult:(MVPlaceSearchTextField *)textField{
    
}

- (void)placeSearchWillShowResult:(MVPlaceSearchTextField *)textField{
    
}

- (void)placeSearch:(MVPlaceSearchTextField *)textField ResponseForSelectedPlace:(GMSPlace *)responseDict{
    searchPlace = responseDict.coordinate;
    CLLocation* location = [[CLLocation alloc]initWithLatitude:responseDict.coordinate.latitude longitude:responseDict.coordinate.longitude];
    tappedOnMap = YES;
    [self addMarkerOnMap:location];
}

- (void)placeSearch:(MVPlaceSearchTextField *)textField ResultCell:(UITableViewCell *)cell withPlaceObject:(PlaceObject *)placeObject atIndex:(NSInteger)index{
    
}

#pragma mark :---------:   Google Map Delegates
-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    [_txtPlaceSearch resignFirstResponder];
    CLLocation *mapLocation = [[CLLocation alloc]initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    
    tappedOnMap = YES;
    if(![_imgPin isHidden]){
        [self fetchCurrentAddress:mapLocation];
    }
    [_imgPin setHidden:YES];
    [self addMarkerOnMap:mapLocation];
    
}

-(void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture{
    [_gMapView clear];
    if(tappedOnMap){
        tappedOnMap = NO;
        [_imgPin setHidden:YES];
    }else{
        [_imgPin setHidden:NO];
    }
}

-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    
    CLLocation *mapLocation = [[CLLocation alloc]initWithLatitude:marker.position.latitude longitude:marker.position.longitude];
    //   [self fetchCurrentAddress:mapLocation];
    
    if(!isEditAddress){
        [self fetchCurrentAddress:mapLocation];
    }else{
        [self fetchCurrentAddress:mapLocation];
    }
    return YES;
}

-(void)fetchCurrentAddress:(CLLocation *)location{
    
    [self addMarkerOnMap:location];
    if([ReachabilityManager isReachable]){
        
        [RTProgressHUD showProgressWithMsg:kWSLoaderDialoagMsg];
        double lat = location.coordinate.latitude;
        double longitude =  location.coordinate.longitude;
        
        [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(lat,longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
            [RTProgressHUD dismiss];
            if (error) {
                [self showAlert:kLocationNotAvail];
                
            } else {
                for(GMSAddress* addressObj in [response results]){
                    
                    NSString * addressString =@"";
                    for (int i = 0; i < addressObj.lines.count; i++) {
                        addressString = [addressString stringByAppendingString:[NSString stringWithFormat:@"%@",[addressObj.lines objectAtIndex:i]]];
                    }
                    addressDetails = addressString;
                    addrLatitude = lat;
                    addrLongitude = longitude;
                    [self fetchUserAddress];
                    
                    break;
                }
                
            }
        }];
    }else{
        ShowNoNetworkAlert(self);
    }
}


-(void)fetchUserAddress{
    
    if([ReachabilityManager isReachable]){
        NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
        NSString *url = [NSString stringWithFormat:@"%@?userId=%@&%@=%@",kFetchUserAddressWS,userId,kAppVersionServiceKey,[self fetchAppVersion]];
        [[WebServiceInvocation alloc]initWithWS:url withParams:nil withServiceType:TYPE_GET withSelector:@selector(userAddressWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)userAddressWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        // [self showAlert:[sender responseErr]];
        [self showAddressAlert];
    }else{
        BOOL status = [[[sender responseDictionary]valueForKey:@"Status"]boolValue];
        if(status){
            __block BOOL isAddressExist = NO;
            NSString *currentAddress = addressDetails;
            [[sender responseArray]enumerateObjectsUsingBlock:^(WSUserAddresses *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSString *addressFromResponse = obj.address;
                if([currentAddress isEqualToString:addressFromResponse]){
                    isAddressExist = YES;
                    [self saveAddressInDB:obj];
                    *stop = YES;
                }
            }];
            if(!isAddressExist){
                if(!isEditAddress){
                    [self showAddressAlert];
                }else{
                    [self saveAddress:_editAddressObj.addresstitle];
                }
            }else{
                [self.navigationController popViewControllerAnimated:YES];
            }
        }else{
            [self showAddressAlert];
        }
        
    }
}

-(void)saveAddressInDB:(WSUserAddresses *)objAddress{
    [[SyncCDAdaptor sycnInstance]saveAddressDetails:objAddress];
}

-(void)saveAddress:(NSString *)addrTitle{
    
    
    if([ReachabilityManager isReachable]){
    
        NSString *strAddress = addressDetails;
        NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
        NSMutableDictionary *dictParams = [NSMutableDictionary dictionaryWithDictionary:@{@"userId":userId,
                                                                                          @"address":strAddress,
                                                                                          @"latitude":[NSNumber numberWithDouble:addrLatitude],
                                                                                          @"longitude":[NSNumber numberWithDouble:addrLongitude],
                                                                                          @"addresstitle":addrTitle
                                                                                          }];
        
        NSString *url = @"";
        if(isEditAddress){
            url = kUpdateAddressWS;
            [dictParams setValue:[NSNumber numberWithInteger:_editAddressObj.internalBaseClassIdentifier] forKey:@"addressId"];
        }else{
           url =   kAddUserAddressWS;
        }
     
        [[WebServiceInvocation alloc]initWithWS:url withParams:dictParams withServiceType:TYPE_POST withSelector:@selector(addUserAddressWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }
    
}

-(void)addUserAddressWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        [[NSNotificationCenter defaultCenter]postNotificationName:DETAILS_ADDED object:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)showAddressAlert{
    
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:kAddressNickName
                                                                              message: @""
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = [NSString stringWithFormat:@"%@ %@",kEnter,kAddressNickName];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:kAlertOk style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * namefield = textfields[0];
        
        if([namefield.text isEqualToString:@""]){
            [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kAddressNickName]];
        }else{
            [self saveAddress:namefield.text];
        }
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

#pragma mark :---------:   Button Actions

- (IBAction)btnMyLocationAction:(id)sender {
    tappedOnMap = YES;
    [self showCurrenctLocationOnMap];
}

- (IBAction)btnSearchClick:(id)sender{
    
    if([_txtPlaceSearch.text isEqualToString:@""]){
        [self showAlert:kEnterParticularAddr];
        return;
    }
    if (CLLocationCoordinate2DIsValid(searchPlace)){
        //        self.mapView.centerCoordinate = searchPlace;
        CLLocationCoordinate2D centre = searchPlace;
        CLLocation *location = [[CLLocation alloc]initWithLatitude:centre.latitude longitude:centre.longitude];
        // [self fetchCurrentAddress:location];
        tappedOnMap = YES;
        [self addMarkerOnMap:location];
        //   [self reverseGeocode:location];
    }
    
}


- (IBAction)btnSatVwAction:(id)sender {
    [_btnNormalType setSelected:NO];
    [_btnSatType setSelected:YES];
    _gMapView.mapType = kGMSTypeHybrid;
}
- (IBAction)btnNormalAction:(id)sender {
    [_btnNormalType setSelected:YES];
    [_btnSatType setSelected:NO];
    _gMapView.mapType = kGMSTypeNormal;
}

@end
