//
//  MapVC.h
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MVPlaceSearchTextField.h"

@interface MapVC : UIViewController


@property (strong, nonatomic) IBOutlet MVPlaceSearchTextField *txtPlaceSearch;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property (weak, nonatomic) IBOutlet UIView *viewTowDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblAdditionalCharges;
@property (weak, nonatomic) IBOutlet UILabel *lblAdditionalDiscription;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalCharge;
@property (weak, nonatomic) IBOutlet UIView *vwTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomVwHeightConst;
@property (strong, nonatomic) IBOutlet UIImageView *imgVwPin;
@property (weak, nonatomic) IBOutlet GMSMapView *gMapView;
@property (weak, nonatomic) IBOutlet UIButton *btnNormalType;
@property (weak, nonatomic) IBOutlet UIButton *btnSatType;

@property(nonatomic,assign)BOOL isServeNow;
@property(nonatomic,assign)BOOL isEditRequest;

- (IBAction)btnCurrenctLocAction:(id)sender;
- (IBAction)btnSatVwAction:(id)sender;
- (IBAction)btnNormalAction:(id)sender;
- (IBAction)btnCancelClicked:(id)sender;
- (IBAction)btnOkClicked:(id)sender;
- (IBAction)btnSearchClick:(id)sender;
@end
