//
//  MapVC.m
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "MapVC.h"
#import "AppointmentsListVC.h"


@interface MapVC ()<PlaceSearchTextFieldDelegate,GMSMapViewDelegate,PaymentPopUpDelegate,RTDatePickerDelegate>{
    
    NSInteger intMaxDistance,intVariableAmount,distanceInKiloMeter;
    double totalCharge;
    BOOL tappedOnMap,isNotFirstTimeRegionSet;
    int dateCounter;
    CLLocationCoordinate2D searchPlace;
    NSString *strDistance;
}

@end

@implementation MapVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    if(_isEditRequest){
        [self showTowAddressOnMap];
    }else{
        [self showCurrenctLocationOnMap];
    }

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark:--------------------: Basic Setups
-(void)setupUI{
    
    _gMapView.delegate = self;
    _txtPlaceSearch.placeSearchDelegate = self;
    _txtPlaceSearch.strApiKey = GOOGLE_API_KEY;
    _txtPlaceSearch.superViewOfList = self.view;
    _txtPlaceSearch.autoCompleteShouldHideOnSelection = true;
    _txtPlaceSearch.maximumNumberOfAutoCompleteRows = 5;
    _vwTextField.layer.cornerRadius = 6.0f;
    
    
    _txtPlaceSearch.autoCompleteTableCornerRadius = 0.6f;
    _txtPlaceSearch.autoCompleteRowHeight = 35;
    _txtPlaceSearch.autoCompleteTableCellTextColor = [UIColor colorWithRed:163.0/255.0 green:165.0/255.0 blue:169.0/255.0 alpha:1.0];
    _txtPlaceSearch.autoCompleteFontSize = 13;
    _txtPlaceSearch.autoCompleteShouldHideOnSelection = true;
    _txtPlaceSearch.autoCompleteShouldHideClosingKeyboard = true;
    _txtPlaceSearch.autoCompleteShouldSelectOnExactMatchAutomatically = true;
    _txtPlaceSearch.autoCompleteTableFrame = CGRectMake(_vwTextField.frame.origin.x,CGRectGetMaxY(_vwTextField.frame)-6, _vwTextField.frame.size.width, 200.0);

}

-(void)showCurrenctLocationOnMap{
    
    double userLat   =  AppContext.requestHelper.addressLatitude;//[Functions getDoubleValueFromDefaults:USER_LATITUDE];
    double userLongitude =AppContext.requestHelper.addressLongitude; //[Functions getDoubleValueFromDefaults:USER_LONGITUDE];
    
    CLLocation *location = [[CLLocation alloc]  initWithLatitude:userLat longitude:userLongitude];
    [self addMarkerOnMap:location];
  
}

-(void)showTowAddressOnMap{
    
    double   userLat = AppContext.requestHelper.towAddressLatitude;
    double   userLongitude = AppContext.requestHelper.towAddressLongitude;
    
    CLLocation *location = [[CLLocation alloc]  initWithLatitude:userLat longitude:userLongitude];
    [self addMarkerOnMap:location];
}

#pragma mark :---------:   Google Map Delegates
-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    [_txtPlaceSearch resignFirstResponder];
    CLLocation *mapLocation = [[CLLocation alloc]initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    
    tappedOnMap = YES;
    if(![_imgVwPin isHidden]){
        [self fetchCurrentAddress:mapLocation];
    }
    
    [_imgVwPin setHidden:YES];
    isNotFirstTimeRegionSet = YES;
    [self addMarkerOnMap:mapLocation];
    
}


-(void)addMarkerOnMap:(CLLocation *)location{
    
    
    isNotFirstTimeRegionSet = YES;
    [_gMapView clear];
    [_imgVwPin setHidden:YES];
    
    GMSCameraPosition *userCamPosition = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                                                     longitude:location.coordinate.longitude
                                                                          zoom:18];
    [_gMapView setCamera:userCamPosition];
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
    [marker setAppearAnimation:kGMSMarkerAnimationPop];
    marker.icon = [UIImage imageNamed:@"icn_red_pin"];
    [marker setTappable:YES];
    [marker setDraggable: YES];
    marker.map = _gMapView;
    
}


-(void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture{
    [_gMapView clear];
    if(tappedOnMap){
        tappedOnMap = NO;
        [_imgVwPin setHidden:YES];
    }else{
        [_imgVwPin setHidden:NO];
    }
}


-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    
    CLLocation *mapLocation = [[CLLocation alloc]initWithLatitude:marker.position.latitude longitude:marker.position.longitude];
    [self fetchCurrentAddress:mapLocation];
    return YES;
}




#pragma mark :---------:   Place search Textfield Delegates
- (void)placeSearchWillHideResult:(MVPlaceSearchTextField *)textField{
    
}

- (void)placeSearchWillShowResult:(MVPlaceSearchTextField *)textField{
    
}

- (void)placeSearch:(MVPlaceSearchTextField *)textField ResponseForSelectedPlace:(GMSPlace *)responseDict{
    
    searchPlace = responseDict.coordinate;
    CLLocation* location = [[CLLocation alloc]initWithLatitude:responseDict.coordinate.latitude longitude:responseDict.coordinate.longitude];
    tappedOnMap = YES;
    [self addMarkerOnMap:location];
    
}

- (void)placeSearch:(MVPlaceSearchTextField *)textField ResultCell:(UITableViewCell *)cell withPlaceObject:(PlaceObject *)placeObject atIndex:(NSInteger)index{
    
}


-(void)fetchCurrentAddress:(CLLocation *)location{
    
    [self addMarkerOnMap:location];
    if([ReachabilityManager isReachable]){
        
        [RTProgressHUD showProgressWithMsg:kWSLoaderDialoagMsg];
        double lat = location.coordinate.latitude;
        double longitude =  location.coordinate.longitude;
        
        [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(lat,longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
            [RTProgressHUD dismiss];
            if (error) {
                [self showAlert:kLocationNotAvail];
                
            } else {
                for(GMSAddress* addressObj in [response results]){
                    
                    NSString * addressString =@"";
                    for (int i = 0; i < addressObj.lines.count; i++) {
                        addressString = [addressString stringByAppendingString:[NSString stringWithFormat:@"%@",[addressObj.lines objectAtIndex:i]]];
                    }
                    
                    AppContext.requestHelper.towAddress = addressString;
                    AppContext.requestHelper.towAddressLatitude = lat;
                    AppContext.requestHelper.towAddressLongitude = longitude;
                    
                    if(AppContext.requestHelper.isTowService){
                        if(isNotFirstTimeRegionSet){
                            [self fetchDistanceFromTwoLocation];
                            isNotFirstTimeRegionSet = false;
                        }
                    }
                    break;
                }
                
            }
        }];
    }else{
        ShowNoNetworkAlert(self);
    }
}


-(void)fetchDistanceFromTwoLocation{
    

    if([ReachabilityManager isReachable]){
        
        
        NSString * url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f&mode=driving&language=pl-PL",AppContext.requestHelper.towAddressLatitude,AppContext.requestHelper.towAddressLongitude,AppContext.requestHelper.addressLatitude,AppContext.requestHelper.addressLongitude];
        
         [[WebServiceInvocation alloc]initWithWS:url withParams:nil withServiceType:TYPE_GET withSelector:@selector(distanceWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
    }else{
        ShowNoNetworkAlert(self);
    }
    
}

-(void)distanceWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        
         NSInteger duration =[[[[[[[[sender responseDictionary] valueForKey:@"rows"] objectAtIndex:0] valueForKey:@"elements"] objectAtIndex:0] valueForKey:@"duration"] valueForKey:@"value"]integerValue];
        
        double distance = [NSString stringWithFormat:@"%@",[[[[[[[sender responseDictionary]  valueForKey:@"rows"] objectAtIndex:0] valueForKey:@"elements"] objectAtIndex:0] valueForKey:@"distance"] valueForKey:@"value"]].doubleValue;
         NSInteger intTest = duration / 60;
        
          AppContext.requestHelper.serviceDuration = intTest;
    
        distanceInKiloMeter = round(distance/1000); //rounding kilometer
        
        strDistance =  [NSString stringWithFormat:@"%@",[[[[[[[sender responseDictionary] valueForKey:@"rows"] objectAtIndex:0] valueForKey:@"elements"] objectAtIndex:0] valueForKey:@"distance"] valueForKey:@"text"]];
        
        if([strDistance containsString:@"null"]){
            [self showAlert:kEnterParticularAddr];
        }else{
           [self setValueOfTowDetailView];
        }
    }
}

-(void)setValueOfTowDetailView{
    
     [self.viewTowDetail setHidden:false];
    NSString *strExRate = [NSString stringWithFormat:@"%.2f",AppContext.exchangeRate];
//    double serviceCost  = AppContext.requestHelper.totalLocalAmount * [strExRate floatValue];
    double serviceCost  = AppContext.requestHelper.serviceAmount * [strExRate floatValue];
    serviceCost = round(serviceCost);
    
    _lblServiceAmount.text = [NSString stringWithFormat:@"%.2f %@",serviceCost,AppContext.currency];
   _lblDistance.text    = [NSString stringWithFormat:@"(%@) %@",kAppox,strDistance];
    
   _lblAdditionalDiscription.text = [NSString stringWithFormat:@"%@ %@ %.2f %@/km.",kTowCharges,kWeWillCharge,[self setConvertedAmout:AppContext.requestHelper.variableAmount roundAmount:NO],AppContext.currency];

    NSInteger intAdditionalKm = distanceInKiloMeter;
    double intVaribleAmount = AppContext.requestHelper.variableAmount;
    double  intAdditionalCharge = intAdditionalKm * intVaribleAmount;
    
    double additionalCost  = intAdditionalCharge * [strExRate floatValue];
    additionalCost = round(additionalCost);
    _lblAdditionalCharges.text = [NSString stringWithFormat:@"%.2f %@",additionalCost,AppContext.currency];
    
//   totalCharge = AppContext.requestHelper.totalLocalAmount + intAdditionalCharge;
    totalCharge = AppContext.requestHelper.serviceAmount + intAdditionalCharge;
    
    double roundTotalcharge  = totalCharge * [strExRate floatValue];
    roundTotalcharge = round(roundTotalcharge);
    
    AppContext.requestHelper.totalAmount = totalCharge;
    AppContext.requestHelper.totalLocalAmount = totalCharge;
    _lblTotalCharge.text = [NSString stringWithFormat:@"%.2f %@",roundTotalcharge,AppContext.currency];
  
}

-(float)setConvertedAmout:(float)amount roundAmount:(BOOL)isRound{
    
    amount = amount * AppContext.exchangeRate;
    if(isRound)
        amount = round(amount);
    
    return amount;
}

#pragma mark:--------------------: Button Actions
- (IBAction)btnCurrenctLocAction:(id)sender{
    tappedOnMap = YES;
    [self showCurrenctLocationOnMap];
}

- (IBAction)btnSatVwAction:(id)sender{
    [_btnNormalType setSelected:NO];
    [_btnSatType setSelected:YES];
    _gMapView.mapType = kGMSTypeHybrid;
}

- (IBAction)btnNormalAction:(id)sender{
    [_btnNormalType setSelected:YES];
    [_btnSatType setSelected:NO];
    _gMapView.mapType = kGMSTypeNormal;
    
}

- (IBAction)btnCancelClicked:(id)sender{
    [_viewTowDetail setHidden:YES];
}

- (IBAction)btnOkClicked:(id)sender{

      [_viewTowDetail setHidden:YES];
    if(_isEditRequest){
        [[NSNotificationCenter defaultCenter]postNotificationName:EDIT_REQUEST_TOW_ADDRESS_CHANGED object:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        if(_isServeNow){
            [self showPopUpVC];
        }else{
            [self showDatePicker];
        }
    }
 
}

- (IBAction)btnSearchClick:(id)sender{
    if([_txtPlaceSearch.text isEqualToString:@""]){
        [self showAlert:kEnterParticularAddr];
        return;
    }
    if (CLLocationCoordinate2DIsValid(searchPlace)){
        CLLocationCoordinate2D centre = searchPlace;
        CLLocation *location = [[CLLocation alloc]initWithLatitude:centre.latitude longitude:centre.longitude];
        tappedOnMap = YES;
        [self addMarkerOnMap:location];
    }

}

#pragma mark :----------: Picker Delegates

-(void)showDatePicker{

    RTDatePickerVC *rtdpvc = [[RTDatePickerVC alloc]initWithFrame:self.view.bounds];
    rtdpvc.delegate = self;
    rtdpvc.showHolidayList = YES;
    rtdpvc.arrSeviceId = AppContext.requestHelper.arrServiceId;
    [rtdpvc setMinDate:[NSDate date]];
    [self.navigationController.view addSubview:rtdpvc];
    
}


-(void)pickerSelectedDate:(NSDate *)selectedDate{
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:YYYY_MM_dd];
    NSString *strDate = [formatter stringFromDate:selectedDate];
    AppContext.requestHelper.appointmentDate = strDate;//[NSString stringWithFormat:@"%@",selectedDate];
    AppointmentsListVC *aplvc = loadViewController(kSBRequest, kAppointmentsListVC);
    [self.navigationController pushViewController:aplvc animated:YES];
}

-(void)showPopUpVC{
    PaymentPopUpVC *pvc = [[PaymentPopUpVC alloc] initWithNibName:@"PaymentPopUpVC" bundle:nil];
    pvc.isServeNow = YES;
    pvc.delegate = self;
    [self presentPopupViewController:pvc animationType:MJPopupViewAnimationFade];
}

#pragma mark :----------: Popup Delegates
- (void)cancelButtonClicked:(PaymentPopUpVC *)paymentVC{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    if(paymentVC.cancelRequest){
        [self cancelRequest];
        return;
    }
    if(paymentVC.addAppointment){
        [self addAnotherAppointment];
        return;
    }
    if(paymentVC.addService){
        [self addAnotherServices];
        return;
    }
}

-(void)cancelRequest{
    //Back Changes
    [AppContext.requestHelper deallocInstance];
    [self addAnotherServices];
//    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)addAnotherAppointment{
    [self showDatePicker];
}

-(void)noTimeSlotFound{
    [self showDatePicker];
}

-(void)addAnotherServices{
    NSArray *array = [self.navigationController viewControllers];
    [self.navigationController popToViewController:[array objectAtIndex:2] animated:YES];
}


-(void)checkedOutSuccessfully:(NSMutableDictionary *)dictResponse{
    
    NSString *requestId = [NSString stringWithFormat:@"Request %@",[[[dictResponse objectForKey:kServiceResponseKey]objectAtIndex:0]valueForKey:@"suffix"]];
    [UIAlertController showAlertInVC:self withTitle:requestId withMsg:kAppoinmentBooked withOtherAlertActionTitle:@[kAlertOk] withAlertActionCancel:@"" withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
        if(buttonIndex == 0){
            
//            CATransition* transition = [CATransition animation];
//            transition.duration = 0.25;
//            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//            transition.type = kCATransitionFade; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
//            transition.subtype =kCATransitionFromBottom;// kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
            [self.navigationController popToRootViewControllerAnimated:NO];
            AppContext.appTabVC.selectedIndex =1;
            
        }
    }];
    
}

@end
