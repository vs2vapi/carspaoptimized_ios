//
//  AddCarsVC.h
//  CarSpa
//
//  Created by R@j on 01/08/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSCar.h"

@interface AddCarsVC : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtBrand;
@property (weak, nonatomic) IBOutlet UITextField *txtModel;
@property (weak, nonatomic) IBOutlet UITextField *txtRegisteration;
@property (weak, nonatomic) IBOutlet UIView *containerVw;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property(nonatomic,assign)BOOL isEditCars;
@property(nonatomic,strong)WSCar *objSelCar;

- (IBAction)btnCancelAction:(id)sender;
- (IBAction)btnSaveAction:(id)sender;

@end
