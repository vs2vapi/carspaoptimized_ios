//
//  AddCarsVC.m
//  CarSpa
//
//  Created by R@j on 01/08/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "AddCarsVC.h"

@interface AddCarsVC ()

@end

@implementation AddCarsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark:--------------------: Basic Setup
-(void)setupUI{
    
    _containerVw.layer.cornerRadius = 6;
    _containerVw.clipsToBounds = YES;
    
    _btnSave.layer.cornerRadius = 5;
    _btnSave.clipsToBounds = YES;
    
    _btnCancel.layer.cornerRadius = 5;
    _btnCancel.clipsToBounds = YES;
    
    _txtBrand.layer.borderWidth = 1;
    _txtBrand.layer.borderColor = [UIColor grayColor].CGColor;
    _txtBrand.layer.cornerRadius = 3;
    _txtBrand.clipsToBounds = YES;
    
    _txtModel.layer.borderWidth = 1;
   _txtModel.layer.borderColor = [UIColor grayColor].CGColor;
   _txtModel.layer.cornerRadius = 3;
    _txtModel.clipsToBounds = YES;
    
    _txtRegisteration.layer.borderWidth = 1;
    _txtRegisteration.layer.borderColor = [UIColor grayColor].CGColor;
    _txtRegisteration.layer.cornerRadius = 3;
    _txtRegisteration.clipsToBounds = YES;
    
    [self setPaddingForTextField];
    
    if(_isEditCars){
        _txtModel.text = [NSString stringWithFormat:@"%@",_objSelCar.model];
        _txtBrand.text = [NSString stringWithFormat:@"%@",_objSelCar.brand];
        _txtRegisteration.text =[NSString stringWithFormat:@"%@", _objSelCar.plateNumber];
    }
}

-(void)setPaddingForTextField{
    
    UIView *paddingVwBrand = [[UIView alloc]initWithFrame:CGRectMake(0, 0,8, _txtBrand.frame.size.height)];
    [paddingVwBrand setBackgroundColor:[UIColor clearColor]];
    
    [_txtBrand setLeftViewMode:UITextFieldViewModeAlways];
    [_txtBrand setLeftView:paddingVwBrand];
    
    UIView *paddingVwModel = [[UIView alloc]initWithFrame:CGRectMake(0, 0,8, _txtModel.frame.size.height)];
    [paddingVwModel setBackgroundColor:[UIColor clearColor]];
    
    [_txtModel setLeftViewMode:UITextFieldViewModeAlways];
    [_txtModel setLeftView:paddingVwModel];
    
    
    UIView *paddingVwReg = [[UIView alloc]initWithFrame:CGRectMake(0, 0,8, _txtRegisteration.frame.size.height)];
    [paddingVwReg setBackgroundColor:[UIColor clearColor]];
    
    [_txtRegisteration setLeftViewMode:UITextFieldViewModeAlways];
    [_txtRegisteration setLeftView:paddingVwReg];
    
}

#pragma mark:--------------------: Button Actions
- (IBAction)btnCancelAction:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSaveAction:(id)sender {
    if([_txtBrand.text isEqualToString:@""]){
        [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kVehicleBrand]];
        return;
    }
   else if([_txtModel.text isEqualToString:@""]){
         [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kVehicleModel]];
        return;
    }
   else{
       [self saveCarsDetails];
   }
}

#pragma mark:--------------------: WebService Functions
-(void)saveCarsDetails{
    
    if([ReachabilityManager isReachable]){
        
        NSString *strUrl = kAddUserCarWS;
        NSString *strUserId = [Functions getStringValueFromDefaults:USER_ID];
        NSMutableDictionary *dictParams = [NSMutableDictionary dictionaryWithDictionary:@{@"userId":strUserId,
                                                                                          @"brand":_txtBrand.text,
                                                                                          @"model":_txtModel.text,
                                                                                          @"plateNumber":_txtRegisteration.text
                                                                                          }];
        
        if(_isEditCars){
            [dictParams setValue:[NSNumber numberWithInteger:_objSelCar.carIdentifier] forKey:@"carId"];
            strUrl = kUpdateUserCarWS;
        }
        
        [[WebServiceInvocation alloc]initWithWS:strUrl withParams:dictParams withServiceType:TYPE_POST withSelector:@selector(carDetailsWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }
    
}

-(void)carDetailsWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        [[NSNotificationCenter defaultCenter]postNotificationName:DETAILS_ADDED object:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)saveCars{
    
 /*   UserModel *user = [UserPrefernce getUserModelFromPreference:KEY_UserModel];
    int UserId = user.Id;
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    if(isEditCars){
        [SVProgressHUD showWithStatus:NSLocalizedString(kLK_PLEASE_WAIT, @"")];
    }else{
        [SVProgressHUD showWithStatus:NSLocalizedString(kLK_PLEASE_WAIT, @"")];
    }
    
    NSString * loginUrl = @"";
    
    if(isEditCars){
        loginUrl = [NSString stringWithFormat:@"%@%@",SERVER_PATH,USER_UPDATE_CARS];
    }else{
        loginUrl = [NSString stringWithFormat:@"%@%@",SERVER_PATH,USER_ADD_CARS];
    }
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:loginUrl parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if(isEditCars){
            [formData appendPartWithFormData:[[NSString stringWithFormat:@"%d",editCarModel.Id] dataUsingEncoding:NSUTF8StringEncoding] name:@"carId"];
        }
        [formData appendPartWithFormData:[[NSString stringWithFormat:@"%d",UserId] dataUsingEncoding:NSUTF8StringEncoding] name:@"userId"];
        [formData appendPartWithFormData:[[NSString stringWithFormat:@"%@",carsModel.brand] dataUsingEncoding:NSUTF8StringEncoding] name:@"brand"];
        [formData appendPartWithFormData:[[NSString stringWithFormat:@"%@",carsModel.model] dataUsingEncoding:NSUTF8StringEncoding] name:@"model"];
        [formData appendPartWithFormData:[[NSString stringWithFormat:@"%@",carsModel.plateNumber] dataUsingEncoding:NSUTF8StringEncoding] name:@"plateNumber"];
        
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      dispatch_async(dispatch_get_main_queue(), ^{
                          //Update the progress view
                          //[progressView setProgress:uploadProgress.fractionCompleted];
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      [SVProgressHUD dismiss];
                      BOOL status = [[responseObject objectForKey:@"Status"]boolValue];
                      if(status){
                          [[NSUserDefaults standardUserDefaults]setBool:YES forKey:KEY_CAR_ADDED];
                          [self.navigationController popViewControllerAnimated:YES];
                      }else{
                          [[NSUserDefaults standardUserDefaults]setBool:NO forKey:KEY_CAR_ADDED];
                          [Utility showAlertMessage_Title:NSLocalizedString(kLK_CAR, @"") Message:[responseObject objectForKey:@"Message"] PresentViewController:self];
                      }
                  }];
    [uploadTask resume];
    */
}
@end
