//
//  AppointmentsListVC.h
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDSegmentedControl.h"
#import "WSAppointmentList.h"

@protocol AppointmentsListVCDelegate <NSObject>

-(void)updateRequest:(WSAppointmentList *)objList;

@end

@interface AppointmentsListVC : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet SDSegmentedControl *dateSegmentControl;
@property (weak, nonatomic) IBOutlet UITableView *tblVwAppointment;
- (IBAction)segmentValueAction:(id)sender;
@property(nonatomic,assign)BOOL isEditRequest;
@property(nonatomic,strong)NSMutableArray *arrTimeSlots;
@property(nonatomic,strong)NSMutableArray *arrHolidayList;
@property(nonatomic,strong)NSMutableArray *arrDates;

@property(nonatomic,strong)NSString *selDate;
@property(nonatomic,strong)NSString *serviceIds;
@property(nonatomic,assign) NSInteger duration;
@property(nonatomic,assign) NSInteger ownerId;
@property(nonatomic,assign) NSInteger addressId;
@property(nonatomic,strong)id<AppointmentsListVCDelegate>delegate;
@end
