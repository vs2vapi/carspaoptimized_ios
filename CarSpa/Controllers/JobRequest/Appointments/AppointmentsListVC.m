//
//  AppointmentsListVC.m
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "AppointmentsListVC.h"
#import "CarSpa-Swift.h"
#import "AppointmentCell.h"
#import "WSHolidayList.h"


@interface AppointmentsListVC () <PaymentPopUpDelegate>{
    WSAppointmentList *selectedList;
}

@end

@implementation AppointmentsListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self addNewSegments];
    if(_isEditRequest){
         [self fetchAppointmentList:_selDate];
    }else{
         [self fetchAppointmentList:AppContext.requestHelper.appointmentDate];
    }
   
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark:--------------------: Basic Setups
-(void)setupUI{
    
//    if(!_isEditRequest){
        _arrHolidayList = AppContext.requestHelper.arrHolidayList;
//    }
    
     _tblVwAppointment.separatorColor = [UIColor clearColor];
    _tblVwAppointment.tableFooterView = [UIView new];
    
    _dateSegmentControl.selectedSegmentIndex = 0;
    _dateSegmentControl.titleFont = Dax_Regular(15);
    _dateSegmentControl.selectedTitleFont = Dax_Regular(15);
    _arrDates = [NSMutableArray new];
    
}

- (void)addNewSegments{

     NSString *dateString =@"";
    if(_isEditRequest){
        dateString = _selDate;
    }else{
         dateString = AppContext.requestHelper.appointmentDate;
    }
  
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [self setLocaleForDateFormatter:format];
    
    [format setDateFormat:yyyy_MM_dd];
    
    NSDate *date = [format dateFromString:dateString];
    [format setDateFormat:dd_MMM_YYYY];
    NSString* finalDateStr = [format stringFromDate:date];
    [_arrDates addObject:finalDateStr];
    
    [format setDateFormat:EE_dd_MMM];
    finalDateStr = [format stringFromDate:date];
    
    [self.dateSegmentControl setTitle:finalDateStr  forSegmentAtIndex:0];

    NSString *endDate = [self getDate:dateString byAddingDays:15];
    NSArray *arr = [self getDatesBetweenDates:dateString andEnd:endDate];
    for (int i=0; i < 15; i++) {
        
        NSString *dateString = [arr objectAtIndex:i];
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [self setLocaleForDateFormatter:format];
        
        [format setDateFormat:yyyy_MM_dd];
        NSDate *date = [format dateFromString:dateString];
        [format setDateFormat:dd_MMM_YYYY];
        
        NSString* finalDateString = [format stringFromDate:date];
        
        //Discard date which is
        if(_arrHolidayList.count >0){
            
            //checking for date
            __block BOOL addDateinList = YES;
            [_arrHolidayList enumerateObjectsUsingBlock:^(WSHolidayList   *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                
                //date checking
                if(obj.holidayType == 2){
                    [format setDateFormat:yyyy_MM_dd];
                    NSString *selectedDate = [format stringFromDate:date];
                    NSString *holidayDate =  [NSString stringWithFormat:@"%@",obj.date];
                    if([holidayDate isEqualToString:selectedDate]){
                        *stop = YES;
                        addDateinList = NO;
                    }

                }
               else  if(obj.holidayType == 1){
                   //day checking
                   [format setDateFormat:@"EEEE"];
                   NSString *dayName = [format stringFromDate:date];
                   NSString *holidayDay = [NSString stringWithFormat:@"%@",obj.day];
                   if([holidayDay isEqualToString:dayName]){
                       addDateinList = NO;
                       *stop = YES;
                   }

               }
                
            }];
            
            if(addDateinList){
                
//                [self.dateSegmentControl insertSegmentWithTitle:finalDateString atIndex:self.dateSegmentControl.numberOfSegments - 1 animated:YES];
                [_arrDates addObject:finalDateString];
                NSString *dateInString = [self displayDateInString:date];
                [self.dateSegmentControl insertSegmentWithTitle:dateInString atIndex:self.dateSegmentControl.numberOfSegments - 1 animated:YES];
            }
            
        }else{
              NSString *dateInString = [self displayDateInString:date];
            [self.dateSegmentControl insertSegmentWithTitle:dateInString atIndex:self.dateSegmentControl.numberOfSegments - 1 animated:YES];
            [_arrDates addObject:finalDateString];
//            [self.dateSegmentControl insertSegmentWithTitle:finalDateString atIndex:self.dateSegmentControl.numberOfSegments - 1 animated:YES];

        }
        
    }
    
}

-(void)fetchAppointmentList:(NSString *)appointmentDate{
    
    if([ReachabilityManager isReachable]){
        
        NSDateFormatter *formatter = [NSDateFormatter new];
        [self setENLocaleForDateFormatter:formatter]; //new changes
        [formatter setDateFormat:yyyy_MM_dd];
        
        NSDate *selectedDate = [formatter dateFromString:appointmentDate];
        NSString *timeString = @"00:00:00";
        
        if([self compareDateOnly:selectedDate] == NSOrderedSame){
            [formatter setDateFormat:@"HH:mm:ss"];
            timeString = [formatter stringFromDate:[NSDate date]];
        }
        
        NSString *strUrl = @"";
        if(_isEditRequest){
            NSString *userId =  [Functions getStringValueFromDefaults:USER_ID];
            strUrl = [NSString stringWithFormat:@"%@?requestDate=%@&userId=%@&duration=%lu&addressId=%lu&serviceIds=%@&currentTime=%@&language=%@&%@=%@&ownerId=%lu",kFetchAppointmentListWS,appointmentDate,userId,_duration,_addressId,_serviceIds,timeString,AppContext.appLanguage,kAppVersionServiceKey,[self fetchAppVersion],_ownerId];

        }else{
            
            if(AppContext.requestHelper.arrAppointments.count > 0){
                
                NSDictionary *dictAppointments = [AppContext.requestHelper.arrAppointments lastObject];
                NSString *vanId = [NSString stringWithFormat:@"%@",[dictAppointments valueForKey:@"vanId"]];
                NSString *lastDateTime = [NSString stringWithFormat:@"%@",[dictAppointments valueForKey:@"appointmentDatetime"]];
                NSString *lastduration = [NSString stringWithFormat:@"%@",[dictAppointments valueForKey:@"duration"]];
                
                NSLog(@"Last VanId : %@",vanId);
                NSLog(@"Last Date Time :  %@",lastDateTime);
                NSLog(@"Last Duration :  %@",lastduration);
                
                NSDateFormatter *formatter = [NSDateFormatter new];
                [formatter setDateFormat:YYYY_MM_dd];
                
                NSDate *selectedDate = [formatter dateFromString:appointmentDate];
                NSString *timeString = @"00:00:00";
                
                if([self compareDateOnly:selectedDate] == NSOrderedSame){
                    [formatter setDateFormat:@"HH:mm:ss"];
                    timeString = [formatter stringFromDate:[NSDate date]];
                }
                
                NSString *serviceId = [AppContext.requestHelper.arrServiceId componentsJoinedByString:@","];
                NSString *userId =  [Functions getStringValueFromDefaults:USER_ID];
                
                strUrl = [NSString stringWithFormat:@"%@?requestDate=%@&userId=%@&duration=%lu&addressId=%lu&serviceIds=%@&currentTime=%@&language=%@&%@=%@&vanId=%@&lastdatetime=%@&lastduration=%@&ownerId=%lu",kFetchOtherAppointmentListWS,appointmentDate,userId,AppContext.requestHelper.serviceDuration,AppContext.requestHelper.addressId,serviceId,timeString,AppContext.appLanguage,kAppVersionServiceKey,[self fetchAppVersion],vanId,lastDateTime,lastduration,AppContext.requestHelper.ownerId];
                
            }else{
                
                NSString *serviceId = [AppContext.requestHelper.arrServiceId componentsJoinedByString:@","];
                NSString *userId =  [Functions getStringValueFromDefaults:USER_ID];
                
                strUrl = [NSString stringWithFormat:@"%@?requestDate=%@&userId=%@&duration=%lu&addressId=%lu&serviceIds=%@&currentTime=%@&language=%@&%@=%@&ownerId=%lu",kFetchAppointmentListWS,appointmentDate,userId,AppContext.requestHelper.serviceDuration,AppContext.requestHelper.addressId,serviceId,timeString,AppContext.appLanguage,kAppVersionServiceKey,[self fetchAppVersion],AppContext.requestHelper.ownerId];
            }
            
        }
        
        NSLog(@"Appointment URL : %@",strUrl);
        [[WebServiceInvocation alloc]initWithWS:strUrl withParams:nil withServiceType:TYPE_GET withSelector:@selector(appointmentWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
 
        
    }else{
        ShowNoNetworkAlert(self);
    }

}

-(void)appointmentWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
        [_arrTimeSlots removeAllObjects];
        [_tblVwAppointment reloadData];
    }else{

        _arrTimeSlots = [NSMutableArray arrayWithArray:[sender responseArray]];
        [self.tblVwAppointment reloadData];
    }
}

#pragma mark:--------------------: Tableview Datasource & Delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrTimeSlots.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 58;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static  NSString *cellIdentifier = @"Cell";
    AppointmentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
         cell = [[AppointmentCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    WSAppointmentList *objList = [_arrTimeSlots objectAtIndex:indexPath.row];
   
    
    NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"HH:mm:ss"];
    NSDate *newDate  =  [formatter dateFromString: objList.time];
    if([AppContext.appLanguage isEqualToString:@"ar"]){
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ar"]];
    }
    [formatter setDateFormat:@"hh:mm a"];
    NSString *formattedDate = [formatter stringFromDate:newDate];
     cell.lblTime.text = formattedDate;

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    selectedList = [_arrTimeSlots objectAtIndex:indexPath.row];
    if(_isEditRequest){
        [self showAlertForAppointmentChanged:selectedList.dateTime];
    }else{
        AppContext.requestHelper.strDateTime = selectedList.dateTime;
        AppContext.requestHelper.vanId = selectedList.vanId;
        AppContext.requestHelper.travelingTime = selectedList.travelingTime;
        [self showPopUpVC:selectedList.dateTime];

    }
    
}


- (IBAction)segmentValueAction:(id)sender {
    
   
    if (self.dateSegmentControl.selectedSegmentIndex == self.dateSegmentControl.numberOfSegments - 1) {
         //When load more option is selected
      
        NSString *startDate = [_arrDates objectAtIndex:self.dateSegmentControl.numberOfSegments - 2]; //[self.dateSegmentControl titleForSegmentAtIndex:self.dateSegmentControl.numberOfSegments - 2];
        

        NSString *dateString = startDate;
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [self setLocaleForDateFormatter:format];
        
        [format setDateFormat:dd_MMM_YYYY];
        NSDate *date = [format dateFromString:dateString];
        [self setENLocaleForDateFormatter:format];
        [format setDateFormat:yyyy_MM_dd];
     
        NSString* finalDateString = [format stringFromDate:date];
        
        NSInteger index = self.dateSegmentControl.selectedSegmentIndex;
        NSString *endDate = [self getDate:finalDateString byAddingDays:15];
        NSArray *arr = [self getDatesBetweenDates:finalDateString andEnd:endDate];
        [self loadMoreDateswithAllDates:arr];
        self.dateSegmentControl.selectedSegmentIndex = index;
        
        NSString *strDate = [_arrDates objectAtIndex:index];//[self.dateSegmentControl titleForSegmentAtIndex:index];
        
        //********************* NEW FORMATE FORM NEW DESIGNING *******************//
        NSString *newdateString = strDate;
        NSDateFormatter *newformat = [[NSDateFormatter alloc] init];
        [self setLocaleForDateFormatter:newformat];
        [newformat setDateFormat:dd_MMM_YYYY];
        
        NSDate *newdate = [newformat dateFromString:newdateString];
        [self setENLocaleForDateFormatter:newformat];
        [newformat setDateFormat:yyyy_MM_dd];
        
        NSString *newfinalDateString = [newformat stringFromDate:newdate];
        [self fetchAppointmentList:newfinalDateString];
        
    }
    else{
        
        NSLog(@"selectedSegmentIndex : %lu",self.dateSegmentControl.selectedSegmentIndex);
        NSString *strDate =[_arrDates objectAtIndex:self.dateSegmentControl.selectedSegmentIndex];// [self.dateSegmentControl titleForSegmentAtIndex:self.dateSegmentControl.selectedSegmentIndex];
        NSLog(@"Selected Date : %@",strDate);

        NSString *newdateString = strDate;
        NSDateFormatter *newformat = [[NSDateFormatter alloc] init];
        [self setLocaleForDateFormatter:newformat];
     
        [newformat setDateFormat:dd_MMM_YYYY];
        NSDate *newdate = [newformat dateFromString:newdateString];
        [self setENLocaleForDateFormatter:newformat];
        [newformat setDateFormat:yyyy_MM_dd];
        NSString *newfinalDateString = [newformat stringFromDate:newdate];
        
        [self fetchAppointmentList:newfinalDateString];
        
    }
}

- (NSString *)getDate:(NSString *)myStringDate byAddingDays:(int)addDaysCount{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //  [self setLocaleForDateFormatter:dateFormatter];
    
    [dateFormatter setDateFormat:yyyy_MM_dd];
    // Retrieve NSDate instance from stringified date presentation
    NSDate *dateFromString = [dateFormatter dateFromString:myStringDate];
    // Create and initialize date component instance
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setDay:addDaysCount];
    // Retrieve date with increased days count
    NSDate *newDate = [[NSCalendar currentCalendar]
                       dateByAddingComponents:dateComponents
                       toDate:dateFromString options:0];
    return [self stringFromDate:newDate];
}

- (void) loadMoreDateswithAllDates:(NSArray *)arr{
    
    for (int i=0; i < 15; i++) {
        

        NSString *dateString = [arr objectAtIndex:i];
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [self setLocaleForDateFormatter:format];
        [format setDateFormat:yyyy_MM_dd];
        NSDate *date = [format dateFromString:dateString];
        
        [format setDateFormat:dd_MMM_YYYY];
        NSString* finalDateString = [format stringFromDate:date];
        
        if(_arrHolidayList.count >0){
            
            //checking for date
            __block BOOL addDateinList = YES;
            [_arrHolidayList enumerateObjectsUsingBlock:^(WSHolidayList   *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                
                //date checking
                if(obj.holidayType == 2){
                    [format setDateFormat:yyyy_MM_dd];
                    NSString *selectedDate = [format stringFromDate:date];
                    NSString *holidayDate = [NSString stringWithFormat:@"%@",obj.date];
                    if([holidayDate isEqualToString:selectedDate]){
                        *stop = YES;
                        addDateinList = NO;
                    }

                }
                else  if(obj.holidayType == 1){
                    //day checking
                    [format setDateFormat:@"EEEE"];
                    NSString *dayName = [format stringFromDate:date];
                    NSString *holidayDay = [NSString stringWithFormat:@"%@",obj.day];
                    if([holidayDay isEqualToString:dayName]){
                        addDateinList = NO;
                        *stop = YES;
                    }

                }
                
                
                
            }];
            
            if(addDateinList){
                NSString *dateInString = [self displayDateInString:date];
                [self.dateSegmentControl insertSegmentWithTitle:dateInString atIndex:self.dateSegmentControl.numberOfSegments - 1 animated:YES];
                [_arrDates addObject:finalDateString];
//                [self.dateSegmentControl insertSegmentWithTitle:finalDateString atIndex:self.dateSegmentControl.numberOfSegments - 1 animated:YES];
            }
            
            
        }else{
             NSString *dateInString = [self displayDateInString:date];
             [self.dateSegmentControl insertSegmentWithTitle:dateInString atIndex:self.dateSegmentControl.numberOfSegments - 1 animated:YES];
              [_arrDates addObject:finalDateString];
//            [self.dateSegmentControl insertSegmentWithTitle:finalDateString atIndex:self.dateSegmentControl.numberOfSegments - 1 animated:YES];
            
        }
    }
}

- (NSMutableArray *) getDatesBetweenDates:(NSString *)start andEnd:(NSString *)end{
    
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    //   [self setLocaleForDateFormatter:f];
    [f setDateFormat:yyyy_MM_dd];
    NSDate *startDate = [self dateFromString:start];
    NSDate *endDate = [self dateFromString:end];
    NSMutableArray *dates = [[NSMutableArray alloc] init];
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    
    for (int i = 1; i < components.day; ++i) {
        NSDateComponents *newComponents = [NSDateComponents new];
        newComponents.day = i;
        
        NSDate *date = [gregorianCalendar dateByAddingComponents:newComponents
                                                          toDate:startDate
                                                         options:0];
        NSString *strDate = [self stringFromDate:date];
        [dates addObject:strDate];
    }
    [dates addObject:[self stringFromDate:endDate]];
    return dates;
}

-(NSDate *)dateFromString:(NSString *)aStr{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:yyyy_MM_dd];
    NSDate *date = [dateFormat dateFromString:aStr];
    return date;
}

-(NSString *)stringFromDate:(NSDate *)aDate{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:yyyy_MM_dd];
    NSString *date = [formatter stringFromDate:aDate];
    return date;
}

-(void)setLocaleForDateFormatter:(NSDateFormatter *)formatter{
    
    NSLocale *arabicLocale = [[NSLocale alloc] initWithLocaleIdentifier:AppContext.appLanguage];
    [formatter setLocale:arabicLocale];
}

-(void)setENLocaleForDateFormatter:(NSDateFormatter *)formatter{
    NSLocale *arabicLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en"];
    [formatter setLocale:arabicLocale];
}

-(void)showPopUpVC:(NSString *)dateTime{
    PaymentPopUpVC *pvc = [[PaymentPopUpVC alloc] initWithNibName:@"PaymentPopUpVC" bundle:nil];
    pvc.isServeNow = NO;
    pvc.delegate = self;
    pvc.requestDate = dateTime;
    [self presentPopupViewController:pvc animationType:MJPopupViewAnimationFade];
}

#pragma mark :----------: AlertView For Edit Request
-(void)showAlertForAppointmentChanged:(NSString *)time{
    
//    NSString *strHeader =  [NSString stringWithFormat:@"%@\n%@",kConfirmAppointment,time];
    NSString *strHeader =  [NSString stringWithFormat:@"%@",kChangeAppointmentTime];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:strHeader];
    NSRange range = [strHeader rangeOfString:time];
    [attString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
    
    UIImage *img = [[UIImage imageNamed:@"logo-icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    CDAlertView *alert = [[CDAlertView alloc]init];
    alert.titleLabel.text = strHeader;
    alert.titleFont = Dax_Medium(15);
    alert.messageLabel.text = time;
    alert.messageTextColor =[UIColor redColor];
    alert.messageFont = Dax_Medium(15);
    alert.backgroundColor = RGBA(0, 0, 0, 0.7f);
    alert.circleFillColor = [UIColor colorWithPatternImage:img];
    alert.tintColor = [UIColor whiteColor];
    alert.isActionButtonsVertical = NO;
    alert.isTextFieldHidden = YES;
    alert.alertBackgroundColor = [UIColor whiteColor];
    alert.tintColor = AppThemeColor;
    
    
    CDAlertViewAction *action_yes = [[CDAlertViewAction alloc]initWithTitle:kAlertYes font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
        [self.delegate updateRequest:selectedList];
        [self.navigationController popViewControllerAnimated:YES];
    }];

    CDAlertViewAction *action_cancel = [[CDAlertViewAction alloc]initWithTitle:kAlertNo font:Dax_Regular(15) textColor:[UIColor redColor] backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
        
    }];
    
    [alert addWithAction:action_cancel];
    [alert addWithAction:action_yes];
    [alert show:^(CDAlertView *action) {
        
    }];

}

#pragma mark :----------: Popup Delegates
- (void)cancelButtonClicked:(PaymentPopUpVC *)paymentVC{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    if(paymentVC.cancelRequest){
        [self cancelRequest];
        return;
    }
    if(paymentVC.addAppointment){
        [self addAnotherAppointment];
        return;
    }
    if(paymentVC.addService){
        [self addAnotherServices];
        return;
    }
}

-(void)cancelRequest{
    //Back Changes
    [AppContext.requestHelper deallocInstance];
    [self addAnotherServices];
    
//    CATransition* transition = [CATransition animation];
//    transition.duration = 0.25;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionFade; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
//    transition.subtype =kCATransitionFromBottom;// kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
//
//    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(void)addAnotherAppointment{
  
}

-(void)addAnotherServices{
    NSArray *array = [self.navigationController viewControllers];
    [self.navigationController popToViewController:[array objectAtIndex:2] animated:YES];
}

-(void)checkedOutSuccessfully:(NSMutableDictionary *)dictResponse{
    
      NSString *requestId = [NSString stringWithFormat:@"Request %@",[[[dictResponse objectForKey:kServiceResponseKey]objectAtIndex:0]valueForKey:@"suffix"]];
    [UIAlertController showAlertInVC:self withTitle:requestId withMsg:kAppoinmentBooked withOtherAlertActionTitle:@[kAlertOk] withAlertActionCancel:@"" withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
        if(buttonIndex == 0){
            
//            CATransition* transition = [CATransition animation];
//            transition.duration = 0.25;
//            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//            transition.type = kCATransitionFade; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
//            transition.subtype =kCATransitionFromBottom;// kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
            [self.navigationController popToRootViewControllerAnimated:NO];
            AppContext.appTabVC.selectedIndex =1;
            
        }
    }];
    
}

@end
