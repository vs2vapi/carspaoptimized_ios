//
//  AddRequestVC.h
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>


@class CDAlertView;
@class CDAlertHeaderView;
@class CDAlertViewAction;

@interface AddRequestVC : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

#pragma mark: -------------------: Properties
@property (weak, nonatomic) IBOutlet UIScrollView *scrollVwRequest;
@property (weak, nonatomic) IBOutlet UIView *containerVw;
@property (weak, nonatomic) IBOutlet UICollectionView *collVwVehicleType;
@property (weak, nonatomic) IBOutlet UIButton *btnVehicleDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnAddressDetails;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collVwHeightConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerVwHeightConst;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *lblSwipeForMore;

@property (strong, nonatomic)NSMutableArray *arrCarTypes;
@property (strong, nonatomic)NSMutableArray *arrSelectedCarType;


#pragma mark: -------------------: Button Actions
- (IBAction)btnVehicleDetailAction:(id)sender;
- (IBAction)btnAddressAction:(id)sender;
- (IBAction)btnNextAction:(id)sender;

@end
