//
//  AddRequestVC.m
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "AddRequestVC.h"
#import "CategoriesVC.h"
#import "WSCartype.h"
#import "CarTypeCell.h"
#import "VehicleListVC.h"
#import "CD_User+CoreDataClass.h"
#import "CarSpa-Swift.h"
#import "WSUserAddresses.h"
#import "AddressFromMapVC.h"


@interface AddRequestVC ()

@end

@implementation AddRequestVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setShadowForVw];
    [self fetchCarTypeDetails];
    AppContext.requestHelper =  [AddRequestHelper defaultManager];
    
    _lblSwipeForMore.text = kSwipeForMore;
    if(![self isEnglishLanguage]){
        _collVwVehicleType.transform = CGAffineTransformMakeScale(-1, 1);
        _lblSwipeForMore.textAlignment = NSTextAlignmentLeft;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
    [self displayCarDetailsFromDB];
    [self displayAddressFromDB];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
  
    if(self.navigationController.childViewControllers.count == 1){
        [AppContext.requestHelper deallocInstance];
        //Dealloc all request helper instance when moving to tab bar controller
    }
}
#pragma mark: -------------------: Setup UI

-(void)setShadowForVw{
    
    _btnNext.layer.cornerRadius = 6.0f;
    _containerVw.layer.cornerRadius = 4.0f;
    _containerVw.layer.borderWidth = 1.0f;
    _containerVw.layer.borderColor = RGBA(200, 200, 200, 1).CGColor;
    
    [_containerVw.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [_containerVw.layer setShadowOpacity:0.8];
    [_containerVw.layer setShadowRadius:3.0];
    [_containerVw.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    if(![self isEnglishLanguage]){
        [_btnAddressDetails setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [_btnVehicleDetails setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    }
}

-(void)fetchCarTypeDetails{
    
    _arrSelectedCarType = [NSMutableArray new];
    if([ReachabilityManager isReachable]){
        NSString *url = [NSString stringWithFormat:@"%@?%@=%@",kFetchCarTypeWS,kAppVersionServiceKey,[self fetchAppVersion]];
        [[WebServiceInvocation alloc]initWithWS:url withParams:nil withServiceType:TYPE_GET withSelector:@selector(carTypeWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }

}

-(void)carTypeWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        _arrCarTypes = [NSMutableArray arrayWithArray:[sender responseArray]];
        [_collVwVehicleType reloadData];
        if(_arrSelectedCarType.count > 0){
              [self performSelector:@selector(scrollToSelectedCell) withObject:nil afterDelay:0.3f];
        }
      
    }
}

-(void)scrollToSelectedCell{
    
    NSInteger carTypeId = [[_arrSelectedCarType objectAtIndex:0]integerValue];
    __block NSInteger indexOfCell;
    [_arrCarTypes enumerateObjectsUsingBlock:^(WSCartype *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if(carTypeId == obj.cartypeIdentifier){
            indexOfCell = idx;
            *stop = YES;
        }
    }];
        [_collVwVehicleType scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:indexOfCell inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
}

-(void)displayCarDetailsFromDB{

    NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
    
    NSMutableArray *tmpArray = [NSMutableArray new];
     [tmpArray addObjectsFromArray:[CORE_DATA_INSTANCE.dataManager fetchRecordsWithFetchRequest:Table_User withPredicate:[NSString stringWithFormat:@"userId = %@ AND carId != 0",userId] withViewContenxt:CORE_DATA_INSTANCE.nsManagedObjectContext]];
    
    if(tmpArray.count > 0){
        for(CD_User *dbUserObj in tmpArray){
            
            WSCar *objCarDetails = [WSCar new];
            
            objCarDetails.carIdentifier =    dbUserObj.carId ;
            objCarDetails.model = dbUserObj.carModel;
            objCarDetails.brand =   dbUserObj.carBrand;
            objCarDetails.plateNumber =  dbUserObj.carPlateNo;
            
            WSCartype *objCarType = [WSCartype new];
            objCarType.cartypeIdentifier = dbUserObj.carTypeId;
            
            AppContext.requestHelper.carTypeId = objCarType.cartypeIdentifier;
            AppContext.requestHelper.carId = objCarDetails.carIdentifier;
            AppContext.requestHelper.carBrand = objCarDetails.brand;
            AppContext.requestHelper.carModel = objCarDetails.model;
            
        }

        if(AppContext.requestHelper.carTypeId!=0){
             [_arrSelectedCarType addObject:[NSNumber numberWithInteger:AppContext.requestHelper.carTypeId]];
        }
       
        [_btnVehicleDetails setTitle:[NSString stringWithFormat:@"%@ - %@",AppContext.requestHelper.carBrand,AppContext.requestHelper.carModel] forState:UIControlStateNormal];
        
    }else{
        AppContext.requestHelper.carId = 0;
        AppContext.requestHelper.carBrand = @"";
        AppContext.requestHelper.carModel = @"";
        [_btnVehicleDetails setTitle:kTapToAddVehicleDetail forState:UIControlStateNormal];
    }
    
}


-(void)configAlertView{
    
    @try {
        UIImage *img = [[UIImage imageNamed:@"logo-icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        CDAlertView *alertView = [[CDAlertView alloc]init];
        alertView.messageLabel.text = kSelAddFrom;
        alertView.backgroundColor = RGBA(0, 0, 0, 0.7f);
        alertView.circleFillColor = [UIColor colorWithPatternImage:img];
        alertView.tintColor = [UIColor whiteColor];
        alertView.isActionButtonsVertical = YES;
        alertView.alertBackgroundColor = [UIColor whiteColor];
        alertView.messageTextColor = [UIColor grayColor];
        
        
        CDAlertViewAction *action_CurrentAddress = [[CDAlertViewAction alloc]initWithTitle:kCurrentAddress font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            [self fetchCurrentAddress];
        }];
        
        CDAlertViewAction *action_SelectFromMap = [[CDAlertViewAction alloc]initWithTitle:kSelFromMap font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            [self moveToMapVC];
        }];
        
        CDAlertViewAction *action_saveAddress = [[CDAlertViewAction alloc]initWithTitle:kSelFromSavedAddr font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            [self moveToListVC];
        }];
        
        CDAlertViewAction *action_cancel = [[CDAlertViewAction alloc]initWithTitle:kCancel font:Dax_Regular(15) textColor:[UIColor redColor] backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            
        }];
        
        [alertView addWithAction:action_CurrentAddress];
        [alertView addWithAction:action_SelectFromMap];
        [alertView addWithAction:action_saveAddress];
        [alertView addWithAction:action_cancel];
        
        [alertView show:^(CDAlertView *action) {
            
        }];

    } @catch (NSException *exception) {
        NSLog(@"Alertview Exception : %@ , In Controller : %@",[exception description],self);
    } @finally {
        
    }
   
}

#pragma mark: -------------------: Address Functions

-(void)fetchCurrentAddress{
   
    if([ReachabilityManager isReachable]){
        
        [RTProgressHUD showProgressWithMsg:kWSLoaderDialoagMsg];
        double lat = [Functions getDoubleValueFromDefaults:USER_LATITUDE];
        double longitude = [Functions getDoubleValueFromDefaults:USER_LONGITUDE];
        
        [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(lat,longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
            [RTProgressHUD dismiss];
            if (error) {
                [self showAlert:kLocationNotAvail];
                
            } else {
                for(GMSAddress* addressObj in [response results]){
                    
                    NSString * addressString =@"";
                    for (int i = 0; i < addressObj.lines.count; i++) {
                        addressString = [addressString stringByAppendingString:[NSString stringWithFormat:@"%@",[addressObj.lines objectAtIndex:i]]];
                    }
                
                    AppContext.requestHelper.addressDetails = addressString;
                    [self fetchUserAddress];
                    break;
                }
                
            }
        }];
    }else{
        ShowNoNetworkAlert(self);
    }


}

-(void)fetchUserAddress{
    
    if([ReachabilityManager isReachable]){
        NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
        NSString *url = [NSString stringWithFormat:@"%@?userId=%@&%@=%@",kFetchUserAddressWS,userId,kAppVersionServiceKey,[self fetchAppVersion]];
        [[WebServiceInvocation alloc]initWithWS:url withParams:nil withServiceType:TYPE_GET withSelector:@selector(userAddressWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)userAddressWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
       // [self showAlert:[sender responseErr]];
           [self showAddressAlert];
    }else{
        BOOL status = [[[sender responseDictionary]valueForKey:@"Status"]boolValue];
        if(status){
            __block BOOL isAddressExist = NO;
            NSString *currentAddress = AppContext.requestHelper.addressDetails;
            [[sender responseArray]enumerateObjectsUsingBlock:^(WSUserAddresses *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSString *addressFromResponse = obj.address;
                if([currentAddress isEqualToString:addressFromResponse]){
                    isAddressExist = YES;
                    [self saveAddressInDB:obj];
                    *stop = YES;
                }
            }];
            if(!isAddressExist){
                [self showAddressAlert];
            }
        }else{
            [self showAddressAlert];
        }

    }
}

-(void)saveAddressInDB:(WSUserAddresses *)objAddress{
   [[SyncCDAdaptor sycnInstance]saveAddressDetails:objAddress];
    [self displayAddressFromDB];
}

-(void)saveAddress:(NSString *)addressTitle{
    
    
    if([ReachabilityManager isReachable]){
        
        double lat = [Functions getDoubleValueFromDefaults:USER_LATITUDE];
        double longitude = [Functions getDoubleValueFromDefaults:USER_LONGITUDE];
        
        NSString *strAddress = AppContext.requestHelper.addressDetails;
        NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
        NSDictionary *dictParams = @{@"userId":userId,
                                                         @"address":strAddress,
                                                        @"latitude":[NSNumber numberWithDouble:lat],
                                                        @"longitude":[NSNumber numberWithDouble:longitude],
                                                        @"addresstitle":addressTitle,
                                                        };
        [[WebServiceInvocation alloc]initWithWS:kAddUserAddressWS withParams:dictParams withServiceType:TYPE_POST withSelector:@selector(addUserAddressWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }

}

-(void)addUserAddressWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        [self displayAddressFromDB];
    }
}
-(void)setAddressWithTitle:(NSString *)title withAddress:(NSString *)address{
    
    self.btnAddressDetails.titleLabel.numberOfLines = 3;
    NSMutableAttributedString *attr1 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@\n",title] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"dax-medium" size:15]}];
    NSMutableAttributedString *attr2 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",address] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"dax-regular" size:15]}];
    
    [attr1 appendAttributedString:attr2];
    
    [self.btnAddressDetails setAttributedTitle:attr1 forState:UIControlStateNormal];
    
}

-(void)showAddressAlert{
    
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:kAddressNickName
                                                                              message: @""
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField){
        textField.placeholder = [NSString stringWithFormat:@"%@ %@",kEnter,kAddressNickName];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        if(![self isEnglishLanguage]){
            textField.textAlignment = NSTextAlignmentRight;
        }
        
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:kAlertOk style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * namefield = textfields[0];
        
        if([namefield.text isEqualToString:@""]){
            [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kAddressNickName]];
        }else{
            [self saveAddress:namefield.text];
        }
    
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}


-(void)displayAddressFromDB{
 
    
    self.btnAddressDetails.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.btnAddressDetails.titleLabel.numberOfLines = 2;
    [self.btnAddressDetails.titleLabel setTextAlignment: NSTextAlignmentCenter];
    
    NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
    
    NSMutableArray *tmpArray = [NSMutableArray new];
    [tmpArray addObjectsFromArray:[CORE_DATA_INSTANCE.dataManager fetchRecordsWithFetchRequest:Table_User withPredicate:[NSString stringWithFormat:@"userId = %@ AND addressId != 0",userId] withViewContenxt:CORE_DATA_INSTANCE.nsManagedObjectContext]];
    
    if(tmpArray.count > 0){
        for(CD_User *dbUserObj in tmpArray){
            
            WSUserAddresses *address = [WSUserAddresses new];
            address.internalBaseClassIdentifier = dbUserObj.addressId;
            address.address = dbUserObj.address;
            address.addresstitle = dbUserObj.addressTitle;
            address.latitude = dbUserObj.latitude;
            address.longitude = dbUserObj.longitude;
            
            AppContext.requestHelper.addressId = address.internalBaseClassIdentifier;
            AppContext.requestHelper.addressDetails = address.address;
            AppContext.requestHelper.addressTitle = address.addresstitle;
            AppContext.requestHelper.addressLatitude = address.latitude;
            AppContext.requestHelper.addressLongitude = address.longitude;
            
        }
        
      [self setAddressWithTitle:AppContext.requestHelper.addressTitle  withAddress:AppContext.requestHelper.addressDetails];
        
    }else{
        AppContext.requestHelper.addressId = 0;
        AppContext.requestHelper.addressDetails =@"";
        AppContext.requestHelper.addressTitle = 0;
        AppContext.requestHelper.addressLatitude = 0;
        AppContext.requestHelper.addressLongitude = 0;
        [self.btnAddressDetails setAttributedTitle:nil forState:UIControlStateNormal];
        [self.btnAddressDetails setTitle:kTapToAddAddressDetail forState:UIControlStateNormal];
    }
    
}

#pragma mark:-------------------: CollectionView Delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _arrCarTypes.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
  
    WSCartype *carType = [_arrCarTypes objectAtIndex:indexPath.row];
    return CGSizeMake(MAX(80, [self widthOfString:carType.name] + 20), 130);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 12;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (CGFloat) widthOfString:(NSString *)aStr{
  
    NSDictionary *userAttributes = @{NSFontAttributeName: Dax_Regular(14),
                                     NSForegroundColorAttributeName: [UIColor blackColor]};
    CGSize stringBoundingBox = [aStr sizeWithAttributes: userAttributes];
    return stringBoundingBox.width;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CarTypeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CarTypeCell" forIndexPath:indexPath];
 
    WSCartype *objCartype = [_arrCarTypes objectAtIndex:indexPath.row];
    
    NSString *strURL = objCartype.iconThump;
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:strURL]
                              placeholderImage:nil];
    
    cell.lblName.text = [self isEnglishLanguage] ? objCartype.name : objCartype.nameAR;
    
    if ([_arrSelectedCarType containsObject:[NSNumber numberWithInteger:objCartype.cartypeIdentifier]]){
//        cell.imgView.backgroundColor = AppThemeColor;
        [cell.bgImgVw setImage:[UIImage imageNamed:@"icn_bg_green"]];
        [cell.imgVwArrow setHidden:NO];
        
    }else{
//        cell.imgView.backgroundColor = RGBA(0, 163, 233, 1);
        [cell.bgImgVw setImage:[UIImage imageNamed:@"icn_bg_blue"]];
        [cell.imgVwArrow setHidden:YES];
    }
    
    if(![self isEnglishLanguage]){
        cell.transform = CGAffineTransformMakeScale(-1, 1);
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    WSCartype *objCarType = [_arrCarTypes objectAtIndex:indexPath.row];
    [_arrSelectedCarType removeAllObjects];
    [_arrSelectedCarType addObject:[NSNumber numberWithInteger:objCarType.cartypeIdentifier]];
    
    //Assign car type ID to request helper.
    AppContext.requestHelper.carTypeId = objCarType.cartypeIdentifier;
    
    //Assigning cartype ID to WSCar model class to just save car type ID
    WSCar *objCar = [WSCar new];
    objCar.carIdentifier = objCarType.cartypeIdentifier;
    [[SyncCDAdaptor sycnInstance]saveCarDetails:objCar withCarTypeId:YES];
    [_collVwVehicleType reloadData];

}

-(void)moveToListVC{
    VehicleListVC *vlvc = loadViewController(kSBRequest, kVehicleListVC);
    vlvc.showAddress = YES;
    [self.navigationController pushViewController:vlvc animated:YES];
}

-(void)moveToMapVC{
    AddressFromMapVC *admpvc = loadViewController(kSBRequest, kAddressFromMapVC);
    [self.navigationController pushViewController:admpvc animated:YES];
}


#pragma mark: -------------------: Button Actions
- (IBAction)btnVehicleDetailAction:(id)sender{
    VehicleListVC *vlvc = loadViewController(kSBRequest, kVehicleListVC);
    [self.navigationController pushViewController:vlvc animated:YES];
}

- (IBAction)btnAddressAction:(id)sender {
    [self configAlertView];
}

- (IBAction)btnNextAction:(id)sender {
    
    NSLog(@"CarType : %lu , Address : %lu , Car : %lu",AppContext.requestHelper.carTypeId,AppContext.requestHelper.addressId,AppContext.requestHelper.carId);
    if(AppContext.requestHelper.carTypeId==0){
        [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseSelect,kVehicleType]];
        return;
    }
   else if(AppContext.requestHelper.carId==0){
       [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseSelect,kVehicleDetails]];
       return;
    }
  else  if(AppContext.requestHelper.addressId==0){
       [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseSelect,kAddressDetails]];
      return;
  }else{
      CategoriesVC *cgvc = loadViewController(kSBRequest, kCategoriesVC);
      [self.navigationController pushViewController:cgvc animated:YES];
  }
  
}
@end
