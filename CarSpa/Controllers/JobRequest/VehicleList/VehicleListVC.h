//
//  VehicleListVC.h
//  CarSpa
//
//  Created by R@j on 31/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JobDetailsVC.h"


@interface VehicleListVC : UIViewController <UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tblVwList;
@property (strong, nonatomic) NSMutableArray *arrVehicleList;
@property(nonatomic,assign)BOOL showAddress;
@property(nonatomic,assign)BOOL fromRequestDetails;
@property (strong, nonatomic) EditRequestObj *objRequest;

- (IBAction)btnAddAction:(id)sender;

@end
