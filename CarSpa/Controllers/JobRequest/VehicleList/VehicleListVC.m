//
//  VehicleListVC.m
//  CarSpa
//
//  Created by R@j on 31/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "VehicleListVC.h"
#import "ListCell.h"
#import "WSCar.h"
#import "WSUserAddresses.h"
#import "AddCarsVC.h"
#import "AddressFromMapVC.h"

@interface VehicleListVC (){
    NSIndexPath *selectedIndexPath;
}

@end

@implementation VehicleListVC

- (void)viewDidLoad {
    [super viewDidLoad];
     [self.tabBarController.tabBar setHidden:YES];
    if(_showAddress){
        self.navigationItem.title = kAddress;
    }else{
         self.navigationItem.title = kVehicle;
    }
    [self fetchDetails];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(detailsAdded:) name:DETAILS_ADDED object:nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)fetchDetails{
    if(!_showAddress){
        [self fetchUserCars];
    }else{
        [self fetchUserAddress];
    }
}

-(void)detailsAdded:(NSNotification *)notification{
    [self fetchDetails];
}

-(void)fetchUserCars{
    
    if([ReachabilityManager isReachable]){
        
        NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
        NSString *url = [NSString stringWithFormat:@"%@?userId=%@&%@=%@&language=%@",kFetchUserCarsWS,userId,kAppVersionServiceKey,[self fetchAppVersion],AppContext.appLanguage];
        [[WebServiceInvocation alloc]initWithWS:url withParams:nil withServiceType:TYPE_GET withSelector:@selector(userCarsWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }
    
}

-(void)userCarsWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        if([sender responseCode] == 203){
            [self btnAddAction:nil];
        }else{
            [self showAlert:[sender responseErr]];
        }
       
    }else{
        _arrVehicleList = [NSMutableArray arrayWithArray:[sender responseArray]];
        [_tblVwList reloadData];
    }
    
}

-(void)fetchUserAddress{
    
    if([ReachabilityManager isReachable]){
        
        NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
        NSString *url = [NSString stringWithFormat:@"%@?userId=%@&%@=%@",kFetchUserAddressWS,userId,kAppVersionServiceKey,[self fetchAppVersion]];
        [[WebServiceInvocation alloc]initWithWS:url withParams:nil withServiceType:TYPE_GET withSelector:@selector(userAddressWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }
    
}

-(void)userAddressWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [UIAlertController showAlertInVC:self withTitle:AppName withMsg:[sender responseErr] withOtherAlertActionTitle:@[kAlertOk] withAlertActionCancel:@"" withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
            if(buttonIndex == 0){
                if(_arrVehicleList.count == 0 || _arrVehicleList == nil){
                    [self btnAddAction:nil];
                }
            }
        }];
      
    }else{
        _arrVehicleList = [NSMutableArray arrayWithArray:[sender responseArray]];
        [_tblVwList reloadData];
    }
    
}

#pragma mark:--------------------: Tableview Datasource & Delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrVehicleList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static  NSString *cellIdentifier = @"Cell";
    ListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ListCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    if(_showAddress){
        WSUserAddresses *objAddress= [_arrVehicleList objectAtIndex:indexPath.row];
        NSString *addressTitle = [NSString stringWithFormat:@"%@",objAddress.addresstitle];
        objAddress.addresstitle = ([addressTitle isEqualToString:@""]) ? kNoAddressTitle :  addressTitle;
        cell.lblTitle.text = [NSString stringWithFormat:@"%@",objAddress.addresstitle];
        cell.lblDetails.text = objAddress.address;
    }else{
        
        WSCar *objCar = [_arrVehicleList objectAtIndex:indexPath.row];
        
        NSMutableAttributedString *attrTitle1 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@ ",kCarBrandModel] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"dax-medium" size:15],NSForegroundColorAttributeName:[UIColor blackColor]}];
        NSMutableAttributedString *attrTitle2 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@ - %@",objCar.brand,objCar.model] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"dax-regular" size:15],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
        [attrTitle1 appendAttributedString:attrTitle2];
        cell.lblTitle.attributedText = attrTitle1;
        
        NSMutableAttributedString *attr1 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@          ",kPlateNo] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"dax-medium" size:15],NSForegroundColorAttributeName:[UIColor blackColor]}];
        NSMutableAttributedString *attr2 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",objCar.plateNumber] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"dax-regular" size:15],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
        [attr1 appendAttributedString:attr2];
        cell.lblDetails.attributedText = attr1;
       
//        cell.lblTitle.text = [NSString stringWithFormat:@"%@ - %@",objCar.brand,objCar.model];
//        cell.lblDetails.text = [NSString stringWithFormat:@"%@ %@",kPlateNo,objCar.plateNumber];
      
    }
  
    [cell.btnEdit addTarget:self action:@selector(btnEditAction:event:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnDelete addTarget:self action:@selector(btnDeleteAction:event:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(_showAddress){
         WSUserAddresses *objAddress = [_arrVehicleList objectAtIndex:indexPath.row];
        if(_fromRequestDetails){
            [[NSNotificationCenter defaultCenter]postNotificationName:EDIT_REQUEST_ADDRESS_CHANGED object:objAddress];
        }else{
            NSString *addressTitle = [NSString stringWithFormat:@"%@",objAddress.addresstitle];
            objAddress.addresstitle = ([addressTitle isEqualToString:@""]) ? kNoAddressTitle :  addressTitle;
            [[SyncCDAdaptor sycnInstance]saveAddressDetails:objAddress];
        }
     
    }else{
         WSCar *objCar = [_arrVehicleList objectAtIndex:indexPath.row];
        if(_fromRequestDetails){
            [[NSNotificationCenter defaultCenter]postNotificationName:EDIT_REQUEST_CAR_CHANGED object:objCar];
        }else{
            [[SyncCDAdaptor sycnInstance]saveCarDetails:objCar withCarTypeId:NO];
        }
    }
  
    [self.navigationController popViewControllerAnimated:YES];

}

#pragma mark:--------------------: Button Actions
- (IBAction)btnAddAction:(id)sender {
    
    if(_showAddress){
        AddressFromMapVC *admpvc = loadViewController(kSBRequest, kAddressFromMapVC);
        [self.navigationController pushViewController:admpvc animated:YES];
    }else{
        AddCarsVC *advc  = loadViewController(kSBOthers,kAddCarsVC);
        [self.navigationController pushViewController:advc animated:YES];
    }
}

-(void)btnEditAction:(id)sender event:(id)event{
    
    UITouch *touch = [event allTouches].anyObject;
    CGPoint location = [touch locationInView:_tblVwList];
    NSIndexPath *indexPath = [_tblVwList indexPathForRowAtPoint:location];
    
    if(_showAddress){
          WSUserAddresses *objAddress = [_arrVehicleList objectAtIndex:indexPath.row];
        AddressFromMapVC *admpvc = loadViewController(kSBRequest, kAddressFromMapVC);
        admpvc.isEditAddress = YES;
        admpvc.editAddressObj = objAddress;
        [self.navigationController pushViewController:admpvc animated:YES];
    }else{
        WSCar *objCar = [_arrVehicleList objectAtIndex:indexPath.row];
        AddCarsVC *advc  = loadViewController(kSBOthers,kAddCarsVC);
        advc.isEditCars = YES;
        advc.objSelCar = objCar;
        [self.navigationController pushViewController:advc animated:YES];
    }
    
}

-(void)btnDeleteAction:(id)sender event:(id)event{
    
    UITouch *touch = [event allTouches].anyObject;
    CGPoint location = [touch locationInView:_tblVwList];
    selectedIndexPath  = [_tblVwList indexPathForRowAtPoint:location];
    if(_showAddress){
        [UIAlertController showAlertInVC:self withTitle:AppName withMsg:kSureDelAddress withOtherAlertActionTitle:@[kAlertYes] withAlertActionCancel:kAlertNo withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
            if(buttonIndex == 0){
                WSUserAddresses *objAddress = [_arrVehicleList objectAtIndex:selectedIndexPath.row];
                [self deleteAddressDetails:objAddress.internalBaseClassIdentifier];
            }
        }];
       
        
    }else{
        [UIAlertController showAlertInVC:self withTitle:AppName withMsg:kSureDelCar withOtherAlertActionTitle:@[kAlertYes] withAlertActionCancel:kAlertNo withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
            if(buttonIndex == 0){
                WSCar *objCar = [_arrVehicleList objectAtIndex:selectedIndexPath.row];
                [self deleteCarDetails:objCar.carIdentifier];
            }
        }];
    }
}

-(void)deleteCarDetails:(NSInteger)carId{
    
    if([ReachabilityManager isReachable]){
    
        NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
        NSDictionary *dictParmas = @{@"carId":[NSNumber numberWithInteger:carId],
                                     @"userId":userId,
                                     @"language":AppContext.appLanguage};
        [[WebServiceInvocation alloc]initWithWS:kDeleteCarWS withParams:dictParmas withServiceType:TYPE_POST withSelector:@selector(deleteCarWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }

}

-(void)deleteCarWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        
        WSCar *objCar = [_arrVehicleList objectAtIndex:selectedIndexPath.row];
         [[SyncCDAdaptor sycnInstance]deleteUserCarDetails:objCar];
        [self showAlert:[[sender responseDictionary] objectForKey:@"Message"]];
        [_arrVehicleList removeObjectAtIndex:selectedIndexPath.row];
        [_tblVwList beginUpdates];
        [_tblVwList deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:selectedIndexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        [_tblVwList endUpdates];
        selectedIndexPath = nil;
    }
}

-(void)deleteAddressDetails:(NSInteger)addrId{
    
    if([ReachabilityManager isReachable]){

        NSDictionary *dictParmas = @{@"addressId":[NSNumber numberWithInteger:addrId],
                                                         @"language":AppContext.appLanguage};
        [[WebServiceInvocation alloc]initWithWS:kDeleteAddressWS withParams:dictParmas withServiceType:TYPE_POST withSelector:@selector(deleteAddressWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }

}

-(void)deleteAddressWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        
        WSUserAddresses *objAddress = [_arrVehicleList objectAtIndex:selectedIndexPath.row];
        [[SyncCDAdaptor sycnInstance]deleteUserAddressDetails:objAddress];
        [_arrVehicleList removeObjectAtIndex:selectedIndexPath.row];
        [_tblVwList beginUpdates];
        [_tblVwList deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:selectedIndexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        [_tblVwList endUpdates];
          selectedIndexPath = nil;
        [UIAlertController showAlertInVC:self withTitle:AppName withMsg:[[sender responseDictionary] objectForKey:@"Message"]withOtherAlertActionTitle:@[kAlertOk] withAlertActionCancel:@"" withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
            if(buttonIndex == 0){
                if(_arrVehicleList.count == 0){
                    [self btnAddAction:nil];
                }
            }
        }];

    }
}

@end
