//
//  CategoriesVC.m
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "CategoriesVC.h"
#import "ServiceCollVwCell.h"
#import "ServiceVC.h"
#import "WSCategory.h"

@interface CategoriesVC ()

@end

@implementation CategoriesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchCategories:YES];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fetchCategories:(BOOL)showLoader{
   
    
    if([ReachabilityManager isReachable]){
        
        NSInteger carTypeId = AppContext.requestHelper.carTypeId;
        NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
        double lat = AppContext.requestHelper.addressLatitude;
        double longitude = AppContext.requestHelper.addressLongitude;
        
//       NSString *url = [NSString stringWithFormat:@"%@?%@=%@",kFetchCategoriesWS,kAppVersionServiceKey,[self fetchAppVersion]];
         NSString *url = [NSString stringWithFormat:@"%@?carTypeId=%lu&latitude=%f&longitude=%f&%@=%@&userId=%@&addressId=%lu&language=%@",kFetchCategoriesWS,carTypeId,lat,longitude,kAppVersionServiceKey,[self fetchAppVersion],userId,AppContext.requestHelper.addressId,AppContext.appLanguage];
        [[WebServiceInvocation alloc]initWithWS:url withParams:nil withServiceType:TYPE_GET withSelector:@selector(categoriesWSAction:) withTarget:self showLoader:showLoader loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }

}

-(void)categoriesWSAction:(id)sender{
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [UIAlertController showAlertInVC:self withTitle:AppName withMsg:[sender responseErr] withOtherAlertActionTitle:@[kAlertOk] withAlertActionCancel:@"" withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
            if(buttonIndex == 0){
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }else{
        _arrCategories = [NSMutableArray arrayWithArray:[sender responseArray]];
        [_collVwCategory reloadData];
    }

}

#pragma mark :---------: CollectionView Delegate
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _arrCategories.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"ServiceCell";
    ServiceCollVwCell *cell =[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    WSCategory *objCategory = [_arrCategories objectAtIndex:indexPath.row];
    cell.lblServiceTitle.text = [self isEnglishLanguage] ?  objCategory.name :  objCategory.nameAR;
    [cell.imgVwService sd_setImageWithURL:[NSURL URLWithString:objCategory.iconThump]placeholderImage:nil];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(collectionView.frame.size.width / 3, 120);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
     WSCategory *objCategory = [_arrCategories objectAtIndex:indexPath.row];
    AppContext.requestHelper.categoryId = objCategory.categoryIdentifier;
    AppContext.requestHelper.categoryName = [self isEnglishLanguage] ? objCategory.name : objCategory.nameAR;
    AppContext.requestHelper.serviceMainImgURL = objCategory.imageUrl;
    if(AppContext.requestHelper.categoryId == 1){
        AppContext.requestHelper.isTowService = YES;
        AppContext.requestHelper.isAccesorryService = NO;
        AppContext.requestHelper.isEmergencyService = NO;
        [self moveToServiceVC];
        return;
    }
    else if(AppContext.requestHelper.categoryId == 2){
        AppContext.requestHelper.isTowService = NO;
        AppContext.requestHelper.isAccesorryService = YES;
        AppContext.requestHelper.isEmergencyService = NO;
        [self moveToServiceVC];
        return;
    }
    else if(AppContext.requestHelper.categoryId == 3){
        AppContext.requestHelper.isTowService = NO;
        AppContext.requestHelper.isAccesorryService = NO;
        AppContext.requestHelper.isEmergencyService = YES;
        [self moveToServiceVC];
        return;
    }else{
        AppContext.requestHelper.isTowService = NO;
        AppContext.requestHelper.isAccesorryService = NO;
        AppContext.requestHelper.isEmergencyService = NO;
        [self moveToServiceVC];
    }
}

-(void)moveToServiceVC{
    ServiceVC *svc = loadViewController(kSBRequest, kServiceVC);
    [self.navigationController pushViewController:svc animated:YES];
}


@end
