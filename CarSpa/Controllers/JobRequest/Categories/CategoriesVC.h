//
//  CategoriesVC.h
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoriesVC : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *collVwCategory;
@property (nonatomic, assign) NSInteger strCarTypeId;
@property (strong,nonatomic) NSString *strServiceId;
@property (strong,nonatomic) NSMutableArray *arrCategories;
@end
