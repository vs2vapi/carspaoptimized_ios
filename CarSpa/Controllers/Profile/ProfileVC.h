//
//  ProfileVC.h
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileVC : BaseVC <UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tblVwProfile;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *lblUsername;
@property (weak, nonatomic) IBOutlet UIButton *btnChangePswd;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UIImageView *bgImgVw;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *imgIndicator;

@property(strong,nonatomic)NSMutableArray *arrCurrency;


- (IBAction)btnCameraActioin:(id)sender;
- (IBAction)btnEditNameAction:(id)sender;
- (IBAction)btnChangePswdAction:(id)sender;
- (IBAction)btnShareAction:(id)sender;

@end
