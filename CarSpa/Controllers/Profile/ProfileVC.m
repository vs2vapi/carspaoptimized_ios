//
//  ProfileVC.m
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "ProfileVC.h"
#import "ProfileCell.h"
#import "ChangePasswordVC.h"
#import "CarSpa-Swift.h"
#import "WSUser.h"
#import "CD_User+CoreDataClass.h"
#import "VehicleListVC.h"
#import "WSAllCurrency.h"
#import "SDImageCache.h"

typedef enum : NSUInteger {
    CELL_EMAIL = 0,
    CELL_MOBILE = 1,
    CELL_ADDRESS = 2,
    CELL_CAR = 3,
    CELL_LANGUAGE = 4,
    CELL_CURRENCY = 5
} Enum_ProfileCell;

@interface ProfileVC ()  <UIImagePickerControllerDelegate,UINavigationControllerDelegate,RTCountryPickerDelegate>{
    WSUser *userObj;
    CDAlertView *alertViewPhone;
    UIButton *btnPhoneNoAction;
    NSString *countryCode,*selectedCurrency;
    CDAlertView *alertViewImgPicker;
    BOOL pickerDismissed;
}

@end

@implementation ProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if(![self isUserLoggedIn]){
        [self showAlertForLogin];
        [self.tblVwProfile setHidden:YES];
    }else{
        [self setupUI];
    }
  
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if([self isUserLoggedIn]){
        [self fetchUserDetailsFromDB];
        [self fetchAllCurrencies];
    }
  
}

#pragma mark:--------------------: Basic Setups
-(void)setupUI{
    
     pickerDismissed = NO;
     userObj = [WSUser new];
    _tblVwProfile.separatorColor = [UIColor clearColor];
    _btnShare.layer.cornerRadius = 6.0f;
    _btnChangePswd.layer.cornerRadius = 6.0f;
    _imgVwProfilePic.layer.cornerRadius = _imgVwProfilePic.frame.size.height/2;
    _imgVwProfilePic.clipsToBounds = YES;
}

-(void)fetchUserDetailsFromDB{
    
    if(!pickerDismissed){
    
    NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
    
    NSMutableArray *tmpArray = [NSMutableArray new];
    [tmpArray addObjectsFromArray:[CORE_DATA_INSTANCE.dataManager fetchRecordsWithFetchRequest:Table_User withPredicate:[NSString stringWithFormat:@"userId = %@",userId] withViewContenxt:CORE_DATA_INSTANCE.nsManagedObjectContext]];
    
    
     for(CD_User *dbUserObj in tmpArray){
         userObj.internalBaseClassIdentifier = dbUserObj.userId;
         userObj.fullName = dbUserObj.fullName;
         userObj.email = dbUserObj.email ;
         userObj.latitude = dbUserObj.latitude;
         userObj.longitude =  dbUserObj.longitude;
         userObj.socialId = dbUserObj.socialId;
         userObj.socialType = dbUserObj.socialType;
         userObj.status = dbUserObj.status;
         userObj.countryCode = dbUserObj.countryCode;
         userObj.mobileNumber = dbUserObj.mobileNumber;
         userObj.fileUrl = dbUserObj.fileUrl;
         userObj.thumbUrl =  dbUserObj.thumbUrl;
         userObj.datetime =  dbUserObj.dateTime;
         userObj.carModel=  dbUserObj.carModel ;
         userObj.carBrand =  dbUserObj.carBrand;
         userObj.address = dbUserObj.address;
         userObj.addressTitle = dbUserObj.addressTitle;
         if([userObj.address isEqualToString:@""] || userObj.address == nil){
             userObj.address = kNotProvided;
         }
         if([userObj.carBrand isEqualToString:@""] || userObj.carBrand == nil){
             userObj.carBrand = kNotProvided;
         }
         if([userObj.carModel isEqualToString:@""] || userObj.carModel == nil){
             userObj.carModel = @"";
         }
         _lblUsername.text = userObj.fullName;
     }
    
    countryCode = [NSString stringWithFormat:@"%lu",userObj.countryCode];
    [Functions setCustomObjectToDefaults:userObj withkey:USER_OBJ];
    
    [_imgVwProfilePic sd_setImageWithURL:[NSURL URLWithString:userObj.fileUrl] placeholderImage:[UIImage imageNamed:@"icn_default_profile"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
             [self stopIndicator];
    }];

    [_bgImgVw sd_setImageWithURL:[NSURL URLWithString:userObj.fileUrl] placeholderImage:[UIImage imageNamed:@"icn_default_profile"]];
    [_tblVwProfile reloadData];
    }
}

-(void)startIndicator{
    [_imgIndicator setHidden:NO];
    [_imgIndicator startAnimating];
}

-(void)stopIndicator{
    [_imgIndicator stopAnimating];
}

-(void)fetchAllCurrencies{

    if([ReachabilityManager isReachable]){
        NSString *url = [NSString stringWithFormat:@"%@?%@=%@",kFetchAllCurrencyWS,kAppVersionServiceKey,[self fetchAppVersion]];
        [[WebServiceInvocation alloc]initWithWS:url withParams:nil withServiceType:TYPE_GET withSelector:@selector(currencyWSAction:) withTarget:self showLoader:NO loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)currencyWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        _arrCurrency = [NSMutableArray arrayWithArray:[sender responseArray]];
    }
}

-(NSMutableAttributedString *)setAddress:(NSString *)title withAddress:(NSString *)address{
    
    NSMutableAttributedString *attr1 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",title] attributes:@{NSFontAttributeName:Dax_Medium(15)}];
    NSAttributedString *attr2 = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"\n%@",address] attributes:@{NSFontAttributeName:Dax_Regular(15)}];
    
    [attr1 appendAttributedString:attr2];
    
 
   return attr1;
 
}

#pragma mark:--------------------: Tableview Datasource & Delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == CELL_ADDRESS){
        if([userObj.address isEqualToString:kNotProvided]){
              return 80;
        }else{
              return 110;
        }
    }
    else if(indexPath.row == CELL_MOBILE){
        return 60;
    }
    return 84;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static  NSString *cellIdentifier = @"Cell";
    ProfileCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        cell = [[ProfileCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    [self configCell:cell withIndexPath:indexPath];
    return cell;
}

-(void)configCell:(ProfileCell *)cell withIndexPath:(NSIndexPath *)idxPath{

    switch (idxPath.row) {
        case CELL_EMAIL:{
            cell.lblTapToChange.text = [NSString stringWithFormat:@"%@ %@",kTapHereToChange,kEmail];
            cell.imgVwProfileIcons.image = [UIImage imageNamed:@"ic_profile_emailaddress"];
            cell.lblTitle.text = userObj.email;
            cell.lblTapToChangeHeightConst.constant = 21;
        }
        break;
        case CELL_MOBILE:{
//            cell.lblTapToChange.text = [NSString stringWithFormat:@"%@ %@",kTapHereToChange,kMobileNo];
            cell.lblTapToChange.text = @"";
            cell.imgVwProfileIcons.image = [UIImage imageNamed:@"ic_profile_mobileno"];
            cell.lblTitle.text = [NSString stringWithFormat:@"%lu%.0f",userObj.countryCode,userObj.mobileNumber];
            [ cell.lblTitle setFrame:CGRectMake(cell.lblTitle.frame.origin.x,cell.lblTitle.frame.origin.y,cell.lblTitle.frame.size.width,21)];
            cell.lblTapToChangeHeightConst.constant = 0;
        
        }
        break;
        case CELL_ADDRESS:{
            cell.lblTapToChange.text = [NSString stringWithFormat:@"%@ %@",kTapHereToChange,kAddress];
            cell.imgVwProfileIcons.image = [UIImage imageNamed:@"icn_location_new"];
            if([userObj.address isEqualToString:kNotProvided]){
                cell.lblTitle.text = [NSString stringWithFormat:@"%@: %@",kAddress,userObj.address];
            }else{
                
                UILabel *tmpLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.lblTitle.frame.origin.x,cell.lblTitle.frame.origin.y,cell.lblTitle.frame.size.width,21)];
                tmpLabel.numberOfLines = 0;
                tmpLabel.attributedText = [self setAddress:userObj.addressTitle withAddress:userObj.address];
                [tmpLabel sizeToFit];
                cell.lblTitle.attributedText =  tmpLabel.attributedText;
                
                CGFloat heightOfLbl = tmpLabel.frame.size.height;
                if(heightOfLbl > 60){
                    heightOfLbl = 60;
                }
                [ cell.lblTitle setFrame:CGRectMake(cell.lblTitle.frame.origin.x,cell.lblTitle.frame.origin.y,cell.lblTitle.frame.size.width,heightOfLbl)];
                cell.lblTapToChangeHeightConst.constant = 21;
            }
            
        }
        break;
        case CELL_CAR:{
            cell.lblTapToChange.text = [NSString stringWithFormat:@"%@ %@",kTapHereToChange,kCar];
             cell.imgVwProfileIcons.image = [UIImage imageNamed:@"icn_new_car"];
            if([userObj.carBrand isEqualToString:kNotProvided]){
                cell.lblTitle.text = [NSString stringWithFormat:@"%@: %@",kCar,userObj.carBrand];
            }else{
                  cell.lblTitle.text = [NSString stringWithFormat:@"%@ - %@",userObj.carBrand,userObj.carModel];
            }
            cell.lblTapToChangeHeightConst.constant = 21;
        }
        break;
        case CELL_LANGUAGE:{
            cell.lblTapToChange.text = [NSString stringWithFormat:@"%@ %@",kTapHereToChange,kLanguage];
            cell.imgVwProfileIcons.image = [UIImage imageNamed:@"icn_language"];
            cell.lblTitle.text = [NSString stringWithFormat:@"%@: %@",kLanguage,([AppContext.appLanguage isEqualToString:@"ar"])?kArabic : kEnglish];
            cell.lblTapToChangeHeightConst.constant = 21;
            
        }
        break;
        case CELL_CURRENCY:{
            cell.lblTapToChange.text = [NSString stringWithFormat:@"%@ %@",kTapHereToChange,kCurrency];
            cell.imgVwProfileIcons.image = [UIImage imageNamed:@"icn_dollar"];
            cell.lblTitle.text = [NSString stringWithFormat:@"%@: %@",kCurrency,AppContext.currency];
            cell.lblTapToChangeHeightConst.constant = 21;
        }
        break;
            
        default:
            break;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case CELL_EMAIL:{
            [self configAlertVwForEmail:YES];
        }
            break;
        case CELL_MOBILE:{
//            [self configPhoneAlertVw];
        }
            break;
        case CELL_ADDRESS:{
            [self moveToListVCForAddress:YES];
        }
            break;
        case CELL_CAR:{
             [self moveToListVCForAddress:NO];
        }
            break;
        case CELL_LANGUAGE:{
            [self configAlertVwForLanguage];
        }
            break;
        case CELL_CURRENCY:{
            [self configAlertVwForCurrency];
        }
            break;
            
        default:
            break;
    }
    
}

#pragma mark:--------------------: Alertview Functions
-(void)configImgPickerAlertVw{
    
    UIImage *img = [[UIImage imageNamed:@"logo-icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
   alertViewImgPicker = [[CDAlertView alloc]init];
    alertViewImgPicker.messageLabel.text = kSelPicFrom;
    alertViewImgPicker.backgroundColor = RGBA(0, 0, 0, 0.7f);
    alertViewImgPicker.circleFillColor = [UIColor colorWithPatternImage:img];
    alertViewImgPicker.tintColor = [UIColor whiteColor];
    alertViewImgPicker.isActionButtonsVertical = YES;
    alertViewImgPicker.alertBackgroundColor = [UIColor whiteColor];
    alertViewImgPicker.messageTextColor = [UIColor grayColor];
    
    
    CDAlertViewAction *action_Camera = [[CDAlertViewAction alloc]initWithTitle:kCamera font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
        [self configImagePickerWithCamera:YES];
    }];
    
    CDAlertViewAction *action_Photos = [[CDAlertViewAction alloc]initWithTitle:kPhotoLibrary font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
        [self configImagePickerWithCamera:NO];
    }];
    
    
    CDAlertViewAction *action_cancel = [[CDAlertViewAction alloc]initWithTitle:kCancel font:Dax_Regular(15) textColor:[UIColor redColor] backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
        
    }];
    
    [alertViewImgPicker addWithAction:action_Camera];
    [alertViewImgPicker addWithAction:action_Photos];
    [alertViewImgPicker addWithAction:action_cancel];
    
    [alertViewImgPicker show:^(CDAlertView *action) {
        
    }];
    
}

-(void)configAlertVwForEmail:(BOOL)isEmail{
    
    
    @try {
        
        UIImage *img = [[UIImage imageNamed:@"logo-icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        CDAlertView *alertView = [[CDAlertView alloc]init];
        alertView.messageLabel.text = @"";
        alertView.titleLabel.text = (isEmail) ? [NSString stringWithFormat:@"%@ %@",kUpdate,kEmail] : [NSString stringWithFormat:@"%@ %@",kUpdate,kUsername];
        alertView.titleFont = Dax_Medium(15);
        alertView.backgroundColor = RGBA(0, 0, 0, 0.7f);
        alertView.circleFillColor = [UIColor colorWithPatternImage:img];
        alertView.tintColor = [UIColor whiteColor];
        alertView.isActionButtonsVertical = NO;
        alertView.isTextFieldHidden = NO;
        alertView.alertBackgroundColor = [UIColor whiteColor];
        alertView.messageTextColor = [UIColor grayColor];
        alertView.textFieldFont = Dax_Regular(15);
        alertView.textFieldText =  (isEmail) ? userObj.email : userObj.fullName;
        alertView.tintColor = AppThemeColor;
        
        
        CDAlertViewAction *action_txt = [[CDAlertViewAction alloc]initWithTitle:kSave font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            if([alertView.textFieldText isEqualToString:@""]){
                if(isEmail){
                    [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kEmail]];
                }else{
                    [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kUsername]];
                }
                
            }else{
                if(isEmail){
                    [self updateUserEmail:alertView.textFieldText];
                }else{
                    [self updateUserName:alertView.textFieldText];
                }
            }
        }];
        
        
        CDAlertViewAction *action_cancel = [[CDAlertViewAction alloc]initWithTitle:kCancel font:Dax_Regular(15) textColor:[UIColor redColor] backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            
        }];
        
        [alertView addWithAction:action_cancel];
        [alertView addWithAction:action_txt];
        [alertView show:^(CDAlertView *action) {
            
        }];
        
    } @catch (NSException *exception) {
        NSLog(@"Alertview Exception : %@ , In Controller : %@",[exception description],self);
    } @finally {
        
    }

    
   

}

-(void)configPhoneAlertVw{
    
  
    UIImage *img = [[UIImage imageNamed:@"logo-icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    alertViewPhone = [[CDAlertView alloc]initWithFrame:self.view.bounds];
    alertViewPhone.messageLabel.text = kTapOnCountryCode;
    alertViewPhone.titleLabel.text = [NSString stringWithFormat:@"%@ %@",kUpdate,kMobileNo];
    alertViewPhone.titleFont = Dax_Medium(15);
    alertViewPhone.backgroundColor = RGBA(0, 0, 0, 0.7f);
    alertViewPhone.circleFillColor = [UIColor colorWithPatternImage:img];
    alertViewPhone.tintColor = [UIColor whiteColor];
    alertViewPhone.isActionButtonsVertical = NO;
    alertViewPhone.isTextFieldHidden = NO;
    alertViewPhone.alertBackgroundColor = [UIColor whiteColor];
    alertViewPhone.messageTextColor = AppThemeBlueColor;
    alertViewPhone.textFieldFont = Dax_Regular(15);
    alertViewPhone.textFieldText = [NSString stringWithFormat:@"%.0f",userObj.mobileNumber];
    alertViewPhone.textField.keyboardType = UIKeyboardTypePhonePad;
    alertViewPhone.tintColor = AppThemeColor;
    alertViewPhone.messageFont = Dax_Regular(13);
    [self setPaddingVwforTextField:alertViewPhone.textField];
    
    
    CDAlertViewAction *action_txt = [[CDAlertViewAction alloc]initWithTitle:kSave font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
        if([alertViewPhone.textFieldText isEqualToString:@""]){
          [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kMobileNo]];
        }else{
            
            if(![self validatePhoneNumber:countryCode withNumber:alertViewPhone.textField.text]){
                [self showAlert:[NSString stringWithFormat:@"%@ %@",KInvalid,kPhoneNo]];
            }else{
                [self updateUserMobileNo:alertViewPhone.textField.text withCountryCode:countryCode];
            }
        }

    }];
    
    
    CDAlertViewAction *action_cancel = [[CDAlertViewAction alloc]initWithTitle:kCancel font:Dax_Regular(15) textColor:[UIColor redColor] backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
        btnPhoneNoAction = nil;
    }];
    
    [alertViewPhone addWithAction:action_cancel];
    [alertViewPhone addWithAction:action_txt];
    
    
    [alertViewPhone show:^(CDAlertView *action) {
        
    }];
}

-(void)configAlertVwForLanguage{
    
    
    @try {
        
        UIImage *img = [[UIImage imageNamed:@"logo-icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        CDAlertView *alertView = [[CDAlertView alloc]init];
        alertView.messageLabel.text = kSelectLanguage;
        alertView.backgroundColor = RGBA(0, 0, 0, 0.7f);
        alertView.circleFillColor = [UIColor colorWithPatternImage:img];
        alertView.tintColor = [UIColor whiteColor];
        alertView.isActionButtonsVertical = YES;
        alertView.alertBackgroundColor = [UIColor whiteColor];
        alertView.messageTextColor = [UIColor grayColor];
        
        
        CDAlertViewAction *action_eng = [[CDAlertViewAction alloc]initWithTitle:kEnglish font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            [self changeLanguage:@"en"];
        }];
        
        CDAlertViewAction *action_arabic = [[CDAlertViewAction alloc]initWithTitle:kArabic font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            [self changeLanguage:@"ar"];
        }];
        
        
        CDAlertViewAction *action_cancel = [[CDAlertViewAction alloc]initWithTitle:kCancel font:Dax_Regular(15) textColor:[UIColor redColor] backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            
        }];
        
        [alertView addWithAction:action_eng];
        [alertView addWithAction:action_arabic];
        [alertView addWithAction:action_cancel];
        
        [alertView show:^(CDAlertView *action) {
            
        }];
        
    } @catch (NSException *exception) {
        NSLog(@"Alertview Exception : %@ , In Controller : %@",[exception description],self);
    } @finally {
        
    }
    
    
    
    
}

-(void)configAlertVwForCurrency{
    
    
    @try {
        
        UIImage *img = [[UIImage imageNamed:@"logo-icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        CDAlertView *alertView = [[CDAlertView alloc]init];
        alertView.messageLabel.text = [NSString stringWithFormat:@"%@ %@",kSelect,kCurrency];
        alertView.backgroundColor = RGBA(0, 0, 0, 0.7f);
        alertView.circleFillColor = [UIColor colorWithPatternImage:img];
        alertView.tintColor = [UIColor whiteColor];
        alertView.isActionButtonsVertical = YES;
        alertView.alertBackgroundColor = [UIColor whiteColor];
        alertView.messageTextColor = [UIColor grayColor];
        
        [_arrCurrency enumerateObjectsUsingBlock:^(WSAllCurrency *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            CDAlertViewAction *action_currency = [[CDAlertViewAction alloc]initWithTitle:obj.currency font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
                [self setCurrencyExchangeRate:action1.accessibilityValue.integerValue];
                
            }];
            action_currency.accessibilityValue = [NSString stringWithFormat:@"%lu",obj.internalBaseClassIdentifier];
            [alertView addWithAction:action_currency];
        }];
        
        
        CDAlertViewAction *action_cancel = [[CDAlertViewAction alloc]initWithTitle:kCancel font:Dax_Regular(15) textColor:[UIColor redColor] backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            
        }];
        
        [alertView addWithAction:action_cancel];
        
        [alertView show:^(CDAlertView *action) {
            
        }];

        
    } @catch (NSException *exception) {
        NSLog(@"Alertview Exception : %@ , In Controller : %@",[exception description],self);
    } @finally {
        
    }
    
    
    
}

-(void)setPaddingVwforTextField:(UITextField *)txtField{
    

    btnPhoneNoAction = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnPhoneNoAction setFrame:CGRectMake(5, 0,35, 30)];
    [btnPhoneNoAction setBackgroundColor:[UIColor clearColor]];
    [btnPhoneNoAction setTitle:[NSString stringWithFormat:@"+%@",countryCode] forState:UIControlStateNormal];
    [btnPhoneNoAction setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnPhoneNoAction.titleLabel setFont:Dax_Bold(15)];
    [btnPhoneNoAction addTarget:self action:@selector(showCountryPicker:) forControlEvents:UIControlEventTouchUpInside];
    txtField.leftViewMode = UITextFieldViewModeAlways;
    txtField.leftView = btnPhoneNoAction;
  
}

-(void)showCountryPicker:(id)sender{
    [alertViewPhone endEditing:YES];
    RTCountryPicker *countryPicker = [[RTCountryPicker alloc]initWithFrame:self.view.bounds];
    countryPicker.delegate = self;
    [alertViewPhone addSubview:countryPicker];
}

-(void)setCurrencyExchangeRate:(NSInteger)currencyId{
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"internalBaseClassIdentifier == %lu",currencyId]];
    NSArray *filteredArr = [_arrCurrency filteredArrayUsingPredicate:pred];
    if(filteredArr.count > 0){
          WSAllCurrency *objCurrency = [filteredArr objectAtIndex:0];
        AppContext.currency =[self isEnglishLanguage] ?  objCurrency.currency :  objCurrency.currencyAR;
        AppContext.exchangeRate = objCurrency.exchangeRate;
        [Functions setIntegerValueToUserDefaults:objCurrency.internalBaseClassIdentifier withKey:CURRENCY_ID];
        [_tblVwProfile reloadData];
    }
  
    
}
#pragma mark: -------------------: CountryPicker Delegates
-(void)pickerSelectedCountry:(NSString *)selectedCode{
    countryCode = selectedCode;
    [btnPhoneNoAction setTitle:[NSString stringWithFormat:@"+%@",countryCode] forState:UIControlStateNormal];
}

#pragma mark:--------------------: Button Actions
- (IBAction)btnCameraActioin:(id)sender {
    [self configImgPickerAlertVw];
}

- (IBAction)btnEditNameAction:(id)sender {
    [self configAlertVwForEmail:NO];
}

- (IBAction)btnChangePswdAction:(id)sender {
    WSUser *objUser = [Functions getCustomObjectFromDefaults:USER_OBJ];
    if(objUser.socialType > 0){
        [self showAlert:kCannotChangePswd];
    }else{
        ChangePasswordVC *cpvc = loadViewController(kSBOthers, kChangePasswordVC);
        [self.navigationController pushViewController:cpvc animated:YES];
    }
   
}

- (IBAction)btnShareAction:(id)sender {
    
    NSString *itunesAppLinK = kApp_AppStoreLink;
    NSURL *shareUrl = [[NSURL alloc]initWithString:itunesAppLinK];

    NSArray *activityItems = @[shareUrl];
    UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewControntroller.excludedActivityTypes = @[];
    [self presentViewController:activityViewControntroller animated:true completion:nil];

}


-(void)moveToListVCForAddress:(BOOL)showAddress{
    VehicleListVC *vlvc = loadViewController(kSBRequest, kVehicleListVC);
    vlvc.showAddress = showAddress;
    [self.navigationController pushViewController:vlvc animated:YES];
}

#pragma mark: -------------------: ImagePicker Functions
-(void)configImagePickerWithCamera:(BOOL)isCamera{
    
    alertViewImgPicker = nil;
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    
    if(isCamera){
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        
    }else{
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    [self presentViewController:picker animated:YES completion:NULL];
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    pickerDismissed = YES;
    UIImage *selectedImg =(UIImage *)info[UIImagePickerControllerEditedImage];
    [self updateProfilePic:selectedImg];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    pickerDismissed = YES;
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark: -------------------: WebService Functions
-(void)updateProfilePic:(UIImage *)img{
    
    NSData *imgData=UIImageJPEGRepresentation( img,0.7);
    UIImage *compressedImage =[UIImage imageWithData:imgData];
    
    if([ReachabilityManager isReachable]){

        [self startIndicator];
        NSDictionary *dictParams = @{@"userImg" : compressedImage,
                                                        @"userId":[Functions getStringValueFromDefaults:USER_ID]};
        
        [[WebServiceInvocation alloc]initWithWS:kUpdateProfileWS withParams:dictParams withServiceType:TYPE_POST withSelector:@selector(profileWSAction:) withTarget:self showLoader:NO loaderMsg:kWSLoaderDialoagMsg];
    }else{
        
        ShowNoNetworkAlert(self);
    }

}

-(void)updateUserName:(NSString *)fullname{
    
    if([ReachabilityManager isReachable]){

        NSDictionary *dictParams = @{@"fullName" : fullname,
                                     @"userId":[Functions getStringValueFromDefaults:USER_ID]};
        
        [[WebServiceInvocation alloc]initWithWS:kUpdateProfileWS withParams:dictParams withServiceType:TYPE_POST withSelector:@selector(profileWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
    }else{
        ShowNoNetworkAlert(self);
    }

}

-(void)updateUserEmail:(NSString *)email{
    
    if([ReachabilityManager isReachable]){
  
        NSDictionary *dictParams = @{@"email" : email,
                                     @"userId":[Functions getStringValueFromDefaults:USER_ID]};
        
        [[WebServiceInvocation alloc]initWithWS:kUpdateProfileWS withParams:dictParams withServiceType:TYPE_POST withSelector:@selector(profileWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
    }else{
        ShowNoNetworkAlert(self);
    }
    
}

-(void)updateUserMobileNo:(NSString *)phoneNo withCountryCode:(NSString *)code{
    
    if([ReachabilityManager isReachable]){
        NSDictionary *dictParams = @{@"mobileNumber" : phoneNo,
                                                        @"countryCode" : code,
                                     @"userId":[Functions getStringValueFromDefaults:USER_ID]};
        
        [[WebServiceInvocation alloc]initWithWS:kUpdateProfileWS withParams:dictParams withServiceType:TYPE_POST withSelector:@selector(profileWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)profileWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self stopIndicator];
        [self showAlert:[sender responseErr]];
    }else{
        pickerDismissed = NO;
        [self fetchUserDetailsFromDB];
    }
}

-(void)changeLanguage:(NSString *)language{
    
    [UIAlertController showAlertInVC:self withTitle:AppName withMsg:kChangeLanguage withOtherAlertActionTitle:@[kAlertYes] withAlertActionCancel:kAlertNo withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
        if(buttonIndex == 0){
            [Functions setStringValueToUserDefaults:language withKey:DEFAULT_APP_LANGUAGE];
            AppContext.appLanguage = language;
            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:language, nil] forKey:@"AppleLanguages"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [self updateLanguage:language];
        }
    }];
}

-(void)updateLanguage:(NSString *)language{
    

    NSString *deviceToken = [Functions getStringValueFromDefaults:DEVICE_TOKEN];
    if(deviceToken == nil){
        deviceToken = @"FailToRetrievetoken";
    }
    if([ReachabilityManager isReachable]){
        NSDictionary *dictParams = @{   @"deviceToken" : deviceToken,
                                                            @"languageCode" : language
                                                        };
        [[WebServiceInvocation alloc]initWithWS:kUpdateLanguageWS withParams:dictParams withServiceType:TYPE_POST withSelector:@selector(updateLanguageWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
     
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)updateLanguageWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        exit(EXIT_SUCCESS);
       /* if([AppContext.appLanguage isEqualToString:@"ar"]){
            [[Localization sharedInstance] setPreferred:@"ar" fallback:@"en"];
            [UIView appearance].semanticContentAttribute = UISemanticContentAttributeForceRightToLeft;
        }else{
            [[Localization sharedInstance] setPreferred:@"en" fallback:@"ar"];
            [UIView appearance].semanticContentAttribute = UISemanticContentAttributeForceLeftToRight;
        }
        [self dismissViewControllerAnimated:NO completion:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:LANGUAGE_CHANGED object:AppContext.appLanguage];
        */
    }
}
@end
