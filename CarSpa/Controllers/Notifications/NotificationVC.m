//
//  NotificationVC.m
//  CarSpa
//
//  Created by R@j on 22/02/18.
//  Copyright © 2018 Rajan@VS2. All rights reserved.
//

#import "NotificationVC.h"
#import "NotificationCell.h"
#import "WSNotifications.h"
#import "CD_NOTIFICATIONS+CoreDataClass.h"

@interface NotificationVC (){
    UIBarButtonItem *btnDelete;
}

@end

@implementation NotificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}

-(void)setupUI{

    btnDelete = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"icn_trash_white"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(btnDeleteClick:)];
    self.navigationItem.rightBarButtonItem = btnDelete;
    
    self.tblVwNotification.estimatedRowHeight = 114;
    self.tblVwNotification.rowHeight = UITableViewAutomaticDimension;
    self.tblVwNotification.allowsMultipleSelectionDuringEditing = NO;
    self.tblVwNotification.tableFooterView = [UIView new];
    [self fetchNotificationFromDB];
}

-(void)fetchNotificationFromDB{
    
    _arrNotifications = [NSMutableArray new];
    NSMutableArray *tmpArray = [NSMutableArray new];
    [tmpArray  addObjectsFromArray:[CORE_DATA_INSTANCE.dataManager fetchRecordsWithFetchRequest:Table_Notifications withPredicate:nil withViewContenxt:CORE_DATA_INSTANCE.nsManagedObjectContext]];
    
    for(CD_NOTIFICATIONS *dbObj in tmpArray){
        WSNotifications *objNotification = [WSNotifications new];
        objNotification.notificationType =  dbObj.notificationType;
        objNotification.title = [NSString stringWithFormat:@"%@",dbObj.notificationTitle];
        objNotification.message = [NSString stringWithFormat:@"%@",dbObj.notificationMsg];
        objNotification.notificationId = dbObj.notificationId;
        objNotification.notificationDate = [NSString stringWithFormat:@"%@",dbObj.notificationDate];
        [_arrNotifications addObject:objNotification];
    }
    
    NSSortDescriptor *sortDesc = [[NSSortDescriptor alloc]initWithKey:@"notificationId" ascending:NO];
    NSArray *sortedArray = [_arrNotifications sortedArrayUsingDescriptors:@[sortDesc]];
    _arrNotifications.array = sortedArray;
    [_tblVwNotification reloadData];
    
    if(_arrNotifications.count > 0){
        [btnDelete setEnabled:YES];
    }else{
        [btnDelete setEnabled:NO];
        [_tblVwNotification setScrollEnabled:NO];
    }
    
}

-(void)btnDeleteClick:(id)sender{
    if(_arrNotifications.count > 0){
        [UIAlertController showAlertInVC:self withTitle:AppName withMsg:kDeleteAllNotificationMsg withOtherAlertActionTitle:@[kAlertYes] withAlertActionCancel:kAlertNo withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
            if(buttonIndex == 0){
                [self deleteAllNotifications];
            }
        }];
    }
    
}

-(void)deleteAllNotifications{
    [[SyncCDAdaptor sycnInstance]deleteAllRecords:Table_Notifications];
    [self fetchNotificationFromDB];
}

#pragma mark : --------- :  TableView Delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrNotifications.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return (_arrNotifications.count == 0) ? tableView.frame.size.height : 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if(_arrNotifications.count == 0){
        
        UIView *notificationVw = [[UIView alloc]initWithFrame:CGRectMake(0, 0,tableView.frame.size.width,tableView.frame.size.height-64)];
        notificationVw.backgroundColor = RGBA(230, 230, 230, 1);
        
        UILabel *lblMsg = [[UILabel alloc]initWithFrame:notificationVw.frame];
        lblMsg.text = kNoNotificationMsg;
        lblMsg.font = Dax_Medium(16);
        lblMsg.textColor = RGBA(120,120, 120, 1);
        lblMsg.textAlignment = NSTextAlignmentCenter;
        
        UIImageView *imgVwLogo = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,44, 44)];
        [imgVwLogo setImage:[UIImage imageNamed:@"icn_loader_img"]];
        [imgVwLogo setCenter:CGPointMake(notificationVw.center.x,notificationVw.center.y - 40)];
        
        [notificationVw addSubview:lblMsg];
        [notificationVw addSubview:imgVwLogo];
        return notificationVw;
    }else{
        return [UIView new];
    }
   
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"Cell";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    WSNotifications *objNotification = [_arrNotifications objectAtIndex:indexPath.row];
    cell.lblHeader.text = [NSString stringWithFormat:@"%@",objNotification.title];
    cell.lblContent.text = [NSString stringWithFormat:@"%@",objNotification.message];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    [formatter setDateFormat:@"dd/MM/yyyy hh:mm:ss a"];
    NSDate *createdDate = [formatter dateFromString:objNotification.notificationDate];
    cell.lblTime.text = [createdDate timeAgo];
    
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}



-(NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewRowAction *rowEditAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:kShare handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        WSNotifications *objNotification = [_arrNotifications objectAtIndex:indexPath.row];
        [self shareOffer:objNotification];
    }];
    rowEditAction.backgroundColor = AppThemeColor;
    
    
    UITableViewRowAction *rowDeleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:kDelete handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        WSNotifications *objNotification = [_arrNotifications objectAtIndex:indexPath.row];
        [self deleteNotificationWithId:objNotification.notificationId withIndexPath:indexPath];
    }];
    rowDeleteAction.backgroundColor = [UIColor redColor];
    return @[rowDeleteAction,rowEditAction];
    
}



-(void)deleteNotificationWithId:(NSInteger)notifiactionId withIndexPath:(NSIndexPath *)indexPath{
    [[SyncCDAdaptor sycnInstance]deleteRecords:Table_Notifications withPredicate:[NSString stringWithFormat:@"notificationId == %lu",notifiactionId]];
    [_arrNotifications removeObjectAtIndex:indexPath.row];
    [_tblVwNotification beginUpdates];
    [_tblVwNotification deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:indexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    [_tblVwNotification endUpdates];
}

-(void)shareOffer:(WSNotifications *)notification{
    
    NSString *offerToShare = [NSString stringWithFormat:@"%@ \n %@",notification.title,notification.message];
    NSString *url=kApp_AppStoreLink;
    NSString * title =[NSString stringWithFormat:@"%@!  \n\n %@ %@ %@ \n %@",offerToShare,kDownload,AppName,kApp,url];
    
    NSArray* dataToShare = @[title];
    
    UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:dataToShare applicationActivities:nil];
    [self presentViewController:activityViewControntroller animated:true completion:nil];
}

@end
