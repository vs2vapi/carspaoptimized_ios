//
//  NotificationVC.h
//  CarSpa
//
//  Created by R@j on 22/02/18.
//  Copyright © 2018 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationVC : UIViewController <UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tblVwNotification;
@property (strong, nonatomic) NSMutableArray *arrNotifications;
@end
