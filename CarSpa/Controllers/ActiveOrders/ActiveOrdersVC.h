//
//  ActiveOrdersVC.h
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActiveOrdersVC : BaseVC <UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tblVwOrders;
@property(strong,nonatomic)NSMutableArray *arrRequest;

@end
