//
//  ActiveOrdersVC.m
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "ActiveOrdersVC.h"
#import "RequestCell.h"
#import "JobDetailsVC.h"
#import "RTTableLoader.h"
#import "WSJobRequests.h"
#import "WSBill.h"
#import "WSVan.h"
#import "WSCartype.h"
#import "WSServices.h"
#import "WSAppointmentStatus.h"
#import "WSCategory.h"
#import "WSOwner.h"


@interface ActiveOrdersVC (){
    BOOL loadMore;
    NSInteger pageIndex;
    RTTableLoader *objTableLoader;
    UIRefreshControl *refreshControl;
}

@end

@implementation ActiveOrdersVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(![self isUserLoggedIn]){
        [self showAlertForLogin];
    }else{
        [self setupUI];
        pageIndex = 0;
        _arrRequest = [NSMutableArray new];
        [self fetchUserRequest:YES withPageIndex:pageIndex];
    }

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:NO];
     AppContext.isOrderScreen = YES;
   
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    AppContext.isOrderScreen = NO;
}

#pragma mark:--------------------: Basic Setups
-(void)setupUI{
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateOrder) name:REFRESH_ORDER_LIST object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationReceived:) name:NOTIFICATION_RECEIVED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAllList) name:NEW_ORDER_BOOKED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appointmentChanged:) name:EDIT_REQUEST_APPOINTMENT_CHANGED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appointmentCancelled:) name:APPOINTMENT_CANCELLED object:nil];

    
    _tblVwOrders.separatorColor = [UIColor clearColor];
    objTableLoader = [[RTTableLoader alloc]initWithFrame:_tblVwOrders.frame];
    [objTableLoader setLoaderMsg:@""];
    _tblVwOrders.tableFooterView = objTableLoader;
    [objTableLoader stopIndicator];
    
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor lightGrayColor];
    [refreshControl addTarget:self action:@selector(refershControlAction) forControlEvents:UIControlEventValueChanged];
    [self.tblVwOrders addSubview:refreshControl];
    
}

-(void)refershControlAction{
    [refreshControl beginRefreshing];
    [self updateOrder];
}

-(void)updateOrder{
    pageIndex=0;
    [_arrRequest removeAllObjects];
    [_tblVwOrders reloadData];
    [self fetchUserRequest:NO withPageIndex:pageIndex];
}

-(void)refreshAllList{
    pageIndex=0;
    [_arrRequest removeAllObjects];
    [_tblVwOrders reloadData];
    [self fetchUserRequest:NO withPageIndex:pageIndex];
}

-(void)notificationReceived:(NSNotification *)note {

    
    NSString *appointmentId = [[note object]valueForKey:@"appointmentId"];
    NSInteger statusType =  [[[note object]valueForKey:@"notificationType"]integerValue];
    
    
    WSAppointmentStatus *objStatus = [WSAppointmentStatus new];
    objStatus.appointmentStatusIdentifier = appointmentId.doubleValue;
    objStatus.status = statusType;
    objStatus.datetime = kJustNow;
    
    
    __block NSInteger index = -1;
    
    [_arrRequest enumerateObjectsUsingBlock:^(WSJobRequests *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if(obj.internalBaseClassIdentifier == appointmentId.integerValue){
            NSMutableArray *tmpArr = [NSMutableArray arrayWithArray:obj.appointmentStatus];
            [tmpArr addObject:objStatus];
            obj.appointmentStatus = [tmpArr mutableCopy];
            obj.status = statusType;
            index = idx;
        }
    }];
    
    if(index != -1){
        if(statusType == STATUS_COMPLETED){
            [_arrRequest removeObjectAtIndex:index];
            [_tblVwOrders beginUpdates];
            [_tblVwOrders deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            [_tblVwOrders endUpdates];
        }else{
            [_tblVwOrders reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationMiddle];
        }
       
    }else{
        [self updateOrder];
    }
}

-(void)appointmentChanged:(NSNotification *)notification{
    
    WSJobRequests *request = [notification object];
    __block NSInteger index;
    [_arrRequest enumerateObjectsUsingBlock:^(WSJobRequests *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if(obj.internalBaseClassIdentifier == request.internalBaseClassIdentifier){
            index = idx;
            *stop = YES;
        }
    }];
    [_arrRequest replaceObjectAtIndex:index withObject:request];
    [_tblVwOrders reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

-(void)appointmentCancelled:(NSNotification *)notification{
    
    NSString *appointmentId = [notification object];
      __block NSInteger index;
    [_arrRequest enumerateObjectsUsingBlock:^(WSJobRequests *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if(obj.internalBaseClassIdentifier == appointmentId.integerValue){
            index = idx;
            *stop = YES;
        }
    }];
    
    [_arrRequest removeObjectAtIndex:index];
    [_tblVwOrders beginUpdates];
    [_tblVwOrders deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    [_tblVwOrders endUpdates];
}

-(void)fetchUserRequest:(BOOL)showLoader withPageIndex:(NSInteger)pgIndex{
    
    if([ReachabilityManager isReachable]){
        
        if(!showLoader){
            [objTableLoader startIndicator];
        }
     
        NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
        NSString *url = [NSString stringWithFormat:@"%@?userId=%@&pageIndex=%lu&language=%@&%@=%@",kFetchRequestWS,userId,pgIndex,AppContext.appLanguage,kAppVersionServiceKey,[self fetchAppVersion]];
        
        [[WebServiceInvocation alloc]initWithWS:url withParams:nil withServiceType:TYPE_GET withSelector:@selector(requestWSAction:) withTarget:self showLoader:showLoader loaderMsg:kWSLoaderDialoagMsg];

    }else{
         [objTableLoader stopIndicator];
        ShowNoNetworkAlert(self);
    }
}

-(void)requestWSAction:(id)sender{
    
    if([refreshControl isRefreshing]){
         [refreshControl endRefreshing];
    }
   
    [objTableLoader stopIndicator];
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
   //     [self showAlert:[sender responseErr]];
    }else{
        
        [[sender responseArray]enumerateObjectsUsingBlock:^(WSJobRequests *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [_arrRequest addObject:obj];
        }];
        
        if(_arrRequest.count > 0){
            pageIndex = [[[sender responseDictionary]valueForKey:@"CurrentPage"]integerValue];
            loadMore = (pageIndex > 0) ? YES : NO;
        }else{
            loadMore = NO;
        }
        [_tblVwOrders reloadData];
    }
}

#pragma mark:--------------------: Tableview Datasource & Delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrRequest.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 240;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return (_arrRequest.count == 0) ? tableView.frame.size.height : 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if(_arrRequest.count == 0){
        UIView *headerVw = [[UIView alloc]initWithFrame:tableView.frame];
        UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(20, 0,headerVw.frame.size.width-40,50)];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.center = CGPointMake(headerVw.center.x,headerVw.center.y-50);
        lblTitle.font = Dax_Regular(15);
        lblTitle.text = kNoActiveOrder;
        lblTitle.numberOfLines = 3;
        lblTitle.textColor = [UIColor blackColor];
        [headerVw addSubview:lblTitle];
        return headerVw;
    }
    return [UIView new];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static  NSString *cellIdentifier = @"Cell";
    RequestCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"RequestCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    WSJobRequests *objRequest = [_arrRequest objectAtIndex:indexPath.row];
    cell.lblRequestNo.text = [NSString stringWithFormat:@"%@ %@",kRequest,objRequest.suffix];
    [cell.btnRateMe setHidden:YES];
    cell.btnRateHeightConst.constant = 0;
    
    

    WSBill *objBill = objRequest.bill;
    NSString  *strCurrency =  [NSString stringWithFormat:@"%@",objBill.currency];
//    WSVan *objVan = objRequest.van;
//    WSOwner *objOwner = objVan.owner;
//    if(objOwner.isAppliedForTax == 1){
//        cell.lblAmount.text = [NSString stringWithFormat:@"%.2f %@",objBill.netAmount,strCurrency];
//    }else{
//         cell.lblAmount.text = [NSString stringWithFormat:@"%.2f %@",objBill.totalAmount,strCurrency];
//    }
    
      cell.lblAmount.text = [NSString stringWithFormat:@"%.2f %@",objBill.netAmount,strCurrency];
//    cell.lblAmount.text = [NSString stringWithFormat:@"%.2f %@",objBill.totalAmount,strCurrency];
 
    WSCartype *objCarType = objRequest.carType;
    if(objCarType==nil){
        cell.lblCarType.text = kNotProvided;
    }else{
        cell.lblCarType.text = objCarType.name;
    }
    
    __block NSString *strServices = @"";
    [objRequest.services enumerateObjectsUsingBlock:^(WSServices *objService, NSUInteger idx, BOOL * _Nonnull stop) {
        if(idx == 0){
             strServices = [strServices stringByAppendingString:[NSString stringWithFormat:@"%@",objService.name]];
        }else{
             strServices = [strServices stringByAppendingString:[NSString stringWithFormat:@", %@",objService.name]];
        }
    }];
    
    cell.lblService.text = strServices;

    WSAppointmentStatus *objAppointment = [objRequest.appointmentStatus objectAtIndex:0];
    
    NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *newDate  =  [formatter dateFromString:objRequest.appointmentDatetime];
    if([AppContext.appLanguage isEqualToString:@"ar"]){
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ar"]];
    }
    [formatter setDateFormat:ActiveOrder_DateFormat];
    NSString *formattedDate = [formatter stringFromDate:newDate];
 
    
    cell.lblTime.text = objAppointment.datetime;
//    cell.lblAppointmentTime.text = objRequest.appointmentDatetime;
    cell.lblAppointmentTime.text = formattedDate;
    [cell setRequestStatus:objRequest.status];
    
    WSCategory *objCategory  = objRequest.category;
    cell.lblCategory.text = objCategory.name;
    
    return cell;
}



- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

    if((indexPath.row ==( _arrRequest.count - 1) && (loadMore))){
        [self fetchUserRequest:NO withPageIndex:pageIndex];
    }

}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    WSJobRequests *objRequest = [_arrRequest objectAtIndex:indexPath.row];
    JobDetailsVC *jdvc = loadViewController(kSBRequest, kJobDetailsVC);
    jdvc.objRequestDetail = objRequest;
    [self.navigationController pushViewController:jdvc animated:YES];
}

@end
