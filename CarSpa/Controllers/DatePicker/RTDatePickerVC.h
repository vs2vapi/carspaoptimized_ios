//
//  RTDatePickerVC.h
//  CarSpa
//
//  Created by R@j on 22/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RTDatePickerDelegate <NSObject>

-(void)pickerSelectedDate:(NSDate *)selectedDate;

@end

typedef enum : NSUInteger {
    DateMode = UIDatePickerModeDate,
    DateTimeMode = UIDatePickerModeDateAndTime,
    TimeMode = UIDatePickerModeTime
} DatePickerMode;

@interface RTDatePickerVC : UIView

@property (strong, nonatomic) IBOutlet UIView *bgVw;
@property (weak, nonatomic) IBOutlet UIView *pickerVw;
@property (weak, nonatomic) IBOutlet UIDatePicker *myDatePicker;
@property (weak, nonatomic) IBOutlet UIView *dtHolidayVw;
@property (weak, nonatomic) IBOutlet UILabel *lblOffDay;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;


@property(strong,nonatomic)id<RTDatePickerDelegate>delegate;
@property(strong,nonatomic)NSMutableArray *arrSeviceId;
@property(strong,nonatomic)NSMutableArray *arrHolidays;
@property(nonatomic,assign)BOOL showHolidayList;


- (IBAction)btnCancelAction:(id)sender;
- (IBAction)btnOkAction:(id)sender;
- (IBAction)btnBackgroundAction:(id)sender;
- (IBAction)dateChangedAction:(id)sender;

-(void)setSelectedDate:(NSDate *)selDate;
-(void)setMaxDate:(NSDate *)minDate;
-(void)setDtPickerMode:(NSInteger )mode;
-(void)setMinDate:(NSDate *)minDate;

@end
