//
//  RTDatePickerVC.m
//  CarSpa
//
//  Created by R@j on 22/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "RTDatePickerVC.h"
#import "WSHolidayList.h"


@interface RTDatePickerVC ()

@end


@implementation RTDatePickerVC

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if(self){
        [[NSBundle mainBundle]loadNibNamed:@"RTDatePicker" owner:self options:nil];
        [self addSubview:self.bgVw];
        [self showVw:YES];
        [self.myDatePicker setLocale:[NSLocale localeWithLocaleIdentifier:AppContext.appLanguage]];
    }

    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
     self.bgVw.frame = self.frame;
    if(_showHolidayList){
        [self.pickerVw setHidden:YES];
        _dtHolidayVw.layer.cornerRadius = _dtHolidayVw.frame.size.height/2;
        _lblOffDay.text = kOffDay;
        [self fetchHolidaysList];
    }
}



#pragma mark :--------------------: Setup UI
-(void)showVw:(BOOL)show{
    
    if(show){
        
        [_pickerVw setFrame:CGRectMake(0,self.frame.size.height, _pickerVw.frame.size.width, _pickerVw.frame.size.height)];
        [UIView animateWithDuration:0.5f delay:0.0f usingSpringWithDamping:0.7f initialSpringVelocity:.5f options:UIViewAnimationOptionCurveEaseOut animations:^{
            [_pickerVw setFrame:CGRectMake(0,self.frame.size.height - _pickerVw.frame.size.height, _pickerVw.frame.size.width, _pickerVw.frame.size.height)];
        } completion:^(BOOL finished) {
            
        }];
    }else{
      
        [UIView animateWithDuration:0.3f animations:^{
               [_pickerVw setFrame:CGRectMake(0,self.frame.size.height, _pickerVw.frame.size.width, _pickerVw.frame.size.height)];
        } completion:^(BOOL finished) {
            [self.bgVw removeFromSuperview];
            [self removeFromSuperview];
        }];
    }
   
}

#pragma mark :--------------------: Date Functions
-(void)setSelectedDate:(NSDate *)selDate{
    [_myDatePicker setDate:selDate];
}

-(void)setMaxDate:(NSDate *)minDate{
    [_myDatePicker setMaximumDate:minDate];
}

-(void)setMinDate:(NSDate *)minDate{
    [_myDatePicker setMinimumDate:minDate];
}

-(void)setDtPickerMode:(NSInteger )mode{
    [_myDatePicker setDatePickerMode:mode];
}

#pragma mark :--------------------: Button Actions
- (IBAction)btnCancelAction:(id)sender{
      [self showVw:NO];
}

- (IBAction)btnOkAction:(id)sender {
    if([_dtHolidayVw isHidden]){
        [self.delegate pickerSelectedDate:_myDatePicker.date];
        [self showVw:NO];
    }
}

- (IBAction)btnBackgroundAction:(id)sender {
    [self showVw:NO];
}

#pragma mark :--------------------: Holiday List Functions
-(void)fetchHolidaysList{
    
    if([ReachabilityManager isReachable]){
        NSString *serviceId = [_arrSeviceId componentsJoinedByString:@","];
        NSDictionary *dictParms = @{@"serviceId":serviceId};
        [[WebServiceInvocation alloc]initWithWS:kFetchHolidayWS withParams:dictParms withServiceType:TYPE_POST withSelector:@selector(holidayWSAction:) withTarget:self showLoader:NO loaderMsg:kWSLoaderDialoagMsg];
    }else{
        [self btnBackgroundAction:nil];
    }
}

-(void)holidayWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
     [self.pickerVw setHidden:NO];
    if([sender responseCode] > 200){
        
    }else{
     
      
        _arrHolidays = [NSMutableArray new];
        NSMutableArray *arrId = [NSMutableArray new];
        NSMutableArray *tmpArr = [NSMutableArray arrayWithArray:[sender responseArray]];
        [tmpArr enumerateObjectsUsingBlock:^(WSHolidayList *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSInteger holidayId = obj.internalBaseClassIdentifier;
            if(![arrId containsObject:[NSNumber numberWithInteger:holidayId]]){
                [arrId addObject:[NSNumber numberWithInteger:holidayId]];
                [_arrHolidays addObject:obj];
            }
        }];

        if(_arrHolidays.count > 0){
              AppContext.requestHelper.arrHolidayList = _arrHolidays;
        }
    }
    
}

#pragma mark :--------------------: DatePicker Functions
- (IBAction)dateChangedAction:(id)sender {
    
    [_dtHolidayVw setHidden:YES];
    NSDate *pickedDate = _myDatePicker.date;

    //holiday Type == 2 . then hide date, if holiday type == 1 then hide day
    [_arrHolidays enumerateObjectsUsingBlock:^(WSHolidayList   *obj, NSUInteger idx, BOOL * _Nonnull stop) {
      
        if(obj.holidayType == 1){
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"EEEE"];
            NSString *dayName = [formatter stringFromDate:pickedDate];
             NSString *holidayDay = [NSString stringWithFormat:@"%@",obj.day];
            if([holidayDay isEqualToString:dayName]){
                [_dtHolidayVw setHidden:NO];
                *stop = YES;
            }
            
        }else if(obj.holidayType == 2){
            
            NSString *holidayDate = [NSString stringWithFormat:@"%@",obj.date];
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"yyyy-MM-dd"];
            NSString *selectedDate = [formatter stringFromDate:pickedDate];
            if([holidayDate isEqualToString:selectedDate]){
                [_dtHolidayVw setHidden:NO];
                *stop = YES;
            }
        }
      
    }];
    
}

-(NSInteger )getHolidayMonth:(NSString *)date{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *holidayDate = [formatter dateFromString:date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents *holidayDateComponents = [calendar components:NSCalendarUnitMonth fromDate:holidayDate];
    NSInteger holidayMonth = [holidayDateComponents month];
    return holidayMonth;
    
}

@end
