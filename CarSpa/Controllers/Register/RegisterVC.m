//
//  RegisterVC.m
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "RegisterVC.h"
#import "RegisterCell.h"
#import "VerificationVC.h"
#import "CarSpa-Swift.h"
#import "PrivacyPolicyVC.h"

typedef enum : NSUInteger {
    
    CELL_IMG = 0,
    CELL_NAME = 1,
    CELL_EMAIL = 2,
     CELL_CONFIRM_EMAIL = 3,
    CELL_PHONE = 4,
    CELL_PASSWORD = 5,
    CELL_CONFIRM_PASSWORD = 6,
     CELL_TERMS = 7
    
} REGISTER_CELL;

@implementation RegisterObj

-(instancetype)init{
    if(self = [super init]){
        
        _name = @"";
         _email = @"";
         _confirmEmail = @"";
         _phoneNo = @"";
         _countryCode = [Functions getStringValueFromDefaults:USER_PHONE_CODE];
        _password = @"";
        _confirmPswd = @"";
        _isTermsChecked = NO;
      //  _profileImg = [UIImage imageNamed:@"user-profile"];
    }
    return self;
}

@end

@interface RegisterVC ()<RTCountryPickerDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    NSIndexPath *selectedIndexPath;
    RegisterObj *objRegister;
}

@end

@implementation RegisterVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    hideNavBar(NO);
    [self setupNavBar];
    [self setUI];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark: -------------------: SetupUI
-(void)setUI{
    
    objRegister = [[RegisterObj alloc]init];
    objRegister.countryCode = [self fetchUserCountryCode];
    
    _tblVwRegister.separatorColor = [UIColor clearColor];
    _tblVwRegister.tableFooterView = [UIView new];
    _tblVwRegister.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _tblVwRegister.layer.borderWidth = 1.0f;
}


#pragma mark: -------------------: Tableview Delegates & DataSources
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 7;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == CELL_IMG){
        return 145;
    }
    return 75;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RegisterCell *cell;
    switch (indexPath.row) {
        case CELL_IMG:{
            NSString *cellIdentifier = @"CellImg";
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            [cell.btnProfileImg addTarget:self action:@selector(btnProfileImgAction:) forControlEvents:UIControlEventTouchUpInside];
            if(objRegister.profileImg == nil){
                 [cell.btnProfileImg setImage:[UIImage imageNamed:@"user-profile"] forState:UIControlStateNormal];
            }else{
                 [cell.btnProfileImg setImage:objRegister.profileImg forState:UIControlStateNormal];
            }
           
        }
        break;
            
        case CELL_NAME:
        case CELL_EMAIL:
//        case CELL_CONFIRM_EMAIL:
        case CELL_PASSWORD:
        case CELL_CONFIRM_PASSWORD:{
            NSString *cellIdentifier = @"CellTextField";
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            [self setInfoTitle:indexPath.row withCell:cell];
            cell.txtFieldInfo.tag = indexPath.row;
            cell.txtFieldInfo.delegate = self;
            if(indexPath.row == CELL_PASSWORD || indexPath.row == CELL_CONFIRM_PASSWORD){
                [cell.txtFieldInfo setSecureTextEntry:YES];
            }else{
                 [cell.txtFieldInfo setSecureTextEntry:NO];
            }
        }
        break;
        case CELL_PHONE:{
            NSString *cellIdentifier = @"CellPhoneNo";
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            [cell.btnPhoneNo addTarget:self action:@selector(btnPhoneNoAction:event:) forControlEvents:UIControlEventTouchUpInside];
            cell.txtFieldInfo.tag = indexPath.row;
            cell.txtFieldInfo.delegate =self;
            cell.txtFieldInfo.text = objRegister.phoneNo;
            [cell.btnPhoneNo setTitle:[NSString stringWithFormat:@"+%@",objRegister.countryCode] forState:UIControlStateNormal];
        }
        break;
        case CELL_TERMS:{
            NSString *cellIdentifier = @"CellTerms";
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            [cell.btnCheck addTarget:self action:@selector(btnCheckAction:event:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnVwTerms addTarget:self action:@selector(btnVwTermsAction:event:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.btnVwTerms setAttributedTitle:[[NSAttributedString alloc]initWithString:kPrivacyPolicy attributes:@{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),NSForegroundColorAttributeName:AppThemeBlueColor}] forState:UIControlStateNormal];
            if(objRegister.isTermsChecked){
                [cell.btnCheck setImage:[UIImage imageNamed:@"icn_check_gray"] forState:UIControlStateNormal];
            }else{
                [cell.btnCheck setImage:[UIImage imageNamed:@"icn_uncheck_gray"] forState:UIControlStateNormal];
            }
            
            if([self isEnglishLanguage]){
                cell.btnVwTerms.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            }else{
                 cell.btnVwTerms.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
           
        }
            break;
        default:
            break;
    }
    
    return cell;
}

-(void)setInfoTitle:(NSInteger)row withCell:(RegisterCell *)cell{
    switch (row) {
        case CELL_NAME:{
            cell.lblTitle.text = kName;
        }
        break;
        case CELL_EMAIL:{
             cell.lblTitle.text = kEmail;
        }
            break;
        case CELL_CONFIRM_EMAIL:{
             cell.lblTitle.text = kConfirmEmail;
        }
        break;
        case CELL_PASSWORD:{
             cell.lblTitle.text = kPassword;
        }
        break;
        case CELL_CONFIRM_PASSWORD:{
             cell.lblTitle.text = kConfirmPswd;
        }
        break;
            
            
        default:
            break;
    }
}

#pragma mark: -------------------: TextFieldDelegates
-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    switch (textField.tag) {
        case CELL_NAME:{
            objRegister.name = textField.text;
        }
        break;
        case CELL_EMAIL:{
             objRegister.email = textField.text;
        }
        break;
        case CELL_CONFIRM_EMAIL:{
             objRegister.confirmEmail = textField.text;
        }
        break;
        case CELL_PASSWORD:{
            objRegister.password = textField.text;
        }
        break;
        case CELL_CONFIRM_PASSWORD:{
             objRegister.confirmPswd = textField.text;
        }
        break;
        case CELL_PHONE:{
            objRegister.phoneNo = textField.text;
        }
            break;
            
        default:
            break;
    }
}

#pragma mark: -------------------: Button Actions
-(void)btnProfileImgAction:(id)sender{
    [self configImgPickerAlertVw];
}

-(void)btnPhoneNoAction:(id)sender event:(id)event{
    
    UITouch *touch = [event allTouches].anyObject;
    CGPoint location = [touch locationInView:_tblVwRegister];
    selectedIndexPath = [_tblVwRegister indexPathForRowAtPoint:location];
    RTCountryPicker *countryPicker = [[RTCountryPicker alloc]initWithFrame:self.view.bounds];
    countryPicker.delegate = self;
    [self.navigationController.view addSubview:countryPicker];
    
}

-(void)btnCheckAction:(id)sender event:(id)event{
    objRegister.isTermsChecked = (objRegister.isTermsChecked) ? NO : YES;
    [_tblVwRegister reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CELL_TERMS inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

-(void)btnVwTermsAction:(id)sender event:(id)event{
    PrivacyPolicyVC *ppvc = loadViewController(kSBOthers, kPrivacyPolicyVC);
    [self.navigationController pushViewController:ppvc animated:YES];
}


- (IBAction)btnRegisterAction:(id)sender {
    
    if([self validateAllFields]){
        
        [UIAlertController showAlertInVC:self withTitle:AppName withMsg:kWillSendSMS withOtherAlertActionTitle:@[kAlertOk] withAlertActionCancel:@"" withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
            [self registerUser];
        }];
       
    }
}

-(void)registerUser{
    
    if([ReachabilityManager isReachable]){
        
        NSString *strPhoneNumber = [NSString stringWithFormat:@"%@",[self validatePhoneField:objRegister.phoneNo]];
        NSMutableDictionary *dictParam = [NSMutableDictionary dictionaryWithDictionary:@{@"fullName":objRegister.name,
                                                                                         @"email":objRegister.email,
                                                                                         @"password":objRegister.password,
                                                                                         @"countryCode":objRegister.countryCode,
                                                                                         @"mobileNumber":strPhoneNumber,
                                                                                         @"userType":@"0",
                                                                                          @"language":AppContext.appLanguage
                                                                                         }];
        
        if(objRegister.profileImg!=nil){
            [dictParam setObject:objRegister.profileImg forKey:@"file"];
        }
       
        [[WebServiceInvocation alloc]initWithWS:kRegisterUserWS withParams:dictParam withServiceType:TYPE_POST withSelector:@selector(registerUserWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)registerUserWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        NSString *strPhoneNumber = [NSString stringWithFormat:@"%@",[self validatePhoneField:objRegister.phoneNo]];
        VerificationVC *vfvc = loadViewController(kSBMain, kVerificationVC);
        vfvc.mobileNo = strPhoneNumber;
        vfvc.countryCode = objRegister.countryCode;
        [self.navigationController pushViewController:vfvc animated:YES];
    }
}

#pragma mark: -------------------: Validations
-(BOOL)validateAllFields{
    
    if([objRegister.name isEqualToString:@""]){
        [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kName]];
        return NO;
    }
   else if([objRegister.email isEqualToString:@""]){
          [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kEmail]];
       return NO;
    }
   else  if(![self isValidEmail:objRegister.email]){
       [self showAlert:[NSString stringWithFormat:@"%@",kInvalidEmail]];
       return NO;
   }
  else  if([objRegister.confirmEmail isEqualToString:@""]){
        [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kConfirmEmail]];
        
      return NO;
    }
//  else  if(![self isValidEmail:objRegister.confirmEmail]){
//      [self showAlert:[NSString stringWithFormat:@"%@",kInvalidEmail]];
//      return NO;
//  }
//  else if(![objRegister.email isEqualToString:objRegister.confirmEmail]){
//      [self showAlert:[NSString stringWithFormat:@"%@",kConfirmEmailNotMatched]];
//      return NO;
//  }
  else  if([objRegister.countryCode isEqualToString:@""]){
      [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kCountry]];
      return NO;
  }
  else  if([objRegister.phoneNo isEqualToString:@""]){
      [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kPhoneNo]];
      return NO;
  }
  else  if(![self validatePhoneNumber:objRegister.countryCode withNumber:objRegister.phoneNo]){
      [self showAlert:[NSString stringWithFormat:@"%@ %@",KInvalid,kPhoneNo]];
      return NO;
  }
   else  if([objRegister.password isEqualToString:@""]){
          [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kPassword]];
        return NO;
    }
   else if([objRegister.confirmPswd isEqualToString:@""]){
          [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseEnter,kConfirmPswd]];
         return NO;
    }
   else if(![objRegister.password isEqualToString:objRegister.confirmPswd]){
       [self showAlert:[NSString stringWithFormat:@"%@",kPasswordNotMatched]];
       return NO;
   }
   else if(!objRegister.isTermsChecked){
       [self showAlert:[NSString stringWithFormat:@"%@",kAcceptTermsMsg]];
       return NO;
   }
    return YES;
    
}


#pragma mark: -------------------: CountryPicker Delegates
-(void)pickerSelectedCountry:(NSString *)selectedCode{
    objRegister.countryCode = selectedCode;
    [_tblVwRegister reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:selectedIndexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark: -------------------: ImagePicker Functions
-(void)showProfileImageActionController{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:nil
                                 message: nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* camera = [UIAlertAction
                             actionWithTitle:kCamera
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action) {
                                 
                                 [self configImagePickerWithCamera:YES];
                                 
                             }];
    
    UIAlertAction* photos = [UIAlertAction
                             actionWithTitle:kPhotoLibrary
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action) {
                                 [self configImagePickerWithCamera:NO];
                             }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:kCancel
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action) {
                                 
                             }];
    
    [camera setValue:[[UIImage imageNamed:@"icn_blue_camera"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    [photos setValue:[[UIImage imageNamed:@"icn_photo_gallery"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    
    [alert addAction:camera];
    [alert addAction:photos];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)configImgPickerAlertVw{
    
    
    @try {
        
        UIImage *img = [[UIImage imageNamed:@"logo-icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        CDAlertView *alertView = [[CDAlertView alloc]init];
        alertView.messageLabel.text = kSelPicFrom;
        alertView.backgroundColor = RGBA(0, 0, 0, 0.7f);
        alertView.circleFillColor = [UIColor colorWithPatternImage:img];
        alertView.tintColor = [UIColor whiteColor];
        alertView.isActionButtonsVertical = YES;
        alertView.alertBackgroundColor = [UIColor whiteColor];
        alertView.messageTextColor = [UIColor grayColor];
        
        
        CDAlertViewAction *action_Camera = [[CDAlertViewAction alloc]initWithTitle:kCamera font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            [self configImagePickerWithCamera:YES];
        }];
        
        CDAlertViewAction *action_Photos = [[CDAlertViewAction alloc]initWithTitle:kPhotoLibrary font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            [self configImagePickerWithCamera:NO];
        }];
        
        
        CDAlertViewAction *action_cancel = [[CDAlertViewAction alloc]initWithTitle:kCancel font:Dax_Regular(15) textColor:[UIColor redColor] backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            
        }];
        
        [alertView addWithAction:action_Camera];
        [alertView addWithAction:action_Photos];
        [alertView addWithAction:action_cancel];
        
        [alertView show:^(CDAlertView *action) {
            
        }];
        
    } @catch (NSException *exception) {
        NSLog(@"Alertview Exception : %@ , In Controller : %@",[exception description],self);
    } @finally {
        
    }
}

-(void)configImagePickerWithCamera:(BOOL)isCamera{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    
    if(isCamera){
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        
    }else{
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    [self presentViewController:picker animated:YES completion:NULL];
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    UIImage *selectedImg =(UIImage *)info[UIImagePickerControllerEditedImage];
//    NSData *imgData=UIImageJPEGRepresentation( img,0.7);
//    UIImage *compressedImage =[UIImage imageWithData:imgData];
    objRegister.profileImg = selectedImg;
     [_tblVwRegister reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CELL_IMG inSection:0]] withRowAnimation:UITableViewRowAnimationNone];

    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(NSString *)validatePhoneField:(NSString *)txt{
    
    NSString *strPhoneText = txt;
    if([strPhoneText containsString:@"+"]){
        strPhoneText = [strPhoneText stringByReplacingOccurrencesOfString:@"+" withString:@""];
    }
    
    NSRange range = NSMakeRange(0,1);
    NSString *discardZero = [strPhoneText substringWithRange:range];
    
    if([discardZero isEqualToString:@"0"]){
        strPhoneText = [strPhoneText stringByReplacingCharactersInRange:range withString:@""];
    }
    
    range = NSMakeRange(0,1);
    discardZero = [strPhoneText substringWithRange:range];
    if([discardZero isEqualToString:@"0"]){
        strPhoneText = [strPhoneText stringByReplacingCharactersInRange:range withString:@""];
    }
    
    return strPhoneText;
}

@end
