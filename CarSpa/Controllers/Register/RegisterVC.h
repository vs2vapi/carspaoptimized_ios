//
//  RegisterVC.h
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterObj : NSObject
@property(strong,nonatomic)NSString *name;
@property(strong,nonatomic)NSString *email;
@property(strong,nonatomic)NSString *confirmEmail;
@property(strong,nonatomic)NSString *phoneNo;
@property(strong,nonatomic)NSString *countryCode;
@property(strong,nonatomic)NSString *password;
@property(strong,nonatomic)NSString *confirmPswd;
@property(strong,nonatomic)UIImage *profileImg;
@property(nonatomic,assign) BOOL isTermsChecked;
@end

@interface RegisterVC : UIViewController <UITableViewDataSource,UITableViewDelegate> 

@property (weak, nonatomic) IBOutlet UITableView *tblVwRegister;
- (IBAction)btnRegisterAction:(id)sender;

@end
