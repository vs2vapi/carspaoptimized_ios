//
//  VerificationVC.m
//  CarSpa
//
//  Created by R@j on 24/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "VerificationVC.h"
#import "WSContact.h"


@interface VerificationVC (){
    NSTimer *otpTimer;
    int secoundCount;
}

@end

@implementation VerificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
      hideNavBar(NO);
    _btnVerify.layer.cornerRadius = 6.0f;
     _btnAskAgain.layer.cornerRadius = 6.0f;
    self.navigationItem.hidesBackButton = YES;
    [_btnCall setTitle:kCallUs forState:UIControlStateNormal];
    // Do any additional setup after loading the view.
    [self invokeTimer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)invokeTimer{
    secoundCount = 30;
    [_btnAskAgain setEnabled:NO];
    otpTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(startTimer:) userInfo:nil repeats:YES];
    [otpTimer fire];
}

-(void)startTimer:(NSTimer *)timer{
    secoundCount--;
    if(secoundCount == 0){
        [otpTimer invalidate];
        [_btnAskAgain setEnabled:YES];
        [_btnAskAgain setTitle:kAskAgain forState:UIControlStateNormal];
    }else{
        if(secoundCount < 10){
             [_btnAskAgain setTitle:[NSString stringWithFormat:@"00:0%d",secoundCount] forState:UIControlStateNormal];
        }else{
             [_btnAskAgain setTitle:[NSString stringWithFormat:@"00:%d",secoundCount] forState:UIControlStateNormal];
        }
       
    }
}

#pragma mark: -------------------: Button Actions

- (IBAction)btnVerifyAction:(id)sender {
    if([_txtVerificationCode.text isEqualToString:@""]){
        [self showAlert:[NSString stringWithFormat:@"%@ %@",KInvalid,kCode]];
    }else{
        [self verifyOTP];
    }
}

- (IBAction)btnAskAgainAction:(id)sender {
    [self askAgainForOTP];
}

- (IBAction)btnCallAction:(id)sender {
    [self fetchSupportContact];
}


#pragma mark: -------------------: WebService Functions

-(void)verifyOTP{
    
    
    if([ReachabilityManager isReachable]){
        
        NSString *token = [Functions getStringValueFromDefaults:DEVICE_TOKEN];
        if(token == nil){
            token = @"FailToRetrievetoken";
        }

        double lat = [Functions getDoubleValueFromDefaults:USER_LATITUDE];
        double longitude = [Functions getDoubleValueFromDefaults:USER_LONGITUDE];
        
        NSDictionary *dictParams= @{@"mobileNumber":_mobileNo,
                                    @"OTPCode":_txtVerificationCode.text,
                                    @"deviceType":kDeviceType,
                                    @"deviceToken":token,
                                    @"latitude":[NSNumber numberWithDouble:lat],
                                    @"longitude":[NSNumber numberWithDouble:longitude],
                                    @"language":AppContext.appLanguage};
        
        [[WebServiceInvocation alloc]initWithWS:kVerifyOTPWS withParams:dictParams withServiceType:TYPE_POST withSelector:@selector(otpWSAction:) withTarget:self showLoader:YES loaderMsg:kVerifyOTPMsg];
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)otpWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        [Functions setBoolValueToUserDefaults:YES withKey:IS_USER_LOGGED_IN];
        [[NSNotificationCenter defaultCenter]postNotificationName:LOGIN_SUCCESS object:nil];
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

-(void)askAgainForOTP{
    
    if([ReachabilityManager isReachable]){
        
        NSDictionary *dictParams= @{@"mobileNumber":_mobileNo,@"language":AppContext.appLanguage};
        [[WebServiceInvocation alloc]initWithWS:kAskAgainOTPWS withParams:dictParams withServiceType:TYPE_POST withSelector:@selector(askAgainWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)askAgainWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
       [self invokeTimer];
       [self showAlert:[[sender responseDictionary]valueForKey:kServiceResponseMsgKey]];
    }
}

-(void)fetchSupportContact{
    if([ReachabilityManager isReachable]){
        NSString *url = [NSString stringWithFormat:@"%@?countryCode=%@",kFetchSupportContactWS,_countryCode];
        [[WebServiceInvocation alloc]initWithWS:url withParams:nil withServiceType:TYPE_GET withSelector:@selector(supportContactWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)supportContactWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        WSContact *objContact = [[sender responseArray]objectAtIndex:0];
    //    NSString *countryCode = [NSString stringWithFormat:@"%.0f",objContact.countryCode];
        NSString *mobileNumber = [NSString stringWithFormat:@"%@",objContact.mobileNumber];
        NSString *phoneNumber = [@"telprompt://" stringByAppendingString:mobileNumber];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }
}


@end
