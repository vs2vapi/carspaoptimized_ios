//
//  VerificationVC.h
//  CarSpa
//
//  Created by R@j on 24/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VerificationVC.h"

@interface VerificationVC : UIViewController


@property (weak, nonatomic) IBOutlet UITextField *txtVerificationCode;
@property (weak, nonatomic) IBOutlet UIButton *btnVerify;
@property (weak, nonatomic) IBOutlet UIButton *btnAskAgain;
@property (weak, nonatomic) IBOutlet UIButton *btnCall;
@property (weak, nonatomic) IBOutlet UILabel *lblCounter;

@property (strong,nonatomic) NSString *mobileNo;
@property (strong,nonatomic) NSString *countryCode;


- (IBAction)btnVerifyAction:(id)sender;
- (IBAction)btnAskAgainAction:(id)sender;
- (IBAction)btnCallAction:(id)sender;

@end
