//
//  PaymentPopUpVC.m
//  CarCleaner
//
//  Created by Rajan on 19/04/17.
//  Copyright © 2017 Rajan. All rights reserved.
//

#import "PaymentPopUpVC.h"
#import "WSAppointmentList.h"



@interface PaymentPopUpVC (){
    int dateCounter;
}
@end

@implementation PaymentPopUpVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _containerVw.layer.cornerRadius = 4.0f;
   
    [_containerVw setHidden:YES];
    

    if(_isServeNow){
        if(AppContext.requestHelper.arrAppointments.count > 0){
            NSLog(@"appointment is added");
            NSLog(@"%@",AppContext.requestHelper.arrAppointments);
            dateCounter = 0;
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]]; // new changes
            [formatter setDateFormat:YYYY_MM_dd];
            
            NSDate *todayDate = [NSDate date];
            [self fetchOtherTimeSlotForDateRequest:[formatter stringFromDate:todayDate]];
        }else{
            NSLog(@"no appointments");
            dateCounter = 0;
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]]; // new changes
            [formatter setDateFormat:YYYY_MM_dd];
             NSDate *todayDate = [NSDate date];
            if(_isServiceByDateSelected){
                todayDate = nil;
                todayDate = [formatter dateFromString:_strServiceByDate];
            }
            [self fetchTimeSlotForDateRequest:[formatter stringFromDate:todayDate]];
        }
      
    }else{
       [self displayTimeSlot:_requestDate];
    }
   
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark :----------------: Fetch TimeSlots

-(void)fetchTimeSlotForDateRequest:(NSString *)date{
    
    dateCounter+=1;
    
    if([ReachabilityManager isReachable]){
        
        NSDateFormatter *formatter = [NSDateFormatter new];
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
        [formatter setDateFormat:YYYY_MM_dd];
        
        NSDate *selectedDate = [formatter dateFromString:date];
        NSString *timeString = @"00:00:00";
        
        if([self compareDateOnly:selectedDate] == NSOrderedSame){
            [formatter setDateFormat:@"HH:mm:ss"];
            timeString = [formatter stringFromDate:[NSDate date]];
        }
        
        NSString *serviceId = [AppContext.requestHelper.arrServiceId componentsJoinedByString:@","];
        NSString *userId =  [Functions getStringValueFromDefaults:USER_ID];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@?requestDate=%@&userId=%@&duration=%lu&addressId=%lu&serviceIds=%@&currentTime=%@&language=%@&%@=%@&ownerId=%lu",kFetchAppointmentListWS,date,userId,AppContext.requestHelper.serviceDuration,AppContext.requestHelper.addressId,serviceId,timeString,AppContext.appLanguage,kAppVersionServiceKey,[self fetchAppVersion],AppContext.requestHelper.ownerId];
        
        
        [[WebServiceInvocation alloc]initWithWS:strUrl withParams:nil withServiceType:TYPE_GET withSelector:@selector(appointmentWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }

}

-(void)appointmentWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){

        if(dateCounter==kMaxDay){
            dateCounter = 0;
            _arrTimeSlots = [NSMutableArray new];
            [self btnCloseAction:nil];
            [self.delegate noTimeSlotFound]; //Invoke delegate if error
        }else{
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
             [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
            [formatter setDateFormat:yyyy_MM_dd];
            NSDate *todayDate = [NSDate date];
            
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDateComponents *comps = [[NSDateComponents alloc] init];
            [comps setDay:dateCounter];
            NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:todayDate options:0];
            NSString *strMaxDate = [formatter stringFromDate:maxDate];
            [self fetchTimeSlotForDateRequest:strMaxDate];
            
        }

     
    }else{
        
        _arrTimeSlots = [NSMutableArray arrayWithArray:[sender responseArray]];
        if(_arrTimeSlots.count > 0){
            
            WSAppointmentList *objList = [_arrTimeSlots objectAtIndex:0];
            NSString *dtTime = [NSString stringWithFormat:@"%@",objList.dateTime];
            AppContext.requestHelper.strDateTime = dtTime;
            AppContext.requestHelper.vanId = objList.vanId;
            AppContext.requestHelper.travelingTime = objList.travelingTime;
            if(dtTime==nil || [dtTime isEqualToString:@""]){
                [self showAlert:kTechnicalIssueMsg];
            }else{
                [self displayTimeSlot:dtTime];
            }
           
        }
    }
}

-(void)fetchOtherTimeSlotForDateRequest:(NSString *)date{
    
    
    dateCounter+=1;
    if([ReachabilityManager isReachable]){
        @try {
            
            NSDictionary *dictAppointments = [AppContext.requestHelper.arrAppointments lastObject];
            NSString *vanId = [NSString stringWithFormat:@"%@",[dictAppointments valueForKey:@"vanId"]];
            NSString *lastDateTime = [NSString stringWithFormat:@"%@",[dictAppointments valueForKey:@"appointmentDatetime"]];
            NSString *lastduration = [NSString stringWithFormat:@"%@",[dictAppointments valueForKey:@"duration"]];
            
            NSLog(@"%@ : %@ : %@",vanId,lastDateTime,lastduration);
            
            NSDateFormatter *formatter = [NSDateFormatter new];
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
            [formatter setDateFormat:YYYY_MM_dd];
            
            NSDate *selectedDate = [formatter dateFromString:date];
            NSString *timeString = @"00:00:00";
            
            if([self compareDateOnly:selectedDate] == NSOrderedSame){
                [formatter setDateFormat:@"HH:mm:ss"];
                timeString = [formatter stringFromDate:[NSDate date]];
            }
            
            NSString *serviceId = [AppContext.requestHelper.arrServiceId componentsJoinedByString:@","];
            NSString *userId =  [Functions getStringValueFromDefaults:USER_ID];
            
            NSString *strUrl = [NSString stringWithFormat:@"%@?requestDate=%@&userId=%@&duration=%lu&addressId=%lu&serviceIds=%@&currentTime=%@&language=%@&%@=%@&vanId=%@&lastdatetime=%@&lastduration=%@&ownerId=%lu",kFetchOtherAppointmentListWS,date,userId,AppContext.requestHelper.serviceDuration,AppContext.requestHelper.addressId,serviceId,timeString,AppContext.appLanguage,kAppVersionServiceKey,[self fetchAppVersion],vanId,lastDateTime,lastduration,AppContext.requestHelper.ownerId];
            
            
            [[WebServiceInvocation alloc]initWithWS:strUrl withParams:nil withServiceType:TYPE_GET withSelector:@selector(otherAppointmentWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
            
        } @catch (NSException *exception) {
            NSLog(@"Exception : %@",[exception description]);
        } @finally {
            
        }
      
    }else{
        ShowNoNetworkAlert(self);
    }

    
}

-(void)otherAppointmentWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        if(dateCounter==kMaxDay){
            dateCounter = 0;
            _arrTimeSlots = [NSMutableArray new];
            [self btnCloseAction:nil];
            [self.delegate noTimeSlotFound]; //Invoke delegate if error
        }else{
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
             [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
            [formatter setDateFormat:YYYY_MM_dd];
            NSDate *todayDate = [NSDate date];
            
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDateComponents *comps = [[NSDateComponents alloc] init];
            [comps setDay:dateCounter];
            NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:todayDate options:0];
            NSLog(@"Other date for appointment : %@",maxDate);
            [self fetchOtherTimeSlotForDateRequest:[formatter stringFromDate:maxDate]];
            
        }

    }else{
        _arrTimeSlots = [NSMutableArray arrayWithArray:[sender responseArray]];
        if(_arrTimeSlots.count > 0){
            
            WSAppointmentList *objList = [_arrTimeSlots objectAtIndex:0];
            NSString *dtTime = [NSString stringWithFormat:@"%@",objList.dateTime];
            AppContext.requestHelper.strDateTime = dtTime;
            AppContext.requestHelper.vanId = objList.vanId;
            AppContext.requestHelper.travelingTime = objList.travelingTime;
            if(dtTime==nil || [dtTime isEqualToString:@""]){
                [self showAlert:kTechnicalIssueMsg];
            }else{
                [self displayTimeSlot:dtTime];
            }
            
        }

    }
}

-(void)displayTimeSlot:(NSString *)date{
   
    @try {
        [_containerVw setHidden:NO];
        NSString *strTimeSlot = [NSString stringWithFormat:@"%@",date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:YYYY_MM_dd_HH];
        
        NSDate *newDate  =  [formatter dateFromString:strTimeSlot];
        if([AppContext.appLanguage isEqualToString:@"ar"]){
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ar"]];
        }
        
        //[formatter setDateFormat:YYYY_MM_dd_HH];
        [formatter setDateFormat:EE_dd_MMM_HH];
        
        NSString *formattedDate = [formatter stringFromDate:newDate];
        
        NSString *strHeader = [NSString stringWithFormat:@"%@ %@ \n %@",kWeCanServe,formattedDate,kConfirmAppointment];
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:strHeader];
        
        NSRange range = [strHeader rangeOfString:formattedDate];
        [attString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
        
        _lblTime.attributedText = attString;
    } @catch (NSException *exception) {
        [self showAlert:[exception description]];
        [self btnCloseAction:nil];
    } @finally {
        
    }
    
 
    
}

#pragma mark :----------------: Button Actions
- (IBAction)btnCloseAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelButtonClicked:)]) {
        [self.delegate cancelButtonClicked:self];
    }
}


- (IBAction)btnYesAction:(id)sender {
    
    [_btnClose setEnabled:NO];
    _lblTitle.text = kSelectAction;
    [_vwConfirmation setHidden:YES];
    
    if(_fromEditRequest){
        [_vwPayment setHidden:NO];
        [_vwCheckout setHidden:YES];
    }else{
        
        [_vwPayment setHidden:YES];
        [_vwCheckout setHidden:NO];
    

        NSString *strExRate = [NSString stringWithFormat:@"%.2f",AppContext.exchangeRate];
          double serviceAmount =  0;
        if(AppContext.requestHelper.isTowService){
             serviceAmount =  AppContext.requestHelper.totalAmount ;
        }else{
             serviceAmount =  AppContext.requestHelper.totalLocalAmount ;
        }
        
        double totalAmount = round(serviceAmount * [strExRate floatValue] ); //Total Amount After Conversion which we need to display
     
        //Summing the total of all services which we will add in appointment dictionary.
  //       AppContext.requestHelper.totalAmount =  AppContext.requestHelper.previousAmount +  totalAmount;
        
        AppContext.requestHelper.totalAmount =   totalAmount;
        
        double displayAmount = AppContext.requestHelper.previousAmount +  totalAmount;
        
        NSLog(@"Total of Local Amount: %f",AppContext.requestHelper.totalLocalAmount);
        NSLog(@"Total amount after conversion: %f",AppContext.requestHelper.totalAmount);
        NSLog(@"Total Duration: %lu",AppContext.requestHelper.serviceDuration);
        NSLog(@"Tax Percentage: %.1f",AppContext.requestHelper.taxPercentage);
    
        if(AppContext.requestHelper.isAppliedForTax){
            double taxAmount = (displayAmount * AppContext.requestHelper.taxPercentage)/100;
            displayAmount = displayAmount + taxAmount;
        }
        
        displayAmount = round(displayAmount);
       
        NSString *strCheckOutValue = [NSString stringWithFormat:@"%@ %.1f %@",kDueAmount, displayAmount,AppContext.currency];
        _lblCheckOut.text = strCheckOutValue;
        
        if(AppContext.requestHelper.isAppliedForTax){
             [_lblVat setHidden:NO];
            _lblVat.text = [NSString stringWithFormat:@"(%@ %.1f%%)",kInclude,AppContext.requestHelper.taxPercentage];
        }else{
            [_lblVat setHidden:YES];
        }
         [self createAppoinemntDict];
    }

}

- (IBAction)btnAppointmentAction:(id)sender {
  
    _addAppointment = YES;
    [self btnCloseAction:nil];
}

- (IBAction)btnCancelRequestAction:(id)sender {

    _cancelRequest = YES;
    [self btnCloseAction:nil];
}

- (IBAction)btnAddServiceAction:(id)sender {
     AppContext.requestHelper.previousAmount = AppContext.requestHelper.previousAmount + AppContext.requestHelper.totalAmount;
    _addService = YES;
    [self btnCloseAction:nil];
    
}

- (IBAction)btnCardAction:(id)sender {

}

- (IBAction)btnCashAction:(id)sender {

    [self btnCloseAction:nil];
    if(_fromEditRequest){
    //    [self.delegate updateRequestForBookingTime];
    }else{
        [self checkOut];
    }
   
}

- (IBAction)btnCheckOutAction:(id)sender {
    _lblTitle.text =kPaymentMethod;
    [_vwConfirmation setHidden:YES];
    [_vwCheckout setHidden:YES];
    [_vwPayment setHidden:NO];
}


#pragma mark :----------------: Payment Functions

-(void)createAppoinemntDict{
    
    NSInteger currencyId = [Functions getIntegerValueFromDefaults:CURRENCY_ID];
    if(currencyId == 0){
        currencyId = 1;
    }
    
    if(!AppContext.requestHelper.isTowService){
        AppContext.requestHelper.towAddress   = @"";
        AppContext.requestHelper.towAddressLatitude  =0;
        AppContext.requestHelper.towAddressLongitude = 0;
    }else{
        AppContext.requestHelper.serviceDuration = 0;
    }
    
    NSString *serviceId = [AppContext.requestHelper.arrServiceId componentsJoinedByString:@","];
    NSMutableDictionary * dictAppointments = [[NSMutableDictionary alloc]init];
    [dictAppointments setValue:[NSNumber numberWithInteger:AppContext.requestHelper.categoryId] forKey:@"categoryId"];
    [dictAppointments setValue:serviceId forKey:@"serviceIds"];
    [dictAppointments setValue:[NSNumber numberWithInteger:AppContext.requestHelper.serviceDuration] forKey:@"duration"];
    [dictAppointments setValue:[NSNumber numberWithInteger:currencyId] forKey:@"currencyId"];
    [dictAppointments setValue:[NSNumber numberWithDouble:AppContext.requestHelper.totalAmount] forKey:@"amount"];
    [dictAppointments setValue:[NSNumber numberWithInteger:AppContext.requestHelper.vanId] forKey:@"vanId"];
    [dictAppointments setValue:[NSNumber numberWithInteger:AppContext.requestHelper.travelingTime] forKey:@"travelingTime"];
    [dictAppointments setValue:AppContext.requestHelper.strDateTime forKey:@"appointmentDatetime"];
    [dictAppointments setValue:[NSNumber numberWithInt:kPaymentStatus] forKey:@"paymentStatus"];
    [dictAppointments setValue:AppContext.requestHelper.towAddress forKey:@"destinationAddress"];
    [dictAppointments setValue:[NSNumber numberWithDouble:AppContext.requestHelper.towAddressLongitude] forKey:@"destinationLongitude"];
    [dictAppointments setValue:[NSNumber numberWithDouble:AppContext.requestHelper.towAddressLatitude] forKey:@"destinationLatitude"];
    [dictAppointments setObject:[NSNumber numberWithDouble:AppContext.requestHelper.totalLocalAmount] forKey:@"localAmount"];
    
    [AppContext.requestHelper.arrAppointments addObject:dictAppointments];
    
}


-(void)checkOut{
    
  
    if([ReachabilityManager isReachable]){
        
        __block BOOL isAmountZero = NO;
        [AppContext.requestHelper.arrAppointments enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            double amount = [[obj valueForKey:@"amount"]doubleValue];
            double localAmount = [[obj valueForKey:@"localAmount"]doubleValue];
            if(amount == 0 || localAmount == 0){
                isAmountZero = YES;
                *stop = YES;
            }
        }];
        
        if(isAmountZero){
            [self showAlert:kTechnicalIssueMsg];
            return;
        }
        
        NSString *appVersion = [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
        NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
        [dictParams setValue:[Functions getStringValueFromDefaults:USER_ID] forKey:@"userId"];
        [dictParams setValue:[NSNumber numberWithInteger:AppContext.requestHelper.carTypeId] forKey:@"carTypeId"];
        [dictParams setValue:[NSNumber numberWithInteger:AppContext.requestHelper.addressId] forKey:@"addressId"];
        [dictParams setValue:[NSNumber numberWithInteger:AppContext.requestHelper.carId] forKey:@"carId"];
        [dictParams setObject:AppContext.requestHelper.arrAppointments forKey:@"Appointments"];
        [dictParams setValue:appVersion forKey:@"appVersion"];
        [dictParams setValue:AppContext.appLanguage forKey:@"language"];
        
//      NSLog(@"%@",dictParams);
//      return;
        
         [[WebServiceInvocation alloc]initWithWS:kBookAppointmentWS withParams:dictParams withServiceType:TYPE_JSON withSelector:@selector(checkOutWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }
 
}

-(void)checkOutWSAction:(id)sender{
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        [AppContext.requestHelper deallocInstance];
        [[NSNotificationCenter defaultCenter]postNotificationName:NEW_ORDER_BOOKED object:nil];
        [self.delegate checkedOutSuccessfully:[[sender responseDictionary]mutableCopy]];
    }
    
}

-(void)updateRequest{
    
/*    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD showWithStatus:NSLocalizedString(kLK_PLEASE_WAIT, @"")];
    NSString * url = [NSString stringWithFormat:@"%@%@",SERVER_PATH,USER_UPDATE_REQUEST];
    NSDictionary *params = @{@"appointmentId":[NSString stringWithFormat:@"%@",man.strEditAppointmentId],@"vanId":_strVanId,@"travelingTime":_strTravelineTime,@"appoinmentDateTime":_strDateTime,@"addressId":man.strEditAddressId,@"duration":man.strDuration,@"destinationAddress":man.strDestinationAddress,@"destinationLatitude":man.strDestinationLatitude,@"destinationLongitude":man.strDestinationLongitude,@"amount":man.strAmountGlobal};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject){
        
        [SVProgressHUD dismiss];
        NSLog(@"JSON: %@",responseObject);
        BOOL status = [[responseObject objectForKey:@"Status"]boolValue];
        if(status){
            man.isEditRequest = false;
//            [self openPaymentmethodActionSheet];
        }else{
            [Utility showAlertMessage_Title:NSLocalizedString(kLK_REQUEST, @"") Message:[responseObject objectForKey:@"Message"] PresentViewController:self];
        }
    } failure:^(NSURLSessionTask *peration, NSError *error){
        NSLog(@"Error: %@", error);
        [SVProgressHUD dismiss];
        [Utility showAlertMessage_Title:NSLocalizedString(kLK_REQUEST, @"") Message:NSLocalizedString(kLK_SOMETHING_WENT_WRONG, @"") PresentViewController:self];
        
    }];
 */
}

@end
