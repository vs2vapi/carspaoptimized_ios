//
//  PaymentPopUpVC.h
//  CarCleaner
//
//  Created by Rajan on 19/04/17.
//  Copyright © 2017 Rajan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PaymentPopUpDelegate;

@interface PaymentPopUpVC : UIViewController
@property (weak, nonatomic) IBOutlet UIView *containerVw;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *vwConfirmation;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UIView *vwCheckout;
@property (weak, nonatomic) IBOutlet UIView *vwPayment;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckout;
@property (weak, nonatomic) IBOutlet UIView *headerVw;
@property (weak, nonatomic) IBOutlet UILabel *lblCheckOut;
@property (weak, nonatomic) IBOutlet UILabel *lblVat;

- (IBAction)btnCloseAction:(id)sender;
- (IBAction)btnYesAction:(id)sender;
- (IBAction)btnAppointmentAction:(id)sender;
- (IBAction)btnCancelRequestAction:(id)sender;

- (IBAction)btnAddServiceAction:(id)sender;
- (IBAction)btnCardAction:(id)sender;
- (IBAction)btnCashAction:(id)sender;
- (IBAction)btnCheckOutAction:(id)sender;

@property (assign, nonatomic) id <PaymentPopUpDelegate>delegate;
@property (strong, nonatomic) NSMutableArray *arrTimeSlots;
@property (strong, nonatomic) NSString *requestDate;
@property(nonatomic,assign)BOOL fromEditRequest;
@property(nonatomic,assign) BOOL isServeNow;
@property(nonatomic,assign) BOOL cancelRequest;
@property(nonatomic,assign) BOOL addAppointment;
@property(nonatomic,assign) BOOL addService;
@property(nonatomic,assign) BOOL isServiceByDateSelected;
@property (strong, nonatomic) NSString *strServiceByDate;

@end

@protocol PaymentPopUpDelegate<NSObject>
@optional
- (void)cancelButtonClicked:(PaymentPopUpVC *)controller;
- (void)checkedOutSuccessfully:(NSMutableDictionary *)dictResponse;
- (void)noTimeSlotFound;
-(void)updateRequestForBookingTime;

@end
