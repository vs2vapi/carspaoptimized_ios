//
//  OurPartnersVC.m
//  CarSpa
//
//  Created by R@j on 25/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "OurPartnersVC.h"
#import "ServiceCollVwCell.h"
#import "WSOurPartners.h"

@interface OurPartnersVC (){
    NSMutableArray *arrPartners;
}

@end

@implementation OurPartnersVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchPartnersDetails];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}

-(void)fetchPartnersDetails{
    
    if([ReachabilityManager isReachable]){
        NSString *url = [NSString stringWithFormat:@"%@?language=%@&%@=%@",kOurPartnersWS,AppContext.appLanguage,kAppVersionServiceKey,[self fetchAppVersion]];
        [[WebServiceInvocation alloc]initWithWS:url withParams:nil withServiceType:TYPE_GET withSelector:@selector(partnersWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }
    
}

-(void)partnersWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        arrPartners = [NSMutableArray arrayWithArray:[sender responseArray]];
        [_collVwPartners reloadData];
    }
}

#pragma mark :----------: CollectionView Delegates & Data Sources
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arrPartners.count;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(collectionView.frame.size.width / 3, 130);
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"Cell";
    ServiceCollVwCell *cell =[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    WSOurPartners *objPartners = [arrPartners objectAtIndex:indexPath.row];
    
    cell.lblServiceTitle.text = objPartners.name;

    [cell.imgVwService sd_setImageWithURL:[NSURL URLWithString:objPartners.iconThump]
                          placeholderImage:nil];
    

    return cell;
}


@end
