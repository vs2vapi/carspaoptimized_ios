//
//  AboutUsVC.m
//  CarSpa
//
//  Created by R@j on 25/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "AboutUsVC.h"

@interface AboutUsVC ()

@end

@implementation AboutUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _lblVersion.text = [NSString stringWithFormat:@"%@: %@",kVersion,[self fetchAppVersion]];
    [self fetchAboutUsDetails];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}

-(void)fetchAboutUsDetails{
    
    if([ReachabilityManager isReachable]){
        NSString *url = [NSString stringWithFormat:@"%@?language=%@&%@=%@",kAboutUsWS,AppContext.appLanguage,kAppVersionServiceKey,[self fetchAppVersion]];
        [[WebServiceInvocation alloc]initWithWS:url withParams:nil withServiceType:TYPE_GET withSelector:@selector(aboutUsWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }

}

-(void)aboutUsWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        
        NSString *strFont = @"<font face='Dax-Regular' size='3'>";
        NSString *strTitle = [[[sender responseDictionary] objectForKey:@"Data"]valueForKey:([AppContext.appLanguage isEqualToString:@"ar"])? @"aboutusArTitle" : @"aboutusTitle"];
        NSString *align = @"left";
        if([AppContext.appLanguage isEqualToString:@"ar"]){
            align = @"right";
        }
        NSString *title = [NSString stringWithFormat:@"<font face='Dax-Bold' size='3' ><p align=\"%@\">%@</p>",align,strTitle];
        
        NSString *htmlString = [NSString stringWithFormat:@"%@ %@ %@",title,strFont,[[[sender responseDictionary] objectForKey:@"Data"]valueForKey:([AppContext.appLanguage isEqualToString:@"ar"])? @"aboutusAR" : @"aboutus"]];
        [_webVwAboutUs loadHTMLString:htmlString baseURL:nil];
    }
}

@end
