//
//  AboutUsVC.h
//  CarSpa
//
//  Created by R@j on 25/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutUsVC : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webVwAboutUs;
@property (weak, nonatomic) IBOutlet UILabel *lblVersion;

@end
