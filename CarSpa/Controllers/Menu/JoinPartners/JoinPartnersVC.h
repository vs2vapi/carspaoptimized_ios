//
//  JoinPartnersVC.h
//  CarSpa
//
//  Created by R@j on 25/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PartnersFiled : NSObject
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *car;
@property (strong, nonatomic) NSString *mobile;
@property (strong, nonatomic) NSString *age;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *nationality;
@property (strong, nonatomic) NSString *passport;
@property (strong, nonatomic) NSString *coveringCity;
@property (strong, nonatomic) NSString *coveringCountry;
@property (strong, nonatomic) NSString *suggesstion;
@property (strong, nonatomic) NSString *countryCode;
@property ( nonatomic,assign) BOOL img1Selected,img2Selected,img3Selected;
@property (strong, nonatomic) UIImage *img1,*img2,*img3;
@property (strong, nonatomic) NSString *strDoc1, *strDoc2, *strDoc3;

@end

@interface JoinPartnersVC : UIViewController <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate,UINavigationControllerDelegate,RTCountryPickerDelegate>


@property (weak, nonatomic) IBOutlet UIView *containerVw;
@property (weak, nonatomic) IBOutlet UITableView *tblVwJoinPartners;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property(strong,nonatomic)NSMutableArray *arrCategories;
- (IBAction)btnSubmitAction:(id)sender;

@end
