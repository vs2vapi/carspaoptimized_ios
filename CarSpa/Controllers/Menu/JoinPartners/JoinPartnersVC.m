//
//  JoinPartnersVC.m
//  CarSpa
//
//  Created by R@j on 25/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "JoinPartnersVC.h"
#import "JoinPartnerCell.h"
#import "WSCategory.h"
#import "CarSpa-Swift.h"

#define kMaxTextLength 1000
#define kBtn1Tag 11
#define kBtn2Tag 22
#define kBtn3Tag 33

typedef enum : NSUInteger {
    
    CELL_NAME = 0,
    CELL_EMAIL = 1,
    CELL_AGE = 2,
    CELL_NATIONALITY = 3,
    CELL_PASSPORT = 4,

    CELL_MOBILE = 5,
    CELL_CAR =6,

    CELL_COVERING_CITY = 7,
    CELL_SERVICES = 8,
    CELL_OTHER_CATEGORY = 9,
    CELL_BROWSE_1 = 10,

    
} PartnersCellType;


@implementation PartnersFiled

-(instancetype)init{
    if(self = [super init]){
        _name = @"";
        _car = @"";
        _mobile = @"";
        _age = @"";
        _email = @"";
        _nationality = @"";
        _passport = @"";
        _coveringCity = @"";
        _suggesstion = @"";
        _countryCode = @"";
        _coveringCountry= @"";
        _suggesstion = @"";
        
    }
    return self;
}

@end

@interface JoinPartnersVC (){
     PartnersFiled *objPartnersField;
    NSMutableArray *arrSelectedServices;
    NSInteger textLength,selectedBtn;
}

@end

@implementation JoinPartnersVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchCategories:YES];
    [self setupUI];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}

-(void)setupUI{
    _tblVwJoinPartners.separatorColor = [UIColor clearColor];
    _tblVwJoinPartners.tableFooterView = [UIView new];
    _btnSubmit.layer.cornerRadius = 6.0f;
    objPartnersField = [[PartnersFiled alloc]init];
    objPartnersField.countryCode = [self fetchUserCountryCode];

}

-(void)fetchCategories:(BOOL)showLoader{
    if([ReachabilityManager isReachable]){
        
        //kAppVersionServiceKey,[self fetchAppVersion]
        NSString *url = [NSString stringWithFormat:@"%@?%@=%@",kFetchCategoriesWS,kAppVersionServiceKey,[self fetchAppVersion]];
        [[WebServiceInvocation alloc]initWithWS:url withParams:nil withServiceType:TYPE_GET withSelector:@selector(categoriesWSAction:) withTarget:self showLoader:showLoader loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }
    
}

-(void)categoriesWSAction:(id)sender{
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        _arrCategories = [NSMutableArray arrayWithArray:[sender responseArray]];
        arrSelectedServices = [NSMutableArray new];
        [_tblVwJoinPartners reloadData];
    }
    
}


#pragma mark :-------------------: TableView Delegates & Datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 11;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == CELL_OTHER_CATEGORY){
        return 175;
    }
    else if(indexPath.row ==  CELL_SERVICES){
        //          return  [[self.tblVwJoinPartners dataSource]tableView:tableView cellForRowAtIndexPath:indexPath].contentView.frame.size.height;
        return 580;
    }
    else if(indexPath.row ==  CELL_BROWSE_1){
        return 150;
    }
    return 72;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    JoinPartnerCell *cell;
    switch (indexPath.row) {
            
        case CELL_NAME:
        case CELL_EMAIL:
        case CELL_AGE:
        case CELL_NATIONALITY:
        case CELL_PASSPORT:
        case CELL_CAR:
        case  CELL_COVERING_CITY:{
            static NSString *cellIdentifier = @"CellTextField";
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            [cell.collectionView removeFromSuperview];
            [cell.txtFieldPartners setDelegate:self];
            cell.txtFieldPartners.tag = indexPath.row;
            [self setTextFieldText:cell withRow:indexPath.row];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setTextFieldPaddingImg:cell withRow:indexPath.row];
            });
            

            if(indexPath.row == CELL_AGE){
                cell.txtFieldPartners.keyboardType  = UIKeyboardTypeNumberPad;
            }
            else{
                cell.txtFieldPartners.keyboardType  = UIKeyboardTypeDefault;
            }
        }
            break;
        case CELL_MOBILE:{
            static NSString *cellIdentifier = @"CellPhone";
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            [cell.collectionView removeFromSuperview];
            [cell.txtFieldPartners setDelegate:self];
            cell.txtFieldPartners.tag = indexPath.row;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setTextFieldPaddingImg:cell withRow:indexPath.row];
            });
            cell.txtFieldPartners.keyboardType  = UIKeyboardTypePhonePad;
            [self setButtonTitle:cell withRow:indexPath.row];
            [cell.btnPartners addTarget:self action:@selector(btnCountryClicked:event:) forControlEvents:UIControlEventTouchUpInside];
            
        }
            break;
            

        case CELL_BROWSE_1:{
            static NSString *cellIdentifier = @"CellBtn";
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            [cell.collectionView removeFromSuperview];
            cell.btnBrowse1.tag = kBtn1Tag;
            [cell.btnBrowse1 addTarget:self action:@selector(btnBrowseClicked:event:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnBrowse2.tag = kBtn2Tag;
            [cell.btnBrowse2 addTarget:self action:@selector(btnBrowseClicked:event:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnBrowse3.tag = kBtn3Tag;
            [cell.btnBrowse3 addTarget:self action:@selector(btnBrowseClicked:event:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.btnPath1 setImage:(objPartnersField.img1Selected) ? objPartnersField.img1 : [UIImage imageNamed:@"icn_document_placeholder"] forState:UIControlStateNormal];
            [cell.btnPath2 setImage:(objPartnersField.img2Selected) ? objPartnersField.img2 : [UIImage imageNamed:@"icn_document_placeholder"] forState:UIControlStateNormal];
            [cell.btnPath3 setImage:(objPartnersField.img3Selected) ? objPartnersField.img3 : [UIImage imageNamed:@"icn_document_placeholder"] forState:UIControlStateNormal];
        }
            break;
        case  CELL_SERVICES:{
            static NSString *cellIdentifier = @"CellServices";
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            
            [cell setCollectionViewDataSourceDelegate:self indexPath:indexPath];
            [cell.collectionView setScrollEnabled:NO];
        }
            break;
        case  CELL_OTHER_CATEGORY:{
            static NSString *cellIdentifier = @"CellTextVw";
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            [cell.collectionView removeFromSuperview];
            cell.txtVwCategory.delegate = self;
            cell.txtVwCategory.text = objPartnersField.suggesstion;
            if([AppContext.appLanguage isEqualToString:@"ar"]){
                cell.lblCounter.textAlignment = NSTextAlignmentLeft;
            }
        }
            break;
            
        default:
            break;
    }
    return  cell;
}


#pragma mark :-------------------: Collectionview Delegates & Datasource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _arrCategories.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    //  return CGSizeMake(collectionView.frame.size.height / 2, 80);
    return CGSizeMake(collectionView.frame.size.width, 40);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    //  return UIEdgeInsetsMake(10, 10, 9, 10);
    return UIEdgeInsetsMake(10, 0, 9, 0);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ServiceCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CollectionViewCellIdentifier forIndexPath:indexPath];
    WSCategory *objCategory = [_arrCategories objectAtIndex:indexPath.row];
    
    NSString *name;
    if([AppContext.appLanguage isEqualToString:@"ar"]){
        name =objCategory.nameAR;
    }else{
         name =objCategory.name;
    }
    
    cell.lblTitle.text = name;
    
    if ([arrSelectedServices containsObject:[NSNumber numberWithInteger:objCategory.categoryIdentifier]]){
        [cell.btnCheck setImage:[UIImage imageNamed:@"icn_check_gray"] forState:UIControlStateNormal];
    }else{
        [cell.btnCheck setImage:[UIImage imageNamed:@"icn_uncheck_gray"] forState:UIControlStateNormal];
    }
    
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    WSCategory *objCategory = [_arrCategories objectAtIndex:indexPath.row];
    if ([arrSelectedServices containsObject:[NSNumber numberWithInteger:objCategory.categoryIdentifier]]){
        [arrSelectedServices removeObject:[NSNumber numberWithInteger:objCategory.categoryIdentifier]];
    }else{
        [arrSelectedServices addObject:[NSNumber numberWithInteger:objCategory.categoryIdentifier]];
    }
    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
}

-(void)setTextFieldPaddingImg:(JoinPartnerCell *)cell withRow:(NSInteger)row{
    
    UIView *containerVw = [[UIView alloc]initWithFrame:CGRectMake(0, 0,40, cell.txtFieldPartners.frame.size.height)];
    [containerVw setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *paddingImgVw = [[UIImageView alloc]initWithFrame:CGRectMake(5,5,30,30)];
    [paddingImgVw setBackgroundColor:[UIColor clearColor]];
    
    switch (row) {
        case CELL_NAME:{
            paddingImgVw.image = [UIImage imageNamed:@"ic_user_profile"];
            cell.txtFieldPartners.placeholder = kFullname;
        }
            break;
        case CELL_EMAIL:{
            paddingImgVw.image = [UIImage imageNamed:@"ic_envelop"];
            cell.txtFieldPartners.placeholder = kEmailOptional;
        }
            break;
        case CELL_AGE:{
            paddingImgVw.image = [UIImage imageNamed:@"ic_user_profile"];
            cell.txtFieldPartners.placeholder = kAge;
        }
            break;
        case CELL_NATIONALITY:{
            paddingImgVw.image = [UIImage imageNamed:@"ic_user_profile"];
            cell.txtFieldPartners.placeholder = kNationality;
        }
            break;
        case CELL_PASSPORT:{
            paddingImgVw.image = [UIImage imageNamed:@"ic_user_profile"];
            cell.txtFieldPartners.placeholder = kPassport;
        }
            break;
        case CELL_CAR:{
            paddingImgVw.image = [UIImage imageNamed:@"ic_profile_car"];
            cell.txtFieldPartners.placeholder = kCarBrand;
        }
            break;
        case CELL_MOBILE:{
            //  paddingImgVw.image = [UIImage imageNamed:@"ic_profile_mobileno"];
            
            containerVw.frame = CGRectMake(0, 0,5, cell.txtFieldPartners.frame.size.height);
            paddingImgVw.frame = CGRectZero;
            
            cell.txtFieldPartners.placeholder = kMobileNumber;
        }
            break;
        case CELL_COVERING_CITY:{
            paddingImgVw.image = [UIImage imageNamed:@"ic_profile_address"];
            cell.txtFieldPartners.placeholder = kCoveringCity;
        }
            break;
            
        default:
            break;
    }
    
    [containerVw addSubview:paddingImgVw];
    [cell.txtFieldPartners setLeftViewMode:UITextFieldViewModeAlways];
    [cell.txtFieldPartners setLeftView:containerVw];
}

-(void)setTextFieldText:(JoinPartnerCell *)cell withRow:(NSInteger)row{
    
    switch (row) {
        case CELL_NAME:{
            cell.txtFieldPartners.text = objPartnersField.name;
        }
            break;
        case CELL_EMAIL:{
            cell.txtFieldPartners.text = objPartnersField.email;
        }
            break;
        case CELL_AGE:{
            cell.txtFieldPartners.text = objPartnersField.age;
        }
            break;
        case CELL_NATIONALITY:{
            cell.txtFieldPartners.text = objPartnersField.nationality;
        }
            break;
        case CELL_PASSPORT:{
            cell.txtFieldPartners.text = objPartnersField.passport;
        }
            break;
        case CELL_CAR:{
            cell.txtFieldPartners.text = objPartnersField.car;
        }
            break;
        case CELL_MOBILE:{
            cell.txtFieldPartners.text = objPartnersField.mobile;
        }
            break;
        case CELL_COVERING_CITY:{
            cell.txtFieldPartners.text = objPartnersField.coveringCity;
        }
            break;
            
        default:
            break;
    }
    
}

-(void)setButtonTitle:(JoinPartnerCell *)cell withRow:(NSInteger)row{
    
  
    if([AppContext.appLanguage isEqualToString:@"ar"]){
        [cell.btnPartners setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    }
    

    [cell.btnPartners setTitle:kCountry forState:UIControlStateNormal];
    [cell.btnPartners addTarget:self action:@selector(btnCountryClicked:event:) forControlEvents:UIControlEventTouchUpInside];
    
      [cell.btnPartners setTitle:[NSString stringWithFormat:@"+%@",objPartnersField.countryCode] forState:UIControlStateNormal];
  
//    if(selectedRow!=-1){
    
//        objPartnersField.countryCode = [arrCountryPhoneCodes objectAtIndex:selectedRow];
//        [cell.btnPartners setTitle:[NSString stringWithFormat:@"+%@",[arrCountryPhoneCodes objectAtIndex:selectedRow]] forState:UIControlStateNormal];
//        [cell.btnPartners setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
//        
//    }

    
}

#pragma mark :-------------------: TextFields Delegates

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    switch (textField.tag) {
        case CELL_NAME:{
            objPartnersField.name = textField.text;
        }
            break;
        case CELL_AGE:{
            objPartnersField.age = textField.text;
        }
            break;
        case CELL_EMAIL:{
            objPartnersField.email = textField.text;
        }
            break;
        case CELL_CAR:{
            objPartnersField.car = textField.text;
        }
            break;
        case CELL_NATIONALITY:{
            objPartnersField.nationality = textField.text;
        }
            break;
        case CELL_PASSPORT:{
            objPartnersField.passport = textField.text;
        }
            break;
        case CELL_MOBILE:{
            objPartnersField.mobile = textField.text;
        }
            break;
        case CELL_COVERING_CITY:{
            objPartnersField.coveringCity = textField.text;
        }
            break;
        default:
            break;
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(textField.tag == CELL_MOBILE){
        if(range.length + range.location > textField.text.length){
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 10;
    }
    if(textField.tag == CELL_AGE){
        if(range.location > 1){
            return NO;
        }
    }
    return YES;
}

#pragma mark :-------------------: Textview Delegates
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if(range.location == kMaxTextLength){
        return NO;
    }else{
        textLength =   range.location + 1;
        if([text isEqualToString:@""] && textLength == 1){
            textLength = 0;
        }
        
        JoinPartnerCell *cell = [_tblVwJoinPartners cellForRowAtIndexPath:[NSIndexPath indexPathForRow:CELL_OTHER_CATEGORY inSection:0]];
        cell.lblCounter.text = [NSString stringWithFormat:@"%lu / 1000",textLength];
    }
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    objPartnersField.suggesstion = textView.text;
}

#pragma mark :-------------------: Button Actions
-(void)btnCountryClicked:(id)sender event:(id)event{
    
    UITouch *touch = [event allTouches].anyObject;
    CGPoint location = [touch locationInView:_tblVwJoinPartners];
    NSIndexPath *indexPath = [_tblVwJoinPartners indexPathForRowAtPoint:location];
    
    if(indexPath.row == CELL_MOBILE){
        [self showCountryPicker];
    }
}

-(void)btnBrowseClicked:(id)sender event:(id)event{
    
    UITouch *touch = [event allTouches].anyObject;
    CGPoint location = [touch locationInView:_tblVwJoinPartners];
    NSIndexPath *indexPath = [_tblVwJoinPartners indexPathForRowAtPoint:location];
    
    if(indexPath.row == CELL_BROWSE_1){
        UIButton *btn = (UIButton *)sender;
        selectedBtn = btn.tag;
        [self configActionSheet:btn.tag];
    }
    
}


- (IBAction)btnSubmitAction:(id)sender {
    if([self isAllFieldValid]){
        [self addPartners];
    }

}

-(void)showCountryPicker{
    [self.view endEditing:YES];
    RTCountryPicker *countryPicker = [[RTCountryPicker alloc]initWithFrame:self.view.bounds];
    countryPicker.delegate = self;
    [self.navigationController.view addSubview:countryPicker];
}

#pragma mark: -------------------: CountryPicker Delegates
-(void)pickerSelectedCountry:(NSString *)selectedCode{
    objPartnersField.countryCode = selectedCode;
   [self.tblVwJoinPartners reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CELL_MOBILE inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark :--------------------: ImagePicker Delegate
-(void)configActionSheet:(NSInteger)tag{
    
    
    @try {
        
        UIImage *img = [[UIImage imageNamed:@"logo-icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        CDAlertView *alertView = [[CDAlertView alloc]init];
        alertView.messageLabel.text = kSelPicFrom;
        alertView.backgroundColor = RGBA(0, 0, 0, 0.7f);
        alertView.circleFillColor = [UIColor colorWithPatternImage:img];
        alertView.tintColor = [UIColor whiteColor];
        alertView.isActionButtonsVertical = YES;
        alertView.alertBackgroundColor = [UIColor whiteColor];
        alertView.messageTextColor = [UIColor grayColor];
        
        
        CDAlertViewAction *action_Camera = [[CDAlertViewAction alloc]initWithTitle:kCamera font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            [self configImagePickerForCamera:YES];
        }];
        
        CDAlertViewAction *action_Photos = [[CDAlertViewAction alloc]initWithTitle:kPhotoLibrary font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
            [self configImagePickerForCamera:NO];
        }];
        
        [alertView addWithAction:action_Camera];
        [alertView addWithAction:action_Photos];
        
        
        [self configDeleteAction:tag withAlertController:alertView];
        
        [alertView show:^(CDAlertView *action) {
            
        }];
        
    } @catch (NSException *exception) {
        NSLog(@"Alertview Exception : %@ , In Controller : %@",[exception description],self);
    } @finally {
        
    }
    
   
    
}

-(void)configDeleteAction:(NSInteger)btnTag withAlertController:(CDAlertView *)alert{
    
    switch (btnTag) {
        case kBtn1Tag:{
            if(objPartnersField.img1Selected){
                
                CDAlertViewAction *action_delete = [[CDAlertViewAction alloc]initWithTitle:kDeletePhoto font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
                    objPartnersField.img1 = nil;
                    objPartnersField.img1Selected = NO;
                    [self.tblVwJoinPartners reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CELL_BROWSE_1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                }];
                [alert addWithAction:action_delete];
            }
        }
            break;
        case kBtn2Tag:{
            if(objPartnersField.img2Selected){
                CDAlertViewAction *action_delete = [[CDAlertViewAction alloc]initWithTitle:kDeletePhoto font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
                    objPartnersField.img2 = nil;
                    objPartnersField.img2Selected = NO;
                    [self.tblVwJoinPartners reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CELL_BROWSE_1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                }];
                [alert addWithAction:action_delete];
            }
        }
            break;
        case kBtn3Tag:{
            if(objPartnersField.img3Selected){
                
                CDAlertViewAction *action_delete = [[CDAlertViewAction alloc]initWithTitle:kDeletePhoto font:Dax_Regular(15) textColor:AppThemeBlueColor backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
                    objPartnersField.img3 = nil;
                    objPartnersField.img3Selected = NO;
                    [self.tblVwJoinPartners reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CELL_BROWSE_1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                }];
                [alert addWithAction:action_delete];
            }
            
        }
            break;
            
        default:
            break;
    }
    
    CDAlertViewAction *action_cancel = [[CDAlertViewAction alloc]initWithTitle:kCancel font:Dax_Regular(15) textColor:[UIColor redColor] backgroundColor:[UIColor whiteColor] handler:^(CDAlertViewAction *action1) {
        
    }];
    [alert addWithAction:action_cancel];
    
}

-(void)configImagePickerForCamera:(BOOL)isCamera{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    
    if(isCamera){
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }else{
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    [self presentViewController:picker animated:YES completion:NULL];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    
    UIImage *selectedImage =(UIImage *)info[UIImagePickerControllerEditedImage];
    NSData *imgData=UIImageJPEGRepresentation( selectedImage,0.7);
    UIImage *compressedImage =[UIImage imageWithData:imgData];
    
    switch (selectedBtn) {
        case kBtn1Tag:{
            objPartnersField.img1Selected = YES;
            objPartnersField.img1 =compressedImage;
            //objPartnersField.strDoc1 = [self encodeToBase64String:compressedImage];
        }
            break;
        case kBtn2Tag:{
            objPartnersField.img2Selected = YES;
            objPartnersField.img2 = compressedImage;
            //    objPartnersField.strDoc2 = [self encodeToBase64String:compressedImage];
        }
            break;
        case kBtn3Tag:{
            objPartnersField.img3Selected = YES;
            objPartnersField.img3 = compressedImage;
            // objPartnersField.strDoc3 = [self encodeToBase64String:compressedImage];
        }
            break;
            
        default:
            break;
    }
    [self.tblVwJoinPartners reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CELL_BROWSE_1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    selectedBtn = 0;
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark :-------------------: Validate Fields

-(BOOL)isAllFieldValid{
    
    if([objPartnersField.name isEqualToString:@""]){
          [self showAlert:[NSString stringWithFormat:@"%@ %@",kRequired,kFullname]];
        return NO;
    }

    if([objPartnersField.age isEqualToString:@""]){
        [self showAlert:[NSString stringWithFormat:@"%@ %@",kRequired,kAge]];
        return NO;
    }
    
    if([objPartnersField.nationality isEqualToString:@""]){
       [self showAlert:[NSString stringWithFormat:@"%@ %@",kRequired,kNationality]];
        return NO;
    }
    
    if([objPartnersField.passport isEqualToString:@""]){
        [self showAlert:[NSString stringWithFormat:@"%@ %@",kRequired,kPassport]];
        return NO;
    }
    
    if([objPartnersField.countryCode isEqualToString:@""]){
        [self showAlert:[NSString stringWithFormat:@"%@ %@",kRequired,kCountry]];
        return NO;
    }
    
    if([objPartnersField.mobile isEqualToString:@""]){
        [self showAlert:[NSString stringWithFormat:@"%@ %@",kRequired,kMobileNo]];
        return NO;
    }
     if(![self validatePhoneNumber:objPartnersField.countryCode withNumber:objPartnersField.mobile]){
        [self showAlert:[NSString stringWithFormat:@"%@ %@",KInvalid,kPhoneNo]];
        return NO;
    }
    
    if([objPartnersField.car isEqualToString:@""]){
        [self showAlert:[NSString stringWithFormat:@"%@ %@",kRequired,kCarBrand]];
        return NO;
    }

    if([objPartnersField.coveringCity isEqualToString:@""]){
       [self showAlert:[NSString stringWithFormat:@"%@ %@",kRequired,kCoveringCity]];
        return NO;
    }
    if(arrSelectedServices.count == 0){
        [self showAlert:[NSString stringWithFormat:@"%@ %@",kRequired,kProvidedServices]];
        return NO;
    }
    if(objPartnersField.img1Selected == NO || objPartnersField.img2Selected == NO || objPartnersField.img3Selected == NO){
        [self showAlert:[NSString stringWithFormat:@"%@ %@",kPleaseSelect,kSelectDocuments]];
        return NO;
    }
    return YES;
}

#pragma mark :-------------------: Add Partners
-(void)addPartners{
    
    if(![self isUserLoggedIn]){
        [self showAlert:kFeatureDisableMsg];
    }else{
        
        NSString *categoryId = [arrSelectedServices componentsJoinedByString:@","];
        NSDate *todayDate = [NSDate date];
        NSDateFormatter *dtFormatter = [[NSDateFormatter alloc]init];
        [dtFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *strDate = [dtFormatter stringFromDate:todayDate];
        
         NSString *strPhoneNumber = [NSString stringWithFormat:@"%@",[self validatePhoneField:objPartnersField.mobile]];
        
        NSDictionary *dictParams =@{
                                    @"userId":[Functions getStringValueFromDefaults:USER_ID],
                                    @"fullName":objPartnersField.name,
                                    @"age":objPartnersField.age,
                                    @"nationality":objPartnersField.nationality,
                                    @"nationalId":objPartnersField.passport,
                                    @"mobileNo":strPhoneNumber,
                                    @"year":objPartnersField.car,
                                    @"coveringCity":objPartnersField.coveringCity,
                                    @"providedcategoryId":categoryId,
                                    @"countryCode":objPartnersField.countryCode,
                                    @"suggestion":objPartnersField.suggesstion,
                                    @"email":objPartnersField.email,
                                    @"documentone":objPartnersField.img1,
                                    @"documenttwo":objPartnersField.img2,
                                    @"documentthree":objPartnersField.img3,
                                    @"joinDate":strDate
                                    };
        
        
        if([ReachabilityManager isReachable]){
            [[WebServiceInvocation alloc]initWithWS:kAddPartnersWS withParams:dictParams withServiceType:TYPE_POST withSelector:@selector(addPartnersWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        }else{
            ShowNoNetworkAlert(self);
        }

    }
  
}

-(void)addPartnersWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        [self resetAllfields];
        [self redirectToHome:[[sender responseDictionary] objectForKey:@"Message"]];
    }
}

-(NSString *)validatePhoneField:(NSString *)txt{
    
    NSString *strPhoneText = txt;
    if([strPhoneText containsString:@"+"]){
        strPhoneText = [strPhoneText stringByReplacingOccurrencesOfString:@"+" withString:@""];
    }
    
    NSRange range = NSMakeRange(0,1);
    NSString *discardZero = [strPhoneText substringWithRange:range];
    
    if([discardZero isEqualToString:@"0"]){
        strPhoneText = [strPhoneText stringByReplacingCharactersInRange:range withString:@""];
    }
    
    range = NSMakeRange(0,1);
    discardZero = [strPhoneText substringWithRange:range];
    if([discardZero isEqualToString:@"0"]){
        strPhoneText = [strPhoneText stringByReplacingCharactersInRange:range withString:@""];
    }
    
    return strPhoneText;
}

-(void)redirectToHome:(NSString *)msg{
    
    [UIAlertController showAlertInVC:self withTitle:AppName withMsg:msg withOtherAlertActionTitle:@[kAlertOk] withAlertActionCancel:@"" withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
        if(buttonIndex == 0){
               [self.navigationController popViewControllerAnimated:YES];
        }
    }];
 
}

-(void)resetAllfields{

    objPartnersField = nil;
    [arrSelectedServices removeAllObjects];
    objPartnersField = [[PartnersFiled alloc]init];
    [_tblVwJoinPartners reloadData];
}

@end
