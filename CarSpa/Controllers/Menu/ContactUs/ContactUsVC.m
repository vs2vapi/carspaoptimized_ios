//
//  ContactUsVC.m
//  CarSpa
//
//  Created by R@j on 25/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "ContactUsVC.h"
#import "WSContactUs.h"
#import "ContactUsCell.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "WSContactUs.h"
#import "WSContact.h"

@interface ContactUsVC ()<MFMailComposeViewControllerDelegate>{
    MFMailComposeViewController *mailController;
    NSMutableArray *arrContacts;
}

@end

@implementation ContactUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _tblVwContacts.tableFooterView= [UIView new];
    _tblVwContacts.separatorColor = [UIColor clearColor];
    [self fetchContactDetails];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}

-(void)fetchContactDetails{
    
    if([ReachabilityManager isReachable]){
        NSString *url = [NSString stringWithFormat:@"%@?language=%@&%@=%@",kContactUsWS,AppContext.appLanguage,kAppVersionServiceKey,[self fetchAppVersion]];
        [[WebServiceInvocation alloc]initWithWS:url withParams:nil withServiceType:TYPE_GET withSelector:@selector(contactWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }
    
}

-(void)contactWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        arrContacts =[NSMutableArray arrayWithArray:[sender responseArray]];
        [_tblVwContacts reloadData];
    }
}

#pragma mark:-----------------: Tableview Delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrContacts.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"Cell";
    ContactUsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        cell = [[ContactUsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    
    WSContactUs *objContact = [arrContacts objectAtIndex:indexPath.row];
    WSContact *contacts = [objContact.contact objectAtIndex:0];
    
    if([AppContext.appLanguage isEqualToString:@"ar"]){

        [cell.btnAddress setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [cell.btnEmail setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [cell.btnMobile setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    }
    
    
    cell.lblCity.text  = objContact.name;
   
    [cell.btnAddress setTitle:contacts.address forState:UIControlStateNormal];
    [cell.btnEmail setTitle:contacts.email forState:UIControlStateNormal];

    [cell.btnMobile setTitle:[NSString stringWithFormat:@"%@",contacts.mobileNumber] forState:UIControlStateNormal];
    [cell.btnMobile addTarget:self action:@selector(btnSupportPhoneClick:event:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnEmail addTarget:self action:@selector(btnSupportMailClick:event:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
}

#pragma mark:-----------------: Button Actions
- (void)btnSupportMailClick:(id)sender event:(id)event{
    
 
    
    
    if ([MFMailComposeViewController canSendMail]) {
        
        UITouch *touch = [event allTouches].anyObject;
        CGPoint location = [touch locationInView:_tblVwContacts];
        NSIndexPath *indexPath = [_tblVwContacts indexPathForRowAtPoint:location];
        
        WSContactUs *objContact = [arrContacts objectAtIndex:indexPath.row];
        WSContact *contacts = [objContact.contact objectAtIndex:0];

        NSString *emailId = contacts.email;
        
        mailController = [[MFMailComposeViewController alloc] init];
        [mailController setToRecipients:[NSArray arrayWithObjects:emailId, nil]];
        [mailController setMailComposeDelegate:self];
        // Present mail view controller on screen
        [self presentViewController:mailController animated:YES completion:NULL];
        
    }else{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:AppName message:@"Mail is not configured in device." preferredStyle:(UIAlertControllerStyleAlert)];
        
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:kAlertOk style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:actionOk];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}

- (void)btnSupportPhoneClick:(id)sender event:(id)event{
    
    
    UITouch *touch = [event allTouches].anyObject;
    CGPoint location = [touch locationInView:_tblVwContacts];
    NSIndexPath *indexPath = [_tblVwContacts indexPathForRowAtPoint:location];
    
    WSContactUs *objContact = [arrContacts objectAtIndex:indexPath.row];
    WSContact *contacts = [objContact.contact objectAtIndex:0];
    
    
    NSString *countryCode = [NSString stringWithFormat:@"%.0f",contacts.countryCode];
    NSString *number = [NSString stringWithFormat:@"+%@%@",countryCode,contacts.mobileNumber];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",number]];
    
    //  NSString *phoneNumber = [@"tel://" stringByAppendingString:number];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:AppName message:@"Unable to open phone app." preferredStyle:(UIAlertControllerStyleAlert)];
        
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:kAlertOk style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:actionOk];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark:-----------------: MailComposer Delegates

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end
