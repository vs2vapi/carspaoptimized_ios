//
//  PrivacyPolicyVC.m
//  CarSpa
//
//  Created by R@j on 25/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "PrivacyPolicyVC.h"

@interface PrivacyPolicyVC ()

@end

@implementation PrivacyPolicyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchPolicyDetails];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}

-(void)fetchPolicyDetails{
    
    if([ReachabilityManager isReachable]){
        NSString *url = [NSString stringWithFormat:@"%@?language=%@&%@=%@",kPrivacyPolicyWS,AppContext.appLanguage,kAppVersionServiceKey,[self fetchAppVersion]];
        [[WebServiceInvocation alloc]initWithWS:url withParams:nil withServiceType:TYPE_GET withSelector:@selector(policyWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
        
    }else{
        ShowNoNetworkAlert(self);
    }
    
}

-(void)policyWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        
        NSString *strFont = @"<font face='Dax-Regular' size='3'>";
        NSString *strTitle = [[[sender responseDictionary] objectForKey:@"Data"]valueForKey:([AppContext.appLanguage isEqualToString:@"ar"])? @"policyTitleAR" : @"policyTitle"];
        
        NSString *align = @"left";
        if([AppContext.appLanguage isEqualToString:@"ar"]){
            align = @"right";
        }
        NSString *title = [NSString stringWithFormat:@"<font face='Dax-Bold' size='3'><p align=\"%@\">%@</p>",align,strTitle];
        
        NSString *htmlString = [NSString stringWithFormat:@"%@ %@ %@",title,strFont,[[[sender responseDictionary] objectForKey:@"Data"]valueForKey:([AppContext.appLanguage isEqualToString:@"ar"])? @"policyAR" : @"policy"]];
        [_webVwPolicy loadHTMLString:htmlString baseURL:nil];
    }
}

@end
