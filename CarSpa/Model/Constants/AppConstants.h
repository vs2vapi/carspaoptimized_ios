//
//  AppConstants.h
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#ifndef AppConstants_h
#define AppConstants_h


#pragma mark: --------------------: UI Macros
#define kDeviceType @"2"
#define kMaxDay 9
#define kMaxMonth 2
#define kPaymentStatus 1
#define YYYY_MM_dd @"YYYY-MM-dd"
#define YYYY_MM_dd_HH @"YYYY-MM-dd HH:mm:ss"
#define EE_dd_MMM_HH @"EEE dd MMM hh:mm a" //DayName DD-MM- YYYY HH:MM AM/PM
#define ActiveOrder_DateFormat @"EEEE dd-MM-yyyy hh:mm a"
#define dd_MMM_YYYY @"dd MMM yyyy"
#define yyyy_MM_dd @"yyyy-MM-dd"
#define EE_dd_MMM @"EEE dd MMM"
#define DD_MM_YYYY @"EEE dd-MM-yyyy"

#define kApp_AppStoreLink @"https://itunes.apple.com/us/app/carspauser/id1241656458?ls=1&mt=8"
#define kAuth_UserName @"digestingcarspaauth"
#define kAuth_Password @"C@rSp@Unknown"
#define kAuth_Key @"Basic ZGlnZXN0aW5nY2Fyc3BhYXV0aDpDQHJTcEBVbmtub3du"

/*
 OLD
#define TWITTER_CONSUMER_KEY @"RzKZqwAdtZ3sCZJK96w2bz6Ku"
#define TWITTER_CONSUMER_SECERT_KEY @"TPHx9ONiZYO8vLdIHTkc6ztPWBlMQkbzdP59izfxJTLFDUrqJb"
*/

#define TWITTER_CONSUMER_KEY @"mfDMHSojuPe6A993dDDZ2HLiz"
#define TWITTER_CONSUMER_SECERT_KEY @"utYSmePVl21XvuLGTkFvcdqFJryeljHGDJQMz5bTJTgNizCYmB"


#define AppContext   ((AppDelegate *)[[UIApplication sharedApplication]delegate])
#define CORE_DATA_INSTANCE  [AppContext dataAdaptor]

#define hideNavBar(hideNavBar)  [self.navigationController.navigationBar setHidden:hideNavBar]
#define SCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT  [UIScreen mainScreen].bounds.size.height


#pragma mark: --------------------: Social Login Constants
#define  GOOGLE_PLUS_LOGIN  1
#define  TWITTER_LOGIN  2
#define  LINKEDIN_LOGIN 3
#define  INSTAGRAM_LOGIN 4
#define  FACEBOOK_LOGIN  5
#define  EMAIL_LOGIN 6

#pragma mark: --------------------: Appointment Booked Stauts

#define STATUS_BOOKED 1
#define STATUS_IN_PROGRESS 2
#define STATUS_COMPLETED 3
#define STATUS_CANCELLED 4
#define STATUS_ON_THE_WAY 5


#pragma mark: --------------------: UIColor Contants

#define RGBA(r, g, b, a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]
#define AppThemeColor RGBA(76, 175, 80, 1)
#define AppThemeBlueColor RGBA(0, 176, 241, 1)

#define GOOGLE_API_KEY @"AIzaSyBj5brDZ0TxMN9BmDVwwLaGBNQf2dQrfgg"

#pragma mark: --------------------: Load StoryBoard
#define getStoryBoard(sbname)  [UIStoryboard storyboardWithName:sbname bundle:nil]
#define loadViewController(sbname,vcIdentifier) [getStoryBoard(sbname) instantiateViewControllerWithIdentifier:vcIdentifier]

#pragma mark: --------------------:  StoryBoardIdentifier
#define kSBMain @"Main"
#define kSBRequest @"Request"
#define kSBOthers @"Others"

#pragma mark: --------------------:  ViewController Identifier
#define kLoginVC @"idLoginVC"
#define kRegisterVC @"idRegisterVC"
#define kVerificationVC @"idVerificationVC"
#define kTabBarVC @"idTabBarVC"
#define kAddRequestVC @"idAddRequestVC"
#define kCategoriesVC @"idCategoriesVC"
#define kServiceVC @"idServiceVC"
#define kMapVC @"idMapVC"
#define kAppointmentsListVC @"idAppointmentsListVC"
#define kJobDetailsVC @"idJobDetailsVC"
#define kMapRouteVC @"idMapRouteVC"
#define kChangePasswordVC @"idChangePasswordVC"
#define kRatingVC @"idRatingVC"
#define kVehicleListVC @"idVehicleListVC"
#define kAddCarsVC @"idAddCarsVC"
#define kAddressFromMapVC @"idAddressFromMapVC"

#define kAboutUsVC @"idAboutUsVC"
#define kJoinPartnersVC @"idJoinPartnersVC"
#define kOurPartnersVC @"idOurPartnersVC"
#define kPrivacyPolicyVC @"idPrivacyPolicyVC"
#define kContactUsVC @"idContactUsVC"
#define kNotificationVC @"idNotificationVC"


#pragma mark :-----------: Fonts
#define Dax_Regular(fontSize) [UIFont fontWithName:@"dax-regular" size:fontSize]
#define Dax_Bold(fontSize) [UIFont fontWithName:@"dax-bold" size:fontSize]
#define Dax_Medium(fontSize) [UIFont fontWithName:@"dax-medium" size:fontSize]


#pragma mark: --------------------:  UserDefault Keys
#define IS_USER_LOGGED_IN @"IS_USER_LOGGED_IN"
#define DEVICE_TOKEN @"DEVICE_TOKEN"
#define DEFAULT_APP_LANGUAGE @"DEFAULT_APP_LANGUAGE"
#define SOCIAL_LOGIN_TYPE @"SOCIAL_LOGIN_TYPE"
#define USER_OBJ @"USER_OBJ"
#define USER_COUNTRY_CODE @"USER_COUNTRY_CODE"
#define LOCATION_FOUND @"LOCATION_FOUND"
#define LOGIN_SUCCESS @"LOGIN_SUCCESS"
#define USER_PHONE_CODE @"USER_PHONE_CODE"
#define USER_ID @"USER_ID"
#define USER_EMAIL @"USER_EMAIL"
#define USER_PSWD @"USER_PSWD"
#define USER_LATITUDE @"USER_LATITUDE"
#define USER_LONGITUDE @"USER_LONGITUDE"
#define CURRENCY_ID @"CURRENCY_ID"
#define CURRENCY @"CURRENCY"
#define EXCHANGE_RATE @"EXCHANGE_RATE"
#define RATING_ADDED @"RATING_ADDED"
#define DETAILS_ADDED @"DETAILS_ADDED"
#define UPDATE_LOCATION @"UPDATE_LOCATION"
#define REFRESH_ORDER_LIST @"REFRESH_ORDER_LIST"
#define NEW_ORDER_BOOKED @"NEW_ORDER_BOOKED"
#define NOTIFICATION_RECEIVED @"NOTIFICATION_RECEIVED"
#define EDIT_REQUEST_CAR_CHANGED @"EDIT_REQUEST_CAR_CHANGED"
#define EDIT_REQUEST_ADDRESS_CHANGED @"EDIT_REQUEST_ADDRESS_CHANGED"
#define EDIT_REQUEST_APPOINTMENT_CHANGED @"EDIT_REQUEST_APPOINTMENT_CHANGED"
#define EDIT_REQUEST_TOW_ADDRESS_CHANGED @"EDIT_REQUEST_TOW_ADDRESS_CHANGED"
#define APPOINTMENT_CANCELLED @"APPOINTMENT_CANCELLED"
#define LANGUAGE_CHANGED @"LANGUAGE_CHANGED"
#define CHECK_FORCE_UPDATE @"CHECK_FORCE_UPDATE"


#pragma mark: --------------------:  Notification Type Keys
#define kNotificationKey_Title @"title"
#define kNotificationKey_Msg @"message"
#define kNotificationKey_Type @"notification_type"
#define kNotificationKey_Date @"date"

#endif /* AppConstants_h */
