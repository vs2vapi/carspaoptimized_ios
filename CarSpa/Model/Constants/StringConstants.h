//
//  StringConstants.h
//  CarSpa
//
//  Created by R@j on 19/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "Localization.h"
#ifndef StringConstants_h
#define StringConstants_h



#define AppName NSLocalizedString(@"LK_AppName", nil)
#define kAlertYes NSLocalizedString(@"LK_AlertYes", nil)
#define kAlertNo NSLocalizedString(@"LK_AlertNo", nil)
#define kAlertOk NSLocalizedString(@"LK_AlertOk", nil)
#define kCancel NSLocalizedString(@"LK_Cancel", nil)
#define kCANCEL NSLocalizedString(@"LK_CANCEL", nil)


#define kAlreadyMember NSLocalizedString(@"LK_AlreadyMember", nil)
#define kLoginWelcome NSLocalizedString(@"LK_LoginWelcome", nil)

#define kNoDataAvailable NSLocalizedString(@"LK_NoDataAvailable", nil)
#define kServerIssueFailure NSLocalizedString(@"LK_ServerIssueFailure", nil)

#define kIssueInResponse  NSLocalizedString(@"LK_IssueInResponse", nil)
#define kWSLoaderDialoagMsg NSLocalizedString(@"LK_WSLoaderDialoagMsg", nil)


#define KNoNetwork  NSLocalizedString(@"LK_NoNetwork", nil)
#define kNoNetworkMsg  NSLocalizedString(@"LK_NoNetworkMsg", nil)

#define kNavigationBarTitle NSLocalizedString(@"LK_NavigationBarTitle", nil)
#define kRequestService NSLocalizedString(@"LK_RequestService", nil)
#define kAboutUs  NSLocalizedString(@"LK_AboutUs", nil)
#define kJoinOurPartners  NSLocalizedString(@"LK_JoinOurPartners", nil)

#define kOurPartners NSLocalizedString(@"LK_OurPartners", nil)
#define kPrivacyPolicy  NSLocalizedString(@"LK_PrivacyPolicy", nil)
#define kContactUs  NSLocalizedString(@"LK_ContactUs", nil)
#define kSignOut NSLocalizedString(@"LK_SignOut", nil)
#define kTapHereToChange  NSLocalizedString(@"LK_TapHereToChange", nil)
#define kNotifications NSLocalizedString(@"LK_Notifications", nil)


#define kEmail  NSLocalizedString(@"LK_Email", nil)
#define kMobileNo  NSLocalizedString(@"LK_MobileNo", nil)
#define kAddress  NSLocalizedString(@"LK_Address", nil)
#define kCar NSLocalizedString(@"LK_Car", nil)
#define kLanguage  NSLocalizedString(@"LK_Language", nil)
#define kCurrency  NSLocalizedString(@"LK_Currency", nil)

#define kCountry_Bahrain  NSLocalizedString(@"LK_Country_Bahrain", nil)
#define kCountry_Kuwait   NSLocalizedString(@"LK_Country_Kuwait", nil)
#define kCountry_Qatar  NSLocalizedString(@"LK_Country_Qatar", nil)
#define kCountry_Saudi  NSLocalizedString(@"LK_Country_Saudi", nil)
#define kCountry_UnitedArab  NSLocalizedString(@"LK_Country_UnitedArab", nil)
#define kCountry_Lebanon  NSLocalizedString(@"LK_Country_Lebanon", nil)
#define kCountry_Oman  NSLocalizedString(@"LK_Country_Omanl", nil)

#define kIntro_text_1  NSLocalizedString(@"LK_Intro_text_1", nil)
#define kIntro_text_2  NSLocalizedString(@"LK_Intro_text_2", nil)
#define kIntro_text_3  NSLocalizedString(@"LK_Intro_text_3", nil)
#define kIntro_text_4  NSLocalizedString(@"LK_Intro_text_4", nil)
#define kIntro_text_5  NSLocalizedString(@"LK_Intro_text_5", nil)
#define kIntro_text_6  NSLocalizedString(@"LK_Intro_text_6", nil)

#define kIntro_desc_1  NSLocalizedString(@"LK_Intro_desc_1", nil)
#define kIntro_desc_2  NSLocalizedString(@"LK_Intro_desc_2", nil)
#define kIntro_desc_3  NSLocalizedString(@"LK_Intro_desc_3", nil)
#define kIntro_desc_4  NSLocalizedString(@"LK_Intro_desc_4", nil)
#define kIntro_desc_5  NSLocalizedString(@"LK_Intro_desc_5", nil)
#define kIntro_desc_6  NSLocalizedString(@"LK_Intro_desc_6", nil)

#define kEnglish NSLocalizedString(@"LK_English", nil)
#define kArabic NSLocalizedString(@"LK_Arabic", nil)
#define kSelectLanguage NSLocalizedString(@"LK_SelectLanguage", nil)

#define kPleaseEnter NSLocalizedString(@"LK_PleaseEnter", nil)
#define KInvalid NSLocalizedString(@"LK_Invalid", nil)
#define kPassword NSLocalizedString(@"LK_Password", nil)
#define kSignOutConfirmation NSLocalizedString(@"LK_SignOutConfirmation", nil)
#define kDestination NSLocalizedString(@"LK_Destination", nil)

#define kName NSLocalizedString(@"LK_Name", nil)
#define kConfirmEmail NSLocalizedString(@"LK_ConfirmEmail", nil)
#define kConfirmPswd NSLocalizedString(@"LK_ConfirmPswd", nil)
#define kCountry NSLocalizedString(@"LK_Country", nil)
#define kPhoneNo NSLocalizedString(@"LK_PhoneNo", nil)
#define kConfirmEmailNotMatched NSLocalizedString(@"LK_ConfirmEmailNotMatched", nil)
#define kPasswordNotMatched NSLocalizedString(@"LK_PasswordNotMatched", nil)
#define kWillSendSMS NSLocalizedString(@"LK_WillSendSMS", nil)
#define kVerifyOTPMsg NSLocalizedString(@"LK_VerifyOTPMsg", nil)
#define kCode NSLocalizedString(@"LK_Code", nil)
#define kRequest NSLocalizedString(@"LK_Request", nil)
#define kNotProvided NSLocalizedString(@"LK_NotProvided", nil)


#define kTapToAddVehicleDetail NSLocalizedString(@"LK_TapToAddVehicleDetail", nil)
#define kTapToAddAddressDetail NSLocalizedString(@"LK_TapToAddAddressDetail", nil)
#define kCurrentAddress NSLocalizedString(@"LK_CurrentAddress", nil)
#define kSelFromMap NSLocalizedString(@"LK_SelFromMap", nil)
#define kSelFromSavedAddr NSLocalizedString(@"LK_SelFromSavedAddr", nil)
#define kSelAddFrom NSLocalizedString(@"LK_SelAddFrom", nil)
#define kSelPicFrom NSLocalizedString(@"LK_SelPicFrom", nil)
#define kLocationNotAvail NSLocalizedString(@"LK_LocationNotAvail", nil)
#define kAddressNickName NSLocalizedString(@"LK_AddressNickName", nil)
#define kEnter NSLocalizedString(@"LK_Enter", nil)


#define kPleaseSelect NSLocalizedString(@"LK_PleaseSelect", nil)
#define kVehicleType NSLocalizedString(@"LK_VehicleType", nil)
#define kVehicleDetails NSLocalizedString(@"LK_VehicleDetails", nil)
#define kAddressDetails NSLocalizedString(@"LK_AddressDetails", nil)
#define kNoAddressTitle NSLocalizedString(@"LK_NoAddressTitle", nil)

#define kVehicle NSLocalizedString(@"LK_Vehicle", nil)
#define kVehicleBrand NSLocalizedString(@"LK_VehicleBrand", nil)
#define kVehicleModel NSLocalizedString(@"LK_VehicleModel", nil)
#define kVehicleNo NSLocalizedString(@"LK_VehicleNo", nil)
#define kCost NSLocalizedString(@"LK_Cost", nil)

#define kAtleastOneService NSLocalizedString(@"LK_AtleastOneService", nil)
#define kOffDay NSLocalizedString(@"LK_OffDay", nil)
#define kWeCanServe NSLocalizedString(@"LK_WeCanServe", nil)
#define kConfirmAppointment NSLocalizedString(@"LK_ConfirmAppointment", nil)
#define kSelectAction NSLocalizedString(@"LK_SelectAction", nil)

#define kTotalAmount NSLocalizedString(@"LK_TotalAmount", nil)
#define kCheckout NSLocalizedString(@"LK_Checkout", nil)
#define kPaymentMethod NSLocalizedString(@"LK_PaymentMethod", nil)
#define kSAR NSLocalizedString(@"LK_SAR", nil)
#define kAppoinmentBooked NSLocalizedString(@"LK_AppoinmentBooked", nil)
#define kEnterParticularAddr NSLocalizedString(@"LK_EnterParticularAddr", nil)

#define kTowCharges NSLocalizedString(@"LK_TowCharges", nil)
#define kWeWillCharge NSLocalizedString(@"LK_WeWillCharge", nil)
#define kAppox NSLocalizedString(@"LK_Appox", nil)
#define kSave NSLocalizedString(@"LK_Save", nil)
#define kUpdate NSLocalizedString(@"LK_Update", nil)
#define kUsername NSLocalizedString(@"LK_Username", nil)
#define kTapOnCountryCode  NSLocalizedString(@"LK_TapOnCountryCode", nil)
#define kSelect NSLocalizedString(@"LK_Select", nil)

#define kConfirmPswd_c NSLocalizedString(@"LK_ConfirmPswd_c", nil)
#define kOldPswd NSLocalizedString(@"LK_OldPswd", nil)
#define kNewPswd NSLocalizedString(@"LK_NewPswd", nil)

#define kEditRequest NSLocalizedString(@"LK_EditRequest", nil)
#define kEdit NSLocalizedString(@"LK_Edit", nil)

#define kCancelRequest NSLocalizedString(@"LK_CancelRequest", nil)
#define kChangeAppointmentTime NSLocalizedString(@"LK_ChangeAppointmentTime", nil)
#define kReasonForCancelRequest NSLocalizedString(@"LK_ReasonForCancelRequest", nil)


#pragma mark:--------------------: Join Partners
#define kFullname NSLocalizedString(@"LK_Fullname", nil)
#define kEmailAdd NSLocalizedString(@"LK_EmailAdd", nil)
#define kAge NSLocalizedString(@"LK_Age", nil)
#define kNationality NSLocalizedString(@"LK_Nationality", nil)

#define kPassport NSLocalizedString(@"LK_Passport", nil)
#define kMobileNumber NSLocalizedString(@"LK_MobileNumber", nil)
#define kCarBrand NSLocalizedString(@"LK_CarBrand", nil)
#define kCoveringCountry NSLocalizedString(@"LK_CoveringCountry", nil)
#define kCoveringCity NSLocalizedString(@"LK_CoveringCity", nil)
#define kProvidedServices NSLocalizedString(@"LK_ProvidedServices", nil)
#define kEmailOptional NSLocalizedString(@"LK_EmailOptional", nil)
#define kRequired NSLocalizedString(@"LK_Required", nil)

#define kSelectDocuments NSLocalizedString(@"LK_SelectDocuments", nil)
#define kDeletePhoto NSLocalizedString(@"LK_DeletePhoto", nil)
#define kSocialLoginAlertMsg NSLocalizedString(@"LK_SocialLoginAlertMsg", nil)

#define kTechnicalIssueMsg NSLocalizedString(@"LK_TechnicalIssueMsg", nil)
#define kServiceProviderLocation NSLocalizedString(@"LK_ServiceProviderLocation", nil)

#define kPlsAddRating NSLocalizedString(@"LK_PlsAddRating", nil)
#define kRatingSuccess NSLocalizedString(@"LK_RatingSuccess", nil)
#define kPlateNo NSLocalizedString(@"LK_PlateNo", nil)
#define kCarBrandModel NSLocalizedString(@"LK_CarBrandModel", nil)
#define kSureDelAddress NSLocalizedString(@"LK_SureDelAddress", nil)
#define kSureDelCar NSLocalizedString(@"LK_SureDelCar", nil)
#define kJustNow NSLocalizedString(@"LK_JustNow", nil)
#define kNoActiveOrder NSLocalizedString(@"LK_NoActiveOrder", nil)
#define kUpdateAddressMsg NSLocalizedString(@"LK_UpdateAddressMsg", nil)
#define kSubmit NSLocalizedString(@"LK_Submit", nil)

#pragma mark:--------------------: Job Status

#define kStatus_Booked NSLocalizedString(@"LK_Status_Booked", nil)
#define kStatus_InProgress NSLocalizedString(@"LK_Status_InProgress", nil)
#define kStatus_OntheWay NSLocalizedString(@"LK_Status_OntheWay", nil)
#define kStatus_Completed NSLocalizedString(@"LK_Status_Completed", nil)
#define kStatus_Cancelled NSLocalizedString(@"LK_Status_Cancelled", nil)


#define kCamera NSLocalizedString(@"LK_Camera", nil)
#define kPhotoLibrary NSLocalizedString(@"LK_PhotoLibrary", nil)
#define kChangeLanguage NSLocalizedString(@"LK_ChangeLanguage", nil)
#define kSwipeForMore NSLocalizedString(@"LK_SwipeForMore", nil)
#define kCancelOrder NSLocalizedString(@"LK_CancelOder", nil)
#define kEmailNotFoundErr NSLocalizedString(@"LK_EmailNotFoundErr", nil)
#define kBack NSLocalizedString(@"LK_Back", nil)
#define kInvalidEmail NSLocalizedString(@"LK_IncorrectEmail", nil)

#define kServiceDetails NSLocalizedString(@"LK_ServiceDetails", nil)
#define kGuestLogin NSLocalizedString(@"LK_GuestLogin", nil)
#define kLogin  NSLocalizedString(@"LK_Login", nil)
#define kFeatureDisableMsg  NSLocalizedString(@"LK_FeatureDisableMsg", nil)
#define kRequestServiceDisableMsg  NSLocalizedString(@"LK_RequestServiceDisableMsg", nil)
#define kSignUp  NSLocalizedString(@"LK_Signup", nil)
#define kAcceptTermsMsg  NSLocalizedString(@"LK_AcceptTerms", nil)
#define kAlertUpdateNow NSLocalizedString(@"LK_UpdateNow", nil)
#define kForceUpdateMsg NSLocalizedString(@"LK_ForceUpdateMsg", nil)
#define kVersion NSLocalizedString(@"LK_Version", nil)
#define kTaxAmount NSLocalizedString(@"LK_TaxAmount", nil)
#define kDueAmount NSLocalizedString(@"LK_DueAmount", nil)
#define kInclude NSLocalizedString(@"LK_Incl", nil)
#define kVATAmount NSLocalizedString(@"LK_VAT", nil)
#define kDiscount NSLocalizedString(@"LK_Discount", nil)
#define kDeleteAllNotificationMsg NSLocalizedString(@"LK_DeleteAllNotification", nil)
#define kNoNotificationMsg NSLocalizedString(@"LK_NoNotifications", nil)

#define kDelete NSLocalizedString(@"LK_Delete", nil)
#define kShare NSLocalizedString(@"LK_Share", nil)

#define kDownload NSLocalizedString(@"LK_Download", nil)
#define kApp NSLocalizedString(@"LK_App", nil)
#define kCannotSelService NSLocalizedString(@"LK_CannotSelService", nil)
#define kCallUs NSLocalizedString(@"LK_CallUs", nil)


#define kSupplier NSLocalizedString(@"LK_Supplier", nil)
#define kDate NSLocalizedString(@"LK_Date", nil)
#define kAskAgain NSLocalizedString(@"LK_AskAgain", nil)
#define kCannotChangePswd NSLocalizedString(@"LK_CannotChangePswd", nil)

#endif /* StringConstants_h */
