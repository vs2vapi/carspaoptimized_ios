//
//  WSUser.m
//
//  Created by Krishna Bhadola on 26/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSUser.h"


NSString *const kWSUserMobileNumber = @"mobileNumber";
NSString *const kWSUserId = @"Id";
NSString *const kWSUserLongitude = @"longitude";
NSString *const kWSUserCountryCode = @"countryCode";
NSString *const kWSUserLatitude = @"latitude";
NSString *const kWSUserSocialId = @"socialId";
NSString *const kWSUserSocialType = @"socialType";
NSString *const kWSUserFullName = @"fullName";
NSString *const kWSUserThumbUrl = @"thumbUrl";
NSString *const kWSUserFileUrl = @"fileUrl";
NSString *const kWSUserEmail = @"email";
NSString *const kWSUserStatus = @"status";
NSString *const kWSUserDatetime = @"datetime";


@interface WSUser ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSUser

@synthesize mobileNumber = _mobileNumber;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize longitude = _longitude;
@synthesize countryCode = _countryCode;
@synthesize latitude = _latitude;
@synthesize socialId = _socialId;
@synthesize socialType = _socialType;
@synthesize fullName = _fullName;
@synthesize thumbUrl = _thumbUrl;
@synthesize fileUrl = _fileUrl;
@synthesize email = _email;
@synthesize status = _status;
@synthesize datetime = _datetime;

@synthesize address = _address;
@synthesize addressId = _addressId;
@synthesize carId = _carId;
@synthesize carBrand = _carBrand;
@synthesize carModel = _carModel;
@synthesize carTypeId = _carTypeId;
@synthesize addressTitle = _addressTitle;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.mobileNumber = [[self objectOrNilForKey:kWSUserMobileNumber fromDictionary:dict] doubleValue];
            self.internalBaseClassIdentifier = [[self objectOrNilForKey:kWSUserId fromDictionary:dict] integerValue];
            self.longitude = [[self objectOrNilForKey:kWSUserLongitude fromDictionary:dict] doubleValue];
            self.countryCode = [[self objectOrNilForKey:kWSUserCountryCode fromDictionary:dict] integerValue];
            self.latitude = [[self objectOrNilForKey:kWSUserLatitude fromDictionary:dict] doubleValue];
            self.socialId = [self objectOrNilForKey:kWSUserSocialId fromDictionary:dict] ;
            self.socialType = [[self objectOrNilForKey:kWSUserSocialType fromDictionary:dict] integerValue];
            self.fullName = [self objectOrNilForKey:kWSUserFullName fromDictionary:dict];
            self.thumbUrl = [self objectOrNilForKey:kWSUserThumbUrl fromDictionary:dict];
            self.fileUrl = [self objectOrNilForKey:kWSUserFileUrl fromDictionary:dict];
            self.email = [self objectOrNilForKey:kWSUserEmail fromDictionary:dict];
            self.status = [[self objectOrNilForKey:kWSUserStatus fromDictionary:dict] integerValue];
            self.datetime = [self objectOrNilForKey:kWSUserDatetime fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.mobileNumber] forKey:kWSUserMobileNumber];
    [mutableDict setValue:[NSNumber numberWithInteger:self.internalBaseClassIdentifier] forKey:kWSUserId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.longitude] forKey:kWSUserLongitude];
    [mutableDict setValue:[NSNumber numberWithInteger:self.countryCode] forKey:kWSUserCountryCode];
    [mutableDict setValue:[NSNumber numberWithDouble:self.latitude] forKey:kWSUserLatitude];
    [mutableDict setValue:self.socialId forKey:kWSUserSocialId];
    [mutableDict setValue:[NSNumber numberWithInteger:self.socialType] forKey:kWSUserSocialType];
    [mutableDict setValue:self.fullName forKey:kWSUserFullName];
    [mutableDict setValue:self.thumbUrl forKey:kWSUserThumbUrl];
    [mutableDict setValue:self.fileUrl forKey:kWSUserFileUrl];
    [mutableDict setValue:self.email forKey:kWSUserEmail];
    [mutableDict setValue:[NSNumber numberWithInteger:self.status] forKey:kWSUserStatus];
    [mutableDict setValue:self.datetime forKey:kWSUserDatetime];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.mobileNumber = [aDecoder decodeDoubleForKey:kWSUserMobileNumber];
    self.internalBaseClassIdentifier = [aDecoder decodeIntegerForKey:kWSUserId];
    self.longitude = [aDecoder decodeDoubleForKey:kWSUserLongitude];
    self.countryCode = [aDecoder decodeIntegerForKey:kWSUserCountryCode];
    self.latitude = [aDecoder decodeDoubleForKey:kWSUserLatitude];
    self.socialId = [aDecoder decodeObjectForKey:kWSUserSocialId];
    self.socialType = [aDecoder decodeIntegerForKey:kWSUserSocialType];
    self.fullName = [aDecoder decodeObjectForKey:kWSUserFullName];
    self.thumbUrl = [aDecoder decodeObjectForKey:kWSUserThumbUrl];
    self.fileUrl = [aDecoder decodeObjectForKey:kWSUserFileUrl];
    self.email = [aDecoder decodeObjectForKey:kWSUserEmail];
    self.status = [aDecoder decodeIntegerForKey:kWSUserStatus];
    self.datetime = [aDecoder decodeObjectForKey:kWSUserDatetime];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_mobileNumber forKey:kWSUserMobileNumber];
    [aCoder encodeInteger:_internalBaseClassIdentifier forKey:kWSUserId];
    [aCoder encodeDouble:_longitude forKey:kWSUserLongitude];
    [aCoder encodeInteger:_countryCode forKey:kWSUserCountryCode];
    [aCoder encodeDouble:_latitude forKey:kWSUserLatitude];
    [aCoder encodeObject:_socialId forKey:kWSUserSocialId];
    [aCoder encodeInteger:_socialType forKey:kWSUserSocialType];
    [aCoder encodeObject:_fullName forKey:kWSUserFullName];
    [aCoder encodeObject:_thumbUrl forKey:kWSUserThumbUrl];
    [aCoder encodeObject:_fileUrl forKey:kWSUserFileUrl];
    [aCoder encodeObject:_email forKey:kWSUserEmail];
    [aCoder encodeInteger:_status forKey:kWSUserStatus];
    [aCoder encodeObject:_datetime forKey:kWSUserDatetime];
}

- (id)copyWithZone:(NSZone *)zone {
    WSUser *copy = [[WSUser alloc] init];
    
    
    
    if (copy) {

        copy.mobileNumber = self.mobileNumber;
        copy.internalBaseClassIdentifier = self.internalBaseClassIdentifier;
        copy.longitude = self.longitude;
        copy.countryCode = self.countryCode;
        copy.latitude = self.latitude;
        copy.socialId = [self.socialId copyWithZone:zone];
        copy.socialType = self.socialType;
        copy.fullName = [self.fullName copyWithZone:zone];
        copy.thumbUrl = [self.thumbUrl copyWithZone:zone];
        copy.fileUrl = [self.fileUrl copyWithZone:zone];
        copy.email = [self.email copyWithZone:zone];
        copy.status = self.status;
        copy.datetime = [self.datetime copyWithZone:zone];
    }
    
    return copy;
}


@end
