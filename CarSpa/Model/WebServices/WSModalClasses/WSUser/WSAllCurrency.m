//
//  WSAllCurrency.m
//
//  Created by Krishna Bhadola on 04/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSAllCurrency.h"
#import "WSCountry.h"


NSString *const kWSAllCurrencyCurrency = @"currency";
NSString *const kWSAllCurrencyCountry = @"country";
NSString *const kWSAllCurrencyCurrencyAR = @"currencyAR";
NSString *const kWSAllCurrencyExchangeRate = @"exchangeRate";
NSString *const kWSAllCurrencyId = @"Id";


@interface WSAllCurrency ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSAllCurrency

@synthesize currency = _currency;
@synthesize country = _country;
@synthesize currencyAR = _currencyAR;
@synthesize exchangeRate = _exchangeRate;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.currency = [self objectOrNilForKey:kWSAllCurrencyCurrency fromDictionary:dict];
            self.country = [WSCountry modelObjectWithDictionary:[dict objectForKey:kWSAllCurrencyCountry]];
            self.currencyAR = [self objectOrNilForKey:kWSAllCurrencyCurrencyAR fromDictionary:dict];
            self.exchangeRate = [[self objectOrNilForKey:kWSAllCurrencyExchangeRate fromDictionary:dict] doubleValue];
            self.internalBaseClassIdentifier = [[self objectOrNilForKey:kWSAllCurrencyId fromDictionary:dict] integerValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.currency forKey:kWSAllCurrencyCurrency];
    [mutableDict setValue:[self.country dictionaryRepresentation] forKey:kWSAllCurrencyCountry];
    [mutableDict setValue:self.currencyAR forKey:kWSAllCurrencyCurrencyAR];
    [mutableDict setValue:[NSNumber numberWithDouble:self.exchangeRate] forKey:kWSAllCurrencyExchangeRate];
    [mutableDict setValue:[NSNumber numberWithInteger:self.internalBaseClassIdentifier] forKey:kWSAllCurrencyId];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.currency = [aDecoder decodeObjectForKey:kWSAllCurrencyCurrency];
    self.country = [aDecoder decodeObjectForKey:kWSAllCurrencyCountry];
    self.currencyAR = [aDecoder decodeObjectForKey:kWSAllCurrencyCurrencyAR];
    self.exchangeRate = [aDecoder decodeDoubleForKey:kWSAllCurrencyExchangeRate];
    self.internalBaseClassIdentifier = [aDecoder decodeIntegerForKey:kWSAllCurrencyId];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_currency forKey:kWSAllCurrencyCurrency];
    [aCoder encodeObject:_country forKey:kWSAllCurrencyCountry];
    [aCoder encodeObject:_currencyAR forKey:kWSAllCurrencyCurrencyAR];
    [aCoder encodeDouble:_exchangeRate forKey:kWSAllCurrencyExchangeRate];
    [aCoder encodeInteger:_internalBaseClassIdentifier forKey:kWSAllCurrencyId];
}

- (id)copyWithZone:(NSZone *)zone {
    WSAllCurrency *copy = [[WSAllCurrency alloc] init];
    
    
    
    if (copy) {

        copy.currency = [self.currency copyWithZone:zone];
        copy.country = [self.country copyWithZone:zone];
        copy.currencyAR = [self.currencyAR copyWithZone:zone];
        copy.exchangeRate = self.exchangeRate;
        copy.internalBaseClassIdentifier = self.internalBaseClassIdentifier;
    }
    
    return copy;
}


@end
