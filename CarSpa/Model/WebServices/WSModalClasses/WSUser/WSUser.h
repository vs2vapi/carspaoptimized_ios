//
//  WSUser.h
//
//  Created by Krishna Bhadola on 26/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface WSUser : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double mobileNumber;
@property (nonatomic, assign) NSInteger internalBaseClassIdentifier;
@property (nonatomic, assign) double longitude;
@property (nonatomic, assign) NSInteger countryCode;
@property (nonatomic, assign) double latitude;
@property (nonatomic, strong) NSString *socialId;
@property (nonatomic, assign) NSInteger socialType;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *thumbUrl;
@property (nonatomic, strong) NSString *fileUrl;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, strong) NSString *datetime;

//Added for storing more details of user.
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *carBrand;
@property (nonatomic, strong) NSString *carModel;
@property (nonatomic, assign) NSInteger carId;
@property (nonatomic, assign) NSInteger carTypeId;
@property (nonatomic, assign) NSInteger addressId;
@property (nonatomic, strong) NSString *addressTitle;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
