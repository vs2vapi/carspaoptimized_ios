//
//  WSNotifications.h
//
//  Created by Krishna Bhadola on 01/12/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface WSNotifications : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) NSInteger notyType;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, assign) NSInteger notificationId;
@property (nonatomic, assign) NSInteger notificationType;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, assign) NSInteger categoryId;
@property (nonatomic, assign) NSInteger outletType;
@property (nonatomic, assign) NSInteger outletId;
@property (nonatomic, strong) NSString *notificationDate;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
