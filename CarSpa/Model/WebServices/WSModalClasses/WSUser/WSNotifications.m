//
//  WSNotifications.m
//
//  Created by Krishna Bhadola on 01/12/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSNotifications.h"


NSString *const kWSNotificationsNotyType = @"notyType";
NSString *const kWSNotificationsCategoryName = @"categoryName";
NSString *const kWSNotificationsNotificationId = @"notificationId";
NSString *const kWSNotificationsTitle = @"title";
NSString *const kWSNotificationsMessage = @"message";
NSString *const kWSNotificationsCategoryId = @"categoryId";
NSString *const kWSNotificationsOutletType = @"outletType";
NSString *const kWSNotificationsOutletId = @"outletId";
NSString *const kWSNotificationsNotificationType = @"notyType";
NSString *const kWSNotificationsNotificationDate = @"date";

@interface WSNotifications ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSNotifications

@synthesize notyType = _notyType;
@synthesize categoryName = _categoryName;
@synthesize notificationId = _notificationId;
@synthesize title = _title;
@synthesize message = _message;
@synthesize categoryId = _categoryId;
@synthesize outletType = _outletType;
@synthesize outletId = _outletId;
@synthesize notificationType = _notificationType;
@synthesize notificationDate = _notificationDate;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.notyType = [[self objectOrNilForKey:kWSNotificationsNotyType fromDictionary:dict] integerValue];
            self.categoryName = [self objectOrNilForKey:kWSNotificationsCategoryName fromDictionary:dict];
            self.notificationId = [[self objectOrNilForKey:kWSNotificationsNotificationId fromDictionary:dict] integerValue];
            self.title = [self objectOrNilForKey:kWSNotificationsTitle fromDictionary:dict];
            self.message = [self objectOrNilForKey:kWSNotificationsMessage fromDictionary:dict];
            self.categoryId = [[self objectOrNilForKey:kWSNotificationsCategoryId fromDictionary:dict] integerValue];
            self.outletType = [[self objectOrNilForKey:kWSNotificationsOutletType fromDictionary:dict] integerValue];
            self.outletId = [[self objectOrNilForKey:kWSNotificationsOutletId fromDictionary:dict] integerValue];
            self.notificationType = [[self objectOrNilForKey:kWSNotificationsNotificationType fromDictionary:dict] integerValue];
            self.notificationDate = [self objectOrNilForKey:kWSNotificationsNotificationDate fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithInteger:self.notyType] forKey:kWSNotificationsNotyType];
    [mutableDict setValue:self.categoryName forKey:kWSNotificationsCategoryName];
    [mutableDict setValue:[NSNumber numberWithInteger:self.notificationId] forKey:kWSNotificationsNotificationId];
    [mutableDict setValue:self.title forKey:kWSNotificationsTitle];
    [mutableDict setValue:self.message forKey:kWSNotificationsMessage];
    [mutableDict setValue:[NSNumber numberWithInteger:self.categoryId] forKey:kWSNotificationsCategoryId];
    [mutableDict setValue:[NSNumber numberWithInteger:self.outletType] forKey:kWSNotificationsOutletType];
    [mutableDict setValue:[NSNumber numberWithInteger:self.outletId] forKey:kWSNotificationsOutletId];
    [mutableDict setValue:[NSNumber numberWithInteger:self.notificationType] forKey:kWSNotificationsNotificationType];
     [mutableDict setValue:self.notificationDate forKey:kWSNotificationsNotificationDate];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.notyType = [aDecoder decodeIntegerForKey:kWSNotificationsNotyType];
    self.categoryName = [aDecoder decodeObjectForKey:kWSNotificationsCategoryName];
    self.notificationId = [aDecoder decodeIntegerForKey:kWSNotificationsNotificationId];
    self.title = [aDecoder decodeObjectForKey:kWSNotificationsTitle];
    self.message = [aDecoder decodeObjectForKey:kWSNotificationsMessage];
    self.categoryId = [aDecoder decodeIntegerForKey:kWSNotificationsCategoryId];
    self.outletType = [aDecoder decodeIntegerForKey:kWSNotificationsOutletType];
    self.outletId = [aDecoder decodeIntegerForKey:kWSNotificationsOutletId];
     self.notificationType = [aDecoder decodeIntegerForKey:kWSNotificationsNotificationType];
     self.notificationDate = [aDecoder decodeObjectForKey:kWSNotificationsNotificationDate];
    
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeInteger:_notyType forKey:kWSNotificationsNotyType];
    [aCoder encodeObject:_categoryName forKey:kWSNotificationsCategoryName];
    [aCoder encodeInteger:_notificationId forKey:kWSNotificationsNotificationId];
    [aCoder encodeObject:_title forKey:kWSNotificationsTitle];
    [aCoder encodeObject:_message forKey:kWSNotificationsMessage];
    [aCoder encodeInteger:_categoryId forKey:kWSNotificationsCategoryId];
    [aCoder encodeInteger:_outletType forKey:kWSNotificationsOutletType];
    [aCoder encodeInteger:_outletId forKey:kWSNotificationsOutletId];
    [aCoder encodeInteger:_notificationType forKey:kWSNotificationsNotificationType];
      [aCoder encodeObject:_notificationDate forKey:kWSNotificationsNotificationDate];
    
}

- (id)copyWithZone:(NSZone *)zone {
    WSNotifications *copy = [[WSNotifications alloc] init];
    
    
    
    if (copy) {

        copy.notyType = self.notyType;
        copy.categoryName = [self.categoryName copyWithZone:zone];
        copy.notificationId = self.notificationId;
        copy.title = [self.title copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
        copy.categoryId = self.categoryId;
        copy.outletType = self.outletType;
        copy.outletId = self.outletId;
        copy.notificationType = self.notificationType;
        copy.notificationDate = [self.notificationDate copyWithZone:zone];
    }
    
    return copy;
}


@end
