//
//  WSAllCurrency.h
//
//  Created by Krishna Bhadola on 04/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WSCountry;

@interface WSAllCurrency : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) WSCountry *country;
@property (nonatomic, strong) NSString *currencyAR;
@property (nonatomic, assign) double exchangeRate;
@property (nonatomic, assign) NSInteger internalBaseClassIdentifier;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
