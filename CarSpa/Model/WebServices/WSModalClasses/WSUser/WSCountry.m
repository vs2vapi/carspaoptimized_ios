//
//  WSCountry.m
//
//  Created by Krishna Bhadola on 04/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSCountry.h"


NSString *const kWSCountryName = @"name";
NSString *const kWSCountryId = @"Id";
NSString *const kWSCountryCountryCode = @"countryCode";


@interface WSCountry ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSCountry

@synthesize name = _name;
@synthesize countryIdentifier = _countryIdentifier;
@synthesize countryCode = _countryCode;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.name = [self objectOrNilForKey:kWSCountryName fromDictionary:dict];
            self.countryIdentifier = [[self objectOrNilForKey:kWSCountryId fromDictionary:dict] doubleValue];
            self.countryCode = [[self objectOrNilForKey:kWSCountryCountryCode fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.name forKey:kWSCountryName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.countryIdentifier] forKey:kWSCountryId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.countryCode] forKey:kWSCountryCountryCode];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.name = [aDecoder decodeObjectForKey:kWSCountryName];
    self.countryIdentifier = [aDecoder decodeDoubleForKey:kWSCountryId];
    self.countryCode = [aDecoder decodeDoubleForKey:kWSCountryCountryCode];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_name forKey:kWSCountryName];
    [aCoder encodeDouble:_countryIdentifier forKey:kWSCountryId];
    [aCoder encodeDouble:_countryCode forKey:kWSCountryCountryCode];
}

- (id)copyWithZone:(NSZone *)zone {
    WSCountry *copy = [[WSCountry alloc] init];
    
    
    
    if (copy) {

        copy.name = [self.name copyWithZone:zone];
        copy.countryIdentifier = self.countryIdentifier;
        copy.countryCode = self.countryCode;
    }
    
    return copy;
}


@end
