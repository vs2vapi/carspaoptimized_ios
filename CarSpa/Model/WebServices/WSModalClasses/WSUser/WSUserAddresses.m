//
//  WSUserAddresses.m
//
//  Created by Krishna Bhadola on 01/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSUserAddresses.h"


NSString *const kWSUserAddressesAddress = @"address";
NSString *const kWSUserAddressesCity = @"city";
NSString *const kWSUserAddressesId = @"Id";
NSString *const kWSUserAddressesCountry = @"country";
NSString *const kWSUserAddressesLongitude = @"longitude";
NSString *const kWSUserAddressesLatitude = @"latitude";
NSString *const kWSUserAddressesAddresstitle = @"addresstitle";


@interface WSUserAddresses ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSUserAddresses

@synthesize address = _address;
@synthesize city = _city;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize country = _country;
@synthesize longitude = _longitude;
@synthesize latitude = _latitude;
@synthesize addresstitle = _addresstitle;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.address = [self objectOrNilForKey:kWSUserAddressesAddress fromDictionary:dict];
            self.city = [self objectOrNilForKey:kWSUserAddressesCity fromDictionary:dict];
            self.internalBaseClassIdentifier = [[self objectOrNilForKey:kWSUserAddressesId fromDictionary:dict] integerValue];
            self.country = [self objectOrNilForKey:kWSUserAddressesCountry fromDictionary:dict];
            self.longitude = [[self objectOrNilForKey:kWSUserAddressesLongitude fromDictionary:dict] doubleValue];
            self.latitude = [[self objectOrNilForKey:kWSUserAddressesLatitude fromDictionary:dict] doubleValue];
            self.addresstitle = [self objectOrNilForKey:kWSUserAddressesAddresstitle fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.address forKey:kWSUserAddressesAddress];
    [mutableDict setValue:self.city forKey:kWSUserAddressesCity];
    [mutableDict setValue:[NSNumber numberWithInteger:self.internalBaseClassIdentifier] forKey:kWSUserAddressesId];
    [mutableDict setValue:self.country forKey:kWSUserAddressesCountry];
    [mutableDict setValue:[NSNumber numberWithDouble:self.longitude] forKey:kWSUserAddressesLongitude];
    [mutableDict setValue:[NSNumber numberWithDouble:self.latitude] forKey:kWSUserAddressesLatitude];
    [mutableDict setValue:self.addresstitle forKey:kWSUserAddressesAddresstitle];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.address = [aDecoder decodeObjectForKey:kWSUserAddressesAddress];
    self.city = [aDecoder decodeObjectForKey:kWSUserAddressesCity];
    self.internalBaseClassIdentifier = [aDecoder decodeIntegerForKey:kWSUserAddressesId];
    self.country = [aDecoder decodeObjectForKey:kWSUserAddressesCountry];
    self.longitude = [aDecoder decodeDoubleForKey:kWSUserAddressesLongitude];
    self.latitude = [aDecoder decodeDoubleForKey:kWSUserAddressesLatitude];
    self.addresstitle = [aDecoder decodeObjectForKey:kWSUserAddressesAddresstitle];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_address forKey:kWSUserAddressesAddress];
    [aCoder encodeObject:_city forKey:kWSUserAddressesCity];
    [aCoder encodeInteger:_internalBaseClassIdentifier forKey:kWSUserAddressesId];
    [aCoder encodeObject:_country forKey:kWSUserAddressesCountry];
    [aCoder encodeDouble:_longitude forKey:kWSUserAddressesLongitude];
    [aCoder encodeDouble:_latitude forKey:kWSUserAddressesLatitude];
    [aCoder encodeObject:_addresstitle forKey:kWSUserAddressesAddresstitle];
}

- (id)copyWithZone:(NSZone *)zone {
    WSUserAddresses *copy = [[WSUserAddresses alloc] init];
    
    
    
    if (copy) {

        copy.address = [self.address copyWithZone:zone];
        copy.city = [self.city copyWithZone:zone];
        copy.internalBaseClassIdentifier = self.internalBaseClassIdentifier;
        copy.country = [self.country copyWithZone:zone];
        copy.longitude = self.longitude;
        copy.latitude = self.latitude;
        copy.addresstitle = [self.addresstitle copyWithZone:zone];
    }
    
    return copy;
}


@end
