//
//  WSRating.m
//
//  Created by Krishna Bhadola on 08/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSRating.h"


NSString *const kWSRatingQuestionAR = @"questionAR";
NSString *const kWSRatingQuestion = @"question";
NSString *const kWSRatingDatetime = @"datetime";
NSString *const kWSRatingId = @"Id";


@interface WSRating ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSRating

@synthesize questionAR = _questionAR;
@synthesize question = _question;
@synthesize datetime = _datetime;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.questionAR = [self objectOrNilForKey:kWSRatingQuestionAR fromDictionary:dict];
            self.question = [self objectOrNilForKey:kWSRatingQuestion fromDictionary:dict];
            self.datetime = [self objectOrNilForKey:kWSRatingDatetime fromDictionary:dict];
            self.internalBaseClassIdentifier = [[self objectOrNilForKey:kWSRatingId fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.questionAR forKey:kWSRatingQuestionAR];
    [mutableDict setValue:self.question forKey:kWSRatingQuestion];
    [mutableDict setValue:self.datetime forKey:kWSRatingDatetime];
    [mutableDict setValue:[NSNumber numberWithDouble:self.internalBaseClassIdentifier] forKey:kWSRatingId];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.questionAR = [aDecoder decodeObjectForKey:kWSRatingQuestionAR];
    self.question = [aDecoder decodeObjectForKey:kWSRatingQuestion];
    self.datetime = [aDecoder decodeObjectForKey:kWSRatingDatetime];
    self.internalBaseClassIdentifier = [aDecoder decodeDoubleForKey:kWSRatingId];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_questionAR forKey:kWSRatingQuestionAR];
    [aCoder encodeObject:_question forKey:kWSRatingQuestion];
    [aCoder encodeObject:_datetime forKey:kWSRatingDatetime];
    [aCoder encodeDouble:_internalBaseClassIdentifier forKey:kWSRatingId];
}

- (id)copyWithZone:(NSZone *)zone {
    WSRating *copy = [[WSRating alloc] init];
    
    
    
    if (copy) {

        copy.questionAR = [self.questionAR copyWithZone:zone];
        copy.question = [self.question copyWithZone:zone];
        copy.datetime = [self.datetime copyWithZone:zone];
        copy.internalBaseClassIdentifier = self.internalBaseClassIdentifier;
    }
    
    return copy;
}


@end
