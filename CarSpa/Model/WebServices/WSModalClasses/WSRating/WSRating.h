//
//  WSRating.h
//
//  Created by Krishna Bhadola on 08/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface WSRating : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *questionAR;
@property (nonatomic, strong) NSString *question;
@property (nonatomic, strong) NSString *datetime;
@property (nonatomic, assign) double internalBaseClassIdentifier;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
