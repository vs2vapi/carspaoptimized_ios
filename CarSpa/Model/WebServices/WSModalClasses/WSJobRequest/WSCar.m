//
//  WSCar.m
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSCar.h"


NSString *const kWSCarBrand = @"brand";
NSString *const kWSCarModel = @"model";
NSString *const kWSCarPlateNumber = @"plateNumber";
NSString *const kWSCarDatetime = @"datetime";
NSString *const kWSCarId = @"Id";


@interface WSCar ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSCar

@synthesize brand = _brand;
@synthesize model = _model;
@synthesize plateNumber = _plateNumber;
@synthesize datetime = _datetime;
@synthesize carIdentifier = _carIdentifier;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.brand = [self objectOrNilForKey:kWSCarBrand fromDictionary:dict];
            self.model = [self objectOrNilForKey:kWSCarModel fromDictionary:dict];
            self.plateNumber = [self objectOrNilForKey:kWSCarPlateNumber fromDictionary:dict];
            self.datetime = [self objectOrNilForKey:kWSCarDatetime fromDictionary:dict];
            self.carIdentifier = [[self objectOrNilForKey:kWSCarId fromDictionary:dict] integerValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.brand forKey:kWSCarBrand];
    [mutableDict setValue:self.model forKey:kWSCarModel];
    [mutableDict setValue:self.plateNumber forKey:kWSCarPlateNumber];
    [mutableDict setValue:self.datetime forKey:kWSCarDatetime];
    [mutableDict setValue:[NSNumber numberWithInteger:self.carIdentifier] forKey:kWSCarId];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.brand = [aDecoder decodeObjectForKey:kWSCarBrand];
    self.model = [aDecoder decodeObjectForKey:kWSCarModel];
    self.plateNumber = [aDecoder decodeObjectForKey:kWSCarPlateNumber];
    self.datetime = [aDecoder decodeObjectForKey:kWSCarDatetime];
    self.carIdentifier = [aDecoder decodeIntegerForKey:kWSCarId];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_brand forKey:kWSCarBrand];
    [aCoder encodeObject:_model forKey:kWSCarModel];
    [aCoder encodeObject:_plateNumber forKey:kWSCarPlateNumber];
    [aCoder encodeObject:_datetime forKey:kWSCarDatetime];
    [aCoder encodeInteger:_carIdentifier forKey:kWSCarId];
}

- (id)copyWithZone:(NSZone *)zone {
    WSCar *copy = [[WSCar alloc] init];
    
    
    
    if (copy) {

        copy.brand = [self.brand copyWithZone:zone];
        copy.model = [self.model copyWithZone:zone];
        copy.plateNumber = [self.plateNumber copyWithZone:zone];
        copy.datetime = [self.datetime copyWithZone:zone];
        copy.carIdentifier = self.carIdentifier;
    }
    
    return copy;
}


@end
