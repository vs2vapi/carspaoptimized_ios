//
//  WSVan.m
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSVan.h"
#import "WSOwner.h"
#import "WSCartype.h"
#import "WSOperator.h"


NSString *const kWSVanStatus = @"status";
NSString *const kWSVanOwner = @"owner";
NSString *const kWSVanLongitude = @"longitude";
NSString *const kWSVanCartype = @"cartype";
NSString *const kWSVanDatetime = @"datetime";
NSString *const kWSVanPlateno = @"plateno";
NSString *const kWSVanId = @"Id";
NSString *const kWSVanLatitude = @"latitude";
NSString *const kWSVanOperator = @"operator";
NSString *const kWSVanType = @"type";
NSString *const kWSVanName = @"name";


@interface WSVan ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSVan

@synthesize status = _status;
@synthesize owner = _owner;
@synthesize longitude = _longitude;
@synthesize cartype = _cartype;
@synthesize datetime = _datetime;
@synthesize plateno = _plateno;
@synthesize vanIdentifier = _vanIdentifier;
@synthesize latitude = _latitude;
@synthesize operator = _operator;
@synthesize type = _type;
@synthesize name = _name;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.status = [[self objectOrNilForKey:kWSVanStatus fromDictionary:dict] doubleValue];
            self.owner = [WSOwner modelObjectWithDictionary:[dict objectForKey:kWSVanOwner]];
            self.longitude = [[self objectOrNilForKey:kWSVanLongitude fromDictionary:dict] doubleValue];
            self.cartype = [WSCartype modelObjectWithDictionary:[dict objectForKey:kWSVanCartype]];
            self.datetime = [self objectOrNilForKey:kWSVanDatetime fromDictionary:dict];
            self.plateno = [self objectOrNilForKey:kWSVanPlateno fromDictionary:dict];
            self.vanIdentifier = [[self objectOrNilForKey:kWSVanId fromDictionary:dict] doubleValue];
            self.latitude = [[self objectOrNilForKey:kWSVanLatitude fromDictionary:dict] doubleValue];
    NSObject *receivedWSOperator = [dict objectForKey:kWSVanOperator];
    NSMutableArray *parsedWSOperator = [NSMutableArray array];
    
    if ([receivedWSOperator isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedWSOperator) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedWSOperator addObject:[WSOperator modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedWSOperator isKindOfClass:[NSDictionary class]]) {
       [parsedWSOperator addObject:[WSOperator modelObjectWithDictionary:(NSDictionary *)receivedWSOperator]];
    }

    self.operator = [NSArray arrayWithArray:parsedWSOperator];
            self.type = [[self objectOrNilForKey:kWSVanType fromDictionary:dict] doubleValue];
            self.name = [self objectOrNilForKey:kWSVanName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kWSVanStatus];
    [mutableDict setValue:[self.owner dictionaryRepresentation] forKey:kWSVanOwner];
    [mutableDict setValue:[NSNumber numberWithDouble:self.longitude] forKey:kWSVanLongitude];
    [mutableDict setValue:[self.cartype dictionaryRepresentation] forKey:kWSVanCartype];
    [mutableDict setValue:self.datetime forKey:kWSVanDatetime];
    [mutableDict setValue:self.plateno forKey:kWSVanPlateno];
    [mutableDict setValue:[NSNumber numberWithDouble:self.vanIdentifier] forKey:kWSVanId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.latitude] forKey:kWSVanLatitude];
    NSMutableArray *tempArrayForOperator = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.operator) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForOperator addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForOperator addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForOperator] forKey:kWSVanOperator];
    [mutableDict setValue:[NSNumber numberWithDouble:self.type] forKey:kWSVanType];
    [mutableDict setValue:self.name forKey:kWSVanName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.status = [aDecoder decodeDoubleForKey:kWSVanStatus];
    self.owner = [aDecoder decodeObjectForKey:kWSVanOwner];
    self.longitude = [aDecoder decodeDoubleForKey:kWSVanLongitude];
    self.cartype = [aDecoder decodeObjectForKey:kWSVanCartype];
    self.datetime = [aDecoder decodeObjectForKey:kWSVanDatetime];
    self.plateno = [aDecoder decodeObjectForKey:kWSVanPlateno];
    self.vanIdentifier = [aDecoder decodeDoubleForKey:kWSVanId];
    self.latitude = [aDecoder decodeDoubleForKey:kWSVanLatitude];
    self.operator = [aDecoder decodeObjectForKey:kWSVanOperator];
    self.type = [aDecoder decodeDoubleForKey:kWSVanType];
    self.name = [aDecoder decodeObjectForKey:kWSVanName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_status forKey:kWSVanStatus];
    [aCoder encodeObject:_owner forKey:kWSVanOwner];
    [aCoder encodeDouble:_longitude forKey:kWSVanLongitude];
    [aCoder encodeObject:_cartype forKey:kWSVanCartype];
    [aCoder encodeObject:_datetime forKey:kWSVanDatetime];
    [aCoder encodeObject:_plateno forKey:kWSVanPlateno];
    [aCoder encodeDouble:_vanIdentifier forKey:kWSVanId];
    [aCoder encodeDouble:_latitude forKey:kWSVanLatitude];
    [aCoder encodeObject:_operator forKey:kWSVanOperator];
    [aCoder encodeDouble:_type forKey:kWSVanType];
    [aCoder encodeObject:_name forKey:kWSVanName];
}

- (id)copyWithZone:(NSZone *)zone {
    WSVan *copy = [[WSVan alloc] init];
    
    
    
    if (copy) {

        copy.status = self.status;
        copy.owner = [self.owner copyWithZone:zone];
        copy.longitude = self.longitude;
        copy.cartype = [self.cartype copyWithZone:zone];
        copy.datetime = [self.datetime copyWithZone:zone];
        copy.plateno = [self.plateno copyWithZone:zone];
        copy.vanIdentifier = self.vanIdentifier;
        copy.latitude = self.latitude;
        copy.operator = [self.operator copyWithZone:zone];
        copy.type = self.type;
        copy.name = [self.name copyWithZone:zone];
    }
    
    return copy;
}


@end
