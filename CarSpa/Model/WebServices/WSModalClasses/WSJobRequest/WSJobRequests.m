//
//  WSJobRequests.m
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSJobRequests.h"
#import "WSVan.h"
#import "WSCar.h"
#import "WSCarType.h"
#import "WSCategory.h"
#import "WSUser.h"
#import "WSBill.h"
#import "WSServices.h"
#import "WSAppointmentStatus.h"
#import "WSAddress.h"
#import "WSRate.h"


NSString *const kWSJobRequestsDestinationLongitude = @"destinationLongitude";
NSString *const kWSJobRequestsStatusdatetime = @"statusdatetime";
NSString *const kWSJobRequestsSuffix = @"suffix";
NSString *const kWSJobRequestsVan = @"van";
NSString *const kWSJobRequestsStatus = @"status";
NSString *const kWSJobRequestsOperatorDatetime = @"operatorDatetime";
NSString *const kWSJobRequestsCar = @"car";
NSString *const kWSJobRequestsCarType = @"carType";
NSString *const kWSJobRequestsCategory = @"category";
NSString *const kWSJobRequestsDestinationAddress = @"destinationAddress";
NSString *const kWSJobRequestsAppointmentDatetime = @"appointmentDatetime";
NSString *const kWSJobRequestsUser = @"user";
NSString *const kWSJobRequestsBill = @"bill";
NSString *const kWSJobRequestsId = @"Id";
NSString *const kWSJobRequestsRequestId = @"requestId";
NSString *const kWSJobRequestsServices = @"services";
NSString *const kWSJobRequestsAppointmentStatus = @"appointmentStatus";
NSString *const kWSJobRequestsReason = @"reason";
NSString *const kWSJobRequestsStatusTime = @"statusTime";
NSString *const kWSJobRequestsRate = @"rate";
NSString *const kWSJobRequestsDestinationLatitude = @"destinationLatitude";
NSString *const kWSJobRequestsAddress = @"address";


@interface WSJobRequests ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSJobRequests

@synthesize destinationLongitude = _destinationLongitude;
@synthesize statusdatetime = _statusdatetime;
@synthesize suffix = _suffix;
@synthesize van = _van;
@synthesize status = _status;
@synthesize operatorDatetime = _operatorDatetime;
@synthesize car = _car;
@synthesize carType = _carType;
@synthesize category = _category;
@synthesize destinationAddress = _destinationAddress;
@synthesize appointmentDatetime = _appointmentDatetime;
@synthesize user = _user;
@synthesize bill = _bill;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize requestId = _requestId;
@synthesize services = _services;
@synthesize appointmentStatus = _appointmentStatus;
@synthesize reason = _reason;
@synthesize statusTime = _statusTime;
@synthesize rate = _rate;
@synthesize destinationLatitude = _destinationLatitude;
@synthesize address = _address;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.destinationLongitude = [[self objectOrNilForKey:kWSJobRequestsDestinationLongitude fromDictionary:dict] doubleValue];
            self.statusdatetime = [self objectOrNilForKey:kWSJobRequestsStatusdatetime fromDictionary:dict];
            self.suffix = [self objectOrNilForKey:kWSJobRequestsSuffix fromDictionary:dict];
            self.van = [WSVan modelObjectWithDictionary:[dict objectForKey:kWSJobRequestsVan]];
            self.status = [[self objectOrNilForKey:kWSJobRequestsStatus fromDictionary:dict] doubleValue];
            self.operatorDatetime = [self objectOrNilForKey:kWSJobRequestsOperatorDatetime fromDictionary:dict];
            self.car = [WSCar modelObjectWithDictionary:[dict objectForKey:kWSJobRequestsCar]];
            self.carType = [WSCartype modelObjectWithDictionary:[dict objectForKey:kWSJobRequestsCarType]];
            self.category = [WSCategory modelObjectWithDictionary:[dict objectForKey:kWSJobRequestsCategory]];
            self.destinationAddress = [self objectOrNilForKey:kWSJobRequestsDestinationAddress fromDictionary:dict];
            self.appointmentDatetime = [self objectOrNilForKey:kWSJobRequestsAppointmentDatetime fromDictionary:dict];
            self.user = [WSUser modelObjectWithDictionary:[dict objectForKey:kWSJobRequestsUser]];
            self.bill = [WSBill modelObjectWithDictionary:[dict objectForKey:kWSJobRequestsBill]];
            self.internalBaseClassIdentifier = [[self objectOrNilForKey:kWSJobRequestsId fromDictionary:dict] integerValue];
            self.requestId = [[self objectOrNilForKey:kWSJobRequestsRequestId fromDictionary:dict] doubleValue];
    NSObject *receivedWSServices = [dict objectForKey:kWSJobRequestsServices];
    NSMutableArray *parsedWSServices = [NSMutableArray array];
    
    if ([receivedWSServices isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedWSServices) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedWSServices addObject:[WSServices modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedWSServices isKindOfClass:[NSDictionary class]]) {
       [parsedWSServices addObject:[WSServices modelObjectWithDictionary:(NSDictionary *)receivedWSServices]];
    }

    self.services = [NSArray arrayWithArray:parsedWSServices];
    NSObject *receivedWSAppointmentStatus = [dict objectForKey:kWSJobRequestsAppointmentStatus];
    NSMutableArray *parsedWSAppointmentStatus = [NSMutableArray array];
    
    if ([receivedWSAppointmentStatus isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedWSAppointmentStatus) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedWSAppointmentStatus addObject:[WSAppointmentStatus modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedWSAppointmentStatus isKindOfClass:[NSDictionary class]]) {
       [parsedWSAppointmentStatus addObject:[WSAppointmentStatus modelObjectWithDictionary:(NSDictionary *)receivedWSAppointmentStatus]];
    }

    self.appointmentStatus = [NSArray arrayWithArray:parsedWSAppointmentStatus];
            self.reason = [self objectOrNilForKey:kWSJobRequestsReason fromDictionary:dict];
            self.statusTime = [self objectOrNilForKey:kWSJobRequestsStatusTime fromDictionary:dict];
          //  self.rate = [self objectOrNilForKey:kWSJobRequestsRate fromDictionary:dict];
            self.destinationLatitude = [[self objectOrNilForKey:kWSJobRequestsDestinationLatitude fromDictionary:dict] doubleValue];
            self.address = [WSAddress modelObjectWithDictionary:[dict objectForKey:kWSJobRequestsAddress]];

        
        NSObject *receivedWSRate = [dict objectForKey:kWSJobRequestsRate];
        NSMutableArray *parsedWSRate = [NSMutableArray array];
        
        if ([receivedWSRate isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedWSRate) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedWSRate addObject:[WSRate modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedWSRate isKindOfClass:[NSDictionary class]]) {
            [parsedWSRate addObject:[WSRate modelObjectWithDictionary:(NSDictionary *)receivedWSRate]];
        }
        
        self.rate = [NSArray arrayWithArray:parsedWSRate];
     
        

        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.destinationLongitude] forKey:kWSJobRequestsDestinationLongitude];
    [mutableDict setValue:self.statusdatetime forKey:kWSJobRequestsStatusdatetime];
    [mutableDict setValue:self.suffix forKey:kWSJobRequestsSuffix];
    [mutableDict setValue:[self.van dictionaryRepresentation] forKey:kWSJobRequestsVan];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kWSJobRequestsStatus];
    [mutableDict setValue:self.operatorDatetime forKey:kWSJobRequestsOperatorDatetime];
    [mutableDict setValue:[self.car dictionaryRepresentation] forKey:kWSJobRequestsCar];
    [mutableDict setValue:[self.carType dictionaryRepresentation] forKey:kWSJobRequestsCarType];
    [mutableDict setValue:[self.category dictionaryRepresentation] forKey:kWSJobRequestsCategory];
    [mutableDict setValue:self.destinationAddress forKey:kWSJobRequestsDestinationAddress];
    [mutableDict setValue:self.appointmentDatetime forKey:kWSJobRequestsAppointmentDatetime];
    [mutableDict setValue:[self.user dictionaryRepresentation] forKey:kWSJobRequestsUser];
    [mutableDict setValue:[self.bill dictionaryRepresentation] forKey:kWSJobRequestsBill];
    [mutableDict setValue:[NSNumber numberWithInteger:self.internalBaseClassIdentifier] forKey:kWSJobRequestsId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.requestId] forKey:kWSJobRequestsRequestId];
    NSMutableArray *tempArrayForServices = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.services) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForServices addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForServices addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForServices] forKey:kWSJobRequestsServices];
    NSMutableArray *tempArrayForAppointmentStatus = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.appointmentStatus) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForAppointmentStatus addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForAppointmentStatus addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForAppointmentStatus] forKey:kWSJobRequestsAppointmentStatus];
    [mutableDict setValue:self.reason forKey:kWSJobRequestsReason];
    [mutableDict setValue:self.statusTime forKey:kWSJobRequestsStatusTime];
 //   [mutableDict setValue:self.rate forKey:kWSJobRequestsRate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.destinationLatitude] forKey:kWSJobRequestsDestinationLatitude];
    [mutableDict setValue:[self.address dictionaryRepresentation] forKey:kWSJobRequestsAddress];

    NSMutableArray *tempArrayForRate = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.rate) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForRate addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForRate addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForRate] forKey:kWSJobRequestsRate];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.destinationLongitude = [aDecoder decodeDoubleForKey:kWSJobRequestsDestinationLongitude];
    self.statusdatetime = [aDecoder decodeObjectForKey:kWSJobRequestsStatusdatetime];
    self.suffix = [aDecoder decodeObjectForKey:kWSJobRequestsSuffix];
    self.van = [aDecoder decodeObjectForKey:kWSJobRequestsVan];
    self.status = [aDecoder decodeDoubleForKey:kWSJobRequestsStatus];
    self.operatorDatetime = [aDecoder decodeObjectForKey:kWSJobRequestsOperatorDatetime];
    self.car = [aDecoder decodeObjectForKey:kWSJobRequestsCar];
    self.carType = [aDecoder decodeObjectForKey:kWSJobRequestsCarType];
    self.category = [aDecoder decodeObjectForKey:kWSJobRequestsCategory];
    self.destinationAddress = [aDecoder decodeObjectForKey:kWSJobRequestsDestinationAddress];
    self.appointmentDatetime = [aDecoder decodeObjectForKey:kWSJobRequestsAppointmentDatetime];
    self.user = [aDecoder decodeObjectForKey:kWSJobRequestsUser];
    self.bill = [aDecoder decodeObjectForKey:kWSJobRequestsBill];
    self.internalBaseClassIdentifier = [aDecoder decodeIntegerForKey:kWSJobRequestsId];
    self.requestId = [aDecoder decodeDoubleForKey:kWSJobRequestsRequestId];
    self.services = [aDecoder decodeObjectForKey:kWSJobRequestsServices];
    self.appointmentStatus = [aDecoder decodeObjectForKey:kWSJobRequestsAppointmentStatus];
    self.reason = [aDecoder decodeObjectForKey:kWSJobRequestsReason];
    self.statusTime = [aDecoder decodeObjectForKey:kWSJobRequestsStatusTime];
    self.rate = [aDecoder decodeObjectForKey:kWSJobRequestsRate];
    self.destinationLatitude = [aDecoder decodeDoubleForKey:kWSJobRequestsDestinationLatitude];
    self.address = [aDecoder decodeObjectForKey:kWSJobRequestsAddress];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_destinationLongitude forKey:kWSJobRequestsDestinationLongitude];
    [aCoder encodeObject:_statusdatetime forKey:kWSJobRequestsStatusdatetime];
    [aCoder encodeObject:_suffix forKey:kWSJobRequestsSuffix];
    [aCoder encodeObject:_van forKey:kWSJobRequestsVan];
    [aCoder encodeDouble:_status forKey:kWSJobRequestsStatus];
    [aCoder encodeObject:_operatorDatetime forKey:kWSJobRequestsOperatorDatetime];
    [aCoder encodeObject:_car forKey:kWSJobRequestsCar];
    [aCoder encodeObject:_carType forKey:kWSJobRequestsCarType];
    [aCoder encodeObject:_category forKey:kWSJobRequestsCategory];
    [aCoder encodeObject:_destinationAddress forKey:kWSJobRequestsDestinationAddress];
    [aCoder encodeObject:_appointmentDatetime forKey:kWSJobRequestsAppointmentDatetime];
    [aCoder encodeObject:_user forKey:kWSJobRequestsUser];
    [aCoder encodeObject:_bill forKey:kWSJobRequestsBill];
    [aCoder encodeInteger:_internalBaseClassIdentifier forKey:kWSJobRequestsId];
    [aCoder encodeDouble:_requestId forKey:kWSJobRequestsRequestId];
    [aCoder encodeObject:_services forKey:kWSJobRequestsServices];
    [aCoder encodeObject:_appointmentStatus forKey:kWSJobRequestsAppointmentStatus];
    [aCoder encodeObject:_reason forKey:kWSJobRequestsReason];
    [aCoder encodeObject:_statusTime forKey:kWSJobRequestsStatusTime];
    [aCoder encodeObject:_rate forKey:kWSJobRequestsRate];
    [aCoder encodeDouble:_destinationLatitude forKey:kWSJobRequestsDestinationLatitude];
    [aCoder encodeObject:_address forKey:kWSJobRequestsAddress];
}

- (id)copyWithZone:(NSZone *)zone {
    WSJobRequests *copy = [[WSJobRequests alloc] init];
    
    
    
    if (copy) {

        copy.destinationLongitude = self.destinationLongitude;
        copy.statusdatetime = [self.statusdatetime copyWithZone:zone];
        copy.suffix = [self.suffix copyWithZone:zone];
        copy.van = [self.van copyWithZone:zone];
        copy.status = self.status;
        copy.operatorDatetime = [self.operatorDatetime copyWithZone:zone];
        copy.car = [self.car copyWithZone:zone];
        copy.carType = [self.carType copyWithZone:zone];
        copy.category = [self.category copyWithZone:zone];
        copy.destinationAddress = [self.destinationAddress copyWithZone:zone];
        copy.appointmentDatetime = [self.appointmentDatetime copyWithZone:zone];
        copy.user = [self.user copyWithZone:zone];
        copy.bill = [self.bill copyWithZone:zone];
        copy.internalBaseClassIdentifier = self.internalBaseClassIdentifier;
        copy.requestId = self.requestId;
        copy.services = [self.services copyWithZone:zone];
        copy.appointmentStatus = [self.appointmentStatus copyWithZone:zone];
        copy.reason = [self.reason copyWithZone:zone];
        copy.statusTime = [self.statusTime copyWithZone:zone];
        copy.rate = [self.rate copyWithZone:zone];
        copy.destinationLatitude = self.destinationLatitude;
        copy.address = [self.address copyWithZone:zone];
    }
    
    return copy;
}


@end
