//
//  WSAppointmentStatus.m
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSAppointmentStatus.h"
#import "WSActionBy.h"


NSString *const kWSAppointmentStatusStatus = @"status";
NSString *const kWSAppointmentStatusActionBy = @"ActionBy";
NSString *const kWSAppointmentStatusReason = @"reason";
NSString *const kWSAppointmentStatusDatetime = @"datetime";
NSString *const kWSAppointmentStatusId = @"Id";


@interface WSAppointmentStatus ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSAppointmentStatus

@synthesize status = _status;
@synthesize actionBy = _actionBy;
@synthesize reason = _reason;
@synthesize datetime = _datetime;
@synthesize appointmentStatusIdentifier = _appointmentStatusIdentifier;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.status = [[self objectOrNilForKey:kWSAppointmentStatusStatus fromDictionary:dict] doubleValue];
            self.actionBy = [WSActionBy modelObjectWithDictionary:[dict objectForKey:kWSAppointmentStatusActionBy]];
            self.reason = [self objectOrNilForKey:kWSAppointmentStatusReason fromDictionary:dict];
            self.datetime = [self objectOrNilForKey:kWSAppointmentStatusDatetime fromDictionary:dict];
            self.appointmentStatusIdentifier = [[self objectOrNilForKey:kWSAppointmentStatusId fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kWSAppointmentStatusStatus];
    [mutableDict setValue:[self.actionBy dictionaryRepresentation] forKey:kWSAppointmentStatusActionBy];
    [mutableDict setValue:self.reason forKey:kWSAppointmentStatusReason];
    [mutableDict setValue:self.datetime forKey:kWSAppointmentStatusDatetime];
    [mutableDict setValue:[NSNumber numberWithDouble:self.appointmentStatusIdentifier] forKey:kWSAppointmentStatusId];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.status = [aDecoder decodeDoubleForKey:kWSAppointmentStatusStatus];
    self.actionBy = [aDecoder decodeObjectForKey:kWSAppointmentStatusActionBy];
    self.reason = [aDecoder decodeObjectForKey:kWSAppointmentStatusReason];
    self.datetime = [aDecoder decodeObjectForKey:kWSAppointmentStatusDatetime];
    self.appointmentStatusIdentifier = [aDecoder decodeDoubleForKey:kWSAppointmentStatusId];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_status forKey:kWSAppointmentStatusStatus];
    [aCoder encodeObject:_actionBy forKey:kWSAppointmentStatusActionBy];
    [aCoder encodeObject:_reason forKey:kWSAppointmentStatusReason];
    [aCoder encodeObject:_datetime forKey:kWSAppointmentStatusDatetime];
    [aCoder encodeDouble:_appointmentStatusIdentifier forKey:kWSAppointmentStatusId];
}

- (id)copyWithZone:(NSZone *)zone {
    WSAppointmentStatus *copy = [[WSAppointmentStatus alloc] init];
    
    
    
    if (copy) {

        copy.status = self.status;
        copy.actionBy = [self.actionBy copyWithZone:zone];
        copy.reason = [self.reason copyWithZone:zone];
        copy.datetime = [self.datetime copyWithZone:zone];
        copy.appointmentStatusIdentifier = self.appointmentStatusIdentifier;
    }
    
    return copy;
}


@end
