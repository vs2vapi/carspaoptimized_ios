//
//  WSOwner.h
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface WSOwner : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *iconThump;
@property (nonatomic, assign) double mobileNumber;
@property (nonatomic, assign) double ownerIdentifier;
@property (nonatomic, assign) double countryCode;
@property (nonatomic, strong) NSString *contactPersons;
@property (nonatomic, assign) double maxdistance;
@property (nonatomic, strong) NSString *salesPerson;
@property (nonatomic, assign) double countryCodeTwo;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, assign) double mobileNumberTwo;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, assign) double isAppliedForTax;
@property (nonatomic, assign) double taxPercentage;
@property (nonatomic, strong) NSString *taxNumber;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
