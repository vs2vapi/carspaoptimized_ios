//
//  WSOwner.m
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSOwner.h"


NSString *const kWSOwnerIconThump = @"iconThump";
NSString *const kWSOwnerMobileNumber = @"mobileNumber";
NSString *const kWSOwnerId = @"Id";
NSString *const kWSOwnerCountryCode = @"countryCode";
NSString *const kWSOwnerContactPersons = @"contactPersons";
NSString *const kWSOwnerMaxdistance = @"maxdistance";
NSString *const kWSOwnerSalesPerson = @"salesPerson";
NSString *const kWSOwnerCountryCodeTwo = @"countryCodeTwo";
NSString *const kWSOwnerAddress = @"address";
NSString *const kWSOwnerCity = @"city";
NSString *const kWSOwnerIcon = @"icon";
NSString *const kWSOwnerMobileNumberTwo = @"mobileNumberTwo";
NSString *const kWSOwnerEmail = @"email";
NSString *const kWSOwnerName = @"name";
NSString *const kWSOwnerCountry = @"country";
NSString *const kWSOwnerIsAppliedForTax = @"isAppliedForTax";
NSString *const kWSOwnerTaxPercentage = @"taxPercentage";
NSString *const kWSOwnerTaxNumber = @"taxNumber";

@interface WSOwner ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSOwner

@synthesize iconThump = _iconThump;
@synthesize mobileNumber = _mobileNumber;
@synthesize ownerIdentifier = _ownerIdentifier;
@synthesize countryCode = _countryCode;
@synthesize contactPersons = _contactPersons;
@synthesize maxdistance = _maxdistance;
@synthesize salesPerson = _salesPerson;
@synthesize countryCodeTwo = _countryCodeTwo;
@synthesize address = _address;
@synthesize city = _city;
@synthesize icon = _icon;
@synthesize mobileNumberTwo = _mobileNumberTwo;
@synthesize email = _email;
@synthesize name = _name;
@synthesize country = _country;
@synthesize isAppliedForTax = _isAppliedForTax;
@synthesize taxPercentage = _taxPercentage;
@synthesize taxNumber = _taxNumber;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iconThump = [self objectOrNilForKey:kWSOwnerIconThump fromDictionary:dict];
            self.mobileNumber = [[self objectOrNilForKey:kWSOwnerMobileNumber fromDictionary:dict] doubleValue];
            self.ownerIdentifier = [[self objectOrNilForKey:kWSOwnerId fromDictionary:dict] doubleValue];
            self.countryCode = [[self objectOrNilForKey:kWSOwnerCountryCode fromDictionary:dict] doubleValue];
            self.contactPersons = [self objectOrNilForKey:kWSOwnerContactPersons fromDictionary:dict];
            self.maxdistance = [[self objectOrNilForKey:kWSOwnerMaxdistance fromDictionary:dict] doubleValue];
            self.salesPerson = [self objectOrNilForKey:kWSOwnerSalesPerson fromDictionary:dict];
            self.countryCodeTwo = [[self objectOrNilForKey:kWSOwnerCountryCodeTwo fromDictionary:dict] doubleValue];
            self.address = [self objectOrNilForKey:kWSOwnerAddress fromDictionary:dict];
            self.city = [self objectOrNilForKey:kWSOwnerCity fromDictionary:dict];
            self.icon = [self objectOrNilForKey:kWSOwnerIcon fromDictionary:dict];
            self.mobileNumberTwo = [[self objectOrNilForKey:kWSOwnerMobileNumberTwo fromDictionary:dict] doubleValue];
            self.email = [self objectOrNilForKey:kWSOwnerEmail fromDictionary:dict];
            self.name = [self objectOrNilForKey:kWSOwnerName fromDictionary:dict];
            self.country = [self objectOrNilForKey:kWSOwnerCountry fromDictionary:dict];
            self.isAppliedForTax = [[self objectOrNilForKey:kWSOwnerIsAppliedForTax fromDictionary:dict] doubleValue];
            self.taxPercentage = [[self objectOrNilForKey:kWSOwnerTaxPercentage fromDictionary:dict] doubleValue];
            self.taxNumber = [self objectOrNilForKey:kWSOwnerTaxNumber fromDictionary:dict];
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.iconThump forKey:kWSOwnerIconThump];
    [mutableDict setValue:[NSNumber numberWithDouble:self.mobileNumber] forKey:kWSOwnerMobileNumber];
    [mutableDict setValue:[NSNumber numberWithDouble:self.ownerIdentifier] forKey:kWSOwnerId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.countryCode] forKey:kWSOwnerCountryCode];
    [mutableDict setValue:self.contactPersons forKey:kWSOwnerContactPersons];
    [mutableDict setValue:[NSNumber numberWithDouble:self.maxdistance] forKey:kWSOwnerMaxdistance];
    [mutableDict setValue:self.salesPerson forKey:kWSOwnerSalesPerson];
    [mutableDict setValue:[NSNumber numberWithDouble:self.countryCodeTwo] forKey:kWSOwnerCountryCodeTwo];
    [mutableDict setValue:self.address forKey:kWSOwnerAddress];
    [mutableDict setValue:self.city forKey:kWSOwnerCity];
    [mutableDict setValue:self.icon forKey:kWSOwnerIcon];
    [mutableDict setValue:[NSNumber numberWithDouble:self.mobileNumberTwo] forKey:kWSOwnerMobileNumberTwo];
    [mutableDict setValue:self.email forKey:kWSOwnerEmail];
    [mutableDict setValue:self.name forKey:kWSOwnerName];
    [mutableDict setValue:self.country forKey:kWSOwnerCountry];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isAppliedForTax] forKey:kWSOwnerIsAppliedForTax];
    [mutableDict setValue:[NSNumber numberWithDouble:self.taxPercentage] forKey:kWSOwnerTaxPercentage];
    [mutableDict setValue:self.taxNumber forKey:kWSOwnerTaxNumber];
    

     
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.iconThump = [aDecoder decodeObjectForKey:kWSOwnerIconThump];
    self.mobileNumber = [aDecoder decodeDoubleForKey:kWSOwnerMobileNumber];
    self.ownerIdentifier = [aDecoder decodeDoubleForKey:kWSOwnerId];
    self.countryCode = [aDecoder decodeDoubleForKey:kWSOwnerCountryCode];
    self.contactPersons = [aDecoder decodeObjectForKey:kWSOwnerContactPersons];
    self.maxdistance = [aDecoder decodeDoubleForKey:kWSOwnerMaxdistance];
    self.salesPerson = [aDecoder decodeObjectForKey:kWSOwnerSalesPerson];
    self.countryCodeTwo = [aDecoder decodeDoubleForKey:kWSOwnerCountryCodeTwo];
    self.address = [aDecoder decodeObjectForKey:kWSOwnerAddress];
    self.city = [aDecoder decodeObjectForKey:kWSOwnerCity];
    self.icon = [aDecoder decodeObjectForKey:kWSOwnerIcon];
    self.mobileNumberTwo = [aDecoder decodeDoubleForKey:kWSOwnerMobileNumberTwo];
    self.email = [aDecoder decodeObjectForKey:kWSOwnerEmail];
    self.name = [aDecoder decodeObjectForKey:kWSOwnerName];
    self.country = [aDecoder decodeObjectForKey:kWSOwnerCountry];
    self.isAppliedForTax = [aDecoder decodeDoubleForKey:kWSOwnerIsAppliedForTax];
    self.taxPercentage = [aDecoder decodeDoubleForKey:kWSOwnerTaxPercentage];
    self.taxNumber = [aDecoder decodeObjectForKey:kWSOwnerTaxNumber];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_iconThump forKey:kWSOwnerIconThump];
    [aCoder encodeDouble:_mobileNumber forKey:kWSOwnerMobileNumber];
    [aCoder encodeDouble:_ownerIdentifier forKey:kWSOwnerId];
    [aCoder encodeDouble:_countryCode forKey:kWSOwnerCountryCode];
    [aCoder encodeObject:_contactPersons forKey:kWSOwnerContactPersons];
    [aCoder encodeDouble:_maxdistance forKey:kWSOwnerMaxdistance];
    [aCoder encodeObject:_salesPerson forKey:kWSOwnerSalesPerson];
    [aCoder encodeDouble:_countryCodeTwo forKey:kWSOwnerCountryCodeTwo];
    [aCoder encodeObject:_address forKey:kWSOwnerAddress];
    [aCoder encodeObject:_city forKey:kWSOwnerCity];
    [aCoder encodeObject:_icon forKey:kWSOwnerIcon];
    [aCoder encodeDouble:_mobileNumberTwo forKey:kWSOwnerMobileNumberTwo];
    [aCoder encodeObject:_email forKey:kWSOwnerEmail];
    [aCoder encodeObject:_name forKey:kWSOwnerName];
    [aCoder encodeObject:_country forKey:kWSOwnerCountry];
   
    [aCoder encodeDouble:_isAppliedForTax forKey:kWSOwnerIsAppliedForTax];
    [aCoder encodeDouble:_taxPercentage forKey:kWSOwnerTaxPercentage];
    [aCoder encodeObject:_taxNumber forKey:kWSOwnerTaxNumber];
    

}

- (id)copyWithZone:(NSZone *)zone {
    WSOwner *copy = [[WSOwner alloc] init];
    
    
    
    if (copy) {

        copy.iconThump = [self.iconThump copyWithZone:zone];
        copy.mobileNumber = self.mobileNumber;
        copy.ownerIdentifier = self.ownerIdentifier;
        copy.countryCode = self.countryCode;
        copy.contactPersons = [self.contactPersons copyWithZone:zone];
        copy.maxdistance = self.maxdistance;
        copy.salesPerson = [self.salesPerson copyWithZone:zone];
        copy.countryCodeTwo = self.countryCodeTwo;
        copy.address = [self.address copyWithZone:zone];
        copy.city = [self.city copyWithZone:zone];
        copy.icon = [self.icon copyWithZone:zone];
        copy.mobileNumberTwo = self.mobileNumberTwo;
        copy.email = [self.email copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.country = [self.country copyWithZone:zone];
        copy.taxNumber = [self.taxNumber copyWithZone:zone];
        copy.isAppliedForTax = self.isAppliedForTax;
        copy.taxPercentage = self.taxPercentage;
    }
    
    return copy;
}


@end
