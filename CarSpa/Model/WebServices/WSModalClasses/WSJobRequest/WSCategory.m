//
//  WSCategory.m
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSCategory.h"
#import "WSCategoryType.h"


NSString *const kWSCategoryNameAR = @"nameAR";
NSString *const kWSCategoryIconThump = @"iconThump";
NSString *const kWSCategoryId = @"Id";
NSString *const kWSCategoryDatetime = @"datetime";
NSString *const kWSCategoryImageUrlThump = @"imageUrlThump";
NSString *const kWSCategoryImageUrl = @"imageUrl";
NSString *const kWSCategoryName = @"name";
NSString *const kWSCategoryIcon = @"icon";
NSString *const kWSCategoriesCategoryType = @"categoryType";

@interface WSCategory ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSCategory

@synthesize nameAR = _nameAR;
@synthesize iconThump = _iconThump;
@synthesize categoryIdentifier = _categoryIdentifier;
@synthesize datetime = _datetime;
@synthesize imageUrlThump = _imageUrlThump;
@synthesize imageUrl = _imageUrl;
@synthesize name = _name;
@synthesize icon = _icon;
@synthesize categoryType = _categoryType;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.nameAR = [self objectOrNilForKey:kWSCategoryNameAR fromDictionary:dict];
            self.iconThump = [self objectOrNilForKey:kWSCategoryIconThump fromDictionary:dict];
            self.categoryIdentifier = [[self objectOrNilForKey:kWSCategoryId fromDictionary:dict] integerValue];
            self.datetime = [self objectOrNilForKey:kWSCategoryDatetime fromDictionary:dict];
            self.imageUrlThump = [self objectOrNilForKey:kWSCategoryImageUrlThump fromDictionary:dict];
            self.imageUrl = [self objectOrNilForKey:kWSCategoryImageUrl fromDictionary:dict];
            self.name = [self objectOrNilForKey:kWSCategoryName fromDictionary:dict];
            self.icon = [self objectOrNilForKey:kWSCategoryIcon fromDictionary:dict];
          self.categoryType = [WSCategoryType modelObjectWithDictionary:[dict objectForKey:kWSCategoriesCategoryType]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.nameAR forKey:kWSCategoryNameAR];
    [mutableDict setValue:self.iconThump forKey:kWSCategoryIconThump];
    [mutableDict setValue:[NSNumber numberWithInteger:self.categoryIdentifier] forKey:kWSCategoryId];
    [mutableDict setValue:self.datetime forKey:kWSCategoryDatetime];
    [mutableDict setValue:self.imageUrlThump forKey:kWSCategoryImageUrlThump];
    [mutableDict setValue:self.imageUrl forKey:kWSCategoryImageUrl];
    [mutableDict setValue:self.name forKey:kWSCategoryName];
    [mutableDict setValue:self.icon forKey:kWSCategoryIcon];

    [mutableDict setValue:[self.categoryType dictionaryRepresentation] forKey:kWSCategoriesCategoryType];
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.nameAR = [aDecoder decodeObjectForKey:kWSCategoryNameAR];
    self.iconThump = [aDecoder decodeObjectForKey:kWSCategoryIconThump];
    self.categoryIdentifier = [aDecoder decodeIntegerForKey:kWSCategoryId];
    self.datetime = [aDecoder decodeObjectForKey:kWSCategoryDatetime];
    self.imageUrlThump = [aDecoder decodeObjectForKey:kWSCategoryImageUrlThump];
    self.imageUrl = [aDecoder decodeObjectForKey:kWSCategoryImageUrl];
    self.name = [aDecoder decodeObjectForKey:kWSCategoryName];
    self.icon = [aDecoder decodeObjectForKey:kWSCategoryIcon];
    self.categoryType = [aDecoder decodeObjectForKey:kWSCategoriesCategoryType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_nameAR forKey:kWSCategoryNameAR];
    [aCoder encodeObject:_iconThump forKey:kWSCategoryIconThump];
    [aCoder encodeInteger:_categoryIdentifier forKey:kWSCategoryId];
    [aCoder encodeObject:_datetime forKey:kWSCategoryDatetime];
    [aCoder encodeObject:_imageUrlThump forKey:kWSCategoryImageUrlThump];
    [aCoder encodeObject:_imageUrl forKey:kWSCategoryImageUrl];
    [aCoder encodeObject:_name forKey:kWSCategoryName];
    [aCoder encodeObject:_icon forKey:kWSCategoryIcon];
     [aCoder encodeObject:_categoryType forKey:kWSCategoriesCategoryType];
}

- (id)copyWithZone:(NSZone *)zone {
    WSCategory *copy = [[WSCategory alloc] init];
    
    
    
    if (copy) {

        copy.nameAR = [self.nameAR copyWithZone:zone];
        copy.iconThump = [self.iconThump copyWithZone:zone];
        copy.categoryIdentifier = self.categoryIdentifier;
        copy.datetime = [self.datetime copyWithZone:zone];
        copy.imageUrlThump = [self.imageUrlThump copyWithZone:zone];
        copy.imageUrl = [self.imageUrl copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.icon = [self.icon copyWithZone:zone];
         copy.categoryType = [self.categoryType copyWithZone:zone];
    }
    
    return copy;
}


@end
