//
//  WSRate.h
//
//  Created by Krishna Bhadola on 08/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface WSRate : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double userId;
@property (nonatomic, assign) double rateId;
@property (nonatomic, assign) double rate;
@property (nonatomic, strong) NSString *datetime;
@property (nonatomic, assign) double rateIdentifier;
@property (nonatomic, strong) NSString *question;
@property (nonatomic, strong) NSString *comment;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
