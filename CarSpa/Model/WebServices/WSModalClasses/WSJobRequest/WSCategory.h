//
//  WSCategory.h
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WSCategoryType;

@interface WSCategory : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *nameAR;
@property (nonatomic, strong) NSString *iconThump;
@property (nonatomic, assign) NSInteger categoryIdentifier;
@property (nonatomic, strong) NSString *datetime;
@property (nonatomic, strong) NSString *imageUrlThump;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) WSCategoryType *categoryType;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
