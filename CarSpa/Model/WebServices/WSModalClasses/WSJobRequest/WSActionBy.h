//
//  WSActionBy.h
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface WSActionBy : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double mobileNumber;
@property (nonatomic, assign) double actionByIdentifier;
@property (nonatomic, assign) double longitude;
@property (nonatomic, assign) double countryCode;
@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double socialId;
@property (nonatomic, assign) double socialType;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *thumbUrl;
@property (nonatomic, strong) NSString *fileUrl;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, assign) double status;
@property (nonatomic, strong) NSString *datetime;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
