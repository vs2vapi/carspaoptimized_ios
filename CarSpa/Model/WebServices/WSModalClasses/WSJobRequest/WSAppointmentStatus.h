//
//  WSAppointmentStatus.h
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WSActionBy;

@interface WSAppointmentStatus : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double status;
@property (nonatomic, strong) WSActionBy *actionBy;
@property (nonatomic, strong) NSString *reason;
@property (nonatomic, strong) NSString *datetime;
@property (nonatomic, assign) double appointmentStatusIdentifier;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
