//
//  WSVan.h
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WSOwner, WSCartype;

@interface WSVan : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double status;
@property (nonatomic, strong) WSOwner *owner;
@property (nonatomic, assign) double longitude;
@property (nonatomic, strong) WSCartype *cartype;
@property (nonatomic, strong) NSString *datetime;
@property (nonatomic, strong) NSString *plateno;
@property (nonatomic, assign) double vanIdentifier;
@property (nonatomic, assign) double latitude;
@property (nonatomic, strong) NSArray *operator;
@property (nonatomic, assign) double type;
@property (nonatomic, strong) NSString *name;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
