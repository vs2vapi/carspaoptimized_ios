//
//  WSSupplierServices.h
//
//  Created by Krishna Bhadola on 22/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface WSSupplierServices : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double isAppliedForTax;
@property (nonatomic, assign) NSInteger ownerId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *nameAR;
@property (nonatomic, assign) double taxPercentage;
@property (nonatomic, strong) NSArray *services;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
