//
//  WSSupplierServices.m
//
//  Created by Krishna Bhadola on 22/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "WSSupplierServices.h"
#import "WSServices.h"


NSString *const kWSSupplierServicesIsAppliedForTax = @"isAppliedForTax";
NSString *const kWSSupplierServicesOwnerId = @"ownerId";
NSString *const kWSSupplierServicesName = @"name";
NSString *const kWSSupplierServicesNameAR = @"nameAr";
NSString *const kWSSupplierServicesTaxPercentage = @"taxPercentage";
NSString *const kWSSupplierServicesServices = @"Services";


@interface WSSupplierServices ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSSupplierServices

@synthesize isAppliedForTax = _isAppliedForTax;
@synthesize ownerId = _ownerId;
@synthesize name = _name;
@synthesize taxPercentage = _taxPercentage;
@synthesize services = _services;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.isAppliedForTax = [[self objectOrNilForKey:kWSSupplierServicesIsAppliedForTax fromDictionary:dict] doubleValue];
            self.ownerId = [[self objectOrNilForKey:kWSSupplierServicesOwnerId fromDictionary:dict] integerValue];
            self.name = [self objectOrNilForKey:kWSSupplierServicesName fromDictionary:dict];
            self.taxPercentage = [[self objectOrNilForKey:kWSSupplierServicesTaxPercentage fromDictionary:dict] doubleValue];
            self.nameAR = [self objectOrNilForKey:kWSSupplierServicesNameAR fromDictionary:dict];
    NSObject *receivedWSServices = [dict objectForKey:kWSSupplierServicesServices];
    NSMutableArray *parsedWSServices = [NSMutableArray array];
    
    if ([receivedWSServices isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedWSServices) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedWSServices addObject:[WSServices modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedWSServices isKindOfClass:[NSDictionary class]]) {
       [parsedWSServices addObject:[WSServices modelObjectWithDictionary:(NSDictionary *)receivedWSServices]];
    }

    self.services = [NSArray arrayWithArray:parsedWSServices];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isAppliedForTax] forKey:kWSSupplierServicesIsAppliedForTax];
    [mutableDict setValue:[NSNumber numberWithInteger:self.ownerId] forKey:kWSSupplierServicesOwnerId];
    [mutableDict setValue:self.name forKey:kWSSupplierServicesName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.taxPercentage] forKey:kWSSupplierServicesTaxPercentage];
    [mutableDict setValue:self.nameAR forKey:kWSSupplierServicesNameAR];
    
    NSMutableArray *tempArrayForServices = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.services) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForServices addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForServices addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForServices] forKey:kWSSupplierServicesServices];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.isAppliedForTax = [aDecoder decodeDoubleForKey:kWSSupplierServicesIsAppliedForTax];
    self.ownerId = [aDecoder decodeIntegerForKey:kWSSupplierServicesOwnerId];
    self.name = [aDecoder decodeObjectForKey:kWSSupplierServicesName];
    self.taxPercentage = [aDecoder decodeDoubleForKey:kWSSupplierServicesTaxPercentage];
    self.services = [aDecoder decodeObjectForKey:kWSSupplierServicesServices];
    self.nameAR = [aDecoder decodeObjectForKey:kWSSupplierServicesNameAR];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_isAppliedForTax forKey:kWSSupplierServicesIsAppliedForTax];
    [aCoder encodeInteger:_ownerId forKey:kWSSupplierServicesOwnerId];
    [aCoder encodeObject:_name forKey:kWSSupplierServicesName];
    [aCoder encodeDouble:_taxPercentage forKey:kWSSupplierServicesTaxPercentage];
    [aCoder encodeObject:_services forKey:kWSSupplierServicesServices];
    [aCoder encodeObject:_nameAR forKey:kWSSupplierServicesNameAR];
    
}

- (id)copyWithZone:(NSZone *)zone {
    WSSupplierServices *copy = [[WSSupplierServices alloc] init];
    
    
    
    if (copy) {

        copy.isAppliedForTax = self.isAppliedForTax;
        copy.ownerId = self.ownerId;
        copy.name = [self.name copyWithZone:zone];
        copy.taxPercentage = self.taxPercentage;
        copy.services = [self.services copyWithZone:zone];
        copy.nameAR = [self.nameAR copyWithZone:zone];
    }
    
    return copy;
}


@end
