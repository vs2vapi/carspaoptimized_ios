//
//  WSRate.m
//
//  Created by Krishna Bhadola on 08/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSRate.h"


NSString *const kWSRateUserId = @"userId";
NSString *const kWSRateRateId = @"rateId";
NSString *const kWSRateRate = @"rate";
NSString *const kWSRateDatetime = @"datetime";
NSString *const kWSRateId = @"Id";
NSString *const kWSRateQuestion = @"question";
NSString *const kWSRateComment = @"comment";


@interface WSRate ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSRate

@synthesize userId = _userId;
@synthesize rateId = _rateId;
@synthesize rate = _rate;
@synthesize datetime = _datetime;
@synthesize rateIdentifier = _rateIdentifier;
@synthesize question = _question;
@synthesize comment = _comment;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.userId = [[self objectOrNilForKey:kWSRateUserId fromDictionary:dict] doubleValue];
            self.rateId = [[self objectOrNilForKey:kWSRateRateId fromDictionary:dict] doubleValue];
            self.rate = [[self objectOrNilForKey:kWSRateRate fromDictionary:dict] doubleValue];
            self.datetime = [self objectOrNilForKey:kWSRateDatetime fromDictionary:dict];
            self.rateIdentifier = [[self objectOrNilForKey:kWSRateId fromDictionary:dict] doubleValue];
            self.question = [self objectOrNilForKey:kWSRateQuestion fromDictionary:dict];
            self.comment = [self objectOrNilForKey:kWSRateComment fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userId] forKey:kWSRateUserId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rateId] forKey:kWSRateRateId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rate] forKey:kWSRateRate];
    [mutableDict setValue:self.datetime forKey:kWSRateDatetime];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rateIdentifier] forKey:kWSRateId];
    [mutableDict setValue:self.question forKey:kWSRateQuestion];
    [mutableDict setValue:self.comment forKey:kWSRateComment];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.userId = [aDecoder decodeDoubleForKey:kWSRateUserId];
    self.rateId = [aDecoder decodeDoubleForKey:kWSRateRateId];
    self.rate = [aDecoder decodeDoubleForKey:kWSRateRate];
    self.datetime = [aDecoder decodeObjectForKey:kWSRateDatetime];
    self.rateIdentifier = [aDecoder decodeDoubleForKey:kWSRateId];
    self.question = [aDecoder decodeObjectForKey:kWSRateQuestion];
    self.comment = [aDecoder decodeObjectForKey:kWSRateComment];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_userId forKey:kWSRateUserId];
    [aCoder encodeDouble:_rateId forKey:kWSRateRateId];
    [aCoder encodeDouble:_rate forKey:kWSRateRate];
    [aCoder encodeObject:_datetime forKey:kWSRateDatetime];
    [aCoder encodeDouble:_rateIdentifier forKey:kWSRateId];
    [aCoder encodeObject:_question forKey:kWSRateQuestion];
    [aCoder encodeObject:_comment forKey:kWSRateComment];
}

- (id)copyWithZone:(NSZone *)zone {
    WSRate *copy = [[WSRate alloc] init];
    
    
    
    if (copy) {

        copy.userId = self.userId;
        copy.rateId = self.rateId;
        copy.rate = self.rate;
        copy.datetime = [self.datetime copyWithZone:zone];
        copy.rateIdentifier = self.rateIdentifier;
        copy.question = [self.question copyWithZone:zone];
        copy.comment = [self.comment copyWithZone:zone];
    }
    
    return copy;
}


@end
