//
//  WSActionBy.m
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSActionBy.h"


NSString *const kWSActionByMobileNumber = @"mobileNumber";
NSString *const kWSActionById = @"Id";
NSString *const kWSActionByLongitude = @"longitude";
NSString *const kWSActionByCountryCode = @"countryCode";
NSString *const kWSActionByLatitude = @"latitude";
NSString *const kWSActionBySocialId = @"socialId";
NSString *const kWSActionBySocialType = @"socialType";
NSString *const kWSActionByFullName = @"fullName";
NSString *const kWSActionByThumbUrl = @"thumbUrl";
NSString *const kWSActionByFileUrl = @"fileUrl";
NSString *const kWSActionByEmail = @"email";
NSString *const kWSActionByStatus = @"status";
NSString *const kWSActionByDatetime = @"datetime";


@interface WSActionBy ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSActionBy

@synthesize mobileNumber = _mobileNumber;
@synthesize actionByIdentifier = _actionByIdentifier;
@synthesize longitude = _longitude;
@synthesize countryCode = _countryCode;
@synthesize latitude = _latitude;
@synthesize socialId = _socialId;
@synthesize socialType = _socialType;
@synthesize fullName = _fullName;
@synthesize thumbUrl = _thumbUrl;
@synthesize fileUrl = _fileUrl;
@synthesize email = _email;
@synthesize status = _status;
@synthesize datetime = _datetime;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.mobileNumber = [[self objectOrNilForKey:kWSActionByMobileNumber fromDictionary:dict] doubleValue];
            self.actionByIdentifier = [[self objectOrNilForKey:kWSActionById fromDictionary:dict] doubleValue];
            self.longitude = [[self objectOrNilForKey:kWSActionByLongitude fromDictionary:dict] doubleValue];
            self.countryCode = [[self objectOrNilForKey:kWSActionByCountryCode fromDictionary:dict] doubleValue];
            self.latitude = [[self objectOrNilForKey:kWSActionByLatitude fromDictionary:dict] doubleValue];
            self.socialId = [[self objectOrNilForKey:kWSActionBySocialId fromDictionary:dict] doubleValue];
            self.socialType = [[self objectOrNilForKey:kWSActionBySocialType fromDictionary:dict] doubleValue];
            self.fullName = [self objectOrNilForKey:kWSActionByFullName fromDictionary:dict];
            self.thumbUrl = [self objectOrNilForKey:kWSActionByThumbUrl fromDictionary:dict];
            self.fileUrl = [self objectOrNilForKey:kWSActionByFileUrl fromDictionary:dict];
            self.email = [self objectOrNilForKey:kWSActionByEmail fromDictionary:dict];
            self.status = [[self objectOrNilForKey:kWSActionByStatus fromDictionary:dict] doubleValue];
            self.datetime = [self objectOrNilForKey:kWSActionByDatetime fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.mobileNumber] forKey:kWSActionByMobileNumber];
    [mutableDict setValue:[NSNumber numberWithDouble:self.actionByIdentifier] forKey:kWSActionById];
    [mutableDict setValue:[NSNumber numberWithDouble:self.longitude] forKey:kWSActionByLongitude];
    [mutableDict setValue:[NSNumber numberWithDouble:self.countryCode] forKey:kWSActionByCountryCode];
    [mutableDict setValue:[NSNumber numberWithDouble:self.latitude] forKey:kWSActionByLatitude];
    [mutableDict setValue:[NSNumber numberWithDouble:self.socialId] forKey:kWSActionBySocialId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.socialType] forKey:kWSActionBySocialType];
    [mutableDict setValue:self.fullName forKey:kWSActionByFullName];
    [mutableDict setValue:self.thumbUrl forKey:kWSActionByThumbUrl];
    [mutableDict setValue:self.fileUrl forKey:kWSActionByFileUrl];
    [mutableDict setValue:self.email forKey:kWSActionByEmail];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kWSActionByStatus];
    [mutableDict setValue:self.datetime forKey:kWSActionByDatetime];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.mobileNumber = [aDecoder decodeDoubleForKey:kWSActionByMobileNumber];
    self.actionByIdentifier = [aDecoder decodeDoubleForKey:kWSActionById];
    self.longitude = [aDecoder decodeDoubleForKey:kWSActionByLongitude];
    self.countryCode = [aDecoder decodeDoubleForKey:kWSActionByCountryCode];
    self.latitude = [aDecoder decodeDoubleForKey:kWSActionByLatitude];
    self.socialId = [aDecoder decodeDoubleForKey:kWSActionBySocialId];
    self.socialType = [aDecoder decodeDoubleForKey:kWSActionBySocialType];
    self.fullName = [aDecoder decodeObjectForKey:kWSActionByFullName];
    self.thumbUrl = [aDecoder decodeObjectForKey:kWSActionByThumbUrl];
    self.fileUrl = [aDecoder decodeObjectForKey:kWSActionByFileUrl];
    self.email = [aDecoder decodeObjectForKey:kWSActionByEmail];
    self.status = [aDecoder decodeDoubleForKey:kWSActionByStatus];
    self.datetime = [aDecoder decodeObjectForKey:kWSActionByDatetime];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_mobileNumber forKey:kWSActionByMobileNumber];
    [aCoder encodeDouble:_actionByIdentifier forKey:kWSActionById];
    [aCoder encodeDouble:_longitude forKey:kWSActionByLongitude];
    [aCoder encodeDouble:_countryCode forKey:kWSActionByCountryCode];
    [aCoder encodeDouble:_latitude forKey:kWSActionByLatitude];
    [aCoder encodeDouble:_socialId forKey:kWSActionBySocialId];
    [aCoder encodeDouble:_socialType forKey:kWSActionBySocialType];
    [aCoder encodeObject:_fullName forKey:kWSActionByFullName];
    [aCoder encodeObject:_thumbUrl forKey:kWSActionByThumbUrl];
    [aCoder encodeObject:_fileUrl forKey:kWSActionByFileUrl];
    [aCoder encodeObject:_email forKey:kWSActionByEmail];
    [aCoder encodeDouble:_status forKey:kWSActionByStatus];
    [aCoder encodeObject:_datetime forKey:kWSActionByDatetime];
}

- (id)copyWithZone:(NSZone *)zone {
    WSActionBy *copy = [[WSActionBy alloc] init];
    
    
    
    if (copy) {

        copy.mobileNumber = self.mobileNumber;
        copy.actionByIdentifier = self.actionByIdentifier;
        copy.longitude = self.longitude;
        copy.countryCode = self.countryCode;
        copy.latitude = self.latitude;
        copy.socialId = self.socialId;
        copy.socialType = self.socialType;
        copy.fullName = [self.fullName copyWithZone:zone];
        copy.thumbUrl = [self.thumbUrl copyWithZone:zone];
        copy.fileUrl = [self.fileUrl copyWithZone:zone];
        copy.email = [self.email copyWithZone:zone];
        copy.status = self.status;
        copy.datetime = [self.datetime copyWithZone:zone];
    }
    
    return copy;
}


@end
