//
//  WSOperator.m
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSOperator.h"


NSString *const kWSOperatorMobileNumber = @"mobileNumber";
NSString *const kWSOperatorId = @"Id";
NSString *const kWSOperatorLongitude = @"longitude";
NSString *const kWSOperatorCountryCode = @"countryCode";
NSString *const kWSOperatorLatitude = @"latitude";
NSString *const kWSOperatorAddress = @"address";
NSString *const kWSOperatorFullName = @"fullName";
NSString *const kWSOperatorThumbUrl = @"thumbUrl";
NSString *const kWSOperatorFileUrl = @"fileUrl";
NSString *const kWSOperatorEmail = @"email";
NSString *const kWSOperatorStatus = @"status";
NSString *const kWSOperatorDatetime = @"datetime";


@interface WSOperator ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSOperator

@synthesize mobileNumber = _mobileNumber;
@synthesize operatorIdentifier = _operatorIdentifier;
@synthesize longitude = _longitude;
@synthesize countryCode = _countryCode;
@synthesize latitude = _latitude;
@synthesize address = _address;
@synthesize fullName = _fullName;
@synthesize thumbUrl = _thumbUrl;
@synthesize fileUrl = _fileUrl;
@synthesize email = _email;
@synthesize status = _status;
@synthesize datetime = _datetime;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.mobileNumber = [[self objectOrNilForKey:kWSOperatorMobileNumber fromDictionary:dict] doubleValue];
            self.operatorIdentifier = [[self objectOrNilForKey:kWSOperatorId fromDictionary:dict] doubleValue];
            self.longitude = [[self objectOrNilForKey:kWSOperatorLongitude fromDictionary:dict] doubleValue];
            self.countryCode = [[self objectOrNilForKey:kWSOperatorCountryCode fromDictionary:dict] doubleValue];
            self.latitude = [[self objectOrNilForKey:kWSOperatorLatitude fromDictionary:dict] doubleValue];
            self.address = [self objectOrNilForKey:kWSOperatorAddress fromDictionary:dict];
            self.fullName = [self objectOrNilForKey:kWSOperatorFullName fromDictionary:dict];
            self.thumbUrl = [self objectOrNilForKey:kWSOperatorThumbUrl fromDictionary:dict];
            self.fileUrl = [self objectOrNilForKey:kWSOperatorFileUrl fromDictionary:dict];
            self.email = [self objectOrNilForKey:kWSOperatorEmail fromDictionary:dict];
            self.status = [[self objectOrNilForKey:kWSOperatorStatus fromDictionary:dict] doubleValue];
            self.datetime = [self objectOrNilForKey:kWSOperatorDatetime fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.mobileNumber] forKey:kWSOperatorMobileNumber];
    [mutableDict setValue:[NSNumber numberWithDouble:self.operatorIdentifier] forKey:kWSOperatorId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.longitude] forKey:kWSOperatorLongitude];
    [mutableDict setValue:[NSNumber numberWithDouble:self.countryCode] forKey:kWSOperatorCountryCode];
    [mutableDict setValue:[NSNumber numberWithDouble:self.latitude] forKey:kWSOperatorLatitude];
    [mutableDict setValue:self.address forKey:kWSOperatorAddress];
    [mutableDict setValue:self.fullName forKey:kWSOperatorFullName];
    [mutableDict setValue:self.thumbUrl forKey:kWSOperatorThumbUrl];
    [mutableDict setValue:self.fileUrl forKey:kWSOperatorFileUrl];
    [mutableDict setValue:self.email forKey:kWSOperatorEmail];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kWSOperatorStatus];
    [mutableDict setValue:self.datetime forKey:kWSOperatorDatetime];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.mobileNumber = [aDecoder decodeDoubleForKey:kWSOperatorMobileNumber];
    self.operatorIdentifier = [aDecoder decodeDoubleForKey:kWSOperatorId];
    self.longitude = [aDecoder decodeDoubleForKey:kWSOperatorLongitude];
    self.countryCode = [aDecoder decodeDoubleForKey:kWSOperatorCountryCode];
    self.latitude = [aDecoder decodeDoubleForKey:kWSOperatorLatitude];
    self.address = [aDecoder decodeObjectForKey:kWSOperatorAddress];
    self.fullName = [aDecoder decodeObjectForKey:kWSOperatorFullName];
    self.thumbUrl = [aDecoder decodeObjectForKey:kWSOperatorThumbUrl];
    self.fileUrl = [aDecoder decodeObjectForKey:kWSOperatorFileUrl];
    self.email = [aDecoder decodeObjectForKey:kWSOperatorEmail];
    self.status = [aDecoder decodeDoubleForKey:kWSOperatorStatus];
    self.datetime = [aDecoder decodeObjectForKey:kWSOperatorDatetime];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_mobileNumber forKey:kWSOperatorMobileNumber];
    [aCoder encodeDouble:_operatorIdentifier forKey:kWSOperatorId];
    [aCoder encodeDouble:_longitude forKey:kWSOperatorLongitude];
    [aCoder encodeDouble:_countryCode forKey:kWSOperatorCountryCode];
    [aCoder encodeDouble:_latitude forKey:kWSOperatorLatitude];
    [aCoder encodeObject:_address forKey:kWSOperatorAddress];
    [aCoder encodeObject:_fullName forKey:kWSOperatorFullName];
    [aCoder encodeObject:_thumbUrl forKey:kWSOperatorThumbUrl];
    [aCoder encodeObject:_fileUrl forKey:kWSOperatorFileUrl];
    [aCoder encodeObject:_email forKey:kWSOperatorEmail];
    [aCoder encodeDouble:_status forKey:kWSOperatorStatus];
    [aCoder encodeObject:_datetime forKey:kWSOperatorDatetime];
}

- (id)copyWithZone:(NSZone *)zone {
    WSOperator *copy = [[WSOperator alloc] init];
    
    
    
    if (copy) {

        copy.mobileNumber = self.mobileNumber;
        copy.operatorIdentifier = self.operatorIdentifier;
        copy.longitude = self.longitude;
        copy.countryCode = self.countryCode;
        copy.latitude = self.latitude;
        copy.address = [self.address copyWithZone:zone];
        copy.fullName = [self.fullName copyWithZone:zone];
        copy.thumbUrl = [self.thumbUrl copyWithZone:zone];
        copy.fileUrl = [self.fileUrl copyWithZone:zone];
        copy.email = [self.email copyWithZone:zone];
        copy.status = self.status;
        copy.datetime = [self.datetime copyWithZone:zone];
    }
    
    return copy;
}


@end
