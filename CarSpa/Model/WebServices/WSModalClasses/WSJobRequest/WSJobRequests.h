//
//  WSJobRequests.h
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WSVan, WSCar, WSCartype, WSCategory, WSUser, WSBill, WSAddress;

@interface WSJobRequests : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double destinationLongitude;
@property (nonatomic, strong) NSString *statusdatetime;
@property (nonatomic, strong) NSString *suffix;
@property (nonatomic, strong) WSVan *van;
@property (nonatomic, assign) double status;
@property (nonatomic, strong) NSString *operatorDatetime;
@property (nonatomic, strong) WSCar *car;
@property (nonatomic, strong) WSCartype *carType;
@property (nonatomic, strong) WSCategory *category;
@property (nonatomic, strong) NSString *destinationAddress;
@property (nonatomic, strong) NSString *appointmentDatetime;
@property (nonatomic, strong) WSUser *user;
@property (nonatomic, strong) WSBill *bill;
@property (nonatomic, assign) NSInteger internalBaseClassIdentifier;
@property (nonatomic, assign) double requestId;
@property (nonatomic, strong) NSArray *services;
@property (nonatomic, strong) NSArray *appointmentStatus;
@property (nonatomic, strong) NSString *reason;
@property (nonatomic, strong) NSString *statusTime;
@property (nonatomic, strong) NSArray *rate;
@property (nonatomic, assign) double destinationLatitude;
@property (nonatomic, strong) WSAddress *address;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
