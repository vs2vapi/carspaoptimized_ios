//
//  WSAddress.m
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSAddress.h"


NSString *const kWSAddressUserId = @"userId";
NSString *const kWSAddressCity = @"city";
NSString *const kWSAddressId = @"Id";
NSString *const kWSAddressCountry = @"country";
NSString *const kWSAddressLongitude = @"longitude";
NSString *const kWSAddressAddress = @"address";
NSString *const kWSAddressLatitude = @"latitude";
NSString *const kWSAddressAddresstitle = @"addresstitle";


@interface WSAddress ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSAddress

@synthesize userId = _userId;
@synthesize city = _city;
@synthesize addressIdentifier = _addressIdentifier;
@synthesize country = _country;
@synthesize longitude = _longitude;
@synthesize address = _address;
@synthesize latitude = _latitude;
@synthesize addresstitle = _addresstitle;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.userId = [[self objectOrNilForKey:kWSAddressUserId fromDictionary:dict] doubleValue];
            self.city = [self objectOrNilForKey:kWSAddressCity fromDictionary:dict];
            self.addressIdentifier = [[self objectOrNilForKey:kWSAddressId fromDictionary:dict] doubleValue];
            self.country = [self objectOrNilForKey:kWSAddressCountry fromDictionary:dict];
            self.longitude = [[self objectOrNilForKey:kWSAddressLongitude fromDictionary:dict] doubleValue];
            self.address = [self objectOrNilForKey:kWSAddressAddress fromDictionary:dict];
            self.latitude = [[self objectOrNilForKey:kWSAddressLatitude fromDictionary:dict] doubleValue];
            self.addresstitle = [self objectOrNilForKey:kWSAddressAddresstitle fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userId] forKey:kWSAddressUserId];
    [mutableDict setValue:self.city forKey:kWSAddressCity];
    [mutableDict setValue:[NSNumber numberWithDouble:self.addressIdentifier] forKey:kWSAddressId];
    [mutableDict setValue:self.country forKey:kWSAddressCountry];
    [mutableDict setValue:[NSNumber numberWithDouble:self.longitude] forKey:kWSAddressLongitude];
    [mutableDict setValue:self.address forKey:kWSAddressAddress];
    [mutableDict setValue:[NSNumber numberWithDouble:self.latitude] forKey:kWSAddressLatitude];
    [mutableDict setValue:self.addresstitle forKey:kWSAddressAddresstitle];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.userId = [aDecoder decodeDoubleForKey:kWSAddressUserId];
    self.city = [aDecoder decodeObjectForKey:kWSAddressCity];
    self.addressIdentifier = [aDecoder decodeDoubleForKey:kWSAddressId];
    self.country = [aDecoder decodeObjectForKey:kWSAddressCountry];
    self.longitude = [aDecoder decodeDoubleForKey:kWSAddressLongitude];
    self.address = [aDecoder decodeObjectForKey:kWSAddressAddress];
    self.latitude = [aDecoder decodeDoubleForKey:kWSAddressLatitude];
    self.addresstitle = [aDecoder decodeObjectForKey:kWSAddressAddresstitle];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_userId forKey:kWSAddressUserId];
    [aCoder encodeObject:_city forKey:kWSAddressCity];
    [aCoder encodeDouble:_addressIdentifier forKey:kWSAddressId];
    [aCoder encodeObject:_country forKey:kWSAddressCountry];
    [aCoder encodeDouble:_longitude forKey:kWSAddressLongitude];
    [aCoder encodeObject:_address forKey:kWSAddressAddress];
    [aCoder encodeDouble:_latitude forKey:kWSAddressLatitude];
    [aCoder encodeObject:_addresstitle forKey:kWSAddressAddresstitle];
}

- (id)copyWithZone:(NSZone *)zone {
    WSAddress *copy = [[WSAddress alloc] init];
    
    
    
    if (copy) {

        copy.userId = self.userId;
        copy.city = [self.city copyWithZone:zone];
        copy.addressIdentifier = self.addressIdentifier;
        copy.country = [self.country copyWithZone:zone];
        copy.longitude = self.longitude;
        copy.address = [self.address copyWithZone:zone];
        copy.latitude = self.latitude;
        copy.addresstitle = [self.addresstitle copyWithZone:zone];
    }
    
    return copy;
}


@end
