//
//  WSAppointmentList.h
//
//  Created by Krishna Bhadola on 02/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface WSAppointmentList : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) NSInteger status;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *dateTime;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, assign) double maxDistance;
@property (nonatomic, assign) NSInteger vanId;
@property (nonatomic, assign) NSInteger travelingTime;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
