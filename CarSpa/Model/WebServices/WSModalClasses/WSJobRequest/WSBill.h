//
//  WSBill.h
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface WSBill : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double status;
@property (nonatomic, assign) double paidBy;
@property (nonatomic, assign) double billIdentifier;
@property (nonatomic, assign) double tax;
@property (nonatomic, assign) double discount;
@property (nonatomic, assign) double totalAmount;
@property (nonatomic, assign) double localAmount;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, assign) double exchangeRate;
@property (nonatomic, assign) double netAmount;
@property (nonatomic, assign) double outStandingAmount;
@property (nonatomic, assign) double orderAmount;
@property (nonatomic, assign) double discountPercentage;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
