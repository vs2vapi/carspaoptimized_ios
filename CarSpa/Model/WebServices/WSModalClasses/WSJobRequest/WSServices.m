//
//  WSServices.m
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSServices.h"
#import "WSOwner.h"

NSString *const kWSServicesDescription = @"description";
NSString *const kWSServicesIconThump = @"iconThump";
NSString *const kWSServicesDatetime = @"datetime";
NSString *const kWSServicesCurrency = @"currency";
NSString *const kWSServicesId = @"Id";
NSString *const kWSServicesAmount = @"amount";
NSString *const kWSServicesNameAR = @"nameAR";
NSString *const kWSServicesCurrencyAR = @"currencyAR";
NSString *const kWSServicesServiceStartTime = @"serviceStartTime";
NSString *const kWSServicesVariableAmount = @"variableAmount";
NSString *const kWSServicesIcon = @"icon";
NSString *const kWSServicesReason = @"reason";
NSString *const kWSServicesDescriptionAR = @"descriptionAR";
NSString *const kWSServicesServiceEndTime = @"serviceEndTime";
NSString *const kWSServicesDuration = @"duration";
NSString *const kWSServicesStatus = @"status";
NSString *const kWSServicesName = @"name";
NSString *const kWSServicesOwner = @"Owner";
NSString *const kWSServicesOwnerId = @"ownerId";
NSString *const kWSServicesSelected = @"isSelected";

@interface WSServices ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSServices

@synthesize servicesDescription = _servicesDescription;
@synthesize iconThump = _iconThump;
@synthesize datetime = _datetime;
@synthesize currency = _currency;
@synthesize servicesIdentifier = _servicesIdentifier;
@synthesize amount = _amount;
@synthesize nameAR = _nameAR;
@synthesize currencyAR = _currencyAR;
@synthesize serviceStartTime = _serviceStartTime;
@synthesize variableAmount = _variableAmount;
@synthesize icon = _icon;
@synthesize reason = _reason;
@synthesize descriptionAR = _descriptionAR;
@synthesize serviceEndTime = _serviceEndTime;
@synthesize duration = _duration;
@synthesize status = _status;
@synthesize name = _name;
@synthesize owner = _owner;
@synthesize ownerId = _ownerId;
@synthesize isServiceSelected = _isServiceSelected;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.servicesDescription = [self objectOrNilForKey:kWSServicesDescription fromDictionary:dict];
            self.iconThump = [self objectOrNilForKey:kWSServicesIconThump fromDictionary:dict];
            self.datetime = [self objectOrNilForKey:kWSServicesDatetime fromDictionary:dict];
            self.currency = [self objectOrNilForKey:kWSServicesCurrency fromDictionary:dict];
            self.servicesIdentifier = [[self objectOrNilForKey:kWSServicesId fromDictionary:dict] integerValue];
            self.amount = [[self objectOrNilForKey:kWSServicesAmount fromDictionary:dict] doubleValue];
            self.nameAR = [self objectOrNilForKey:kWSServicesNameAR fromDictionary:dict];
            self.currencyAR = [self objectOrNilForKey:kWSServicesCurrencyAR fromDictionary:dict];
            self.serviceStartTime = [self objectOrNilForKey:kWSServicesServiceStartTime fromDictionary:dict];
            self.variableAmount = [[self objectOrNilForKey:kWSServicesVariableAmount fromDictionary:dict] doubleValue];
            self.icon = [self objectOrNilForKey:kWSServicesIcon fromDictionary:dict];
            self.reason = [self objectOrNilForKey:kWSServicesReason fromDictionary:dict];
            self.descriptionAR = [self objectOrNilForKey:kWSServicesDescriptionAR fromDictionary:dict];
            self.serviceEndTime = [self objectOrNilForKey:kWSServicesServiceEndTime fromDictionary:dict];
            self.duration = [[self objectOrNilForKey:kWSServicesDuration fromDictionary:dict] doubleValue];
            self.status = [[self objectOrNilForKey:kWSServicesStatus fromDictionary:dict] doubleValue];
            self.name = [self objectOrNilForKey:kWSServicesName fromDictionary:dict];
            self.owner = [WSOwner modelObjectWithDictionary:[dict objectForKey:kWSServicesOwner]];
            self.ownerId = [[self objectOrNilForKey:kWSServicesOwnerId fromDictionary:dict] integerValue];
            self.isServiceSelected = [[self objectOrNilForKey:kWSServicesSelected fromDictionary:dict] boolValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.servicesDescription forKey:kWSServicesDescription];
    [mutableDict setValue:self.iconThump forKey:kWSServicesIconThump];
    [mutableDict setValue:self.datetime forKey:kWSServicesDatetime];
    [mutableDict setValue:self.currency forKey:kWSServicesCurrency];
    [mutableDict setValue:[NSNumber numberWithInteger:self.servicesIdentifier] forKey:kWSServicesId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.amount] forKey:kWSServicesAmount];
    [mutableDict setValue:self.nameAR forKey:kWSServicesNameAR];
    [mutableDict setValue:self.currencyAR forKey:kWSServicesCurrencyAR];
    [mutableDict setValue:self.serviceStartTime forKey:kWSServicesServiceStartTime];
    [mutableDict setValue:[NSNumber numberWithDouble:self.variableAmount] forKey:kWSServicesVariableAmount];
    [mutableDict setValue:self.icon forKey:kWSServicesIcon];
    [mutableDict setValue:self.reason forKey:kWSServicesReason];
    [mutableDict setValue:self.descriptionAR forKey:kWSServicesDescriptionAR];
    [mutableDict setValue:self.serviceEndTime forKey:kWSServicesServiceEndTime];
    [mutableDict setValue:[NSNumber numberWithDouble:self.duration] forKey:kWSServicesDuration];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kWSServicesStatus];
    [mutableDict setValue:self.name forKey:kWSServicesName];
    [mutableDict setValue:[self.owner dictionaryRepresentation] forKey:kWSServicesOwner];
    [mutableDict setValue:[NSNumber numberWithInteger:self.ownerId] forKey:kWSServicesOwnerId];
    [mutableDict setValue:[NSNumber numberWithBool:self.isServiceSelected] forKey:kWSServicesSelected];
    
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.servicesDescription = [aDecoder decodeObjectForKey:kWSServicesDescription];
    self.iconThump = [aDecoder decodeObjectForKey:kWSServicesIconThump];
    self.datetime = [aDecoder decodeObjectForKey:kWSServicesDatetime];
    self.currency = [aDecoder decodeObjectForKey:kWSServicesCurrency];
    self.servicesIdentifier = [aDecoder decodeIntegerForKey:kWSServicesId];
    self.amount = [aDecoder decodeDoubleForKey:kWSServicesAmount];
    self.nameAR = [aDecoder decodeObjectForKey:kWSServicesNameAR];
    self.currencyAR = [aDecoder decodeObjectForKey:kWSServicesCurrencyAR];
    self.serviceStartTime = [aDecoder decodeObjectForKey:kWSServicesServiceStartTime];
    self.variableAmount = [aDecoder decodeDoubleForKey:kWSServicesVariableAmount];
    self.icon = [aDecoder decodeObjectForKey:kWSServicesIcon];
    self.reason = [aDecoder decodeObjectForKey:kWSServicesReason];
    self.descriptionAR = [aDecoder decodeObjectForKey:kWSServicesDescriptionAR];
    self.serviceEndTime = [aDecoder decodeObjectForKey:kWSServicesServiceEndTime];
    self.duration = [aDecoder decodeDoubleForKey:kWSServicesDuration];
    self.status = [aDecoder decodeDoubleForKey:kWSServicesStatus];
    self.name = [aDecoder decodeObjectForKey:kWSServicesName];
    self.owner = [aDecoder decodeObjectForKey:kWSServicesOwner];
    self.ownerId = [aDecoder decodeIntegerForKey:kWSServicesOwnerId];
    self.isServiceSelected = [aDecoder decodeBoolForKey:kWSServicesSelected];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_servicesDescription forKey:kWSServicesDescription];
    [aCoder encodeObject:_iconThump forKey:kWSServicesIconThump];
    [aCoder encodeObject:_datetime forKey:kWSServicesDatetime];
    [aCoder encodeObject:_currency forKey:kWSServicesCurrency];
    [aCoder encodeInteger:_servicesIdentifier forKey:kWSServicesId];
    [aCoder encodeDouble:_amount forKey:kWSServicesAmount];
    [aCoder encodeObject:_nameAR forKey:kWSServicesNameAR];
    [aCoder encodeObject:_currencyAR forKey:kWSServicesCurrencyAR];
    [aCoder encodeObject:_serviceStartTime forKey:kWSServicesServiceStartTime];
    [aCoder encodeDouble:_variableAmount forKey:kWSServicesVariableAmount];
    [aCoder encodeObject:_icon forKey:kWSServicesIcon];
    [aCoder encodeObject:_reason forKey:kWSServicesReason];
    [aCoder encodeObject:_descriptionAR forKey:kWSServicesDescriptionAR];
    [aCoder encodeObject:_serviceEndTime forKey:kWSServicesServiceEndTime];
    [aCoder encodeDouble:_duration forKey:kWSServicesDuration];
    [aCoder encodeDouble:_status forKey:kWSServicesStatus];
    [aCoder encodeObject:_name forKey:kWSServicesName];
    [aCoder encodeObject:_owner forKey:kWSServicesOwner];
    [aCoder encodeInteger:_ownerId forKey:kWSServicesOwnerId];
     [aCoder encodeBool:_isServiceSelected forKey:kWSServicesSelected];
    
    
}

- (id)copyWithZone:(NSZone *)zone {
    WSServices *copy = [[WSServices alloc] init];
    
    
    
    if (copy) {

        copy.servicesDescription = [self.servicesDescription copyWithZone:zone];
        copy.iconThump = [self.iconThump copyWithZone:zone];
        copy.datetime = [self.datetime copyWithZone:zone];
        copy.currency = [self.currency copyWithZone:zone];
        copy.servicesIdentifier = self.servicesIdentifier;
        copy.amount = self.amount;
        copy.nameAR = [self.nameAR copyWithZone:zone];
        copy.currencyAR = [self.currencyAR copyWithZone:zone];
        copy.serviceStartTime = [self.serviceStartTime copyWithZone:zone];
        copy.variableAmount = self.variableAmount;
        copy.icon = [self.icon copyWithZone:zone];
        copy.reason = [self.reason copyWithZone:zone];
        copy.descriptionAR = [self.descriptionAR copyWithZone:zone];
        copy.serviceEndTime = [self.serviceEndTime copyWithZone:zone];
        copy.duration = self.duration;
        copy.status = self.status;
        copy.name = [self.name copyWithZone:zone];
        copy.owner = [self.owner copyWithZone:zone];
        copy.ownerId = self.ownerId;
        copy.isServiceSelected = self.isServiceSelected;
        
    }
    
    return copy;
}


@end
