//
//  WSAppointmentList.m
//
//  Created by Krishna Bhadola on 02/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSAppointmentList.h"


NSString *const kWSAppointmentListStatus = @"status";
NSString *const kWSAppointmentListTime = @"Time";
NSString *const kWSAppointmentListDateTime = @"dateTime";
NSString *const kWSAppointmentListDate = @"Date";
NSString *const kWSAppointmentListMaxDistance = @"maxDistance";
NSString *const kWSAppointmentListVanId = @"vanId";
NSString *const kWSAppointmentListTravelingTime = @"travelingTime";


@interface WSAppointmentList ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSAppointmentList

@synthesize status = _status;
@synthesize time = _time;
@synthesize dateTime = _dateTime;
@synthesize date = _date;
@synthesize maxDistance = _maxDistance;
@synthesize vanId = _vanId;
@synthesize travelingTime = _travelingTime;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.status = [[self objectOrNilForKey:kWSAppointmentListStatus fromDictionary:dict] integerValue];
            self.time = [self objectOrNilForKey:kWSAppointmentListTime fromDictionary:dict];
            self.dateTime = [self objectOrNilForKey:kWSAppointmentListDateTime fromDictionary:dict];
            self.date = [self objectOrNilForKey:kWSAppointmentListDate fromDictionary:dict];
            self.maxDistance = [[self objectOrNilForKey:kWSAppointmentListMaxDistance fromDictionary:dict] doubleValue];
            self.vanId = [[self objectOrNilForKey:kWSAppointmentListVanId fromDictionary:dict] integerValue];
            self.travelingTime = [[self objectOrNilForKey:kWSAppointmentListTravelingTime fromDictionary:dict] integerValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithInteger:self.status] forKey:kWSAppointmentListStatus];
    [mutableDict setValue:self.time forKey:kWSAppointmentListTime];
    [mutableDict setValue:self.dateTime forKey:kWSAppointmentListDateTime];
    [mutableDict setValue:self.date forKey:kWSAppointmentListDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.maxDistance] forKey:kWSAppointmentListMaxDistance];
    [mutableDict setValue:[NSNumber numberWithInteger:self.vanId] forKey:kWSAppointmentListVanId];
    [mutableDict setValue:[NSNumber numberWithInteger:self.travelingTime] forKey:kWSAppointmentListTravelingTime];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.status = [aDecoder decodeIntegerForKey:kWSAppointmentListStatus];
    self.time = [aDecoder decodeObjectForKey:kWSAppointmentListTime];
    self.dateTime = [aDecoder decodeObjectForKey:kWSAppointmentListDateTime];
    self.date = [aDecoder decodeObjectForKey:kWSAppointmentListDate];
    self.maxDistance = [aDecoder decodeDoubleForKey:kWSAppointmentListMaxDistance];
    self.vanId = [aDecoder decodeIntegerForKey:kWSAppointmentListVanId];
    self.travelingTime = [aDecoder decodeIntegerForKey:kWSAppointmentListTravelingTime];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeInteger:_status forKey:kWSAppointmentListStatus];
    [aCoder encodeObject:_time forKey:kWSAppointmentListTime];
    [aCoder encodeObject:_dateTime forKey:kWSAppointmentListDateTime];
    [aCoder encodeObject:_date forKey:kWSAppointmentListDate];
    [aCoder encodeDouble:_maxDistance forKey:kWSAppointmentListMaxDistance];
    [aCoder encodeInteger:_vanId forKey:kWSAppointmentListVanId];
    [aCoder encodeInteger:_travelingTime forKey:kWSAppointmentListTravelingTime];
}

- (id)copyWithZone:(NSZone *)zone {
    WSAppointmentList *copy = [[WSAppointmentList alloc] init];
    
    
    
    if (copy) {

        copy.status = self.status;
        copy.time = [self.time copyWithZone:zone];
        copy.dateTime = [self.dateTime copyWithZone:zone];
        copy.date = [self.date copyWithZone:zone];
        copy.maxDistance = self.maxDistance;
        copy.vanId = self.vanId;
        copy.travelingTime = self.travelingTime;
    }
    
    return copy;
}


@end
