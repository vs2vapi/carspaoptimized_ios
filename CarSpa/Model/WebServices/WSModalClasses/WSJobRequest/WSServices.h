//
//  WSServices.h
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WSOwner;

@interface WSServices : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *servicesDescription;
@property (nonatomic, strong) NSString *iconThump;
@property (nonatomic, strong) NSString *datetime;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, assign) NSInteger servicesIdentifier;
@property (nonatomic, assign) double amount;
@property (nonatomic, strong) NSString *nameAR;
@property (nonatomic, strong) NSString *currencyAR;
@property (nonatomic, strong) NSString *serviceStartTime;
@property (nonatomic, assign) double variableAmount;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) NSString *reason;
@property (nonatomic, strong) NSString *descriptionAR;
@property (nonatomic, strong) NSString *serviceEndTime;
@property (nonatomic, assign) double duration;
@property (nonatomic, assign) double status;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) WSOwner *owner;
@property (nonatomic, assign) NSInteger ownerId;
@property (nonatomic, assign) BOOL isServiceSelected;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
