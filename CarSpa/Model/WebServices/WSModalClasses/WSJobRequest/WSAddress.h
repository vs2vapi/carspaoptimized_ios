//
//  WSAddress.h
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface WSAddress : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double userId;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, assign) double addressIdentifier;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, assign) double longitude;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, assign) double latitude;
@property (nonatomic, strong) NSString *addresstitle;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
