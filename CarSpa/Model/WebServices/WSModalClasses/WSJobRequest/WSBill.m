//
//  WSBill.m
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSBill.h"


NSString *const kWSBillStatus = @"status";
NSString *const kWSBillPaidBy = @"paidBy";
NSString *const kWSBillId = @"Id";
NSString *const kWSBillTax = @"tax";
NSString *const kWSBillDiscount = @"discount";
NSString *const kWSBillTotalAmount = @"totalAmount";
NSString *const kWSBillLocalAmount = @"localAmount";
NSString *const kWSBillCurrency = @"currency";
NSString *const kWSBillExchangeRate = @"exchangeRate";
NSString *const kWSBillNetAmount = @"netAmount";
NSString *const kWSBillOutstandingAmount = @"outstandingAmount";
NSString *const kWSBillOrderAmount = @"originalAmount";
NSString *const kWSBillDiscountPercentage = @"discountpercentage";

@interface WSBill ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSBill

@synthesize status = _status;
@synthesize paidBy = _paidBy;
@synthesize billIdentifier = _billIdentifier;
@synthesize tax = _tax;
@synthesize discount = _discount;
@synthesize totalAmount = _totalAmount;
@synthesize localAmount = _localAmount;
@synthesize currency = _currency;
@synthesize exchangeRate = _exchangeRate;
@synthesize netAmount = _netAmount;
@synthesize outStandingAmount = _outStandingAmount;
@synthesize orderAmount = _orderAmount;
@synthesize discountPercentage = _discountPercentage;



+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.status = [[self objectOrNilForKey:kWSBillStatus fromDictionary:dict] doubleValue];
            self.paidBy = [[self objectOrNilForKey:kWSBillPaidBy fromDictionary:dict] doubleValue];
            self.billIdentifier = [[self objectOrNilForKey:kWSBillId fromDictionary:dict] doubleValue];
            self.tax = [[self objectOrNilForKey:kWSBillTax fromDictionary:dict] doubleValue];
            self.discount = [[self objectOrNilForKey:kWSBillDiscount fromDictionary:dict] doubleValue];
            self.totalAmount = [[self objectOrNilForKey:kWSBillTotalAmount fromDictionary:dict] doubleValue];
            self.localAmount = [[self objectOrNilForKey:kWSBillLocalAmount fromDictionary:dict] doubleValue];
            self.currency = [self objectOrNilForKey:kWSBillCurrency fromDictionary:dict];
            self.exchangeRate = [[self objectOrNilForKey:kWSBillExchangeRate fromDictionary:dict] doubleValue];
            self.netAmount = [[self objectOrNilForKey:kWSBillNetAmount fromDictionary:dict] doubleValue];
            self.outStandingAmount = [[self objectOrNilForKey:kWSBillOutstandingAmount fromDictionary:dict] doubleValue];
            self.orderAmount = [[self objectOrNilForKey:kWSBillOrderAmount fromDictionary:dict] doubleValue];
            self.discountPercentage = [[self objectOrNilForKey:kWSBillDiscountPercentage fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kWSBillStatus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.paidBy] forKey:kWSBillPaidBy];
    [mutableDict setValue:[NSNumber numberWithDouble:self.billIdentifier] forKey:kWSBillId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.tax] forKey:kWSBillTax];
    [mutableDict setValue:[NSNumber numberWithDouble:self.discount] forKey:kWSBillDiscount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalAmount] forKey:kWSBillTotalAmount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.localAmount] forKey:kWSBillLocalAmount];
    [mutableDict setValue:self.currency forKey:kWSBillCurrency];
    [mutableDict setValue:[NSNumber numberWithDouble:self.exchangeRate] forKey:kWSBillExchangeRate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.netAmount] forKey:kWSBillNetAmount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.outStandingAmount] forKey:kWSBillOutstandingAmount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.orderAmount] forKey:kWSBillOrderAmount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.discountPercentage] forKey:kWSBillDiscountPercentage];
    

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.status = [aDecoder decodeDoubleForKey:kWSBillStatus];
    self.paidBy = [aDecoder decodeDoubleForKey:kWSBillPaidBy];
    self.billIdentifier = [aDecoder decodeDoubleForKey:kWSBillId];
    self.tax = [aDecoder decodeDoubleForKey:kWSBillTax];
    self.discount = [aDecoder decodeDoubleForKey:kWSBillDiscount];
    self.totalAmount = [aDecoder decodeDoubleForKey:kWSBillTotalAmount];
    self.localAmount = [aDecoder decodeDoubleForKey:kWSBillLocalAmount];
    self.currency = [aDecoder decodeObjectForKey:kWSBillCurrency];
    self.exchangeRate = [aDecoder decodeDoubleForKey:kWSBillExchangeRate];
     self.netAmount = [aDecoder decodeDoubleForKey:kWSBillNetAmount];
     self.outStandingAmount = [aDecoder decodeDoubleForKey:kWSBillOutstandingAmount];
    self.orderAmount = [aDecoder decodeDoubleForKey:kWSBillOrderAmount];
     self.discountPercentage = [aDecoder decodeDoubleForKey:kWSBillDiscountPercentage];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_status forKey:kWSBillStatus];
    [aCoder encodeDouble:_paidBy forKey:kWSBillPaidBy];
    [aCoder encodeDouble:_billIdentifier forKey:kWSBillId];
    [aCoder encodeDouble:_tax forKey:kWSBillTax];
    [aCoder encodeDouble:_discount forKey:kWSBillDiscount];
    [aCoder encodeDouble:_totalAmount forKey:kWSBillTotalAmount];
    [aCoder encodeDouble:_localAmount forKey:kWSBillLocalAmount];
    [aCoder encodeObject:_currency forKey:kWSBillCurrency];
    [aCoder encodeDouble:_exchangeRate forKey:kWSBillExchangeRate];
    [aCoder encodeDouble:_netAmount forKey:kWSBillNetAmount];
    [aCoder encodeDouble:_outStandingAmount forKey:kWSBillOutstandingAmount];
    [aCoder encodeDouble:_orderAmount forKey:kWSBillOrderAmount];
    [aCoder encodeDouble:_discountPercentage forKey:kWSBillDiscountPercentage];
    
}

- (id)copyWithZone:(NSZone *)zone {
    WSBill *copy = [[WSBill alloc] init];
    
    
    
    if (copy) {

        copy.status = self.status;
        copy.paidBy = self.paidBy;
        copy.billIdentifier = self.billIdentifier;
        copy.tax = self.tax;
        copy.discount = self.discount;
        copy.totalAmount = self.totalAmount;
        copy.localAmount = self.localAmount;
        copy.currency = [self.currency copyWithZone:zone];
        copy.exchangeRate = self.exchangeRate;
        copy.netAmount = self.netAmount;
        copy.outStandingAmount = self.outStandingAmount;
        copy.orderAmount = self.orderAmount;
         copy.discountPercentage = self.discountPercentage;
    }
    
    return copy;
}


@end
