//
//  WSContact.m
//
//  Created by Krishna Bhadola on 04/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSContact.h"


NSString *const kWSContactCountryCode = @"countryCode";
NSString *const kWSContactEmail = @"email";
NSString *const kWSContactAddress = @"address";
NSString *const kWSContactMobileNumber = @"mobileNumber";
NSString *const kWSContactId = @"Id";


@interface WSContact ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSContact

@synthesize countryCode = _countryCode;
@synthesize email = _email;
@synthesize address = _address;
@synthesize mobileNumber = _mobileNumber;
@synthesize contactIdentifier = _contactIdentifier;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.countryCode = [[self objectOrNilForKey:kWSContactCountryCode fromDictionary:dict] doubleValue];
            self.email = [self objectOrNilForKey:kWSContactEmail fromDictionary:dict];
            self.address = [self objectOrNilForKey:kWSContactAddress fromDictionary:dict];
            self.mobileNumber = [self objectOrNilForKey:kWSContactMobileNumber fromDictionary:dict];
            self.contactIdentifier = [[self objectOrNilForKey:kWSContactId fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.countryCode] forKey:kWSContactCountryCode];
    [mutableDict setValue:self.email forKey:kWSContactEmail];
    [mutableDict setValue:self.address forKey:kWSContactAddress];
    [mutableDict setValue:self.mobileNumber forKey:kWSContactMobileNumber];
    [mutableDict setValue:[NSNumber numberWithDouble:self.contactIdentifier] forKey:kWSContactId];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.countryCode = [aDecoder decodeDoubleForKey:kWSContactCountryCode];
    self.email = [aDecoder decodeObjectForKey:kWSContactEmail];
    self.address = [aDecoder decodeObjectForKey:kWSContactAddress];
    self.mobileNumber = [aDecoder decodeObjectForKey:kWSContactMobileNumber];
    self.contactIdentifier = [aDecoder decodeDoubleForKey:kWSContactId];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_countryCode forKey:kWSContactCountryCode];
    [aCoder encodeObject:_email forKey:kWSContactEmail];
    [aCoder encodeObject:_address forKey:kWSContactAddress];
    [aCoder encodeObject:_mobileNumber forKey:kWSContactMobileNumber];
    [aCoder encodeDouble:_contactIdentifier forKey:kWSContactId];
}

- (id)copyWithZone:(NSZone *)zone {
    WSContact *copy = [[WSContact alloc] init];
    
    
    
    if (copy) {

        copy.countryCode = self.countryCode;
        copy.email = [self.email copyWithZone:zone];
        copy.address = [self.address copyWithZone:zone];
        copy.mobileNumber =[self.mobileNumber copyWithZone:zone];
        copy.contactIdentifier = self.contactIdentifier;
    }
    
    return copy;
}


@end
