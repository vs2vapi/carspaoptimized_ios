//
//  WSContact.h
//
//  Created by Krishna Bhadola on 04/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface WSContact : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double countryCode;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *mobileNumber;
@property (nonatomic, assign) double contactIdentifier;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
