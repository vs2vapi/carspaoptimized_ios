//
//  WSContactUs.m
//
//  Created by Krishna Bhadola on 04/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSContactUs.h"
#import "WSContact.h"


NSString *const kWSContactUsName = @"name";
NSString *const kWSContactUsNameAR = @"nameAR";
NSString *const kWSContactUsCountryId = @"countryId";
NSString *const kWSContactUsContact = @"contact";


@interface WSContactUs ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSContactUs

@synthesize name = _name;
@synthesize nameAR = _nameAR;
@synthesize countryId = _countryId;
@synthesize contact = _contact;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.name = [self objectOrNilForKey:kWSContactUsName fromDictionary:dict];
            self.nameAR = [self objectOrNilForKey:kWSContactUsNameAR fromDictionary:dict];
            self.countryId = [[self objectOrNilForKey:kWSContactUsCountryId fromDictionary:dict] doubleValue];
    NSObject *receivedWSContact = [dict objectForKey:kWSContactUsContact];
    NSMutableArray *parsedWSContact = [NSMutableArray array];
    
    if ([receivedWSContact isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedWSContact) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedWSContact addObject:[WSContact modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedWSContact isKindOfClass:[NSDictionary class]]) {
       [parsedWSContact addObject:[WSContact modelObjectWithDictionary:(NSDictionary *)receivedWSContact]];
    }

    self.contact = [NSArray arrayWithArray:parsedWSContact];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.name forKey:kWSContactUsName];
    [mutableDict setValue:self.nameAR forKey:kWSContactUsNameAR];
    [mutableDict setValue:[NSNumber numberWithDouble:self.countryId] forKey:kWSContactUsCountryId];
    NSMutableArray *tempArrayForContact = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.contact) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForContact addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForContact addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForContact] forKey:kWSContactUsContact];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.name = [aDecoder decodeObjectForKey:kWSContactUsName];
    self.nameAR = [aDecoder decodeObjectForKey:kWSContactUsNameAR];
    self.countryId = [aDecoder decodeDoubleForKey:kWSContactUsCountryId];
    self.contact = [aDecoder decodeObjectForKey:kWSContactUsContact];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_name forKey:kWSContactUsName];
    [aCoder encodeObject:_nameAR forKey:kWSContactUsNameAR];
    [aCoder encodeDouble:_countryId forKey:kWSContactUsCountryId];
    [aCoder encodeObject:_contact forKey:kWSContactUsContact];
}

- (id)copyWithZone:(NSZone *)zone {
    WSContactUs *copy = [[WSContactUs alloc] init];
    
    
    
    if (copy) {

        copy.name = [self.name copyWithZone:zone];
        copy.nameAR = [self.nameAR copyWithZone:zone];
        copy.countryId = self.countryId;
        copy.contact = [self.contact copyWithZone:zone];
    }
    
    return copy;
}


@end
