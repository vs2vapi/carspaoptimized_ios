//
//  WSCartype.m
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSCartype.h"


NSString *const kWSCartypeNameAR = @"nameAR";
NSString *const kWSCartypeIcon = @"icon";
NSString *const kWSCartypeIconThump = @"iconThump";
NSString *const kWSCartypeDatetime = @"datetime";
NSString *const kWSCartypeName = @"name";
NSString *const kWSCartypeId = @"Id";


@interface WSCartype ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSCartype

@synthesize nameAR = _nameAR;
@synthesize icon = _icon;
@synthesize iconThump = _iconThump;
@synthesize datetime = _datetime;
@synthesize name = _name;
@synthesize cartypeIdentifier = _cartypeIdentifier;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.nameAR = [self objectOrNilForKey:kWSCartypeNameAR fromDictionary:dict];
            self.icon = [self objectOrNilForKey:kWSCartypeIcon fromDictionary:dict];
            self.iconThump = [self objectOrNilForKey:kWSCartypeIconThump fromDictionary:dict];
            self.datetime = [self objectOrNilForKey:kWSCartypeDatetime fromDictionary:dict];
            self.name = [self objectOrNilForKey:kWSCartypeName fromDictionary:dict];
            self.cartypeIdentifier = [[self objectOrNilForKey:kWSCartypeId fromDictionary:dict] integerValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.nameAR forKey:kWSCartypeNameAR];
    [mutableDict setValue:self.icon forKey:kWSCartypeIcon];
    [mutableDict setValue:self.iconThump forKey:kWSCartypeIconThump];
    [mutableDict setValue:self.datetime forKey:kWSCartypeDatetime];
    [mutableDict setValue:self.name forKey:kWSCartypeName];
    [mutableDict setValue:[NSNumber numberWithInteger:self.cartypeIdentifier] forKey:kWSCartypeId];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.nameAR = [aDecoder decodeObjectForKey:kWSCartypeNameAR];
    self.icon = [aDecoder decodeObjectForKey:kWSCartypeIcon];
    self.iconThump = [aDecoder decodeObjectForKey:kWSCartypeIconThump];
    self.datetime = [aDecoder decodeObjectForKey:kWSCartypeDatetime];
    self.name = [aDecoder decodeObjectForKey:kWSCartypeName];
    self.cartypeIdentifier = [aDecoder decodeIntegerForKey:kWSCartypeId];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_nameAR forKey:kWSCartypeNameAR];
    [aCoder encodeObject:_icon forKey:kWSCartypeIcon];
    [aCoder encodeObject:_iconThump forKey:kWSCartypeIconThump];
    [aCoder encodeObject:_datetime forKey:kWSCartypeDatetime];
    [aCoder encodeObject:_name forKey:kWSCartypeName];
    [aCoder encodeInteger:_cartypeIdentifier forKey:kWSCartypeId];
}

- (id)copyWithZone:(NSZone *)zone {
    WSCartype *copy = [[WSCartype alloc] init];
    
    
    
    if (copy) {

        copy.nameAR = [self.nameAR copyWithZone:zone];
        copy.icon = [self.icon copyWithZone:zone];
        copy.iconThump = [self.iconThump copyWithZone:zone];
        copy.datetime = [self.datetime copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.cartypeIdentifier = self.cartypeIdentifier;
    }
    
    return copy;
}


@end
