//
//  WSHolidayList.h
//
//  Created by Krishna Bhadola on 02/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface WSHolidayList : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double internalBaseClassIdentifier;
@property (nonatomic, assign) double ownerId;
@property (nonatomic, strong) NSString *day;
@property (nonatomic, assign) double shiftId;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, assign) double holidayType;
@property (nonatomic, assign) double vanId;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
