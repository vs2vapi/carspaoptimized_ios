//
//  WSHolidayList.m
//
//  Created by Krishna Bhadola on 02/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSHolidayList.h"


NSString *const kWSHolidayListId = @"Id";
NSString *const kWSHolidayListOwnerId = @"ownerId";
NSString *const kWSHolidayListDay = @"day";
NSString *const kWSHolidayListShiftId = @"shiftId";
NSString *const kWSHolidayListDate = @"date";
NSString *const kWSHolidayListHolidayType = @"holidayType";
NSString *const kWSHolidayListVanId = @"vanId";


@interface WSHolidayList ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSHolidayList

@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize ownerId = _ownerId;
@synthesize day = _day;
@synthesize shiftId = _shiftId;
@synthesize date = _date;
@synthesize holidayType = _holidayType;
@synthesize vanId = _vanId;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.internalBaseClassIdentifier = [[self objectOrNilForKey:kWSHolidayListId fromDictionary:dict] integerValue];
            self.ownerId = [[self objectOrNilForKey:kWSHolidayListOwnerId fromDictionary:dict] doubleValue];
            self.day = [self objectOrNilForKey:kWSHolidayListDay fromDictionary:dict];
            self.shiftId = [[self objectOrNilForKey:kWSHolidayListShiftId fromDictionary:dict] doubleValue];
            self.date = [self objectOrNilForKey:kWSHolidayListDate fromDictionary:dict];
            self.holidayType = [[self objectOrNilForKey:kWSHolidayListHolidayType fromDictionary:dict] doubleValue];
            self.vanId = [[self objectOrNilForKey:kWSHolidayListVanId fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithInteger:self.internalBaseClassIdentifier] forKey:kWSHolidayListId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.ownerId] forKey:kWSHolidayListOwnerId];
    [mutableDict setValue:self.day forKey:kWSHolidayListDay];
    [mutableDict setValue:[NSNumber numberWithDouble:self.shiftId] forKey:kWSHolidayListShiftId];
    [mutableDict setValue:self.date forKey:kWSHolidayListDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.holidayType] forKey:kWSHolidayListHolidayType];
    [mutableDict setValue:[NSNumber numberWithDouble:self.vanId] forKey:kWSHolidayListVanId];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.internalBaseClassIdentifier = [aDecoder decodeIntegerForKey:kWSHolidayListId];
    self.ownerId = [aDecoder decodeDoubleForKey:kWSHolidayListOwnerId];
    self.day = [aDecoder decodeObjectForKey:kWSHolidayListDay];
    self.shiftId = [aDecoder decodeDoubleForKey:kWSHolidayListShiftId];
    self.date = [aDecoder decodeObjectForKey:kWSHolidayListDate];
    self.holidayType = [aDecoder decodeDoubleForKey:kWSHolidayListHolidayType];
    self.vanId = [aDecoder decodeDoubleForKey:kWSHolidayListVanId];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeInteger:_internalBaseClassIdentifier forKey:kWSHolidayListId];
    [aCoder encodeDouble:_ownerId forKey:kWSHolidayListOwnerId];
    [aCoder encodeObject:_day forKey:kWSHolidayListDay];
    [aCoder encodeDouble:_shiftId forKey:kWSHolidayListShiftId];
    [aCoder encodeObject:_date forKey:kWSHolidayListDate];
    [aCoder encodeDouble:_holidayType forKey:kWSHolidayListHolidayType];
    [aCoder encodeDouble:_vanId forKey:kWSHolidayListVanId];
}

- (id)copyWithZone:(NSZone *)zone {
    WSHolidayList *copy = [[WSHolidayList alloc] init];
    
    
    
    if (copy) {

        copy.internalBaseClassIdentifier = self.internalBaseClassIdentifier;
        copy.ownerId = self.ownerId;
        copy.day = [self.day copyWithZone:zone];
        copy.shiftId = self.shiftId;
        copy.date = [self.date copyWithZone:zone];
        copy.holidayType = self.holidayType;
        copy.vanId = self.vanId;
    }
    
    return copy;
}


@end
