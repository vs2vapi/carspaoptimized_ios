//
//  WSNearByVans.h
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WSCartype;

@interface WSNearByVans : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double status;
@property (nonatomic, assign) double longitude;
@property (nonatomic, strong) WSCartype *cartype;
@property (nonatomic, strong) NSString *datetime;
@property (nonatomic, strong) NSString *plateno;
@property (nonatomic, assign) double internalBaseClassIdentifier;
@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double type;
@property (nonatomic, strong) NSString *name;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
