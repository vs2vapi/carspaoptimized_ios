//
//  WSNearByVans.m
//
//  Created by Krishna Bhadola on 27/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSNearByVans.h"
#import "WSCartype.h"


NSString *const kWSNearByVansStatus = @"status";
NSString *const kWSNearByVansLongitude = @"longitude";
NSString *const kWSNearByVansCartype = @"cartype";
NSString *const kWSNearByVansDatetime = @"datetime";
NSString *const kWSNearByVansPlateno = @"plateno";
NSString *const kWSNearByVansId = @"Id";
NSString *const kWSNearByVansLatitude = @"latitude";
NSString *const kWSNearByVansType = @"type";
NSString *const kWSNearByVansName = @"name";


@interface WSNearByVans ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSNearByVans

@synthesize status = _status;
@synthesize longitude = _longitude;
@synthesize cartype = _cartype;
@synthesize datetime = _datetime;
@synthesize plateno = _plateno;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize latitude = _latitude;
@synthesize type = _type;
@synthesize name = _name;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.status = [[self objectOrNilForKey:kWSNearByVansStatus fromDictionary:dict] doubleValue];
            self.longitude = [[self objectOrNilForKey:kWSNearByVansLongitude fromDictionary:dict] doubleValue];
            self.cartype = [WSCartype modelObjectWithDictionary:[dict objectForKey:kWSNearByVansCartype]];
            self.datetime = [self objectOrNilForKey:kWSNearByVansDatetime fromDictionary:dict];
            self.plateno = [self objectOrNilForKey:kWSNearByVansPlateno fromDictionary:dict];
            self.internalBaseClassIdentifier = [[self objectOrNilForKey:kWSNearByVansId fromDictionary:dict] doubleValue];
            self.latitude = [[self objectOrNilForKey:kWSNearByVansLatitude fromDictionary:dict] doubleValue];
            self.type = [[self objectOrNilForKey:kWSNearByVansType fromDictionary:dict] doubleValue];
            self.name = [self objectOrNilForKey:kWSNearByVansName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kWSNearByVansStatus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.longitude] forKey:kWSNearByVansLongitude];
    [mutableDict setValue:[self.cartype dictionaryRepresentation] forKey:kWSNearByVansCartype];
    [mutableDict setValue:self.datetime forKey:kWSNearByVansDatetime];
    [mutableDict setValue:self.plateno forKey:kWSNearByVansPlateno];
    [mutableDict setValue:[NSNumber numberWithDouble:self.internalBaseClassIdentifier] forKey:kWSNearByVansId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.latitude] forKey:kWSNearByVansLatitude];
    [mutableDict setValue:[NSNumber numberWithDouble:self.type] forKey:kWSNearByVansType];
    [mutableDict setValue:self.name forKey:kWSNearByVansName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.status = [aDecoder decodeDoubleForKey:kWSNearByVansStatus];
    self.longitude = [aDecoder decodeDoubleForKey:kWSNearByVansLongitude];
    self.cartype = [aDecoder decodeObjectForKey:kWSNearByVansCartype];
    self.datetime = [aDecoder decodeObjectForKey:kWSNearByVansDatetime];
    self.plateno = [aDecoder decodeObjectForKey:kWSNearByVansPlateno];
    self.internalBaseClassIdentifier = [aDecoder decodeDoubleForKey:kWSNearByVansId];
    self.latitude = [aDecoder decodeDoubleForKey:kWSNearByVansLatitude];
    self.type = [aDecoder decodeDoubleForKey:kWSNearByVansType];
    self.name = [aDecoder decodeObjectForKey:kWSNearByVansName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_status forKey:kWSNearByVansStatus];
    [aCoder encodeDouble:_longitude forKey:kWSNearByVansLongitude];
    [aCoder encodeObject:_cartype forKey:kWSNearByVansCartype];
    [aCoder encodeObject:_datetime forKey:kWSNearByVansDatetime];
    [aCoder encodeObject:_plateno forKey:kWSNearByVansPlateno];
    [aCoder encodeDouble:_internalBaseClassIdentifier forKey:kWSNearByVansId];
    [aCoder encodeDouble:_latitude forKey:kWSNearByVansLatitude];
    [aCoder encodeDouble:_type forKey:kWSNearByVansType];
    [aCoder encodeObject:_name forKey:kWSNearByVansName];
}

- (id)copyWithZone:(NSZone *)zone {
    WSNearByVans *copy = [[WSNearByVans alloc] init];
    
    
    
    if (copy) {

        copy.status = self.status;
        copy.longitude = self.longitude;
        copy.cartype = [self.cartype copyWithZone:zone];
        copy.datetime = [self.datetime copyWithZone:zone];
        copy.plateno = [self.plateno copyWithZone:zone];
        copy.internalBaseClassIdentifier = self.internalBaseClassIdentifier;
        copy.latitude = self.latitude;
        copy.type = self.type;
        copy.name = [self.name copyWithZone:zone];
    }
    
    return copy;
}


@end
