//
//  WSOurPartners.m
//
//  Created by Krishna Bhadola on 04/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "WSOurPartners.h"


NSString *const kWSOurPartnersIconThump = @"iconThump";
NSString *const kWSOurPartnersMobileNumber = @"mobileNumber";
NSString *const kWSOurPartnersId = @"Id";
NSString *const kWSOurPartnersCountryCode = @"countryCode";
NSString *const kWSOurPartnersContactPersons = @"contactPersons";
NSString *const kWSOurPartnersMaxdistance = @"maxdistance";
NSString *const kWSOurPartnersSalesPerson = @"salesPerson";
NSString *const kWSOurPartnersCountryCodeTwo = @"countryCodeTwo";
NSString *const kWSOurPartnersAddress = @"address";
NSString *const kWSOurPartnersCity = @"city";
NSString *const kWSOurPartnersIcon = @"icon";
NSString *const kWSOurPartnersMobileNumberTwo = @"mobileNumberTwo";
NSString *const kWSOurPartnersEmail = @"email";
NSString *const kWSOurPartnersName = @"name";
NSString *const kWSOurPartnersCountry = @"country";


@interface WSOurPartners ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WSOurPartners

@synthesize iconThump = _iconThump;
@synthesize mobileNumber = _mobileNumber;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize countryCode = _countryCode;
@synthesize contactPersons = _contactPersons;
@synthesize maxdistance = _maxdistance;
@synthesize salesPerson = _salesPerson;
@synthesize countryCodeTwo = _countryCodeTwo;
@synthesize address = _address;
@synthesize city = _city;
@synthesize icon = _icon;
@synthesize mobileNumberTwo = _mobileNumberTwo;
@synthesize email = _email;
@synthesize name = _name;
@synthesize country = _country;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iconThump = [self objectOrNilForKey:kWSOurPartnersIconThump fromDictionary:dict];
            self.mobileNumber = [[self objectOrNilForKey:kWSOurPartnersMobileNumber fromDictionary:dict] doubleValue];
            self.internalBaseClassIdentifier = [[self objectOrNilForKey:kWSOurPartnersId fromDictionary:dict] doubleValue];
            self.countryCode = [[self objectOrNilForKey:kWSOurPartnersCountryCode fromDictionary:dict] doubleValue];
            self.contactPersons = [self objectOrNilForKey:kWSOurPartnersContactPersons fromDictionary:dict];
            self.maxdistance = [[self objectOrNilForKey:kWSOurPartnersMaxdistance fromDictionary:dict] doubleValue];
            self.salesPerson = [self objectOrNilForKey:kWSOurPartnersSalesPerson fromDictionary:dict];
            self.countryCodeTwo = [[self objectOrNilForKey:kWSOurPartnersCountryCodeTwo fromDictionary:dict] doubleValue];
            self.address = [self objectOrNilForKey:kWSOurPartnersAddress fromDictionary:dict];
            self.city = [self objectOrNilForKey:kWSOurPartnersCity fromDictionary:dict];
            self.icon = [self objectOrNilForKey:kWSOurPartnersIcon fromDictionary:dict];
            self.mobileNumberTwo = [[self objectOrNilForKey:kWSOurPartnersMobileNumberTwo fromDictionary:dict] doubleValue];
            self.email = [self objectOrNilForKey:kWSOurPartnersEmail fromDictionary:dict];
            self.name = [self objectOrNilForKey:kWSOurPartnersName fromDictionary:dict];
            self.country = [self objectOrNilForKey:kWSOurPartnersCountry fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.iconThump forKey:kWSOurPartnersIconThump];
    [mutableDict setValue:[NSNumber numberWithDouble:self.mobileNumber] forKey:kWSOurPartnersMobileNumber];
    [mutableDict setValue:[NSNumber numberWithDouble:self.internalBaseClassIdentifier] forKey:kWSOurPartnersId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.countryCode] forKey:kWSOurPartnersCountryCode];
    [mutableDict setValue:self.contactPersons forKey:kWSOurPartnersContactPersons];
    [mutableDict setValue:[NSNumber numberWithDouble:self.maxdistance] forKey:kWSOurPartnersMaxdistance];
    [mutableDict setValue:self.salesPerson forKey:kWSOurPartnersSalesPerson];
    [mutableDict setValue:[NSNumber numberWithDouble:self.countryCodeTwo] forKey:kWSOurPartnersCountryCodeTwo];
    [mutableDict setValue:self.address forKey:kWSOurPartnersAddress];
    [mutableDict setValue:self.city forKey:kWSOurPartnersCity];
    [mutableDict setValue:self.icon forKey:kWSOurPartnersIcon];
    [mutableDict setValue:[NSNumber numberWithDouble:self.mobileNumberTwo] forKey:kWSOurPartnersMobileNumberTwo];
    [mutableDict setValue:self.email forKey:kWSOurPartnersEmail];
    [mutableDict setValue:self.name forKey:kWSOurPartnersName];
    [mutableDict setValue:self.country forKey:kWSOurPartnersCountry];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.iconThump = [aDecoder decodeObjectForKey:kWSOurPartnersIconThump];
    self.mobileNumber = [aDecoder decodeDoubleForKey:kWSOurPartnersMobileNumber];
    self.internalBaseClassIdentifier = [aDecoder decodeDoubleForKey:kWSOurPartnersId];
    self.countryCode = [aDecoder decodeDoubleForKey:kWSOurPartnersCountryCode];
    self.contactPersons = [aDecoder decodeObjectForKey:kWSOurPartnersContactPersons];
    self.maxdistance = [aDecoder decodeDoubleForKey:kWSOurPartnersMaxdistance];
    self.salesPerson = [aDecoder decodeObjectForKey:kWSOurPartnersSalesPerson];
    self.countryCodeTwo = [aDecoder decodeDoubleForKey:kWSOurPartnersCountryCodeTwo];
    self.address = [aDecoder decodeObjectForKey:kWSOurPartnersAddress];
    self.city = [aDecoder decodeObjectForKey:kWSOurPartnersCity];
    self.icon = [aDecoder decodeObjectForKey:kWSOurPartnersIcon];
    self.mobileNumberTwo = [aDecoder decodeDoubleForKey:kWSOurPartnersMobileNumberTwo];
    self.email = [aDecoder decodeObjectForKey:kWSOurPartnersEmail];
    self.name = [aDecoder decodeObjectForKey:kWSOurPartnersName];
    self.country = [aDecoder decodeObjectForKey:kWSOurPartnersCountry];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_iconThump forKey:kWSOurPartnersIconThump];
    [aCoder encodeDouble:_mobileNumber forKey:kWSOurPartnersMobileNumber];
    [aCoder encodeDouble:_internalBaseClassIdentifier forKey:kWSOurPartnersId];
    [aCoder encodeDouble:_countryCode forKey:kWSOurPartnersCountryCode];
    [aCoder encodeObject:_contactPersons forKey:kWSOurPartnersContactPersons];
    [aCoder encodeDouble:_maxdistance forKey:kWSOurPartnersMaxdistance];
    [aCoder encodeObject:_salesPerson forKey:kWSOurPartnersSalesPerson];
    [aCoder encodeDouble:_countryCodeTwo forKey:kWSOurPartnersCountryCodeTwo];
    [aCoder encodeObject:_address forKey:kWSOurPartnersAddress];
    [aCoder encodeObject:_city forKey:kWSOurPartnersCity];
    [aCoder encodeObject:_icon forKey:kWSOurPartnersIcon];
    [aCoder encodeDouble:_mobileNumberTwo forKey:kWSOurPartnersMobileNumberTwo];
    [aCoder encodeObject:_email forKey:kWSOurPartnersEmail];
    [aCoder encodeObject:_name forKey:kWSOurPartnersName];
    [aCoder encodeObject:_country forKey:kWSOurPartnersCountry];
}

- (id)copyWithZone:(NSZone *)zone {
    WSOurPartners *copy = [[WSOurPartners alloc] init];
    
    
    
    if (copy) {

        copy.iconThump = [self.iconThump copyWithZone:zone];
        copy.mobileNumber = self.mobileNumber;
        copy.internalBaseClassIdentifier = self.internalBaseClassIdentifier;
        copy.countryCode = self.countryCode;
        copy.contactPersons = [self.contactPersons copyWithZone:zone];
        copy.maxdistance = self.maxdistance;
        copy.salesPerson = [self.salesPerson copyWithZone:zone];
        copy.countryCodeTwo = self.countryCodeTwo;
        copy.address = [self.address copyWithZone:zone];
        copy.city = [self.city copyWithZone:zone];
        copy.icon = [self.icon copyWithZone:zone];
        copy.mobileNumberTwo = self.mobileNumberTwo;
        copy.email = [self.email copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.country = [self.country copyWithZone:zone];
    }
    
    return copy;
}


@end
