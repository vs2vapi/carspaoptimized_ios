//
//  WSOurPartners.h
//
//  Created by Krishna Bhadola on 04/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface WSOurPartners : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *iconThump;
@property (nonatomic, assign) double mobileNumber;
@property (nonatomic, assign) double internalBaseClassIdentifier;
@property (nonatomic, assign) double countryCode;
@property (nonatomic, assign) id contactPersons;
@property (nonatomic, assign) double maxdistance;
@property (nonatomic, strong) NSString *salesPerson;
@property (nonatomic, assign) double countryCodeTwo;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, assign) double mobileNumberTwo;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *country;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
