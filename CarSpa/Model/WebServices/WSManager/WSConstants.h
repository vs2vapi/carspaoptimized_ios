//
//  WSConstants.h
//  R@jan 012
//
//  Created by R@j012 on 09/02/17.
//  Copyright © 2017 R@j012. All rights reserved.
//


#ifndef WSConstants_h
#define WSConstants_h

#define DisplayNetworkIndicator(flag) [UIApplication sharedApplication].networkActivityIndicatorVisible = flag

#pragma mark : --------- : Set Host
#define kHostPath @"http://carspaco.com"
//#define kServicePath [NSString stringWithFormat:@"%@/carspa/Api",kHostPath] //LIVE OLD
//...................... ENABLE FORCE UPDATE
//#define kServicePath [NSString stringWithFormat:@"%@/carspawebservice/Api",kHostPath] //LIVE NEW
//...................... ENABLE FORCE UPDATE
#define kServicePath [NSString stringWithFormat:@"%@/carspapro/Api",kHostPath] //DEV


#pragma mark : --------- :  All Services
#define kLoginWS [NSString stringWithFormat:@"%@/User/customerLogin",kServicePath]
#define kForgotPswdWS [NSString stringWithFormat:@"%@/User/forgetPassword",kServicePath]
#define kSingOutWS [NSString stringWithFormat:@"%@/User/signout",kServicePath]
#define kFetchNearByVansWS [NSString stringWithFormat:@"%@/Van/getNearByVan",kServicePath]
#define kRegisterUserWS [NSString stringWithFormat:@"%@/User/registration",kServicePath]
#define kVerifyOTPWS [NSString stringWithFormat:@"%@/User/verifyOTP",kServicePath]
#define kAskAgainOTPWS [NSString stringWithFormat:@"%@/User/askAgainActivationCode",kServicePath]
#define kFetchRequestWS [NSString stringWithFormat:@"%@/Request/getRequests",kServicePath]
#define kFetchHistoryWS [NSString stringWithFormat:@"%@/Request/getUserRequestsByTwoDates",kServicePath]
#define kFetchCarTypeWS [NSString stringWithFormat:@"%@/Car/getCarTypes",kServicePath]
#define kFetchUserCarsWS [NSString stringWithFormat:@"%@/Car/getCars",kServicePath]
#define kFetchUserAddressWS [NSString stringWithFormat:@"%@/Address/getAddresses",kServicePath]
#define kAddUserAddressWS [NSString stringWithFormat:@"%@/Address/addAddress",kServicePath]
#define kAddUserCarWS [NSString stringWithFormat:@"%@/Car/addCar",kServicePath]
#define kUpdateUserCarWS [NSString stringWithFormat:@"%@/Car/updateCar",kServicePath]
#define kFetchCategoriesWS [NSString stringWithFormat:@"%@/Category/getCategories",kServicePath]
#define kFetchVehicleServicesWS [NSString stringWithFormat:@"%@/Service/getSupplierWiseServicesByCarTypeAndCategories",kServicePath]
#define kFetchHolidayWS [NSString stringWithFormat:@"%@/Shift/getHolidays",kServicePath]
//#define kFetchAppointmentListWS [NSString stringWithFormat:@"%@/Request/getRequestDate",kServicePath] //Old
#define kFetchAppointmentListWS [NSString stringWithFormat:@"%@/Request/getAppointmentsDateTime",kServicePath] //New
#define kFetchCurrencyWS [NSString stringWithFormat:@"%@/Currency/getCurrencyAndExchangeRateFromCountryCode",kServicePath]
#define kBookAppointmentWS [NSString stringWithFormat:@"%@/Request/addRequestJob",kServicePath]
#define kUpdateProfileWS [NSString stringWithFormat:@"%@/User/updateProfile",kServicePath]
#define kFetchAllCurrencyWS [NSString stringWithFormat:@"%@/Currency/getAllCurrency",kServicePath]
#define kChangePasswordWS [NSString stringWithFormat:@"%@/User/changePassword",kServicePath]
#define kAboutUsWS [NSString stringWithFormat:@"%@/InformationMaster/getAboutus",kServicePath]
#define kPrivacyPolicyWS [NSString stringWithFormat:@"%@/InformationMaster/getPolicy",kServicePath]
#define kOurPartnersWS [NSString stringWithFormat:@"%@/Van/getOwnerList",kServicePath]
#define kContactUsWS [NSString stringWithFormat:@"%@/Contact/getAllContacts",kServicePath]
#define kAddPartnersWS [NSString stringWithFormat:@"%@/OurPartners/addOurPartners",kServicePath]
#define kSocialLoginWS [NSString stringWithFormat:@"%@/User/socialLogin",kServicePath]
#define kRatingWS [NSString stringWithFormat:@"%@/Rate/getRateQuestions",kServicePath]
#define kAddRatingWS [NSString stringWithFormat:@"%@/Rate/addRateOnRequest",kServicePath]
#define kDeleteAddressWS [NSString stringWithFormat:@"%@/Address/deleteAddress",kServicePath]
#define kUpdateAddressWS [NSString stringWithFormat:@"%@/Address/updateAddress",kServicePath]
#define kDeleteCarWS [NSString stringWithFormat:@"%@/Car/deleteCar",kServicePath]
#define kUpdateRequestWS [NSString stringWithFormat:@"%@/Request/updateRequest",kServicePath]
#define kCancelRequestWS [NSString stringWithFormat:@"%@/Request/cancelRequest",kServicePath]
#define kUpdateLanguageWS [NSString stringWithFormat:@"%@/User/updateLanguage",kServicePath]
#define kCheckForceUpdateWS [NSString stringWithFormat:@"%@/version/ios.json",kHostPath]
//#define kFetchOtherAppointmentListWS [NSString stringWithFormat:@"%@/Request/getRequestDateForAnotherAppointment",kServicePath] //Old
#define kFetchOtherAppointmentListWS [NSString stringWithFormat:@"%@/Request/getAppointmentsDateTimeForAnotherAppointment",kServicePath] //New
#define kFetchSupportContactWS [NSString stringWithFormat:@"%@/Contact/getSupportContact",kServicePath]
#define kFetchServicesByDateWS [NSString stringWithFormat:@"%@/Service/getDateWiseServicesByCarTypeAndCategories",kServicePath]

#pragma mark : --------- :  ModelClass key
#define kWSUserClass @"WSUser"
#define kWSNearByVansClass @"WSNearByVans"
#define kWSJobRequestsClass @"WSJobRequests"
#define kWSCarTypeClass @"WSCartype"
#define kWSUserCarClass @"WSCar"
#define kWSUserAddressesClass @"WSUserAddresses"
#define kWSCategoryClass @"WSCategory"
#define kWSVehicleSupplier @"WSSupplierServices"
#define kWSVehicleServiceClass @"WSServices"
#define kWSHolidayListClass @"WSHolidayList"
#define kWSAppointmentListClass @"WSAppointmentList"
#define kWSAllCurrencytClass @"WSAllCurrency"
#define kWSOurPartnersClass @"WSOurPartners"
#define kWSContactUsClass @"WSContactUs"
#define kWSRatingClass @"WSRating"
#define kWSJobRequestsUpdateClass @"WSJobRequestsUpdate"
#define kWSSupportContactClass @"WSContact"

#pragma mark : --------- :  Instagram URL
#define INSTAGRAM_AUTH_URL @"https://api.instagram.com/oauth/authorize/"
#define INSTAGRAM_API_URL  @"https://api.instagram.com/v1/users/"
#define INSTAGRAM_CLIENT_ID  @"bfc564b08f0a4fdd952ef69cb05023cd"
#define INSTAGRAM_CLIENT_SERCRET @"0de34c3d025143a297ce044f4fecf498"
#define INSTAGRAM_REDIRECT_URL @"http://www.carspaco.com"
#define INSTAGRAM_ACCESS_TOKEN @"access_token"
#define INSTAGRAM_SCOPE  @"likes+comments+relationships"
#define INSTAGRAM_ACCESS_TOKEN_URL  @"https://api.instagram.com/oauth/access_token"

#pragma mark : --------- : Service Key
#define kServiceResponseKey @"Data"
#define kServiceResponseMsgKey @"Message"
#define kServiceResponseMsgARKey @"MessageAR"
#define kServiceResponseDateKey @"Dates"
#define kAppVersionServiceKey @"appVersion"


#endif /* WSConstants_h */
