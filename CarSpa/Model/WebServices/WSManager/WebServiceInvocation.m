//
//  WebServiceInvocation.m
//  R@jan 012
//
//  Created by R@j012 on 08/02/17.
//  Copyright © 2017 R@j012. All rights reserved.
//

#import "WebServiceInvocation.h"
#import "WebServiceParser.h"
#import "WebServiceSyncData.h"



#define DEFAULT_TIMEOUT 60.0f


@implementation WebServiceInvocation

- (BOOL)checkInternetConnection{
    return [ReachabilityManager isReachable];
}

#pragma mark : --------- :  Invocation
-(void)initWithWS:(NSString *)WSname withParams:(NSDictionary *)params withServiceType:(WebServiceType)type withSelector:(SEL)selector withTarget:(id)target showLoader:(BOOL)loader loaderMsg:(NSString *)msg{
    
    
    if (![self checkInternetConnection]){
        _responseCode = 204;
        _responseErr = kNoNetworkMsg;
        [target performSelectorOnMainThread:selector withObject:self waitUntilDone:false];
        return;
    }
    
    NSMutableURLRequest *urlRequest;
    
    _responseCode = 200;
    _responseErr = [[NSString alloc]init];
    _responseArray = [[NSArray alloc]init];
    DisplayNetworkIndicator(YES);
    
    
    
  
    switch (type) {
        case  TYPE_GET:{
            urlRequest = [self setGetDataInRequest:WSname withParam:params];
        }
            break;
        case  TYPE_POST:{
            
            NSString *appVersion = [NSString stringWithFormat:@"%@",[self fetchCurrentAppVersion]] ;
            if(params!=nil){
                NSMutableDictionary *mutableDictionary = [params mutableCopy];
                [mutableDictionary setObject:appVersion forKey:kAppVersionServiceKey];
                params = [mutableDictionary mutableCopy];
            }

            if([WSname isEqualToString:kUpdateProfileWS] || [WSname isEqualToString:kAddPartnersWS]){
                urlRequest = [self setPostRequestForWS:WSname withParameters:params];
            }else{
                 urlRequest = [self setPostDataInRequest:WSname withParam:params withTypeJSON:NO];
            }
           
        }
            break;
        case  TYPE_JSON:{
            NSString *appVersion = [NSString stringWithFormat:@"%@",[self fetchCurrentAppVersion]] ;
            if(params!=nil){
                NSMutableDictionary *mutableDictionary = [params mutableCopy];
                [mutableDictionary setObject:appVersion forKey:kAppVersionServiceKey];
                params = [mutableDictionary mutableCopy];
            }

            urlRequest = [self setPostDataInRequest:WSname withParam:params withTypeJSON:YES];
        }
            break;
            
        default:
            break;
    }
    
  //  NSLog(@"URL : %@ \n Param : %@",WSname,params);
    //show loader with msg
    if(loader)
        [RTProgressHUD showProgressWithMsg:msg];
    
    NSString *googleURL = [NSString stringWithFormat:@"%@", urlRequest.URL];
    [[WebServiceParser defaultManager]parserResponseWithURL:urlRequest withCompletionBlock:^(id responseObjects,NSString *responseString,NSError *err) {
        
        if (err){
            _responseCode = 201; //No Response from Server or Internal Error
            _responseErr = [err localizedDescription];
            DisplayNetworkIndicator(NO);
        }
        else{
            if ([responseString isEqualToString:kNoDataAvailable]){
                _responseCode = 202; //Data Not Available.
                _responseErr = kNoDataAvailable;
                DisplayNetworkIndicator(NO);
            }
            else if ([responseString isEqualToString:kServerIssueFailure]){
                _responseCode = 203; //Response Issue
                _responseErr = kIssueInResponse;
                DisplayNetworkIndicator(NO);
            }
            else if ([responseObjects isKindOfClass:[NSDictionary class]]){
            
                if ([googleURL containsString:@"api/distancematrix"] || [googleURL containsString:@"api/directions"]){
                    _responseCode = 200; //Status OK
                    _responseDictionary = [NSDictionary dictionaryWithDictionary:responseObjects];
                    DisplayNetworkIndicator(NO);
                    [target performSelectorOnMainThread:selector withObject:self waitUntilDone:NO];
                    return;
                }
                if ([[responseObjects valueForKey:@"Status"]intValue]  == 2 || [[responseObjects valueForKey:@"Status"]intValue]  == 3  ){
                    _responseCode = 200; //Status OK
                    _responseDictionary = [NSDictionary dictionaryWithDictionary:responseObjects];
                    _responseArray = [[WebServiceSyncData alloc]syncData:WSname withParsedResponse:responseObjects ];
                    DisplayNetworkIndicator(NO);
                   [target performSelectorOnMainThread:selector withObject:self waitUntilDone:NO];
                    return;
                }
                if ([[responseObjects valueForKey:@"Status"]intValue]  != 1){
                    _responseCode = 203; //Error with Request
                    _responseErr = [responseObjects valueForKey:@"Message"];
                    _responseArray = [[WebServiceSyncData alloc]syncData:WSname withParsedResponse:responseObjects ];
                    DisplayNetworkIndicator(NO);
                }
                else{
                    _responseCode = 200; //Status OK
                    _responseDictionary = [NSDictionary dictionaryWithDictionary:responseObjects];
                    _responseArray = [[WebServiceSyncData alloc]syncData:WSname withParsedResponse:responseObjects ];
                    DisplayNetworkIndicator(NO);
                }
                
            }
        }
        [target performSelectorOnMainThread:selector withObject:self waitUntilDone:NO];
    }];
    
}

#pragma mark : --------- :  Setting Parmeters

-(NSMutableURLRequest *)setGetDataInRequest:(NSString *)url withParam:(NSDictionary *)dictParm{
    
    NSString *urlString = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    NSMutableURLRequest   *request = [NSMutableURLRequest requestWithURL:
                                      [NSURL URLWithString:urlString]
                                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                         timeoutInterval:DEFAULT_TIMEOUT];
    [request setHTTPMethod:@"GET"];
     [self setHeaderAuthorization:request];
    if(dictParm != nil){
        NSData *requestData = [NSJSONSerialization dataWithJSONObject:dictParm options:0 error:nil];
        [request setHTTPBody:requestData];
    }
    return request;
}

-(NSMutableURLRequest *)setPostDataInRequest:(NSString *)url withParam:(NSDictionary *)dictParm withTypeJSON:(BOOL)isJSON{
    
    NSString *strUrl = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    NSMutableURLRequest   *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                         timeoutInterval:DEFAULT_TIMEOUT];
    
    NSData *requestData;
    if(isJSON){
        
        requestData = [self httpBodyForJsonParameters:dictParm];
        NSDictionary *headerFields = @{@"Content-Type": @"application/json",
                                       @"Accept": @"application/json" };
        [request setAllHTTPHeaderFields:headerFields];
        [self setHeaderAuthorization:request];
        
    }else{
        
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [self setHeaderAuthorization:request];
        requestData = [self httpBodyForPostParameters:dictParm];
    }
    
    [request setHTTPMethod:@"POST"];
    [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    return request;
}

-(void)setHeaderAuthorization:(NSMutableURLRequest *)request{
    [request setValue:kAuth_UserName forHTTPHeaderField:@"username"];
    [request setValue:kAuth_Password forHTTPHeaderField:@"password"];
    [request setValue:kAuth_Key forHTTPHeaderField:@"Authorization"];
}
- (NSMutableURLRequest *)setPostRequestForWS:(NSString *)url withParameters:(NSDictionary *)dict{
    
    NSString *urlString = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:[NSURL URLWithString:urlString]
                                    cachePolicy:NSURLRequestUseProtocolCachePolicy
                                    timeoutInterval:DEFAULT_TIMEOUT];
    
    
    NSMutableData *body = [NSMutableData data]  ;
    [request setHTTPMethod:@"POST"];
    [self setHeaderAuthorization:request];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];


    
    for(NSString *keys in dict){
        
        if([keys isEqualToString:@"userImg"]){
            
            UIImage *img = [dict objectForKey:@"userImg"];
            NSData *imgData=UIImageJPEGRepresentation(img,1);
            
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Disposition: form-data; name=\"file\"; filename=\"temp.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:imgData]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
       else if([keys isEqualToString:@"documentone"] || [keys isEqualToString:@"documenttwo"] ||[keys isEqualToString:@"documentthree"]){

            UIImage *img = [dict objectForKey:keys];
            NSData *imgData=UIImageJPEGRepresentation(img,0.7);
           
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"temp.jpg\"\r\n",keys] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:imgData]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        else{
            
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",keys] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n",[dict valueForKey:keys]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
    
    }
    
    [request setHTTPBody:body];
    [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
    
    return request;
}

#pragma mark : --------- : Extratcting Request Parameters

-(NSData *)httpBodyForJsonParameters:(NSDictionary *)parameters {
    
    NSError *error = nil;
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&error];
    
    if(error!=nil){
        return nil;
    }
    return requestData;
}



- (NSData *)httpBodyForPostParameters:(NSDictionary *)parameters {
    
    NSMutableArray * content = [NSMutableArray array];
    for(NSString * key in parameters){
            [content  addObject: [NSString stringWithFormat: @"%@=%@", key, parameters[key]]];
    }

    NSString * body = [content componentsJoinedByString: @"&"];
    NSData * bodyData = [body dataUsingEncoding: NSUTF8StringEncoding];
    
    return bodyData;
    
}

- (NSString *)getStringFromDictionary:(NSDictionary *)dict{
    
    NSError * err;
    
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString *convertedString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    if(err!=nil){
        return @"";
    }
    return convertedString;
}

- (NSString *)percentEscapeString:(NSString *)string {
    
    if([string isKindOfClass:[NSString class]]){
        NSCharacterSet *allowed = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~"];
        return [string stringByAddingPercentEncodingWithAllowedCharacters:allowed];
    }
    return string;
    
}

-(NSDictionary *)addVersionNoInDictParams:(NSDictionary *)params{
    NSString *appVersion = [NSString stringWithFormat:@"%@",[self fetchCurrentAppVersion]] ;
    if(params!=nil){
        NSMutableDictionary *mutableDictionary = [params mutableCopy];
        [mutableDictionary setObject:appVersion forKey:kAppVersionServiceKey];
        params = [mutableDictionary mutableCopy];
    }
    return params;
}
-(NSString *)fetchCurrentAppVersion{
    NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
    NSString  *appVersion = [info objectForKey:@"CFBundleShortVersionString"];
    return appVersion;
}

@end
