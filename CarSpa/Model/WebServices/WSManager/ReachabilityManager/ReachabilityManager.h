//
//  ReachabilityManager.h
//  R@jan 012
//
//  Created by Rajan Tandel on 08/02/17.
//  Copyright © 2017 R@jan012. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"


@interface ReachabilityManager : NSObject

@property (nonatomic) Reachability *internetReachability;
@property (nonatomic) Reachability *hostReachability;
@property (nonatomic,assign)BOOL isNetworkReachable;

+(id)defaultManager;
+(BOOL)isReachable;
@end
