//
//  ReachabilityManager.m
//  R@jan 012
//
//  Created by Rajan Tandel on 08/02/17.
//  Copyright © 2017 R@jan012. All rights reserved.
//

#import "ReachabilityManager.h"

@implementation ReachabilityManager

#pragma mark : --------- :  Singleton Allocation

+(id)defaultManager{
    
    static ReachabilityManager *myReachabilityManger = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        myReachabilityManger = [[ReachabilityManager alloc]init];
    });
    return myReachabilityManger;
    
}

-(id)init{
    
    if(self = [super init]){
       [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
        
        _isNetworkReachable = NO;
        self.internetReachability = [Reachability reachabilityForInternetConnection];
        [self.internetReachability startNotifier];
        [self updateStatusWithReachability:self.internetReachability];
        
        self.hostReachability = [Reachability reachabilityWithHostName:@"www.google.com"];
        [self.hostReachability startNotifier];
        [self updateStatusWithReachability:self.hostReachability];
    }
    return self;
}

#pragma mark : --------- :  Checking Reachability

+(BOOL)isReachable{
    return [[ReachabilityManager defaultManager] isNetworkReachable];
}

- (void)reachabilityChanged:(NSNotification *)note{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self updateStatusWithReachability:curReach];
   
}

- (void)updateStatusWithReachability:(Reachability *)reachability{
    
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    switch (netStatus){
        case NotReachable:{
            _isNetworkReachable = NO;
            break;
        }
        case ReachableViaWWAN:{
            _isNetworkReachable = YES;
            break;
        }
        case ReachableViaWiFi:{
            _isNetworkReachable = YES;
            break;
        }
    }

}

#pragma mark : --------- :  Observer DeAllocation
-(void)dealloc{
    
    if(_internetReachability){
         [_internetReachability stopNotifier];
    }
    if(_hostReachability){
        [_hostReachability stopNotifier];
    }
  
   [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

@end
