//
//  WebServiceSyncData.h
//  R@jan 012
//
//  Created by Rajan Tandel on 13/02/17.
//  Copyright © 2017 R@jan012. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebServiceSyncData : NSObject
-(NSMutableArray *)syncData:(NSString *)methodName withParsedResponse:(NSDictionary *)dictResponse;
@end
