//
//  WebServiceParser.h
//  R@jan 012
//
//  Created by Rajan Tandel on 08/02/17.
//  Copyright © 2017 R@jan012. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebServiceParser : NSObject
{
    Reachability *hostReachability;
}
@property(strong,nonatomic)NSOperationQueue *operationQueue;


+(id)defaultManager;

typedef void(^webParserCompletionBlock)(id responseObjects,NSString *responseString, NSError *err);

-(void)parserResponseWithURL:(NSMutableURLRequest *)url withCompletionBlock:(webParserCompletionBlock)completionBlock;

@end
