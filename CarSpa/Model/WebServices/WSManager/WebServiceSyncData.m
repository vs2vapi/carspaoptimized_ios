//
//  WebServiceSyncData.m
//  R@jan 012
//
//  Created by Rajan Tandel on 13/02/17.
//  Copyright © 2017 R@jan012. All rights reserved.
//

#import "WebServiceSyncData.h"
#import "SyncCDAdaptor.h"
#import "WSUser.h"

@implementation WebServiceSyncData
-(NSMutableArray *)syncData:(NSString *)methodName withParsedResponse:(NSDictionary *)dictResponse{
    
    NSMutableArray *arrParsedResponse = [NSMutableArray new];
    if([methodName isEqualToString:kLoginWS] || [methodName isEqualToString:kVerifyOTPWS]){
        
        arrParsedResponse = [[self parseJSONData:dictResponse withClass:kWSUserClass withJSONKey:kServiceResponseKey]mutableCopy];
        
        WSUser *objUser = [arrParsedResponse objectAtIndex:0];
        [Functions setStringValueToUserDefaults:[NSString stringWithFormat:@"%lu",objUser.internalBaseClassIdentifier] withKey:USER_ID];
        [Functions setCustomObjectToDefaults:objUser withkey:USER_OBJ];
        
        [[SyncCDAdaptor sycnInstance]saveUserDetails:arrParsedResponse];
        
        return  arrParsedResponse;
    }
 else   if([methodName isEqualToString:kUpdateProfileWS]){
        
        arrParsedResponse = [[self parseJSONData:dictResponse withClass:kWSUserClass withJSONKey:kServiceResponseKey]mutableCopy];
        WSUser *objUser = [arrParsedResponse objectAtIndex:0];
        [Functions setCustomObjectToDefaults:objUser withkey:USER_OBJ];
        
        [[SyncCDAdaptor sycnInstance]updateUserDetails:arrParsedResponse];
        
        return  arrParsedResponse;
    }
   else if([methodName isEqualToString:kFetchNearByVansWS]){
        arrParsedResponse = [[self parseJSONData:dictResponse withClass:kWSNearByVansClass withJSONKey:kServiceResponseKey]mutableCopy];
        return  arrParsedResponse;
    }
   else if([methodName containsString:kFetchRequestWS] || [methodName containsString:kFetchHistoryWS]){
       arrParsedResponse = [[self parseJSONData:dictResponse withClass:kWSJobRequestsClass withJSONKey:kServiceResponseKey]mutableCopy];
       return  arrParsedResponse;
   }
   else if([methodName containsString:kFetchCarTypeWS]){
       arrParsedResponse = [[self parseJSONData:dictResponse withClass:kWSCarTypeClass withJSONKey:kServiceResponseKey]mutableCopy];
       return  arrParsedResponse;
   }
   else if([methodName containsString:kFetchUserCarsWS]){
       arrParsedResponse = [[self parseJSONData:dictResponse withClass:kWSUserCarClass withJSONKey:kServiceResponseKey]mutableCopy];
       return  arrParsedResponse;
   }
   else if([methodName containsString:kFetchUserAddressWS]){
       arrParsedResponse = [[self parseJSONData:dictResponse withClass:kWSUserAddressesClass withJSONKey:kServiceResponseKey]mutableCopy];
       return  arrParsedResponse;
   }
   else if([methodName isEqualToString:kAddUserAddressWS]){
       arrParsedResponse = [[self parseJSONData:dictResponse withClass:[NSString stringWithFormat:@"%@1",kWSUserAddressesClass] withJSONKey:kServiceResponseKey]mutableCopy];
       [[SyncCDAdaptor sycnInstance]saveAddressDetails:[arrParsedResponse objectAtIndex:0]];
       return  arrParsedResponse;
   }
   else if([methodName isEqualToString:kUpdateAddressWS]){
       arrParsedResponse = [[self parseJSONData:dictResponse withClass:[NSString stringWithFormat:@"%@1",kWSUserAddressesClass] withJSONKey:kServiceResponseKey]mutableCopy];
       [[SyncCDAdaptor sycnInstance]updateAddressDetails:[arrParsedResponse objectAtIndex:0]];
       return  arrParsedResponse;
   }
   else if([methodName containsString:kFetchCategoriesWS]){
       arrParsedResponse = [[self parseJSONData:dictResponse withClass:kWSCategoryClass withJSONKey:kServiceResponseKey]mutableCopy];
       return  arrParsedResponse;
   }
   else if([methodName containsString:kFetchVehicleServicesWS] || [methodName containsString:kFetchServicesByDateWS]){
       arrParsedResponse = [[self parseJSONData:dictResponse withClass:kWSVehicleSupplier withJSONKey:kServiceResponseKey]mutableCopy];
       return  arrParsedResponse;
   }
   else if([methodName isEqualToString:kFetchHolidayWS]){
       arrParsedResponse = [[self parseJSONData:dictResponse withClass:kWSHolidayListClass withJSONKey:kServiceResponseKey]mutableCopy];
       return  arrParsedResponse;
   }
   else if([methodName containsString:kFetchAppointmentListWS]){
       arrParsedResponse = [[self parseJSONData:dictResponse withClass:kWSAppointmentListClass withJSONKey:kServiceResponseKey]mutableCopy];
       return  arrParsedResponse;
   }
   else if([methodName containsString:kFetchAllCurrencyWS]){
       arrParsedResponse = [[self parseJSONData:dictResponse withClass:kWSAllCurrencytClass withJSONKey:kServiceResponseKey]mutableCopy];
       return  arrParsedResponse;
   }
   else if([methodName containsString:kOurPartnersWS]){
       arrParsedResponse = [[self parseJSONData:dictResponse withClass:kWSOurPartnersClass withJSONKey:kServiceResponseKey]mutableCopy];
       return  arrParsedResponse;
   }
   else if([methodName containsString:kContactUsWS]){
       arrParsedResponse = [[self parseJSONData:dictResponse withClass:kWSContactUsClass withJSONKey:kServiceResponseKey]mutableCopy];
       return  arrParsedResponse;
   }
   else if([methodName isEqualToString:kSocialLoginWS]){
       
       arrParsedResponse = [[self parseJSONData:dictResponse withClass:kWSUserClass withJSONKey:kServiceResponseKey]mutableCopy];
       
       WSUser *objUser = [arrParsedResponse objectAtIndex:0];
       if(objUser.internalBaseClassIdentifier>0){
           [Functions setStringValueToUserDefaults:[NSString stringWithFormat:@"%lu",objUser.internalBaseClassIdentifier] withKey:USER_ID];
           [Functions setCustomObjectToDefaults:objUser withkey:USER_OBJ];
           
           [[SyncCDAdaptor sycnInstance]saveUserDetails:arrParsedResponse];
       }
    
   }
   else if([methodName containsString:kRatingWS]){
     arrParsedResponse = [[self parseJSONData:dictResponse withClass:kWSRatingClass withJSONKey:kServiceResponseKey]mutableCopy];
       return  arrParsedResponse;
   }
   else if([methodName isEqualToString:kUpdateRequestWS]){
       arrParsedResponse = [[self parseJSONData:dictResponse withClass:kWSJobRequestsUpdateClass withJSONKey:kServiceResponseKey]mutableCopy];
       return  arrParsedResponse;
   }
   else if([methodName containsString:kFetchSupportContactWS]){
       arrParsedResponse = [[self parseJSONData:dictResponse withClass:kWSSupportContactClass withJSONKey:kServiceResponseKey]mutableCopy];
       return  arrParsedResponse;
   }
    

    return arrParsedResponse;
}

-(NSArray *)parseJSONData:(NSDictionary *)dict withClass:(NSString *)className withJSONKey:(NSString *)key{
    
    NSMutableArray *arrParsedData = [NSMutableArray new];
    
    if([className isEqualToString:kWSUserClass]){
        
        NSDictionary *allValues = [dict objectForKey:key];
        id classFromStr = [[[NSClassFromString(className) alloc] init] initWithDictionary:allValues];
        [arrParsedData addObject:classFromStr];
        return arrParsedData;
    }
    else  if([className isEqualToString:kWSNearByVansClass]){
      
        for(int i=0;i<[[dict objectForKey:key]count];i++){
            id classFromStr = [[[NSClassFromString(className) alloc] init] initWithDictionary:[[dict objectForKey:key]objectAtIndex:i]];
            [arrParsedData addObject:classFromStr];
        }
         return arrParsedData;
    }
    else  if([className isEqualToString:kWSJobRequestsClass]){
        
        for(int i=0;i<[[dict objectForKey:key]count];i++){
            id classFromStr = [[[NSClassFromString(className) alloc] init] initWithDictionary:[[dict objectForKey:key]objectAtIndex:i]];
            [arrParsedData addObject:classFromStr];
        }
         return arrParsedData;
    }
    else  if([className isEqualToString:kWSJobRequestsUpdateClass]){
        
      //  for(int i=0;i<[[dict objectForKey:key]count];i++){
            id classFromStr = [[[NSClassFromString(kWSJobRequestsClass) alloc] init] initWithDictionary:[dict objectForKey:key]];
            [arrParsedData addObject:classFromStr];
       // }
        return arrParsedData;
    }
    else  if([className isEqualToString:kWSCarTypeClass]){
        
        for(int i=0;i<[[dict objectForKey:key]count];i++){
            id classFromStr = [[[NSClassFromString(className) alloc] init] initWithDictionary:[[dict objectForKey:key]objectAtIndex:i]];
            [arrParsedData addObject:classFromStr];
        }
         return arrParsedData;
        
    }
    else  if([className isEqualToString:kWSUserCarClass]){
        
        for(int i=0;i<[[dict objectForKey:key]count];i++){
            id classFromStr = [[[NSClassFromString(className) alloc] init] initWithDictionary:[[dict objectForKey:key]objectAtIndex:i]];
            [arrParsedData addObject:classFromStr];
        }
        return arrParsedData;
        
    }
    else  if([className isEqualToString:kWSUserAddressesClass]){
        
        for(int i=0;i<[[dict objectForKey:key]count];i++){
            id classFromStr = [[[NSClassFromString(className) alloc] init] initWithDictionary:[[dict objectForKey:key]objectAtIndex:i]];
            [arrParsedData addObject:classFromStr];
        }
        return arrParsedData;
        
    }
    else  if([className isEqualToString:[NSString stringWithFormat:@"%@1",kWSUserAddressesClass]]){
        
        id classFromStr = [[[NSClassFromString(kWSUserAddressesClass) alloc] init] initWithDictionary:[dict objectForKey:key]];
        [arrParsedData addObject:classFromStr];
        
        return arrParsedData;
        
    }
    else  if([className isEqualToString:kWSCategoryClass]){
        
        for(int i=0;i<[[dict objectForKey:key]count];i++){
            id classFromStr = [[[NSClassFromString(className) alloc] init] initWithDictionary:[[dict objectForKey:key]objectAtIndex:i]];
            [arrParsedData addObject:classFromStr];
        }
        return arrParsedData;
    }
    else  if([className isEqualToString:kWSVehicleSupplier]){
        
        for(int i=0;i<[[dict objectForKey:key]count];i++){
            id classFromStr = [[[NSClassFromString(className) alloc] init] initWithDictionary:[[dict objectForKey:key]objectAtIndex:i]];
            [arrParsedData addObject:classFromStr];
        }
        return arrParsedData;
        
    }
    else  if([className isEqualToString:kWSHolidayListClass]){
        
        for(int i=0;i<[[dict objectForKey:key]count];i++){
            id classFromStr = [[[NSClassFromString(className) alloc] init] initWithDictionary:[[dict objectForKey:key]objectAtIndex:i]];
            [arrParsedData addObject:classFromStr];
        }
        return arrParsedData;
        
    }
    else  if([className isEqualToString:kWSAppointmentListClass]){
        
        NSDictionary *dataDict = [dict objectForKey:key];
        for(int i=0;i<[[dataDict objectForKey:kServiceResponseDateKey]count];i++){
            id classFromStr = [[[NSClassFromString(className) alloc] init] initWithDictionary:[[dataDict objectForKey:kServiceResponseDateKey]objectAtIndex:i]];
            [arrParsedData addObject:classFromStr];
        }
        return arrParsedData;
    }
    else  if([className isEqualToString:kWSAllCurrencytClass]){
        
        for(int i=0;i<[[dict objectForKey:key]count];i++){
            id classFromStr = [[[NSClassFromString(className) alloc] init] initWithDictionary:[[dict objectForKey:key]objectAtIndex:i]];
            [arrParsedData addObject:classFromStr];
        }
        return arrParsedData;
        
    }
    else  if([className isEqualToString:kWSOurPartnersClass]){
        
        for(int i=0;i<[[dict objectForKey:key]count];i++){
            id classFromStr = [[[NSClassFromString(className) alloc] init] initWithDictionary:[[dict objectForKey:key]objectAtIndex:i]];
            [arrParsedData addObject:classFromStr];
        }
        return arrParsedData;
        
    }
    else  if([className isEqualToString:kWSContactUsClass]){
        
        for(int i=0;i<[[dict objectForKey:key]count];i++){
            NSArray *contactArr =(NSArray *)[[[dict objectForKey:key]objectAtIndex:i]objectForKey:@"contact"];
            if([contactArr isKindOfClass:[NSArray class]]){
                id classFromStr = [[[NSClassFromString(className) alloc] init] initWithDictionary:[[dict objectForKey:key]objectAtIndex:i]];
                [arrParsedData addObject:classFromStr];
            }
          
        }
        return arrParsedData;
    }
    else  if([className isEqualToString:kWSRatingClass]){
        
        for(int i=0;i<[[dict objectForKey:key]count];i++){
            id classFromStr = [[[NSClassFromString(className) alloc] init] initWithDictionary:[[dict objectForKey:key]objectAtIndex:i]];
            [arrParsedData addObject:classFromStr];
        }
        return arrParsedData;
        
    }
    else  if([className isEqualToString:kWSSupportContactClass]){
        
        for(int i=0;i<[[dict objectForKey:key]count];i++){
            id classFromStr = [[[NSClassFromString(className) alloc] init] initWithDictionary:[[dict objectForKey:key]objectAtIndex:i]];
            [arrParsedData addObject:classFromStr];
        }
        return arrParsedData;
        
    }

    
    return arrParsedData;
}

@end
