//
//  WebServiceInvocation.h
//  R@jan 012
//
//  Created by Rajan Tandel on 08/02/17.
//  Copyright © 2017 R@jan012. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    TYPE_GET = 0,
    TYPE_POST = 1,
    TYPE_JSON = 2
} WebServiceType;

@interface WebServiceInvocation : NSObject


@property(strong,nonatomic) NSDictionary *responseDictionary;
@property(strong,nonatomic)NSArray *responseArray;
@property(nonatomic,assign)NSInteger responseCode;
@property(strong,nonatomic)NSString *responseErr;

-(void)initWithWS:(NSString *)WSname withParams:(NSDictionary *)params withServiceType:(WebServiceType)type withSelector:(SEL)selector withTarget:(id)target showLoader:(BOOL)loader loaderMsg:(NSString *)msg;

@end
