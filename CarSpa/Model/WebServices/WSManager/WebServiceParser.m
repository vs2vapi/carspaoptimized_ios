//
//  WebServiceParser.m
//  R@jan 012
//
//  Created by R@j012 on 08/02/17.
//  Copyright © 2017 R@j012. All rights reserved.
//

#import "WebServiceParser.h"

@implementation WebServiceParser
+(id)defaultManager{
    
    static WebServiceParser *myParserManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        myParserManager = [[WebServiceParser alloc]init];
    });
    return myParserManager;
    
}

-(id)init{
    
    if(self = [super init]){
        
        //Check reachability for host
        hostReachability = [Reachability reachabilityWithHostName:@"www.google.com"];
        [hostReachability startNotifier];
        
        //Initialize Operation Queue
        _operationQueue = [[NSOperationQueue alloc] init];
        _operationQueue.maxConcurrentOperationCount = 1;
    }
    
    return self;
    
}

-(void)parserResponseWithURL:(NSMutableURLRequest *)url withCompletionBlock:(webParserCompletionBlock)completionBlock{
    
    
    //Block Operation to add response block in Operation Queue
    NSBlockOperation *blockOperation = [NSBlockOperation blockOperationWithBlock:^{
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            __block id resObj = nil;
            NSURLSession *session = [NSURLSession sharedSession];
            
            NSURLSessionDataTask *task = [session dataTaskWithRequest:url
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              
                                              NSString *strResponseString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                              
                                              if (data.length > 0){
                                                  
                                                  id responseValues = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                  
                                                  if([responseValues isKindOfClass:[NSDictionary class]]){
                                                      if([(NSDictionary *)responseValues count] <= 0)
                                                          strResponseString = kNoDataAvailable;
                                                  }
                                                  resObj = responseValues;
                                              }
                                              else if(error != nil){
                                                  strResponseString = kServerIssueFailure;
                                              }
                                              else if(data.length == 0){
                                                  strResponseString = kNoDataAvailable;
                                              }
                                              dispatch_sync(dispatch_get_main_queue(), ^{
                                                  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                                      if (error) {
                                                          completionBlock(resObj ,strResponseString,error);
                                                      }
                                                      else {
                                                          completionBlock(resObj ,strResponseString,error);
                                                      }
                                                  }];
                                              });
                                          }];
            
            [task resume];
            
        });
    }];
    
    [blockOperation setQueuePriority:NSOperationQueuePriorityNormal];
    [self.operationQueue addOperation:blockOperation];
}
@end
