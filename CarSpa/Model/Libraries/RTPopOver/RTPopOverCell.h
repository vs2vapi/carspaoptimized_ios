//
//  RTPopOverCell.h
//  EnjayLatitude
//
//  Created by Rajan Tandel on 23/02/17.
//  Copyright © 2017 Enjay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTPopOverCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblOptionTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwOptions;

@end
