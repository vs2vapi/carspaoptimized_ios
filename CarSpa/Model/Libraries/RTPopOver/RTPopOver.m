//
//  RTPopOver.m
//  EnjayLatitude
//
//  Created by Rajan Tandel on 03/02/17.
//  Copyright © 2017 Enjay. All rights reserved.
//

#import "RTPopOver.h"
#import "RTPopOverCell.h"



@interface RTPopOver ()<UIGestureRecognizerDelegate>{
    NSInteger tblHeight;
}

@property(strong,nonatomic) UITapGestureRecognizer *tapRecognizer;

@end

@implementation RTPopOver
@synthesize heightOfMenuItem = _heightOfMenuItem;
@synthesize  mutableArrOptions = _mutableArrOptions;

#pragma mark : --------- :  Allocations
-(instancetype)initWithFrame:(CGRect)frame withPosition:(PopOverPostion)pos withPopOverOrigin:(CGFloat)originY withPopOverOptions:(NSArray *)arrOptions withImages:(NSArray *)arrImages{
    
    self = [super initWithFrame:frame];
    if(self){
        
        // load .xib
         [[NSBundle mainBundle]loadNibNamed:@"RTPopOver" owner:self options:nil];

        
        _bgVw.frame = CGRectMake(0, 0, frame.size.width,frame.size.height);
          self.bounds = _bgVw.bounds;
       
        //add it as subview
        [self addSubview:_bgVw];
        
        //Basic Setups
        _tblVwList.tableFooterView = [UIView new];
        _mutableArrOptions = [NSMutableArray arrayWithArray:arrOptions];
          _mutableArrImages = [NSMutableArray arrayWithArray:arrImages];
        [_tblVwList reloadData];
        
       tblHeight   = arrOptions.count * 44;
       _containerVw.layer.cornerRadius = 6.0f;
        
        [self setTapGesture];
        [self setPos:pos];
        [self setOriginY:originY];
 
        [self configPopOver:_pos withPopOverOrigin:_originY];
      
        if(pos == PopOverPositon_Right){
            _tblVwList.frame = CGRectMake(_tblVwList.frame.origin.x,_tblVwList.frame.origin.y, _tblVwList.frame.size.width,tblHeight);
            _containerVw.frame = CGRectMake(_containerVw.frame.origin.x, _containerVw.frame.origin.y, _containerVw.frame.size.width,tblHeight + 16);
             _tblVwList.scrollEnabled = NO;
        }else{
            _tblVwList.frame = CGRectMake(0,_tblVwList.frame.origin.y, _tblVwList.frame.size.width,tblHeight);
            _containerVw.frame = CGRectMake(_containerVw.frame.origin.x, _containerVw.frame.origin.y, _containerVw.frame.size.width,tblHeight + 16);
            _tblVwList.scrollEnabled = NO;
//            _tblVwList.frame = CGRectMake(0,_tblVwList.frame.origin.y, _tblVwList.frame.size.width,300);
//            _containerVw.frame = CGRectMake(_containerVw.frame.origin.x, _containerVw.frame.origin.y, _containerVw.frame.size.width, _tblVwList.frame.size.height + 16);
//            [_tblVwList setShowsVerticalScrollIndicator:NO];
        }

      
     
    }
    return self;
}

//Use it when we directly subclass any UIView from storyboard.
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    if(self){
        //load interface file from .xib
        [[NSBundle mainBundle]loadNibNamed:@"RTPopOver" owner:self options:nil];
        
        //add it as a subview
        [self addSubview:self.bgVw];
    }
    return self;
}

#pragma mark : --------- :  Property Allocation
-(void)setPos:(PopOverPostion)pos{
    _pos = pos;
}

-(void)setOriginY:(CGFloat)originY{
    _originY = originY;
}

-(void)setHeightOfMenuItem:(NSInteger)heightOfMenuItem{
    _heightOfMenuItem = heightOfMenuItem;
}

#pragma mark : --------- :  Basic Setup
-(void)setTapGesture{
    _tapRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tappedOnVw:)];
    [_tapRecognizer setNumberOfTapsRequired:1];
    _tapRecognizer.delegate = self;
    [self.bgVw addGestureRecognizer:_tapRecognizer];
}

-(void)tappedOnVw:(UITapGestureRecognizer *)recognizer{
    
    [self.bgVw removeGestureRecognizer:_tapRecognizer];
    [self deallocInstance];
    [self removeFromSuperview];
}

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if (CGRectContainsPoint(self.tblVwList.bounds, [touch locationInView:self.tblVwList]))
        return NO;
    
    return YES;
}

-(void)configPopOver:(PopOverPostion)pos withPopOverOrigin:(CGFloat)originY{
    

    switch (pos) {
        case PopOverPositon_Left:{
             [_containerVw setFrame:CGRectMake(8,originY,_containerVw.frame.size.width,_containerVw.frame.size.height)];
            [self animateContainerVw];
        }
        break;
        case PopOverPositon_Center:{
            [_containerVw setFrame:CGRectMake(CGRectGetMaxX(self.frame)/2 - _containerVw.frame.size.width/2,originY,_containerVw.frame.size.width, _containerVw.frame.size.height)];
            [self animateContainerVw];
        }
        break;
        case PopOverPositon_Right:{
             [_containerVw setFrame:CGRectMake(CGRectGetMaxX(self.bgVw.frame) - _containerVw.frame.size.width - 8,originY,_containerVw.frame.size.width, _containerVw.frame.size.height)];

            [self animateContainerVw];
        }
        break;
            
        default:
        break;
    }
}

-(void)animateContainerVw{

    _containerVw.transform = CGAffineTransformMakeScale(0, 0);
    [UIView animateWithDuration:.3f delay:.1f usingSpringWithDamping:.8 initialSpringVelocity:.5 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        _containerVw.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];

}

#pragma mark : --------- :  TableView Delegates & Datasources
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _mutableArrOptions.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return _heightOfMenuItem;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"Cell";
    
    RTPopOverCell *cell =(RTPopOverCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"RTPopOverCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    cell.lblOptionTitle.text = [NSString stringWithFormat:@"%@",[_mutableArrOptions objectAtIndex:indexPath.row]];
    cell.imgVwOptions.image = [UIImage imageNamed:[_mutableArrImages objectAtIndex:indexPath.row]];
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.delegate optionsClicked:indexPath.row withValue:@""];
    [self tappedOnVw:_tapRecognizer];
}

#pragma mark : --------- :  Deallocation
-(void)deallocInstance{
    _tapRecognizer = nil;
}



@end
