//
//  RTPopOverCell.m
//  EnjayLatitude
//
//  Created by Rajan Tandel on 23/02/17.
//  Copyright © 2017 Enjay. All rights reserved.
//

#import "RTPopOverCell.h"

@implementation RTPopOverCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
