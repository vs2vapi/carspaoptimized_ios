//
//  RTPopOver.h
//  EnjayLatitude
//
//  Created by Rajan Tandel on 03/02/17.
//  Copyright © 2017 Enjay. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol RTPopOverDelegate <NSObject>

-(void)optionsClicked:(NSInteger)index withValue:(NSString *)value;


@end

typedef enum : NSUInteger {
    PopOverPositon_Left = 0,
    PopOverPositon_Center = 1,
    PopOverPositon_Right = 2
} PopOverPostion;

@interface RTPopOver : UIView <UITableViewDelegate,UITableViewDataSource>
-(instancetype)initWithFrame:(CGRect)frame withPosition:(PopOverPostion)pos withPopOverOrigin:(CGFloat)originY withPopOverOptions:(NSArray *)arrOptions withImages:(NSArray *)arrImages;

#pragma mark : --------- :  Outlets
@property (strong, nonatomic) IBOutlet UIView *bgVw;
@property (nonatomic) PopOverPostion pos;
@property (nonatomic) CGFloat originY;
@property (strong, nonatomic) IBOutlet UIView *containerVw;
@property (weak, nonatomic) IBOutlet UITableView *tblVwList;
@property(strong,nonatomic)id<RTPopOverDelegate>delegate;
@property(nonatomic,assign)NSInteger popUpType; // 11 -> Records  : 22 -> Sorting on Fileds



@property (strong, nonatomic) NSMutableArray *mutableArrOptions;
@property (strong, nonatomic) NSMutableArray *mutableArrImages;
@property(nonatomic,assign) NSInteger heightOfMenuItem;




@end
