//
//  RTProgressHUD.h
//  CustomLoader
//
//  Created by R@j012 on 11/02/17.
//  Copyright © 2017 R@j012. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTProgressHUD : UIView 
{
    UIView *containerVw;
    UIImageView *imgVwLoader;
    UILabel *lblMsg;
    CAShapeLayer *arcLayer;
}

+(id)defaultManager;
+(void)showProgressWithMsg:(NSString *)msg;
+(void)dismiss;
@end
