//
//  RTProgressHUD.m
//  CustomLoader
//
//  Created by R@j012 on 11/02/17.
//  Copyright © 2017 R@j012. All rights reserved.
//

#import "RTProgressHUD.h"

#define ProgessContext ((AppDelegate *)[[UIApplication sharedApplication]delegate])

@implementation RTProgressHUD

#pragma mark : --------- :  Instance Allocation
+(id)defaultManager{
    
    static RTProgressHUD *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[RTProgressHUD alloc]init];
    });
    return sharedManager;
}

-(instancetype)init{
    
    if(self = [super init]){
        
        self.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        self.backgroundColor = RGBA(0, 0, 0, .4);
        
        containerVw = [[UIView alloc]initWithFrame:CGRectMake(0, 0,120, 100)];
        containerVw.backgroundColor = [UIColor colorWithWhite:1 alpha:1];
        containerVw.layer.cornerRadius = 6;
        containerVw.center = self.center;
       [self addSubview:containerVw];
        
        imgVwLoader = [[UIImageView alloc]initWithFrame:CGRectMake(containerVw.frame.size.width/2 - 25,10, 50, 50)];
        imgVwLoader.backgroundColor = [UIColor clearColor];
        imgVwLoader.contentMode = UIViewContentModeScaleAspectFit;
        [imgVwLoader setImage:[UIImage imageNamed:@"icn_loader_img"]];
        [containerVw addSubview:imgVwLoader];
        
        lblMsg = [[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(imgVwLoader.frame)+10,containerVw.frame.size.width,20)];
        lblMsg.textColor = [UIColor blackColor];
        lblMsg.font = Dax_Regular(15);
        lblMsg.textAlignment = NSTextAlignmentCenter;
        [containerVw addSubview:lblMsg];
        
        CAShapeLayer *circleLayer = [CAShapeLayer layer];
        [circleLayer setPath:[[UIBezierPath bezierPathWithOvalInRect:imgVwLoader.frame] CGPath]];
        [circleLayer setStrokeColor:RGBA(235, 235, 235, 1).CGColor];
        [circleLayer setFillColor:[[UIColor clearColor] CGColor]];
        [circleLayer setLineWidth:4];
        [containerVw.layer addSublayer:circleLayer];
        
        CGFloat startAngle = M_PI_2  * 3;
//        CGFloat endAngle = (startAngle + M_PI)*2;
        CGPoint centerPoint = imgVwLoader.center;
        float radius = imgVwLoader.frame.size.height/2;
        
        arcLayer = [CAShapeLayer layer];
        UIBezierPath *arcPath = [UIBezierPath bezierPathWithArcCenter:centerPoint radius:radius startAngle:startAngle endAngle:3 clockwise:YES];
       
        [arcLayer setPath:[arcPath CGPath]];
        [arcLayer setFrame:imgVwLoader.frame];
        arcLayer.bounds = CGPathGetBoundingBox(arcPath.CGPath);
        [arcLayer setBackgroundColor:[UIColor clearColor].CGColor];
        [arcLayer setLineCap:@"round"];
        [arcLayer setStrokeColor:[AppThemeColor CGColor]];
        [arcLayer setFillColor:[[UIColor clearColor] CGColor]];
        [arcLayer setLineWidth:4];
        [containerVw.layer addSublayer:arcLayer];
 
    }
    return self;
}

#pragma mark : --------- :  Basic SetUp
-(void)addVwInWindow{
    [ProgessContext.window addSubview:self];
    [self addAnimationToLayer];
}

-(void)removeVwFromWindow{
    [self removeFromSuperview];
}

-(void)setLabelMsg:(NSString *)msg{
    lblMsg.text = msg;
}

#pragma mark : --------- :  Animations
-(void)addAnimationToLayer{
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"]; //strokeEnd
    animation.fromValue = @0;
    animation.toValue = [NSNumber numberWithFloat: 2 * M_PI];
    animation.duration = 1.0f;
    animation.repeatCount = INFINITY;
    animation.removedOnCompletion = NO;
    [arcLayer addAnimation:animation forKey:@"SpinAnimation"]; //kCAFillModeBoth animateStrokeEnd
    [CATransaction commit];

}

-(void)removeAnimation{
    [arcLayer removeAllAnimations];
}

#pragma mark : --------- :  Instance Method
+(void)showProgressWithMsg:(NSString *)msg{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[RTProgressHUD defaultManager]addVwInWindow];
        [[RTProgressHUD defaultManager]setLabelMsg:msg];
    });
 
}

+(void)dismiss{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[RTProgressHUD defaultManager]removeAnimation];
        [[RTProgressHUD defaultManager]removeVwFromWindow];
    });
    
   
}
@end
