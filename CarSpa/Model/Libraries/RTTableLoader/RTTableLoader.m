//
//  RTTableLoader.m
//  Rajan
//
//  Created by Rajan on 27/02/17.
//  Copyright © 2017 Rajan. All rights reserved.
//

#import "RTTableLoader.h"

@implementation RTTableLoader


-(instancetype)initWithFrame:(CGRect)frame{
    
    if(self = [super initWithFrame:frame]){
       
        
        [[NSBundle mainBundle]loadNibNamed:@"RTTableLoader" owner:self options:nil];
        
        //set bounds
        _bgVw.frame = CGRectMake(0, 0, frame.size.width,44);
        
        self.bounds = _bgVw.bounds;
        [self addSubview:_bgVw];
    }
    return self;
}

-(void)startIndicator{
    [self hideNoResultLable];
    [_indicatorVw startAnimating];
}

-(void)stopIndicator{
     [_indicatorVw stopAnimating];
}

-(void)setLoaderMsg:(NSString *)msg{
    _lblLoaderMsg.text = msg;
}

-(void)setNoResultMsg:(NSString *)msg{
    _indicatorVw.hidden = YES;
    _lblLoaderMsg.hidden = YES;
    _lblNoResult.hidden  = NO;
    _lblNoResult.text = msg;
}

-(void)hideNoResultLable{
    _indicatorVw.hidden = NO;
    _lblLoaderMsg.hidden = NO;
    _lblNoResult.hidden  = YES;
}

@end
