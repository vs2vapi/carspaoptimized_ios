//
//  RTTableLoader.h
//  Rajan
//
//  Created by Rajan on 27/02/17.
//  Copyright © 2017 Rajan. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface RTTableLoader : UIView


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorVw;
@property (weak, nonatomic) IBOutlet UILabel *lblLoaderMsg;
@property (weak, nonatomic) IBOutlet UILabel *lblNoResult;
@property (strong, nonatomic) IBOutlet UIView *bgVw;


-(void)startIndicator;
-(void)stopIndicator;
-(void)setLoaderMsg:(NSString *)msg;
-(void)setNoResultMsg:(NSString *)msg;

@end
