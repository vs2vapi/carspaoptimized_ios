//
//  UIAlertController+AlertView.m
//  R@jan 012
//
//  Created by R@j012 on 27/01/17.
//  Copyright © 2017 R@j012. All rights reserved.
//

#import "UIAlertController+AlertView.h"

@implementation UIAlertController (AlertView)


+(void)showAlertInVC:(UIViewController *)myVC withTitle:(NSString *)title withMsg:(NSString *)msg withOtherAlertActionTitle:(NSArray *)arrayTitle withAlertActionCancel:(NSString *)strCancel withAlertCompletionBlock:(alertCompletionBlock)alertBlock{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:msg
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    if([strCancel isKindOfClass:[NSString class]] && ![strCancel isEqualToString:@""]){
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:strCancel style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
            
        }];
        [alert addAction:alertAction];
    }
  
    [arrayTitle enumerateObjectsUsingBlock:^(NSString *actionTitle, NSUInteger idx, BOOL * _Nonnull stop) {
        
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:actionTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
           alertBlock(action,idx);
            
        }];
        [alert addAction:alertAction];
    }];

   [myVC presentViewController:alert animated:YES completion:nil];
    
}
@end
