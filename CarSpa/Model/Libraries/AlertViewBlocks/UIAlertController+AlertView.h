//
//  UIAlertController+AlertView.h
// R@jan 012
//
//  Created by R@j012 on 27/01/17.
//  Copyright © 2017 R@j012. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (AlertView)

typedef void(^alertCompletionBlock)(UIAlertAction *action,NSInteger buttonIndex) ;

+(void)showAlertInVC:(UIViewController *)myVC withTitle:(NSString *)title withMsg:(NSString *)msg withOtherAlertActionTitle:(NSArray *)arrayTitle withAlertActionCancel:(NSString *)strCancel withAlertCompletionBlock:(alertCompletionBlock)alertBlock;

@end
