//
//  CoreDataAdaptor.h
//  R@jan 012
//
//  Created by  R@j012 on 17/02/17.
//  Copyright © 2017  R@j012. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreDataManager.h"


@interface CoreDataAdaptor : NSObject
//@property (readonly, strong) NSPersistentContainer *persistentContainer;
@property (readonly, strong) NSPersistentStoreCoordinator *persistentStoreCoOrdinator;
@property (readonly, strong) NSManagedObjectContext *nsManagedObjectContext;
@property (readonly, strong) CoreDataManager *dataManager;
@property(strong,nonatomic)NSManagedObjectModel *nsManagedObjectModel;
@end
