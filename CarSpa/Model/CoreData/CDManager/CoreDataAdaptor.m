//
//  CoreDataAdaptor.m
//  R@jan 012
//
//  Created by  R@j012 on 17/02/17.
//  Copyright © 2017  R@j012. All rights reserved.
//

#import "CoreDataAdaptor.h"
#define ModelName @"CarSpa" //Model name should be same as your xcdatamodelId
#define SQLiteDBName @"CarSpa.sqlite"

@implementation CoreDataAdaptor
//@synthesize persistentContainer  = _persistentContainer;
@synthesize dataManager = _dataManager;

- (instancetype)init{
    if (self = [super init]){

          [self persistencontainerForAllOS];
          [self managedObjectContext];
        _dataManager = [[CoreDataManager alloc]init];
    }
    return self;
}


- (NSManagedObjectModel *)managedObjectModel
{
    if(_nsManagedObjectModel != nil){
        return _nsManagedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:ModelName withExtension:@"momd"];
    _nsManagedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    return _nsManagedObjectModel;
}

-(NSPersistentStoreCoordinator *)persistencontainerForAllOS{
    
    if(_persistentStoreCoOrdinator != nil){
        return _persistentStoreCoOrdinator;
    }
    
    
    NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                                  inDomains:NSUserDomainMask] lastObject];
    
    
    NSURL *storeURL = [documentsURL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@",SQLiteDBName]];
     NSLog(@"DB Path : %@",storeURL);
    NSError *error = nil;
    
    _persistentStoreCoOrdinator = [[NSPersistentStoreCoordinator alloc]
                                     initWithManagedObjectModel:[self managedObjectModel]];
  
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption : @YES,
                              NSInferMappingModelAutomaticallyOption : @YES
                              };
    
    if(![_persistentStoreCoOrdinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]){
       // if (error != nil) {
            abort();
      //  }
        
    }
    return _persistentStoreCoOrdinator;
    
}

-(NSManagedObjectContext*)managedObjectContext
{
    if(_nsManagedObjectContext != nil){
        return _nsManagedObjectContext;
    }
    
    if(_persistentStoreCoOrdinator != nil){
        _nsManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_nsManagedObjectContext setPersistentStoreCoordinator:_persistentStoreCoOrdinator];
    }
    
    return _nsManagedObjectContext;
}


//  This method was for iOS 10 as NSPersistentContainer was introduced in iOS 10 and later. So for iOS 9 we have to manage our own core data model :(
//  Sometime i hate Apple :( #moreCodeToManage #wasteOfTime

/*
- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:ModelName];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
 
                 //    Typical reasons for an error here include:
                  //   * The parent directory does not exist, cannot be created, or disallows writing.
                //     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 //    * The device is out of space.
                 //    * The store could not be migrated to the current model version.
                //     Check the error message to determine what the actual problem was.
 
                
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    return _persistentContainer;
}
*/

@end
