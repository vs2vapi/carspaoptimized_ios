//
//  CoreDataManager.h
//  R@jan 012
//
//  Created by  R@j012 on 17/02/17.
//  Copyright © 2017  R@j012. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreDataManager : NSObject

-(NSArray *)fetchRecordsWithFetchRequest:(NSString *)entityName withPredicate:(NSString *)predicate withViewContenxt:(NSManagedObjectContext *)viewContext;
-(NSArray *)fetchRecordsWithSortKey:(NSString *)entityName withPredicate:(NSString *)predicate withViewContenxt:(NSManagedObjectContext *)viewContext withSortKey:(NSString *)sortKey orderByASC:(BOOL)asc;
-(NSInteger)getAutoincrement:(NSString *)entityName  fromViewContext:(NSManagedObjectContext *)viewContext;
@end
