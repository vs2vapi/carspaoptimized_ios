//
//  CoreDataManager.m
//  R@jan 012
//
//  Created by  R@j012 on 17/02/17.
//  Copyright © 2017  R@j012. All rights reserved.
//

#import "CoreDataManager.h"

@implementation CoreDataManager

-(NSArray *)fetchRecordsWithFetchRequest:(NSString *)entityName withPredicate:(NSString *)predicate withViewContenxt:(NSManagedObjectContext *)viewContext{
    
    NSFetchRequest *fetchRequestor = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:entityName
                                                         inManagedObjectContext:viewContext];
    fetchRequestor.predicate = [NSPredicate predicateWithFormat:predicate];
    
    [fetchRequestor setEntity:entityDescription];
    
    NSError *error;
    return [viewContext executeFetchRequest:fetchRequestor error:&error];
    
}


-(NSArray *)fetchRecordsWithSortKey:(NSString *)entityName withPredicate:(NSString *)predicate withViewContenxt:(NSManagedObjectContext *)viewContext withSortKey:(NSString *)sortKey orderByASC:(BOOL)asc{
    
    NSFetchRequest *fetchRequestor = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:entityName
                                                         inManagedObjectContext:viewContext];
    fetchRequestor.predicate = [NSPredicate predicateWithFormat:predicate];
    
    fetchRequestor.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:sortKey ascending:asc]];
    
    [fetchRequestor setEntity:entityDescription];
    
    NSError *error;
    return [viewContext executeFetchRequest:fetchRequestor error:&error];
    
}


-(NSInteger)getAutoincrement:(NSString *)entityName  fromViewContext:(NSManagedObjectContext *)viewContext
{
    @try {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        NSEntityDescription *disentity = [NSEntityDescription entityForName:entityName inManagedObjectContext:viewContext];
        [fetchRequest setEntity:disentity];
        
        NSError *error = nil;
        
        NSArray *result = [viewContext executeFetchRequest:fetchRequest error:&error];
        
        if (error)
            NSLog(@"Execute Error - %@", error.localizedDescription);
        
        return result.count+1;
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    }
    
    return 0;
}

@end
