//
//  SyncCDAdaptor.m
//  R@jan 012
//
//  Created by  R@j012 on 17/02/17.
//  Copyright © 2017  R@j012. All rights reserved.
//

#import "SyncCDAdaptor.h"
#import "WSUser.h"
#import "CD_User+CoreDataClass.h"
#import "CD_NOTIFICATIONS+CoreDataClass.h"


@implementation SyncCDAdaptor

#pragma mark : --------- :  Instance Allocation

+(id)sycnInstance{
    
    static SyncCDAdaptor *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[SyncCDAdaptor alloc]init];
    });
    return sharedManager;
}

-(instancetype)init{
    
    if(self = [super init]){
        
        if(_syncMOC == nil){
            _syncMOC = AppContext.dataAdaptor.nsManagedObjectContext;
        }
    }
    return self;
}

#pragma mark : --------- :  Saving Info
-(void)saveUserDetails:(NSArray *)userArray{
    
    NSError *error = nil;
    for(WSUser *userObj in userArray){
        
        CD_User *dbUserObj = [[CORE_DATA_INSTANCE.dataManager fetchRecordsWithFetchRequest:Table_User  withPredicate:[NSString stringWithFormat:@"userId == %lu",userObj.internalBaseClassIdentifier] withViewContenxt:_syncMOC]firstObject];
        
        if (dbUserObj == nil){
            dbUserObj =[NSEntityDescription insertNewObjectForEntityForName:Table_User inManagedObjectContext:_syncMOC];
        }
        
        dbUserObj.userId = userObj.internalBaseClassIdentifier;
        dbUserObj.fullName = userObj.fullName;
        dbUserObj.email = userObj.email;
        dbUserObj.latitude = userObj.latitude;
        dbUserObj.longitude = userObj.longitude;
        dbUserObj.socialId = [NSString stringWithFormat:@"%@",userObj.socialId];
        dbUserObj.socialType = userObj.socialType;
        dbUserObj.status = userObj.status;
        dbUserObj.countryCode = userObj.countryCode;
        dbUserObj.mobileNumber = userObj.mobileNumber;
        dbUserObj.fileUrl = userObj.fileUrl;
        dbUserObj.thumbUrl = userObj.thumbUrl;
        dbUserObj.dateTime = userObj.datetime;
    
    }
    
    if (![_syncMOC save:&error]) {
        NSLog(@"Unable to save managed object context.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
}

-(void)updateUserDetails:(NSArray *)userArray{
    
    NSError *error = nil;
    for(WSUser *userObj in userArray){
        
        CD_User *dbUserObj = [[CORE_DATA_INSTANCE.dataManager fetchRecordsWithFetchRequest:Table_User  withPredicate:nil withViewContenxt:_syncMOC]firstObject];
        
        if (dbUserObj == nil){
            dbUserObj =[NSEntityDescription insertNewObjectForEntityForName:Table_User inManagedObjectContext:_syncMOC];
        }
        
        dbUserObj.userId = userObj.internalBaseClassIdentifier;
        dbUserObj.fullName = userObj.fullName;
        dbUserObj.email = userObj.email;
        dbUserObj.latitude = userObj.latitude;
        dbUserObj.longitude = userObj.longitude;
        dbUserObj.socialId = [NSString stringWithFormat:@"%@",userObj.socialId];
        dbUserObj.socialType = userObj.socialType;
        dbUserObj.status = userObj.status;
        dbUserObj.countryCode = userObj.countryCode;
        dbUserObj.mobileNumber = userObj.mobileNumber;
        dbUserObj.fileUrl = userObj.fileUrl;
        dbUserObj.thumbUrl = userObj.thumbUrl;
        dbUserObj.dateTime = userObj.datetime;
    
    }
    
    if (![_syncMOC save:&error]) {
        NSLog(@"Unable to save managed object context.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
}

-(void)saveCarDetails:(WSCar *)objCarDetails withCarTypeId:(BOOL)isCarType{
    
    NSError *error = nil;
    NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
    
    CD_User *dbUserObj = [[CORE_DATA_INSTANCE.dataManager fetchRecordsWithFetchRequest:Table_User  withPredicate:[NSString stringWithFormat:@"userId = %@",userId] withViewContenxt:_syncMOC]firstObject];
    
    if (dbUserObj != nil){
     
        if(!isCarType){
            dbUserObj.carId = objCarDetails.carIdentifier;
            dbUserObj.carModel = [NSString stringWithFormat:@"%@",objCarDetails.model];
            dbUserObj.carBrand = [NSString stringWithFormat:@"%@",objCarDetails.brand];
            dbUserObj.carPlateNo = [NSString stringWithFormat:@"%@", objCarDetails.plateNumber];
        }else{
            dbUserObj.carTypeId = objCarDetails.carIdentifier;
        }
      
        if (![_syncMOC save:&error]) {
            NSLog(@"Unable to save managed object context.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        }
        
        
    }
}

-(void)saveAddressDetails:(WSUserAddresses *)addressDetails{
    
    NSError *error = nil;
    NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
    
    CD_User *dbUserObj = [[CORE_DATA_INSTANCE.dataManager fetchRecordsWithFetchRequest:Table_User  withPredicate:[NSString stringWithFormat:@"userId = %@",userId] withViewContenxt:_syncMOC]firstObject];
    
    if (dbUserObj != nil){
        
        dbUserObj.addressId = addressDetails.internalBaseClassIdentifier;
        dbUserObj.addressTitle = [NSString stringWithFormat:@"%@",addressDetails.addresstitle];
        dbUserObj.address = addressDetails.address;
        dbUserObj.latitude = addressDetails.latitude;
        dbUserObj.longitude = addressDetails.longitude;
        
        if (![_syncMOC save:&error]) {
            NSLog(@"Unable to save managed object context.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        }
        
    }
}

-(void)updateAddressDetails:(WSUserAddresses *)addressDetails{
    
    NSError *error = nil;
    NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
    
    CD_User *dbUserObj = [[CORE_DATA_INSTANCE.dataManager fetchRecordsWithFetchRequest:Table_User  withPredicate:[NSString stringWithFormat:@"userId = %@ AND addressId == %lu",userId,addressDetails.internalBaseClassIdentifier] withViewContenxt:_syncMOC]firstObject];
    
    if (dbUserObj != nil){
        
        dbUserObj.addressId = addressDetails.internalBaseClassIdentifier;
        dbUserObj.addressTitle = addressDetails.addresstitle;
        dbUserObj.address = addressDetails.address;
        dbUserObj.latitude = addressDetails.latitude;
        dbUserObj.longitude = addressDetails.longitude;
        
        if (![_syncMOC save:&error]) {
            NSLog(@"Unable to save managed object context.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        }
        
    }
    
}

-(void)saveNotifications:(NSDictionary *)dict{
    
    NSError *error;
    NSInteger notificationId = [CORE_DATA_INSTANCE.dataManager getAutoincrement:Table_Notifications fromViewContext:_syncMOC] + 1;
    CD_NOTIFICATIONS *dbObj = [[CORE_DATA_INSTANCE.dataManager fetchRecordsWithFetchRequest:Table_Notifications  withPredicate:[NSString stringWithFormat:@"notificationId == %lu",notificationId] withViewContenxt:_syncMOC]firstObject];
    
    if(dbObj == nil){
        dbObj =[NSEntityDescription insertNewObjectForEntityForName:Table_Notifications inManagedObjectContext:_syncMOC];
        dbObj.notificationId = notificationId;
    }
    
    NSInteger notificationType = [[dict valueForKey:kNotificationKey_Type]integerValue];
    NSString *strTitle = [NSString stringWithFormat:@"%@",[dict valueForKey:kNotificationKey_Title]];
    NSString *strMsg = [NSString stringWithFormat:@"%@",[dict valueForKey:kNotificationKey_Msg]];
    NSString *strDate = [NSString stringWithFormat:@"%@",[dict valueForKey:kNotificationKey_Date]];
    
    dbObj.notificationType = notificationType;
    dbObj.notificationTitle = strTitle;
    dbObj.notificationMsg = strMsg;
    dbObj.notificationDate = strDate;
    
    if (![_syncMOC save:&error]){
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
}

-(void)deleteUserAddressDetails:(WSUserAddresses *)addressDetails{
    
    NSError *error = nil;
    NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
    
    CD_User *dbUserObj = [[CORE_DATA_INSTANCE.dataManager fetchRecordsWithFetchRequest:Table_User  withPredicate:[NSString stringWithFormat:@"userId = %@ AND addressId == %lu",userId,addressDetails.internalBaseClassIdentifier] withViewContenxt:_syncMOC]firstObject];
    
    if (dbUserObj != nil){
        
        dbUserObj.addressId = 0;
        dbUserObj.addressTitle = @"";
        dbUserObj.address = @"";
        dbUserObj.latitude = 0;
        dbUserObj.longitude = 0;
        
        if (![_syncMOC save:&error]) {
            NSLog(@"Unable to save managed object context.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        }
        
    }
    
}

-(void)deleteUserCarDetails:(WSCar *)objCarDetails{
    
    NSError *error = nil;
    NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
    
    CD_User *dbUserObj = [[CORE_DATA_INSTANCE.dataManager fetchRecordsWithFetchRequest:Table_User  withPredicate:[NSString stringWithFormat:@"userId = %@ AND carId == %lu",userId,objCarDetails.carIdentifier] withViewContenxt:_syncMOC]firstObject];
    
    if (dbUserObj != nil){
        
        dbUserObj.carId = 0;
        dbUserObj.carModel = @"";
        dbUserObj.carBrand = @"";
        dbUserObj.carPlateNo = @"";
        
        if (![_syncMOC save:&error]) {
            NSLog(@"Unable to save managed object context.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        }
        
        
    }
}

#pragma mark : --------- : Delete Records
-(void)deleteRecords:(NSString *)tblName withPredicate:(NSString *)pred{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:tblName];
    NSPredicate *p=[NSPredicate predicateWithFormat:pred];
    [fetchRequest setPredicate:p];
    
    NSError *error;
    NSArray *fetchedObjects = [_syncMOC executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject *object in fetchedObjects){
        [_syncMOC deleteObject:object];
    }
    error = nil;
    if(![_syncMOC save:&error]){
        NSLog(@"Unable to delete records.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    
}

- (void)deleteAllRecords:(NSString *)tblName{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:tblName];
    [fetchRequest setIncludesPropertyValues:NO];
    
    NSError *error;
    NSArray *fetchedObjects = [_syncMOC executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject *object in fetchedObjects){
        [_syncMOC deleteObject:object];
    }
    error = nil;
    
    if(![_syncMOC save:&error]){
        NSLog(@"Unable to delete records.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
}


@end
