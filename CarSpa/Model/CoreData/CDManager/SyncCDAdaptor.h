//
//  SyncCDAdaptor.h
//  R@jan 012
//
//  Created by  R@j012 on 17/02/17.
//  Copyright © 2017  R@j012. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreDataManager.h"
#import "WSCar.h"
#import "WSUserAddresses.h"

@interface SyncCDAdaptor : NSObject


+(id)sycnInstance;

@property (readonly, strong) NSManagedObjectContext *syncMOC;

-(void)saveUserDetails:(NSArray *)userArray;
-(void)updateUserDetails:(NSArray *)userArray;
-(void)saveCarDetails:(WSCar *)objCarDetails withCarTypeId:(BOOL)isCarType;
-(void)updateAddressDetails:(WSUserAddresses *)addressDetails;
-(void)deleteUserCarDetails:(WSCar *)objCarDetails;
-(void)saveAddressDetails:(WSUserAddresses *)addressDetails;
-(void)deleteUserAddressDetails:(WSUserAddresses *)addressDetails;
-(void)deleteRecords:(NSString *)tblName withPredicate:(NSString *)pred;
- (void)deleteAllRecords:(NSString *)tblName;
-(void)saveNotifications:(NSDictionary *)dict;
@end
