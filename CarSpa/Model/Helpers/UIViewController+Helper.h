//
//  UIViewController+Helper.h
//  Easy Visit
//
//  Created by Rajan on 06/05/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    

    TYPE_TEXTFIELD = 1,
    TYPE_NUMBER = 2,
    TYPE_DROPDOWN = 3,
    TYPE_RADIO = 4,
    TYPE_CHECKBOX = 5,
    TYPE_ATTACHMENT = 6,
    
} controlsEnum;


typedef enum : NSUInteger {
    
    
    PERMANENT_CONSTRAINTS = 1,
    REQUIRED_CONSTRAINTS = 2,
    BOTH_CONSTRAINTS = 3,
    NULL_CONSTRAINTS = 0,
   
    
}constraintsEnum;

#define ShowNoNetworkAlert(VC) [VC showNoNetworkAlert]
#define isEnLanguage(VC) [VC isEnglishLanguage]

@interface UIViewController (Helper) 

-(void)setupNavBar;
-(BOOL)isValidEmail:(NSString *)strEmail;
- (BOOL)isValidURL:(NSString *)strURL;
-(void)showAlert:(NSString *)msg;
-(void)showNoNetworkAlert;
-(NSString *)convertDateToUTC:(NSDate *)myDate;
-(UIBezierPath *)setVwShadow:(UIView *)shadowVw withColor:(UIColor *)shadowColor;
-(UIBezierPath *)setBtnShadow:(UIButton *)btn withColor:(UIColor *)shadowColor shadowOpacity:(float)opacity;
- (BOOL) validatePhoneNumber:(NSString *)countryCode withNumber:(NSString *)number;
- (NSComparisonResult)compareDateOnly:(NSDate *)otherDate;
-(NSString *)fetchUserCountryCode;
-(BOOL)isEnglishLanguage;
-(NSString *)displayDateInString:(NSDate *)date;
-(BOOL)isUserLoggedIn;
-(void)showAlertForLogin;
-(NSString *)fetchAppVersion;
@end
