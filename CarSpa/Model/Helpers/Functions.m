//
//  Functions.m
//  Easy Visit
//
//  Created by Rajan on 06/05/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "Functions.h"

@implementation Functions

+(void)setStringValueToUserDefaults:(NSString *)str withKey:(NSString *)key{
    
    if([NSUserDefaults standardUserDefaults]){
        [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",str] forKey:key];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
    
}

+(NSString *)getStringValueFromDefaults:(NSString *)key{
    
     NSString *str = @"";
     if([NSUserDefaults standardUserDefaults]){
         str =  [[NSUserDefaults standardUserDefaults]objectForKey:key];
     }
    return str;
}

+(void)setIntegerValueToUserDefaults:(NSInteger)intgValue withKey:(NSString *)key{
    
     if([NSUserDefaults standardUserDefaults]){
         [[NSUserDefaults standardUserDefaults]setInteger:intgValue forKey:key];
         [[NSUserDefaults standardUserDefaults]synchronize];
     }
}

+(int)getIntegerValueFromDefaults:(NSString *)key{
    
    int value = 0;
     if([NSUserDefaults standardUserDefaults]){
          value = (int)[[NSUserDefaults standardUserDefaults]integerForKey:key];
     }
    return value;
}

+(void)setBoolValueToUserDefaults:(BOOL)boolValue withKey:(NSString *)key{
    
    if([NSUserDefaults standardUserDefaults]){
        [[NSUserDefaults standardUserDefaults]setBool:boolValue forKey:key];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
}

+(BOOL)getBoolValueFromDefaults:(NSString *)key{
    
    BOOL myBoolValue = NO;
    if([NSUserDefaults standardUserDefaults]){
        myBoolValue = [[NSUserDefaults standardUserDefaults]boolForKey:key];
    }
    return myBoolValue;
}

+(void)setDoubleValueToUserDefaults:(double)doubleValue withKey:(NSString *)key{
    
    if([NSUserDefaults standardUserDefaults]){
        [[NSUserDefaults standardUserDefaults]setDouble:doubleValue forKey:key];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
}

+(double)getDoubleValueFromDefaults:(NSString *)key{
    
    double myDoubleValue = 0.0;
    if([NSUserDefaults standardUserDefaults]){
        myDoubleValue = [[NSUserDefaults standardUserDefaults]doubleForKey:key];
    }
    return myDoubleValue;
}


+ (void)setObjectToDefaults:(id)value withKey:(NSString *)key
{
    if ([NSUserDefaults standardUserDefaults]) {
        [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

+ (id)getObjectValueFromDefaultsWithKey:(NSString *)key
{
    id myObj = nil;
    if ([NSUserDefaults standardUserDefaults]) {
        myObj =[[NSUserDefaults standardUserDefaults] objectForKey:key];
    }
    return myObj;
}

+ (void)storeCustomObj:(id)value withKey:(NSString *)key {
    [[NSUserDefaults standardUserDefaults] setObject:value  forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setCustomObjectToDefaults:(id)encodableObject withkey:(NSString *)key {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:encodableObject];
    [self storeCustomObj:data withKey:key];
}

+ (id)getCustomObjectFromDefaults:(NSString*)key {
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

+ (void)resetAllDefaults {
    
    NSUserDefaults * allDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [allDefaults dictionaryRepresentation];
    for (id key in dict) {
        [allDefaults removeObjectForKey:key];
    }
    [allDefaults synchronize];
}

@end
