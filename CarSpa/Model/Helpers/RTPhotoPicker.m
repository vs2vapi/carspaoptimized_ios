//
//  RTPhotoPicker.m
//  CarSpa
//
//  Created by R@j on 27/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "RTPhotoPicker.h"

@interface RTPhotoPicker ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@end

@implementation RTPhotoPicker

-(instancetype)initWithDelegate:(id)delegate withVC:(UIViewController *)vc{
    
    if(self = [super init]){
        _delegate = delegate;
        _pickerVC = vc;
    }
    return self;
}


-(void)showProfileImageActionController{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:nil
                                 message: nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* camera = [UIAlertAction
                             actionWithTitle:kCamera
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action) {
                                 
                                 [self configImagePickerWithCamera:YES];
                                 
                             }];
    
    UIAlertAction* photos = [UIAlertAction
                             actionWithTitle:kPhotoLibrary
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action) {
                                 [self configImagePickerWithCamera:NO];
                             }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:kCancel
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action) {
                                 
                             }];
    
    [camera setValue:[[UIImage imageNamed:@"icn_blue_camera"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    [photos setValue:[[UIImage imageNamed:@"icn_photo_gallery"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    
    [alert addAction:camera];
    [alert addAction:photos];
    [alert addAction:cancel];
    
    [_pickerVC presentViewController:alert animated:YES completion:nil];
    
}

-(void)configImagePickerWithCamera:(BOOL)isCamera{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    
    if(isCamera){
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }else{
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    [_pickerVC presentViewController:picker animated:YES completion:NULL];
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    UIImage *selectedImage =(UIImage *)info[UIImagePickerControllerEditedImage];
    NSData *imgData=UIImageJPEGRepresentation( selectedImage,0.7);
    UIImage *compressedImage =[UIImage imageWithData:imgData];
    [self.delegate pickerSelectedImage:compressedImage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
