//
//  AddRequestHelper.m
//  CarSpa
//
//  Created by R@j on 31/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "AddRequestHelper.h"

@implementation AddRequestHelper

+(id)defaultManager{
    
    static AddRequestHelper *requestManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        requestManager = [[AddRequestHelper alloc]init];
    });
    return requestManager;
}

-(id)init{
    
    if(self = [super init]){
        _carTypeId = 0;
        _carId= 0;
        _carBrand= @"";
         _carModel= @"";
        _addressId= 0;
        _addressDetails= @"";
        _categoryId= 0; // 1 -> TOW : 2 -> Accessories : 3 -> Emergency
       _arrServiceId= [NSMutableArray new];
        _arrHolidayList= [NSMutableArray new];
       _isTowService= NO;
        _isAccesorryService= NO;
        _isEmergencyService= NO;
        _arrAppointments = [NSMutableArray new];
        _totalAmount = 0;
        _addressTitle= @"";
        _addressLatitude = 0;
        _addressLongitude = 0;
        _towAddressLatitude = 0;
        _towAddressLongitude = 0;
        _towAddress = @"";
        _categoryName = @"";
        _serviceMainImgURL = @"";
        _appointmentDate =@"";
        _serviceDuration = 0;
        _previousAmount = 0;
        _totalLocalAmount = 0;
        _vanId = 0;
        _travelingTime = 0;
        _strDateTime = @"";
        _variableAmount = 0;
        _taxPercentage = 0;
        _isAppliedForTax = NO;
        _serviceAmount = 0;
        _ownerId = 0;
        
    }
    return self;
}

-(void)deallocInstance{
//    _carTypeId = 0;
//    _carId= 0;
//    _carBrand= @"";
//    _carModel= @"";
//    _addressId= 0;
//    _addressDetails= @"";
    _categoryId= 0;
    [_arrServiceId removeAllObjects];
    _isTowService= NO;
    _isAccesorryService= NO;
    _isEmergencyService= NO;
    [_arrAppointments removeAllObjects];
    _totalAmount = 0;
//    _addressTitle=@"";
//    _addressLatitude = 0;
//    _addressLongitude = 0;
//    _towAddressLatitude = 0;
//    _towAddressLongitude = 0;
//    _towAddress = @"";
    _categoryName= @"";
    _serviceMainImgURL=@"";
    _appointmentDate = @"";
    _serviceDuration = 0;
    _previousAmount = 0;
    _totalLocalAmount = 0;
    _vanId = 0;
    _travelingTime = 0;
    _strDateTime = @"";
    _variableAmount = 0;
    _taxPercentage = 0;
    _isAppliedForTax = NO;
    _serviceAmount = 0;
    _ownerId = 0;
    [_arrHolidayList removeAllObjects];
   
}
@end
