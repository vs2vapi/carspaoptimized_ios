//
//  UIViewController+Helper.m
//  Easy Visit
//
//  Created by Rajan on 06/05/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "UIViewController+Helper.h"
#import "NBPhoneNumber.h"
#import "NBPhoneNumberUtil.h"
#import "NBMetadataHelper.h"


@implementation UIViewController (Helper) 


-(void)setupNavBar{
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBarTintColor:AppThemeColor];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"dax-bold" size:20]}];
}

-(BOOL)isValidEmail:(NSString *)strEmail{
    
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:strEmail];
}


-(void)showAlert:(NSString *)msg{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:AppName message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:kAlertOk style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
 
    }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (BOOL)isValidURL:(NSString *)strURL {
    
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:strURL];
}

-(void)showNoNetworkAlert{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:KNoNetwork message:kNoNetworkMsg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:kAlertOk style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
        
    }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(BOOL)isEnglishLanguage{
    if([AppContext.appLanguage isEqualToString:@"ar"] || [AppContext.appLanguage isEqualToString:@"ar-US"]){
        return NO;
    }else{
        return YES;
    }
}

-(NSString *)convertDateToUTC:(NSDate *)myDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:myDate];
    
    return dateString;
}

-(UIBezierPath *)setVwShadow:(UIView *)shadowVw withColor:(UIColor *)shadowColor{
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:shadowVw.bounds];
    shadowVw.layer.masksToBounds = NO;
    shadowVw.layer.shadowColor = shadowColor.CGColor;
    shadowVw.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    shadowVw.layer.shadowOpacity = 0.5f;
    shadowVw.layer.shadowPath = shadowPath.CGPath;
    return shadowPath;
}



-(UIBezierPath *)setBtnShadow:(UIButton *)btn withColor:(UIColor *)shadowColor shadowOpacity:(float)opacity{
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:btn.bounds];
    btn.layer.masksToBounds = NO;
    btn.layer.shadowColor =shadowColor.CGColor;
    btn.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    btn.layer.shadowOpacity = opacity;
    btn.layer.shadowPath = shadowPath.CGPath;
    return shadowPath;
}



- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

- (BOOL) validatePhoneNumber:(NSString *)countryCode withNumber:(NSString *)number{
    NSString *strPhoneAndCountry = [NSString stringWithFormat:@"%@%@",countryCode,number];
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *myNumber = [f numberFromString:countryCode];
    NBMetadataHelper *helper = [[NBMetadataHelper alloc]init];
    NSArray *arr = [helper regionCodeFromCountryCode:myNumber];
    NSString *strRegionCode = [arr objectAtIndex:0];
    return [self valideMobile:strPhoneAndCountry andRegionCode:strRegionCode];
}

-(BOOL) valideMobile:(NSString *)strMobileNumber andRegionCode:(NSString *)regionCode{
    NBPhoneNumberUtil *phoneUtil = [[NBPhoneNumberUtil alloc] init];
    NSError *anError = nil;
    NBPhoneNumber *myNumber = [phoneUtil parse:strMobileNumber
                                 defaultRegion:regionCode error:&anError];
    if (anError == nil) {
        if ([phoneUtil isValidNumber:myNumber]) {
            return true;
        }else{
            return false;
        }
        
    } else {
        return  false;
    }
}

- (NSComparisonResult)compareDateOnly:(NSDate *)otherDate {
    
    NSUInteger dateFlags = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay;
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *selfComponents = [gregorianCalendar components:dateFlags fromDate:[NSDate date]];
    NSDate *selfDateOnly = [gregorianCalendar dateFromComponents:selfComponents];
    
    NSDateComponents *otherCompents = [gregorianCalendar components:dateFlags fromDate:otherDate];
    NSDate *otherDateOnly = [gregorianCalendar dateFromComponents:otherCompents];
    
    return [selfDateOnly compare:otherDateOnly];
}

-(NSString *)fetchUserCountryCode{
    
    NSArray *arrCountryPhoneCodes = [[NSArray alloc] initWithObjects:@"973", @"965",@"974",@"966", @"971",@"968",@"961", nil];
    NSArray *arrRegion = [[NSArray alloc] initWithObjects:@"BH", @"KW",@"QA",@"SA", @"AE",@"OM",@"LB", nil];
    __block NSInteger index = 3;
    [arrRegion enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        NSString *countryCode = [Functions getStringValueFromDefaults:USER_COUNTRY_CODE];
        if([countryCode isEqualToString:obj]){
            index = idx;
            *stop = YES;
        }
    }];

    NSString *code =  [arrCountryPhoneCodes objectAtIndex:index];
    return code;
}

-(NSString *)displayDateInString:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    NSLocale *arabicLocale = [[NSLocale alloc] initWithLocaleIdentifier:AppContext.appLanguage];
    [formatter setLocale:arabicLocale];
    [formatter setDateFormat:EE_dd_MMM];
    NSString *dateInString = [formatter stringFromDate:date];
    if(dateInString == nil || [dateInString isEqualToString:@""]){
         dateInString = @"";
    }
    return dateInString;
}

-(BOOL)isUserLoggedIn{
    BOOL isUserLogin = [Functions getBoolValueFromDefaults:IS_USER_LOGGED_IN];
    return isUserLogin;
}

-(void)showAlertForLogin{
    [UIAlertController showAlertInVC:self withTitle:AppName withMsg:kFeatureDisableMsg withOtherAlertActionTitle:@[kAlertYes] withAlertActionCancel:kAlertNo withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
        if(buttonIndex == 0){
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }];

}

-(NSString *)fetchAppVersion{
    NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
    NSString  *appVersion = [info objectForKey:@"CFBundleShortVersionString"];
    return appVersion;
}

@end
