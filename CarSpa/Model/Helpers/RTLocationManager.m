//
//  RTLocationManager.m
//  CarSpa
//
//  Created by R@j on 26/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "RTLocationManager.h"

@implementation RTLocationManager
#pragma mark : --------- :  Singleton Allocation

+(id)defaultManager{
    
    static RTLocationManager *myLocationManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        myLocationManager = [[RTLocationManager alloc]init];
    });
    return myLocationManager;
}

-(id)init{
    
    if(self = [super init]){
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        [_locationManager requestAlwaysAuthorization];
      
    }
    return self;
}

#pragma mark : --------- :  Location Manager Delegates
- (void)locationManager:(CLLocationManager*)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
            NSLog(@"User still thinking..");
        }
        break;
        case kCLAuthorizationStatusDenied: {
            [Functions  setDoubleValueToUserDefaults:0 withKey:USER_LATITUDE];
            [Functions setDoubleValueToUserDefaults:0 withKey:USER_LONGITUDE];
            [self setDefaultCurrency];
        }
        break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        case kCLAuthorizationStatusAuthorizedAlways: {
            [_locationManager startUpdatingLocation];
           
        }
        break;
        default:
        break;
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    CLLocation *userLocation = [locations objectAtIndex:0];
    double latt =  userLocation.coordinate.latitude;
    double longitude = userLocation.coordinate.longitude;
    
    [Functions  setDoubleValueToUserDefaults:latt withKey:USER_LATITUDE];
    [Functions setDoubleValueToUserDefaults:longitude withKey:USER_LONGITUDE];
    if(!_locationFound){
        _locationFound = YES;
        [[NSNotificationCenter defaultCenter]postNotificationName:LOCATION_FOUND object:userLocation];
    }
  
}


-(void)setDefaultCurrency{
    
    AppContext.exchangeRate = 1;
    AppContext.currency = kSAR;
    [Functions setIntegerValueToUserDefaults:1 withKey:CURRENCY_ID];
    [Functions setStringValueToUserDefaults:AppContext.currency withKey:CURRENCY];
    [Functions setDoubleValueToUserDefaults:1 withKey:EXCHANGE_RATE];
    
}

/*
-(void)fetchCountryCode:(CLLocation *)loc{
    
    CLGeocoder *reverseGeocoder = [[CLGeocoder alloc] init];
    [reverseGeocoder reverseGeocodeLocation:loc completionHandler:^(NSArray *placemarks, NSError *error)
     {
         NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
         if (error){
             NSLog(@"Geocode failed with error: %@", error);
             return;
         }
         
         NSLog(@"Received placemarks: %@", placemarks);
         
         CLPlacemark *myPlacemark = [placemarks objectAtIndex:0];
         NSString  *countryCode = myPlacemark.ISOcountryCode;
         NSString *countryName = myPlacemark.country;
         NSLog(@"My country code: %@ and countryName: %@", countryCode, countryName);
         [Functions setStringValueToUserDefaults:countryCode withKey:USER_COUNTRY_CODE];
        
     }];

}
*/
@end
