//
//  AddRequestHelper.h
//  CarSpa
//
//  Created by R@j on 31/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddRequestHelper : NSObject
+(id)defaultManager;
@property(nonatomic,assign)NSInteger carTypeId;
@property(nonatomic,assign)NSInteger carId;
@property(strong,nonatomic)NSString *carBrand;
@property(strong,nonatomic)NSString *carModel;
@property(nonatomic,assign)NSInteger addressId;
@property(strong,nonatomic)NSString *addressDetails;
@property(strong,nonatomic)NSString *addressTitle;
@property(strong,nonatomic)NSString *categoryName;
@property(strong,nonatomic)NSString *serviceMainImgURL;
@property(nonatomic,assign)NSInteger categoryId;
@property(nonatomic,assign)NSInteger ownerId;
@property(nonatomic,assign)NSInteger serviceDuration;
@property(strong,nonatomic)NSMutableArray *arrServiceId;
@property(strong,nonatomic)NSMutableArray *arrHolidayList;
@property(nonatomic,assign)BOOL isTowService;
@property(nonatomic,assign)BOOL isAccesorryService;
@property(nonatomic,assign)BOOL isEmergencyService;
@property(strong,nonatomic)NSMutableArray *arrAppointments;
@property(nonatomic,assign) double totalAmount;
@property(nonatomic,assign) double totalLocalAmount;
@property(nonatomic,assign) double serviceAmount;
@property(nonatomic,assign) double previousAmount;
@property(nonatomic,assign) double addressLatitude;
@property(nonatomic,assign) double addressLongitude;
@property(nonatomic,assign) double towAddressLatitude;
@property(nonatomic,assign) double towAddressLongitude;
@property(strong,nonatomic)NSString *towAddress;
@property(strong,nonatomic)NSString *appointmentDate;
@property(nonatomic,assign)NSInteger vanId;
@property(nonatomic,assign)NSInteger travelingTime;
@property(nonatomic,assign)double variableAmount;
@property(strong,nonatomic)NSString *strDateTime;
@property(nonatomic,assign)double  taxPercentage;
@property(nonatomic,assign)BOOL isAppliedForTax;


-(void)deallocInstance;

@end
