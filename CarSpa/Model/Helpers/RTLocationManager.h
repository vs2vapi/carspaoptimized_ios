//
//  RTLocationManager.h
//  CarSpa
//
//  Created by R@j on 26/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface RTLocationManager : NSObject <CLLocationManagerDelegate>
+(id)defaultManager;
@property(strong,nonatomic)CLLocationManager *locationManager;
@property(nonatomic,assign)BOOL locationFound;
@end
