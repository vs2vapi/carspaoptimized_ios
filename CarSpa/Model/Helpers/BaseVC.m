//
//  BaseVC.m
//  Easy Visit
//
//  Created by Rajan on 06/05/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import "BaseVC.h"
#import "AddRequestVC.h"
#import "LoginVC.h"
#import "RTPopOver.h"
#import "AboutUsVC.h"
#import "JoinPartnersVC.h"
#import "OurPartnersVC.h"
#import "PrivacyPolicyVC.h"
#import "ContactUsVC.h"
#import "NotificationVC.h"


@interface BaseVC () <RTPopOverDelegate>{
    NSMutableArray *arrImg;
    NSMutableArray *arrItem;
}

@end

@implementation BaseVC

-(void)viewDidLoad{
    
    [super viewDidLoad];

    [self setUpNavigationBarUI];
    [self setRequestButton];
    [self setUpMenuItems];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     [self.tabBarController.tabBar setHidden:NO];
}

#pragma mark: -------------------: Setup UI
-(void)setUpNavigationBarUI{
 
    self.navigationItem.title = kNavigationBarTitle;
    
        UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"NavigationMenu"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                                        style:UIBarButtonItemStylePlain
                                                                       target:self
                                                                       action:@selector(rightBarItemAction:)];
        self.navigationItem.rightBarButtonItem = rightButton;
    
        UIImage *image = [UIImage imageNamed:@"logo-icon-white"];
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35, 35)];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        button.userInteractionEnabled = NO;
        UIBarButtonItem *leftimage =[[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.leftBarButtonItem = leftimage;
        
  
}

-(void)setUpMenuItems{
    
    arrImg = [NSMutableArray new];
    arrItem = [NSMutableArray new];
    
    for (int i=1; i<=7; i++){
        
        NSString *imageName = [NSString stringWithFormat:@"icn_menu_item%d", i];
        [arrItem addObject:[self setMenuItemText:i]];
        [arrImg addObject:imageName];
    }

}

-(NSString *)setMenuItemText:(int)idx{
    NSString *strItm = @"";
    switch (idx) {
        case 1:{
            strItm = kAboutUs;
        }
        break;
        case 2:{
            strItm = kJoinOurPartners;
        }
        break;
        case 3:{
            strItm = kOurPartners;
        }
        break;
        case 4:{
            strItm = kPrivacyPolicy;
        }
        break;
        case 5:{
            strItm = kContactUs;
        }
        break;
        case 6:{
            strItm =kNotifications;
            
        }
            break;
        case 7:{
            strItm = ([self isUserLoggedIn]) ? kSignOut : [NSString stringWithFormat:@"%@/%@",kLogin,kSignUp];
            
        }
        break;
            
        default:
            break;
    }
    return strItm;
}

-(void)setRequestButton{
    
    UIButton *btnRequest = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRequest setFrame:CGRectMake(0,self.view.frame.size.height - 108, self.view.frame.size.width, 60)];
    [btnRequest setBackgroundColor:[UIColor darkGrayColor]];
    [btnRequest setTitle:kRequestService forState:UIControlStateNormal];
    btnRequest.titleLabel.font = Dax_Medium(23);
    [btnRequest addTarget:self action:@selector(btnRequestAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnRequest];
    
}

#pragma mark: -------------------: Button Actions
-(void)rightBarItemAction:(id)sender{

    NSInteger position = 0;
    if([AppContext.appLanguage isEqualToString:@"ar"]){
        position = PopOverPositon_Left;
    }else{
        position = PopOverPositon_Right;
    }
    
    RTPopOver *myPopOver= [[RTPopOver alloc]initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH,SCREEN_HEIGHT) withPosition:position withPopOverOrigin:20 withPopOverOptions:arrItem withImages:arrImg];
    myPopOver.delegate = self;
    myPopOver.heightOfMenuItem = 44;
    [self.tabBarController.view addSubview:myPopOver];
}

-(void)btnRequestAction:(id)sender{
    
    BOOL isUserLogin = [Functions getBoolValueFromDefaults:IS_USER_LOGGED_IN];
    if(isUserLogin){
        
        CATransition* transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionMoveIn; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
        transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
        [self.navigationController.view.layer addAnimation:transition forKey:nil];
        
        AddRequestVC *addrqvc = loadViewController(kSBRequest, kAddRequestVC);
        [self.navigationController pushViewController:addrqvc animated:NO];
        
    }else{
        [UIAlertController showAlertInVC:self withTitle:AppName withMsg:kRequestServiceDisableMsg withOtherAlertActionTitle:@[kAlertYes] withAlertActionCancel:kAlertNo withAlertCompletionBlock:^(UIAlertAction *action, NSInteger buttonIndex) {
            if(buttonIndex == 0){
                [self moveToLoginVC];
            }
        }];
        
    }
}

-(void)moveToLoginVC{

    [UIView animateWithDuration:0.2 animations:^{
        self.view.alpha = 0.3f;
    } completion:^(BOOL b){
        [UIView animateWithDuration:0.1 animations:^{
            self.view.alpha = 0.2f;
        } completion:^(BOOL b){
            [self dismissViewControllerAnimated:NO completion:nil];
            self.view.alpha = 0;
        }];
    }];
    
  //  [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark: -------------------: PopOver Delegates
-(void)optionsClicked:(NSInteger)index withValue:(NSString *)value{
    
    switch (index) {
        case 0:{
            AboutUsVC *abvc = loadViewController(kSBOthers, kAboutUsVC);
            [self.navigationController pushViewController:abvc animated:YES];
        }
        break;
        case 1:{
            JoinPartnersVC *jpvc = loadViewController(kSBOthers, kJoinPartnersVC);
            [self.navigationController pushViewController:jpvc animated:YES];
        }
        break;
        case 2:{
            OurPartnersVC *opvc = loadViewController(kSBOthers, kOurPartnersVC);
            [self.navigationController pushViewController:opvc animated:YES];
        }
        break;
        case 3:{
            PrivacyPolicyVC *ppvc = loadViewController(kSBOthers, kPrivacyPolicyVC);
            [self.navigationController pushViewController:ppvc animated:YES];
        }
        break;
        case 4:{
            ContactUsVC *cuvc = loadViewController(kSBOthers, kContactUsVC);
            [self.navigationController pushViewController:cuvc animated:YES];
        }
        break;
        case 5:{
            NotificationVC *cuvc = loadViewController(kSBOthers, kNotificationVC);
            [self.navigationController pushViewController:cuvc animated:YES];
        }
            break;

        case 6:{
            if(![self isUserLoggedIn]){
                [self moveToLoginVC];
            }else{
               [self signOut];
            }
            
        }
        break;
            
        default:
            break;
    }
}

-(void)signOut{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:AppName
                                 message:kSignOutConfirmation
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* alertYes = [UIAlertAction
                               actionWithTitle:kAlertYes
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                                   [self logoutUser];
                                
                               }];
    
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:kAlertNo
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action) {
                                 //Handle no, thanks button
                             }];
    
    
    [alert addAction:alertYes];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)logoutUser{
    
    if([ReachabilityManager isReachable]){

        NSString *userId = [Functions getStringValueFromDefaults:USER_ID];
        NSString *signOutUrl = [NSString stringWithFormat:@"%@?userId=%@&%@=%@",kSingOutWS,userId,kAppVersionServiceKey,[self fetchAppVersion]];
        [[WebServiceInvocation alloc]initWithWS:signOutUrl withParams:nil withServiceType:TYPE_GET withSelector:@selector(signOutWSAction:) withTarget:self showLoader:YES loaderMsg:kWSLoaderDialoagMsg];
    }else{
        ShowNoNetworkAlert(self);
    }
}

-(void)signOutWSAction:(id)sender{
    
    [RTProgressHUD dismiss];
    if([sender responseCode] > 200){
        [self showAlert:[sender responseErr]];
    }else{
        [self deallocUserDefaults];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

-(void)deallocUserDefaults{
    
//    [[SyncCDAdaptor sycnInstance]deleteAllRecords:Table_User];
    [Functions setBoolValueToUserDefaults:NO withKey:IS_USER_LOGGED_IN];
    [Functions setCustomObjectToDefaults:nil withkey:USER_OBJ];
    [Functions setStringValueToUserDefaults:@"" withKey:USER_ID];
    NSInteger socialType = [Functions getIntegerValueFromDefaults:SOCIAL_LOGIN_TYPE];
    if(socialType == GOOGLE_PLUS_LOGIN){
          [[GIDSignIn sharedInstance] signOut];
    }
   else if(socialType == TWITTER_LOGIN){
        TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
        NSString *userID = store.session.userID;
        [store logOutUserID:userID];
    }
    [Functions setIntegerValueToUserDefaults:0 withKey:SOCIAL_LOGIN_TYPE];
    [AppContext.fetchVanTimer invalidate]; //Stop Timer of fetching vans.
    [self removeAllObservers];
}

-(void)removeAllObservers{
    
     [[NSNotificationCenter defaultCenter]removeObserver:REFRESH_ORDER_LIST];
     [[NSNotificationCenter defaultCenter]removeObserver:UPDATE_LOCATION];
     [[NSNotificationCenter defaultCenter]removeObserver:RATING_ADDED];
     [[NSNotificationCenter defaultCenter]removeObserver:NOTIFICATION_RECEIVED];
     [[NSNotificationCenter defaultCenter]removeObserver:NEW_ORDER_BOOKED];
     [[NSNotificationCenter defaultCenter]removeObserver:EDIT_REQUEST_CAR_CHANGED];
     [[NSNotificationCenter defaultCenter]removeObserver:EDIT_REQUEST_ADDRESS_CHANGED];
}

@end
