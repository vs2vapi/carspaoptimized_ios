//
//  Functions.h
//  Easy Visit
//
//  Created by Rajan on 06/05/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Functions : NSObject
@property(strong,nonatomic)NSUserDefaults *functionDefaults;

+(void)setStringValueToUserDefaults:(NSString *)str withKey:(NSString *)key;
+(NSString *)getStringValueFromDefaults:(NSString *)key;

+(void)setIntegerValueToUserDefaults:(NSInteger)intgValue withKey:(NSString *)key;
+(int)getIntegerValueFromDefaults:(NSString *)key;

+(void)setBoolValueToUserDefaults:(BOOL)boolValue withKey:(NSString *)key;
+(BOOL)getBoolValueFromDefaults:(NSString *)key;

+(void)setDoubleValueToUserDefaults:(double)doubleValue withKey:(NSString *)key;
+(double)getDoubleValueFromDefaults:(NSString *)key;

+ (void)setObjectToDefaults:(id)value withKey:(NSString *)key;
+ (id)getObjectValueFromDefaultsWithKey:(NSString *)key;

+ (void)setCustomObjectToDefaults:(id)encodableObject withkey:(NSString *)key;
+ (id)getCustomObjectFromDefaults:(NSString*)key;

+ (void)resetAllDefaults;

@end
