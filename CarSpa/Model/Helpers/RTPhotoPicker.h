//
//  RTPhotoPicker.h
//  CarSpa
//
//  Created by R@j on 27/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RTPhotoPickerDelegate <NSObject>

-(void)pickerSelectedImage:(UIImage *)selImage;

@end

@interface RTPhotoPicker : UIView
-(instancetype)initWithDelegate:(id)delegate withVC:(UIViewController *)vc;
@property(strong,nonatomic)id<RTPhotoPickerDelegate>delegate;
@property(strong,nonatomic)UIViewController *pickerVC;
@end
