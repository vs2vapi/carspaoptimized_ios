//
//  AppDelegate.h
//  CarSpa
//
//  Created by R@j on 10/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "CoreDataAdaptor.h"
#import "TabBarVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <linkedin-sdk/LISDK.h>
#import <Google/SignIn.h>
#import <TwitterKit/TwitterKit.h>

@import Firebase;

typedef enum : NSUInteger {
    NOTIFICATION_TYPE_MSG = 1,
    NOTIFICATION_TYPE_HOTEL = 2,
    NOTIFICATION_TYPE_OFFER = 3,
    NOTIFICATION_TYPE_CATEGORY = 4,
    NOTIFICATION_TYPE_RATING = 11
} NOTIFICATION_TYPE;


@interface AppDelegate : UIResponder <UIApplicationDelegate,FIRMessagingDelegate,GIDSignInDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(strong,nonatomic)CoreDataAdaptor *dataAdaptor;
@property(strong,nonatomic)NSString *appLanguage;
@property(strong,nonatomic)AddRequestHelper *requestHelper;
@property(nonatomic,assign)double exchangeRate;
@property(strong,nonatomic)NSString *currency;
@property(strong,nonatomic)TabBarVC *appTabVC;
@property(strong,nonatomic)NSTimer *fetchVanTimer;
@property (nonatomic, assign) BOOL isOrderScreen;

@end

