//
//  AppDelegate.m
//  CarSpa
//
//  Created by R@j on 10/07/17.
//  Copyright © 2017 Rajan@VS2. All rights reserved.
//


#import "AppDelegate.h"
#import "CMNavBarNotificationView.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>




/*
@import Firebase;
//@import FirebaseMessaging;

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@interface AppDelegate () <UNUserNotificationCenterDelegate,FIRMessagingDelegate>{
  
}
@end
#endif

@interface AppDelegate ()

@end

NSString *const kGCMMessageIDKey = @"gcm.message_id";
*/

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif

// Implement UNUserNotificationCenterDelegate to receive display notification via APNS for devices
// running iOS 10 and above.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@interface AppDelegate () <UNUserNotificationCenterDelegate>
@end
#endif

// Copied from Apple's header in case it is missing in some cases (e.g. pre-Xcode 8 builds).
#ifndef NSFoundationVersionNumber_iOS_9_x_Max
#define NSFoundationVersionNumber_iOS_9_x_Max 1299
#endif


@implementation AppDelegate

NSString *const kGCMMessageIDKey = @"gcm.message_id";

#pragma mark:-----------------: App Delegates
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [self configAppSetup];
    [self setNavigationBarAppearance];
    [self configRemoteNotifications:application];
    [self configFBSDK:application didFinishLaunchingWithOptions:launchOptions];
    [self configFabricSDK];
    [self configTwitterSDK];
    [self configGmailSDK];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
     [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[NSNotificationCenter defaultCenter]postNotificationName:UPDATE_LOCATION object:nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:CHECK_FORCE_UPDATE object:nil];
    //if(_isOrderScreen){
        [[NSNotificationCenter defaultCenter]postNotificationName:REFRESH_ORDER_LIST object:nil];
   // }

}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
      [FBSDKAppEvents activateApp];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}

-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    
    int socialType = [Functions getIntegerValueFromDefaults:SOCIAL_LOGIN_TYPE];
    if(socialType == LINKEDIN_LOGIN){
        
        if ([LISDKCallbackHandler shouldHandleUrl:url]) {
            return [LISDKCallbackHandler application:app openURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey] annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
        }
    }
    if(socialType == FACEBOOK_LOGIN){
        
        BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:app
                                                                      openURL:url
                                                            sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                                   annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
                        ];

        
    // Add any custom logic here.
        return handled;
    }
    if(socialType == TWITTER_LOGIN){
        return [[Twitter sharedInstance] application:app openURL:url options:options];
    }
    if(socialType == GOOGLE_PLUS_LOGIN){
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                          annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    }
    return NO;

    
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    int socialType = [Functions getIntegerValueFromDefaults:SOCIAL_LOGIN_TYPE];
    if(socialType == FACEBOOK_LOGIN){
        BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                      openURL:url
                                                            sourceApplication:sourceApplication
                                                                   annotation:annotation
                        ];
        // Add any custom logic here.
        return handled;
    }
    if(socialType == LINKEDIN_LOGIN){
        
        if ([LISDKCallbackHandler shouldHandleUrl:url]) {
            
            return [LISDKCallbackHandler application:application openURL:url sourceApplication:sourceApplication annotation:annotation[UIApplicationOpenURLOptionsAnnotationKey]];
        }
    }
    if(socialType == TWITTER_LOGIN){
        return [[Twitter sharedInstance] application:application openURL:url options:annotation];
    }
    
    if(socialType == GOOGLE_PLUS_LOGIN){
        
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:annotation[UIApplicationOpenURLOptionsSourceApplicationKey]
                                          annotation:annotation[UIApplicationOpenURLOptionsAnnotationKey]];
    }
    return NO;
}

#pragma mark:-----------------: App Setup
-(void)setNavigationBarAppearance{
    
    [[UINavigationBar appearance]setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setBarTintColor:AppThemeColor];
//    [UINavigationBar appearance].translucent = NO;
//    [UINavigationBar appearance].opaque = YES;
    [[UINavigationBar appearance]setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],
                                                          NSFontAttributeName:[UIFont fontWithName:@"dax-bold" size:20]}];
}

-(void)configAppSetup{
    
    _appLanguage = [Functions getStringValueFromDefaults:DEFAULT_APP_LANGUAGE];
    if(_appLanguage == nil){
        //check phone language
        NSString *phoneLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
        if([phoneLanguage isEqualToString:@"ar"] || [phoneLanguage isEqualToString:@"ar-US"]){
             _appLanguage = @"ar";
            [Functions setStringValueToUserDefaults:_appLanguage withKey:DEFAULT_APP_LANGUAGE];
        }else{
            _appLanguage = @"en";
             [Functions setStringValueToUserDefaults:_appLanguage withKey:DEFAULT_APP_LANGUAGE];
        }
    }

    [ReachabilityManager defaultManager];
    [RTLocationManager defaultManager];
    
    [GMSPlacesClient provideAPIKey:GOOGLE_API_KEY];
    [GMSServices provideAPIKey:GOOGLE_API_KEY];
    
    _dataAdaptor =  [[CoreDataAdaptor alloc]init];
    
    _exchangeRate = [Functions getDoubleValueFromDefaults:EXCHANGE_RATE];
    if(_exchangeRate == 0){
        _exchangeRate = 1;
        _currency = kSAR;
    }else{
        _currency = [Functions getStringValueFromDefaults:CURRENCY];
    }

}

-(void)configRemoteNotifications:(UIApplication *)application{
    
    [FIRApp configure];
    [FIRMessaging messaging].delegate = self;

    // Register for remote notifications. This shows a permission dialog on first run, to
    // show the dialog at a more appropriate time move this registration accordingly.
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        // iOS 7.1 or earlier. Disable the deprecation warnings.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIRemoteNotificationType allNotificationTypes =
        (UIRemoteNotificationTypeSound |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeBadge);
        [application registerForRemoteNotificationTypes:allNotificationTypes];
#pragma clang diagnostic pop
    } else {
        // iOS 8 or later
        // [START register_for_notifications]
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
            UIUserNotificationType allNotificationTypes =
            (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
            UIUserNotificationSettings *settings =
            [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        } else {
            // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
            // For iOS 10 display notification (sent via APNS)
            
            [UNUserNotificationCenter currentNotificationCenter].delegate = self;
            UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
            [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
                
             /*   UNNotificationAction *action = [UNNotificationAction actionWithIdentifier:@"markCompleted" title:@"Approve" options:0];
                 UNNotificationAction *action1 = [UNNotificationAction actionWithIdentifier:@"rejected" title:@"Reject " options:UNNotificationActionOptionDestructive];
                UNNotificationCategory *category = [UNNotificationCategory categoryWithIdentifier:@"mycustomnotification" actions:@[action,action1] intentIdentifiers:@[] options:0];
                [UNUserNotificationCenter.currentNotificationCenter setNotificationCategories:[NSSet setWithObject:category]];
                [self scheduleNotification];
                */

            }];
#endif
        }
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];

    }
}

-(void)scheduleNotification{
    
    UNMutableNotificationContent *notificationContent = [[UNMutableNotificationContent alloc]init];
    notificationContent.title = AppName;
    notificationContent.subtitle = @"Local Notifications";
    notificationContent.body = @"In this tutorial, you learn how to schedule local notifications with the User Notifications framework.";
    notificationContent.categoryIdentifier = @"mycustomnotification";
    
    UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:10.0 repeats:false];
    
    UNNotificationRequest *notificationRequest = [UNNotificationRequest requestWithIdentifier:@"com.vs2.carspa" content:notificationContent trigger:trigger];
    [[UNUserNotificationCenter currentNotificationCenter]addNotificationRequest:notificationRequest withCompletionHandler:^(NSError * _Nullable error) {
        if(error){
            NSLog(@"Error : %@",[error localizedDescription]);
        }
    }];
}

/*private func scheduleLocalNotification() {
    // Create Notification Content
    let notificationContent = UNMutableNotificationContent()
    
    // Configure Notification Content
    notificationContent.title = "Cocoacasts"
    notificationContent.subtitle = "Local Notifications"
    notificationContent.body = "In this tutorial, you learn how to schedule local notifications with the User Notifications framework."
    
    // Set Category Identifier
    notificationContent.categoryIdentifier = Notification.Category.tutorial
    
    // Add Trigger
    let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 10.0, repeats: false)
    
    // Create Notification Request
    let notificationRequest = UNNotificationRequest(identifier: "cocoacasts_local_notification", content: notificationContent, trigger: notificationTrigger)
    
    // Add Request to User Notification Center
    UNUserNotificationCenter.current().add(notificationRequest) { (error) in
        if let error = error {
            print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
        }
    }
}
*/

#pragma mark:-----------------: Config SDKS
-(void)configFBSDK:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
}

-(void)configTwitterSDK{
       [[Twitter sharedInstance] startWithConsumerKey:TWITTER_CONSUMER_KEY consumerSecret:TWITTER_CONSUMER_SECERT_KEY];
}

-(void)configGmailSDK{
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    [GIDSignIn sharedInstance].delegate = self;
    
    
}
-(void)configFabricSDK{
    [Fabric with:@[[Crashlytics class]]];
}

#pragma mark:-----------------: Notification Delegates
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.

    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
      [self handleNotification:userInfo];
    if (userInfo[kGCMMessageIDKey]) {
       // NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
//    NSLog(@"%@", userInfo);
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_RECEIVED object:userInfo];
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
 
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    [self handleNotification:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        //NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
//    NSLog(@"%@", userInfo);
    
    completionHandler(UIBackgroundFetchResultNewData);
}

// Receive displayed notifications for iOS 10 devices.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *userInfo = notification.request.content.userInfo;
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
      //  NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }

//    NSLog(@"%@", userInfo);
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_RECEIVED object:userInfo];
    
    UIView *view = [CMNavBarNotificationView notifyWithText:AppName
                                                     detail:[[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"title"]
                                                      image:[UIImage imageNamed:@"notificationIcon"]
                                                andDuration:5.0];
    view.backgroundColor = [UIColor whiteColor];
    
    completionHandler(UNNotificationPresentationOptionSound);
    
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionNone);
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)())completionHandler {
    
    NSDictionary *userInfo = response.notification.request.content.userInfo;
//    if (userInfo[kGCMMessageIDKey]) {
////        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
//    }

 
    @try {
        [self handleNotification:userInfo];
    } @catch (NSException *exception) {
        NSLog(@"Exception : %@",[exception description]);
    } @finally {
        
    }

    
//    NSLog(@"%@", userInfo);
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_RECEIVED object:userInfo];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

    completionHandler();
}
#endif


// [START refresh_token]
- (void)messaging:(nonnull FIRMessaging *)messaging didRefreshRegistrationToken:(nonnull NSString *)fcmToken {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSLog(@"FCM registration token: %@", fcmToken);
     [Functions setStringValueToUserDefaults:fcmToken withKey:DEVICE_TOKEN];
}
// [END refresh_token]

// [START ios_10_data_message]
// Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
// To enable direct data messages, you can set [Messaging messaging].shouldEstablishDirectChannel to YES.
- (void)messaging:(FIRMessaging *)messaging didReceiveMessage:(FIRMessagingRemoteMessage *)remoteMessage {
//    NSLog(@"Received data message: %@", remoteMessage.appData);
}
// [END ios_10_data_message]

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Unable to register for remote notifications: %@", error);
}

// This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
// If swizzling is disabled then this function must be implemented so that the APNs device token can be paired to
// the FCM registration token.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
//    NSLog(@"APNs device token retrieved: %@", deviceToken);
    
    // With swizzling disabled you must set the APNs device token here.
    
}
/*
#pragma mark:-----------------: Google SingIn Delegates
- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *fullName = user.profile.name;
    NSString *givenName = user.profile.givenName;
    NSString *familyName = user.profile.familyName;
    NSString *email = user.profile.email;
    NSString *imgUrl = @"";
    if(user.profile.hasImage){
      NSURL *url =   [user.profile imageURLWithDimension:1];
        imgUrl = url.absoluteString;
    }
    // ...
    NSLog(@"%@,%@,%@,%@,%@,%@,%@",userId,idToken,fullName,givenName,familyName,email,imgUrl);
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
}
*/

#pragma mark:-----------------: Hangling Notifications
-(void)handleNotification:(NSDictionary *)dict{
    

        @try {
            NSInteger notificationType = [[dict valueForKey:kNotificationKey_Type]integerValue];
            switch (notificationType) {
                case NOTIFICATION_TYPE_MSG:{
                    //Message
                    [[SyncCDAdaptor sycnInstance]saveNotifications:dict];
                }
                    break;
                case NOTIFICATION_TYPE_HOTEL:{
                    //Hotel
                    [[SyncCDAdaptor sycnInstance]saveNotifications:dict];
                }
                    break;
                case NOTIFICATION_TYPE_OFFER:{
                    //Offer
                    [[SyncCDAdaptor sycnInstance]saveNotifications:dict];
                }
                    break;
                case NOTIFICATION_TYPE_CATEGORY:{
                    //Category
                    [[SyncCDAdaptor sycnInstance]saveNotifications:dict];
                }
                    break;
                default:{
        
                }
                    break;
            }
            
        } @catch (NSException *exception) {
            NSLog(@"Exception : %@",[exception description]);
        } @finally {
            
        }

    
    
}
@end
